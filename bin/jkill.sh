#!/bin/bash
# Program:
# Parallell computation script: this one is modified based on check script to examine the machines status
# Use the same program($PROGRAM) with different parameters
# History:
# 2006/1/29	doyen	First release
# $1 is the program name 
# function check() $1 is machine name,$2 is total number of cpus in that machine,  $3 is process name
# when machine is available return 0, not available return 1, error return 2
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin:~/myscripts:
export PATH
if  test -z $1 
then 
  echo "Usage: killp file"
  exit
fi

let num=`wc -l myhosts | cut -d ' ' -f1`
for ((i=1;i<=$num;i=i+1))
do
  opp=$i,${i}p
  com="cat myhosts | sed -n $opp | cut -d ' ' -f1"
  name=`eval $com`
  com="cat myhosts | sed -n $opp | cut -d ' ' -f2"
  cpu=`eval $com`
  com="let machcpu[$i]=$cpu"
  eval $com
  com="mach[$i]=$name"
  eval $com
done

numa=$((num+1)) #initialize num of process in hosts


PROGRAM=$1

check() #check the number of existing processes
{
 ssh $1 ls >/dev/null
 if [ $? -eq 0 ]
 then
    num=0
    ssh $1 killall $2
 fi
 }
   pid_numbers=`ps -C $1 -o pid,user --no-heading | grep $USER | sed -e 's/^\s*//g' | cut -d' ' -f1`
   for pids in $pid_numbers
   do
	kill -9 $pids
   done

 let iter=1; #iteratering within machines and find out status
 while true ; do
   pid_numbers=`ssh  ${mach[$iter]} ps -C $1 -o pid,user --no-heading | grep $USER | sed -e 's/^\s*//g' | cut -d' ' -f1`
   echo ${mach[$iter]} $pid_numbers
   for pids in $pid_numbers
   do
#	   echo ${mach[$iter]} kill -9 $pids
	   ssh ${mach[$iter]} kill -9 $pids
   done
#   $PROGRAM
    iter=$((iter+1))
    if [ $iter -eq $numa ]
      then exit 0
    fi
 done


