#!/bin/bash
#
# author ...:  Yi Dong
# author ...:  David Wallace Croft

mkdir log

mkdir results

export CLASSPATH=.:$PWD/simulator.jar

if [[ $# == 0 ]]
then
	java -cp simulator.jar jpvm.jpvmDaemon 
else
	java -cp simulator.jar jpvm.jpvmDaemon $*
fi
