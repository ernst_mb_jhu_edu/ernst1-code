#!/bin/bash
# filename .........:  setup
# purpose ..........:  Launches JPVM daemon on named hosts and port numbers
# version.date .....:  $Date: 2012-04-02 21:34:05 -0500 (Mon, 02 Apr 2012) $
# version.rev ......:  $Rev: 406 $
# version.author ...:  $Author: croft $
# author ...........:  unattributed
# author ...........:  David Wallace Croft

# Java simulation path

JAVASIMPATH="$HOME/jpvm"

# myhosts is a text file with a list of host names and port numbers

# Sets num to the number of host names in file myhosts

let num=`wc -l myhosts | cut -d ' ' -f1`

# Parses out the host name and port number from each line of myhosts.
# Creates arrays of host names and port numbers.

for ((i=1;i<=$num;i=i+1))
do
  opp=$i,${i}p
  
  com="cat myhosts | sed -n $opp | cut -d ' ' -f1"
  
  name=`eval $com`
  
  com="cat myhosts | sed -n $opp | cut -d ' ' -f2"
  
  cpu=`eval $com`
  
  com="let machcpu[$i]=$cpu"
  
  eval $com
  
  com="mach[$i]=$name"
  
  eval $com  
done

# Launches the JPVM daemon on each named host on the given port number.
 
numa=$((num+1)) 

let iter=1;

while true; do
  echo Connect to ${mach[$iter]} port:${machcpu[$iter]}
  
	ssh -f ${mach[$iter]}	"cd $JAVASIMPATH; bin/jpvmd ${machcpu[$iter]}"
	
  iter=$((iter+1))
  
  if [ $iter -eq $numa ]
    then exit 0
  fi
done
 
#ssh -f trinity	"cd $JAVASIMPATH; bin/jpvmd 10000"
#ssh -f fermi	"cd $JAVASIMPATH; bin/jpvmd 10001"
#ssh -f feynman	"cd $JAVASIMPATH; bin/jpvmd 10002"
#ssh -f thomson	"cd $JAVASIMPATH; bin/jpvmd 10003"
#ssh -f heisenberg	"cd $JAVASIMPATH; bin/jpvmd 10004"
#ssh -f hal	"cd $JAVASIMPATH; bin/jpvmd 10005"
#ssh -f pauli	"cd $JAVASIMPATH; bin/jpvmd 10006"
#ssh -f landau	"cd $JAVASIMPATH; bin/jpvmd 10007"
#ssh -f cell	"cd $JAVASIMPATH; bin/jpvmd 10008"
#ssh -f dirac	"cd $JAVASIMPATH; bin/jpvmd 10009"
