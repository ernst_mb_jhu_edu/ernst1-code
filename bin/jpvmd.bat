@echo off

REM author ...:  Yi Dong
REM author ...:  David Wallace Croft

mkdir log

mkdir results

set op=%1

set CLASSPATH=%cd%\simulator.jar;.

IF "%op%"=="" GOTO B

:A
java -cp simulator.jar jpvm.jpvmDaemon %op%
GOTO END

:B
java -cp simulator.jar jpvm.jpvmDaemon
GOTO END


:END
