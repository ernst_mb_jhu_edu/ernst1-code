<?xml version="1.0" encoding='ISO-8859-1' ?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" indent="yes"
              doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"/>

  <xsl:template match="COURS">
    <html>
      <head><title><xsl:value-of select="*/@titre"/></title></head>
      <body  bgcolor="#ffffff" text="#000000">
        <xsl:apply-templates/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="CHAPITRE">
    <hr/>
	<xsl:if test="@label">
		<a name="{@label}"/>
	</xsl:if>
    <h1><xsl:value-of select="@titre"/></h1>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="SOUSCHAPITRE">
    <br/>
	<xsl:if test="@label">
		<a name="{@label}"/>
	</xsl:if>
    <h2><xsl:value-of select="@titre"/></h2>
    <xsl:apply-templates/>
    <br/>
  </xsl:template>

  <xsl:template match="SECTION">
    <br/>
	<xsl:if test="@label">
		<a name="{@label}"/>
	</xsl:if>
    <h3><xsl:value-of select="@titre"/></h3>
    <xsl:apply-templates/>
    <br/>
  </xsl:template>

  <xsl:template match="PAGE">
    <br/>
	<xsl:if test="@label">
		<a name="{@label}"/>
	</xsl:if>
    <h4><xsl:value-of select="@titre"/></h4>
    <xsl:apply-templates/>
    <br/>
  </xsl:template>

  <xsl:template match="PARAGRAPHE">
	<xsl:if test="@label">
		<a name="{@label}"/>
	</xsl:if>
	<xsl:if test="@titre">
		<b><xsl:value-of select="@titre"/></b><br/>
	</xsl:if>
    &#xA0;&#xA0;&#xA0;&#xA0;<xsl:apply-templates/>
    <br/>
  </xsl:template>

  <xsl:template match="TP">
	<xsl:if test="@label">
		<a name="{@label}"/>
	</xsl:if>
    <div align="center"><h1>TP: <xsl:value-of select="@titre"/></h1></div>

<!--Tableau en en-tete-->

    <table border="1" cellpadding="2" align="center">
    <tr>
    <td><b>Champs</b></td>
    <td><b><xsl:value-of select="CHAMP"/></b></td>
    </tr>
    <tr>
    <td><b>Niveau scolaire</b></td>
    <td><b><xsl:value-of select="NIVEAU/@niveau"/></b></td>
    </tr>
    <tr>
    <td><b>Temps n�cessaire</b></td>
    <td><b><xsl:value-of select="TEMPS"/></b></td>
    </tr>
    <tr>
    <td colspan="2">
    <ul>
    <xsl:if test="OBJECTIFS">
    <li><b><a href="#objectifs">Objectifs</a></b></li>
    </xsl:if>
    <xsl:if test="PREREQUIS">
    <li><b><a href="#prerequis">Pr�requis</a></b></li>
    </xsl:if>
    <xsl:if test="DOCUMENTS">
    <li><b><a href="#documents">Documents et/ou mat�riel</a></b></li>
    </xsl:if>
    <xsl:if test="DEROULEMENT">
    <li><b><a href="#deroulement">D�roulement</a></b></li>
    </xsl:if>
    <xsl:if test="CONCLUSION">
    <li><b><a href="#conclusion">Conclusion</a></b></li>
    </xsl:if>
    <xsl:if test="BIBLIOGRAPHIE">
    <li><b><a href="#bibliographie">Bibliographie</a></b></li>
    </xsl:if>
    </ul>
    </td>
    </tr>
    </table>

<!--Fin tableau en en-tete-->

    <xsl:apply-templates/>
    <br/>
  </xsl:template>

  <xsl:template match="AUTEUR">
    <br/><i>Auteur:</i> <xsl:apply-templates/><br/>
  </xsl:template>

  <xsl:template match="DATE">
    <br/>Date de mise � jour: <i><xsl:apply-templates/></i><br/>
  </xsl:template>

  <xsl:template match="DATECRE">
    <br/>Date de cr�ation: <i><xsl:apply-templates/></i><br/>
  </xsl:template>

  <xsl:template match="DATEMAJ">
    <br/>Date de mise � jour: <i><xsl:apply-templates/></i><br/>
  </xsl:template>

<!--  <xsl:template match="LIEN">
    <xsl:if test="@href">
      <a href="#{normalize-space(@href)}"><xsl:apply-templates/></a>
    </xsl:if>
    <xsl:if test="@name">
      <a name="X{normalize-space(@name)}"><xsl:apply-templates/></a>
    </xsl:if>
  </xsl:template>

  <xsl:template match="LIENHTML">
    <a href="{normalize-space(@url)}"><xsl:value-of select="normalize-space(.)"/></a>
  </xsl:template>
-->

  <xsl:template match="LIEN">
    <xsl:choose>
      <xsl:when test="normalize-space(@type)='http'">
	<a href="{normalize-space(@label)}"><xsl:apply-templates/></a>
      </xsl:when>
      <xsl:otherwise>
        <a href="#{normalize-space(@label)}"><xsl:apply-templates/></a>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="A">
    <xsl:copy-of select="."/>
  </xsl:template>

  <xsl:template match="IMG">
    <xsl:copy-of select="."/>
  </xsl:template>

  <xsl:template match="EQUATION">
	<xsl:if test="@label"><a name="{@label}"/></xsl:if>
	<img style="vertical-align: middle" src="{@image}" border="0" alt="{@texte}"/>
  </xsl:template>

  <xsl:template match="FIGURE">
    <br/>
    <xsl:if test="@label">
        <a name="{normalize-space(@label)}"/>
    </xsl:if>
    <div align="center">
    <xsl:apply-templates/>
    </div>
    <br/>
  </xsl:template>

  <xsl:template match="FICHIER">
    <xsl:if test="@type='png' or @type='jpeg' or @type='gif'">
      <xsl:choose>
      <xsl:when test="LIENFICHIER">
        <a href="{normalize-space(LIENFICHIER/@url)}">
          <img src="{normalize-space(@nom)}" alt="{@nom}" name="{normalize-space(@label)}"/>
        </a>
      </xsl:when>
      <xsl:otherwise>
        <img src="{normalize-space(@nom)}" alt="{@nom}" name="{normalize-space(@label)}"/>
      </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template match="TABLEAU">
    <br/>
    <a name="{normalize-space(@label)}"/>
    <div align="center">
    <b><xsl:value-of select="@titre"/></b>
    </div>
    <br/><xsl:apply-templates select="TABLE"/><br/>
  </xsl:template>
  
  <xsl:template match="TABLE">
    <table border="1" cellspacing="0" cellpadding="2">
      <xsl:apply-templates select="TR"/>
    </table>
  </xsl:template>
  
  <xsl:template match="TR">
    <tr><xsl:apply-templates/></tr>
  </xsl:template>

  <xsl:template match="TD">
    <td><xsl:apply-templates/></td>
  </xsl:template>

  <xsl:template match="TH">
    <th><xsl:apply-templates/></th>
  </xsl:template>

  <xsl:template match="SUP">
    <sup><xsl:apply-templates/></sup>
  </xsl:template>

  <xsl:template match="SUB">
    <sub><xsl:apply-templates/></sub>
  </xsl:template>

  <xsl:template match="LEGENDE">
    <br/><div align="center"><b><xsl:apply-templates/></b></div>
  </xsl:template>

  <xsl:template match="MATH">
    <i><xsl:apply-templates/></i>
  </xsl:template>

  <xsl:template match="BR">
    <br/>
  </xsl:template>

  <xsl:template match="LABEL">
    <a name="normalize-space(.)"/>
  </xsl:template>

  <xsl:template match="LISTEPOINTS">
    <ul><xsl:apply-templates/></ul>
  </xsl:template>

  <xsl:template match="LISTENUM">
    <ol type="1"><xsl:apply-templates/></ol>
  </xsl:template>

  <xsl:template match="LISTEDEF">
    <dl><xsl:apply-templates/></dl>
  </xsl:template>

  <xsl:template match="ITEM">
    <li><xsl:apply-templates/></li>
  </xsl:template>

  <xsl:template match="ITEMDEF">
    <dt><xsl:value-of select="@nom"/></dt><dd><xsl:apply-templates/></dd>
  </xsl:template>

  <xsl:template match="TT">
    <tt><xsl:apply-templates/></tt>
  </xsl:template>

  <xsl:template match="I">
    <i><xsl:apply-templates/></i>
  </xsl:template>

  <xsl:template match="B">
    <b><xsl:apply-templates/></b>
  </xsl:template>

  <xsl:template match="EM">
    <em><xsl:apply-templates/></em>
  </xsl:template>

  <xsl:template match="NBSP">&#xA0;</xsl:template>
  
  <xsl:template match="CENTER">
    <div align="center"><xsl:apply-templates/></div>
  </xsl:template>

  <xsl:template match="BIBLIOGRAPHIE">
    <hr/>
    <a name="bibliographie"></a><h2>Bibliographie:</h2>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="CREDIT">
    <br/><i>Cr�dit: </i><xsl:apply-templates/>
  </xsl:template>

<!--Balises pour les differentes parties des TPs-->

  <xsl:template match="CHAMP">
  </xsl:template>

  <xsl:template match="NIVEAU">
  </xsl:template>

  <xsl:template match="TEMPS">
  </xsl:template>

  <xsl:template match="INTRODUCTION">
    <br/><a name="intro"></a><h2>Introduction:</h2><xsl:apply-templates/><br/>
  </xsl:template>

  <xsl:template match="OBJECTIFS">
    <br/><a name="objectifs"></a><h2>Objectifs:</h2><xsl:apply-templates/><br/>
  </xsl:template>

  <xsl:template match="PREREQUIS">
    <br/><a name="prerequis"></a><h2>Pr�requis:</h2><xsl:apply-templates/><br/>
  </xsl:template>

  <xsl:template match="DOCUMENTS">
    <br/><a name="documents"></a><h2>Documents et/ou mat�riel:</h2><xsl:apply-templates/><br/>
  </xsl:template>

  <xsl:template match="DEROULEMENT">
    <br/><a name="deroulement"></a><h2>D�roulement d�taill�:</h2><xsl:apply-templates/><br/>
  </xsl:template>

  <xsl:template match="CONCLUSION">
    <br/><a name="conclusion"></a><h2>Conclusion:</h2><xsl:apply-templates/><br/>
  </xsl:template>

  <xsl:template match="CONSEILS">
    <br/><a name="conseil"></a><h2>Conseils:</h2><xsl:apply-templates/><br/>
  </xsl:template>

  <xsl:template match="REMARQUE">
    <br/><b><u>Remarque:</u></b><xsl:apply-templates/><br/>
  </xsl:template>

<!--Fin balises pour les differentes parties des TPs-->

<!-- balises pour les exercices -->
  <xsl:template match="EXERCICE">
    <a name="{@label}"/>
    <b><xsl:value-of select="@titre"/></b><br/>
    <blockquote>
    <xsl:apply-templates/>
    </blockquote>
  </xsl:template>

  <xsl:template match="QUESTION">
    <!-- num�ro  -->
    <br/><i>Question:</i><br/>
    <xsl:apply-templates/>
    <br/>
  </xsl:template>

  <xsl:template match="AIDE">
    <br/>
    <i>Indice:</i> <xsl:apply-templates/>
    <br/>
  </xsl:template>

  <xsl:template match="SOLUTION">
    <i>Solution:</i> <xsl:apply-templates/>
    <br/>
  </xsl:template>

  <xsl:template match="QUESTIONQCM">
    <form action="cgi-bin/correction" method="POST">
    <br/>
    <i><xsl:value-of select="child::text()"/></i><br/>
    <blockquote>
    <xsl:apply-templates select="REPONSE"/>
    </blockquote>
    </form>
  </xsl:template>

  <xsl:template match="REPONSE">
    <!--<li><input type="radio" name="{normalize-space(../NUMERO)}" value="{NUMERO}"/><xsl:value-of select="TEXTE"/></li>-->
    <input type="radio" name="{position()}" value="position()"/><xsl:value-of select="TEXTE"/>
    <br/>
  </xsl:template>
<!-- fin balises exercices -->

</xsl:stylesheet>
