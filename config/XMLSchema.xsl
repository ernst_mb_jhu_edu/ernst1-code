<?xml version="1.0" encoding='ISO-8859-1' ?>

<xsl:stylesheet version="1.0"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
  <xsl:namespace-alias stylesheet-prefix="xs" result-prefix="#default"/>
  
  <xsl:output method="html" indent="yes"
              doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"/>

  <xsl:template match="xs:schema">
    <html>
      <head><title>Sch�ma XML</title></head>
      <body  bgcolor="#ffffff" text="#000000">
        <div align="center">
        <h1>Sch�ma XML</h1>
        </div>
        <br/>
        <font size="+1"><b>
        <a href="#index">Aller � l'index</a><br/>
        </b></font>
        <hr/>
        <xsl:apply-templates/>
        <br/>
        <a name="index"/>
        <h3>Index</h3>
        <xsl:for-each select="*[@name]">
          <xsl:sort select="@name"/>
          <a href="#{@name}"><xsl:value-of select="@name"/></a><br/>
        </xsl:for-each>
      </body>
    </html>
  </xsl:template>
  
  <xsl:template name="print-ref">
    <xsl:choose>
    <xsl:when test="contains(@ref,':')">
      <a href="#{substring-after(@ref,':')}"><xsl:value-of select="@ref"/></a>
    </xsl:when>
    <xsl:otherwise>
      <a href="#{@ref}"><xsl:value-of select="@ref"/></a>
    </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="xs:element">
    <xsl:choose>
    <xsl:when test="@ref">
      * <xsl:call-template name="print-ref"/><br/>
    </xsl:when>
    <xsl:otherwise>
      <a name="{@name}"/>
      <h3>El�ment <font color="#550000"><xsl:value-of select="@name"/></font></h3>
      <xsl:if test="@type">
        type: <xsl:value-of select="@type"/><br/>
      </xsl:if>
      <xsl:apply-templates/>
      <br/>
      <xsl:for-each select="//xs:element[.//xs:element[@ref=current()/@name or substring-after(@ref,':')=current()/@name]] | //xs:group[.//xs:element[@ref=current()/@name or substring-after(@ref,':')=current()/@name]]">
        <xsl:if test="position() = 1">
          Parents:
        </xsl:if>
        <xsl:if test="position() != 1">, </xsl:if>
        <a href="#{@name}"><xsl:value-of select="@name"/></a> 
      </xsl:for-each>
      <br/>
      <hr/>
    </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="xs:group">
    <xsl:choose>
    <xsl:when test="@ref">
      * <xsl:call-template name="print-ref"/>
      <br/>
    </xsl:when>
    <xsl:otherwise>
      <a name="{@name}"/>
      <h3>Groupe <font color="#550000"><xsl:value-of select="@name"/></font></h3>
      <xsl:apply-templates/>
      <br/>
      <xsl:for-each select="//xs:element[.//xs:group[@ref=current()/@name or substring-after(@ref,':')=current()/@name]] | //xs:group[.//xs:group[@ref=current()/@name or substring-after(@ref,':')=current()/@name]]">
        <xsl:if test="position() = 1">
          Parents:
        </xsl:if>
        <xsl:if test="position() != 1">, </xsl:if>
        <a href="#{@name}"><xsl:value-of select="@name"/></a> 
      </xsl:for-each>
      <br/>
      <hr/>
    </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="xs:documentation">
    <font color="#005500"><pre><xsl:value-of select="."/></pre></font>
    <br/>
  </xsl:template>
  
  <xsl:template match="xs:choice">
    Choix parmi:
    <blockquote>
      <xsl:apply-templates/>
    </blockquote>
  </xsl:template>
  
  <xsl:template match="xs:sequence">
    S�quence parmi:
    <blockquote>
      <xsl:apply-templates/>
    </blockquote>
  </xsl:template>
  
  <xsl:template match="xs:complexType">
    <!--Type complexe-->
    <xsl:if test="@name">
      <a name="{@name}"/>
      <h3>Type complexe <xsl:value-of select="@name"/></h3>
    </xsl:if>
    <xsl:if test="@ref">
      <a href="{@ref}"><xsl:value-of select="@ref"/></a><br/>
    </xsl:if>
    <xsl:if test="@mixed='true'">
      Peut contenir du texte<br/>
    </xsl:if>
    <blockquote>
    <xsl:apply-templates/>
    </blockquote>
    <xsl:if test="@name">
      <hr/>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="xs:attribute">
    Attribut <font color="#000055"><b><xsl:value-of select="@name"/></b></font>:
    <blockquote>
    <xsl:choose>
    <xsl:when test="@use='required'">
      obligatoire<br/>
    </xsl:when>
    <xsl:otherwise>
      facultatif<br/>
    </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="@type">
      type: <tt><xsl:value-of select="@type"/></tt><br/>
    </xsl:if>
    <xsl:apply-templates/>
    </blockquote>
  </xsl:template>
  
  <xsl:template match="xs:simpleType">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="xs:restriction">
    Restriction des valeurs:
    <ul>
      <xsl:apply-templates/>
    </ul>
  </xsl:template>
  
  <xsl:template match="xs:enumeration">
    <li><tt><xsl:value-of select="@value"/></tt></li>
  </xsl:template>
  
  <xsl:template match="xs:attributeGroup">
    <xsl:choose>
    <xsl:when test="@ref">
      * <xsl:call-template name="print-ref"/>
      <br/>
    </xsl:when>
    <xsl:otherwise>
      <a name="{@name}"/>
      <h3>Groupe d'attributs <font color="#550000"><xsl:value-of select="@name"/></font></h3>
      <xsl:apply-templates/>
      <br/>
      <xsl:for-each select="//xs:element[.//xs:attributeGroup[@ref=current()/@name or substring-after(@ref,':')=current()/@name]] | //xs:attributeGroup[.//xs:attributeGroup[@ref=current()/@name or substring-after(@ref,':')=current()/@name]] | //xs:complexType[.//xs:attributeGroup[@ref=current()/@name or substring-after(@ref,':')=current()/@name] and @name]">
        <xsl:if test="position() = 1">
          Parents:
        </xsl:if>
        <xsl:if test="position() != 1">, </xsl:if>
        <a href="#{@name}"><xsl:value-of select="@name"/></a> 
      </xsl:for-each>
      <br/>
      <hr/>
    </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
