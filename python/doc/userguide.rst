************
User Guide
************

=============
Introduction
=============

The Event-Related Neuronal Simulation Tool (ERNST) is an event-based simulator
of biological neural networks implemented in Java.  It was developed by the 
`Computational Neuroscience Lab`_ at Johns Hopkins University.  ERNST is run
using a Python interface.  This document is the user guide to that package.

.. _Computational Neuroscience Lab: http://cnslab.mb.jhu.edu

==============
Installation
==============

ERNST Python requires NumPy, SciPy, and matplotlib.  You must first install
these packages if you do not already have them.

Download the latest version of ERNST from
https://sourceforge.net/projects/ernst/files/ and unzip into your desired
installation directory.  ERNST consists of a number of folders and files in
support of the Java simulator core, as well as a folder named ``python``,
which contains scripts for the Python interface.  

One configuration step is required before you can install ERNST.  Edit 
``python/ernst/conf.py`` such that the variable ``SIMULATOR_DIRECTORY``
is the absolute path of the directory on your file system where the Java 
simulator files reside.  This directory is usually the one that you downloaded
named ``ernst-1.4``.

Next, to install the ERNST Python package, ``cd`` into the ``python`` directory
and execute::

  sudo python setup.py install

If all goes well, you will now be able to call ERNST Python functions from
anywhere on your system.

==================
The ERNST Workflow
==================

In PyNN or another procedural simulator, you might carry out a simulation like
this:

  #) Create a network of neurons.
  #) Simulate the network for 1 second.
  #) Look over the recorded data.
  #) Add some more neurons.
  #) Simulate the network for 1 second
  #) Inject an input current into the network.
  #) Simulate the network for 1 second.
  #) Look over the recorded data.

Not so in ERNST.  You must *describe* a simulation entirely before you can run
it, and you must run it entirely before you can look over the recorded data.
The description of a simulation includes both the structure of the network
to be simulated and the operations to be performed on the network in the course
of the simulation; all of this must be specified in advance.

The ERNST workflow can therefore be divided into three completely distinct phases:

  1. Creating the simulation
  2. Running the simulation
  3. Analyzing recorded data

The advantage of this modularity is that the phases do not have to be executed
at the same time. For example, you could create and run the simulation in a 
Python script, then launch an interactive session for viewing and plotting the
data.  Or you could create the simulation interactively, pickle it, and run
it later in a script.

================
Importing ERNST
================

Before you can use any ERNST functions or classes, you must first import
the library into your Python script or session::

  from ernst import *

=====================
Creating a Simulation
=====================

An ERNST *Simulation* consists of two pieces: a *Network* and an *Experiment*.
The *Network* defines the neurons and the connections between them.  The
*Experiment* defines the timeline of inputs to be injected into the *Network*. 

---------------------
Defining the Network
---------------------

A :class:`create.Network` encapsulates a set of neurons and the synaptic
connections between them.

+++++++++++++++++++++
Creating Populations
+++++++++++++++++++++

In ERNST, the most fundamental unit of network structure is the *Population*.
A :class:`create.Population` object encapsulates a group of neurons of the same
type with the same parameters.

::

  pop1 = Population("Input", (10, 10), MiNi)

This creates a population of 100 neurons laid out in a 10 x 10 grid.  The label
of the population is ``"Output"``; this label will come into play later, when we
access recorded data.  The third argument to the *Population* constructor is a
"cell class" that determines the model of the neuron.  These neurons belong to
the *MiNi*, or Mihalas-Niebur, cell type, described in `this paper`_.  

.. _this paper: http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2954058/

Since no parameters were specified, the neurons in this population assume the
default *MiNi* parameters.  To override one or more of those parameters, pass
a ``cellparams`` dictionary to the constructor::

  pop2 = Population("Output", (10, 10), MiNi, cellparams={'ThresholdAdaptation':10,
                                                          'SpikeCurrentsAddition':[1e-15, 0]})

This sets the threshold adaptation to ``10`` and the additive resets of two
spike-induced currents to 1e-15 and 0, respectively.

To see the possible parameters and their default values, check the
``default_parameters`` variable of the cell class::

  print MiNi.default_parameters

For a reference of all cell classes and their parameters, see
:ref:`cellclasses`.

+++++++++++++++++++++++
Connecting Populations
+++++++++++++++++++++++

To connect two *Populations*, we use a :class:`create.Projection` object.  The
*Projection* constructor takes both the pre- and post-synaptic populations, as
well as a Connector that defines the connection scheme between them::

  connect_matrix = numpy.array([[.25, .5, .25],
                                [.5,   1, .5 ],
                                [.25, .5, .25]])

  connector = PatternConnector(connect_matrix, weight=1e-9, delay=1e-4, periodic=True)

  projection = Projection(pop1, pop2, connector)

Here, we create a projection from ``pop1`` to ``pop2`` according to the
connection scheme defined by ``connector``, an instance of the 
:class:`create.PatternConnector` class.  *PatternConnector* objects connect two
populations by tiling a connection matrix (a NumPy array) over every neuron
in one population.  ``connector`` will connect each neuron in the pre-synaptic
population to exactly nine neurons in the post-synaptic population.  

The weight of each synapse in the projection will be determined by multiplying
the corresponding element in the connection matrix by the weight of the
projection, ``1e-9.``  The weight of a synapse is the current, in amps, that
is absorbed by the post-synaptic neuron after the pre-synaptic neuron fires.

Because *periodic* is set to ``True``, neurons on the edges of the pre-synaptic
population will wrap around and connect to neurons on the opposite edge of the
post-synaptic population.

The *delay* of all synapses --- the time interval in between when the 
pre-synaptic neuron sends a spike and when the post-synaptic neuron receives it
--- is set to ``1e-4`` seconds.

The other major type of Connector in ERNST is the :class:`PointsConnector`,
in which each connection is specified explicitly with an adjacency matrix.
Probabilistic variations of both of these connector types also exist, as do
:class:`create.OneToOneConnector` and :class:`create.AllToAllConnector` for
convenience.  For list of all types of Connectors, see :ref:`connectors`.

+++++++++++++++++++
Recording Neurons
+++++++++++++++++++

Running a simulation only returns output in the form of recorder data, so you're
going to want to record at least *some* neurons.  Two types of activity can be
recorded:

  1) spike times; and
  2) intracellular voltages and currents.

To record spike times, call the :func:`create.Population.record` method of a
*Population*::

  pop2.record( (0, 0) )

That will record the neuron at position ``(0, 0)`` in ``pop1``.  To record
every neuron in a population, call `record()` without arguments::

  pop2.record()

To record intracellular voltages and currents (usually for debugging purposes),
use :func:`create.Population.record_intracellular`::

  pop1.record_intracellular( (0, 0) )

.. warning::

   Each additional neuron that is recorded slows down the simulation 
   considerably, so only record as many neurons as you need.

+++++++++++++++++++++++
Building the Network
+++++++++++++++++++++++

Finally, we must wrap the populations and projections into a
:class:`create.Network` object::

  network = Network()
  network.add(pop1)
  network.add(pop2)
  network.add(projection)

Alternatively, we could have passed the populations and projections directly
to the *Network* constructor as lists::

  network = Network(populations=[pop1, pop1], projections=[projection])

------------------------
Defining the Experiment
------------------------

The second top-level component of a *Simulation* (along with the *Network*)
is the *Experiment*.  Whereas the *Network* defines the physical structure
of the *Simulation*, the *Experiment* defines its temporal structure.

An *Experiment* consists of a number of *Subexperiments*, each of which
consists of a number of *Inputs* to be presented to a network::

  experiment = Experiment()

The constructor for a :class:`create.Subexperiment` looks like this::

  subexperiment = Subexperiment("Training", 10, 3)

This creates a *Subexperiment* labeled ``"Training"`` that last for 10
seconds and is repeated 3 times.

Next, we'll add an *Input* to the *Subexperiment*.  A :class:`create.Input`
represents a current that is to be injected into the network as an
inhomogeneous Poisson process.  In the context of the vision system, you could
also think of it as a visual scene to be presented to the network.

We specify the particular neurons in the target population that are to be
affected by an *Input* with a *Stimulus* object::

  background_input = Input(pop1, 2, 5, BackgroundStimulus(), weight=1e-9, frequency=10)

This creates an *Input* that injects ``1e-9`` amps of current into ``pop1``
every ``1/10`` seconds (on average) between ``time = 2`` and ``time = 5``.  A
:class:`create.BackgroundStimulus` is a *Stimulus* that targets every neuron
in a population, so all 100 neurons in ``pop1`` will receive this input.

Technically, *weight* and *frequency* are both optional parameters, but your
*Input* won't do anything unless they are both set to non-zero values.
  
Another *Stimulus* class is :class:`create.FilledSquareStimulus`.  It
stimulates a region of neurons in the shape of a solid rectangle::

  square_input = Input(pop1, 5, 8, FilledSquareStimulus(3, 3, 7, 7), weight=2e-9, frequency=20)

``square_input`` will inject ``2e-9`` amps of current into several neurons in
``pop1`` between ``time = 5`` and ``time = 8`` with an average frequency of
``20`` injections per second.  The corner coordinates of the rectangular
region of neurons that will be targeted are ``(3, 3)`` and ``(7, 7)``.

Finally, we add the *Inputs* to our *Subexperiment* and the *Subexperiment* 
to our :class:`create.Experiment`::

  subexperiment.add(background_input)
  subexperiment.add(square_input)
  experiment.add(subexperiment)

One optional argument to the :class:`create.Input` constructor is the
exponential *decay* of the average frequency of the inhomogeneous Poisson
process.  This can be useful for modeling visual stimuli.  It is by default
set to zero, meaning that the frequency of the input does not decay.

------------------------
Building the Simulation
------------------------

The final step in defining a simulation is to wrap the :class:`create.Network`
and the :class:`create.Experiment` into a :class:`create.Simulation` object::

  simulation = Simulation(network, experiment)

This *Simulation* can be immediately run within the same script or session, or
pickled and run later.

Here is the complete code for building this example simulation (written in a
slightly different order than was presented above)::

  from ernst import *

  # Define the Network

  network = Network()

  pop1 = Population("Input", (10, 10), MiNi)
  pop1.record_intracellular((0,0))
  network.add(pop1)

  pop2 = Population("Output", (10, 10), MiNi, cellparams={'ThresholdAdaptation':10,
                                                          'SpikeCurrentsAddition':[1e-15, 0]})
  pop2.record()
  network.add(pop2)

  connect_matrix = numpy.array([[.25, .5, .25],
                                [.5,   1, .5 ],
                                [.25, .5, .25]])
  connector = PatternConnector(connect_matrix, weight=1e-9, delay=1e-4, periodic=True)
  projection = Projection(pop1, pop2, connector)
  network.add(projection)

  # Define the Experiment

  experiment = Experiment()
  subexperiment = Subexperiment("Training", 10, 3)
  background_input = Input(pop1, 2, 5, BackgroundStimulus(), weight=1e-9, frequency=10)
  square_input = Input(pop1, 5, 8, FilledSquareStimulus(3, 3, 7, 7), weight=2e-9, frequency=20)
  subexperiment.add(background_input)
  subexperiment.add(square_input)
  experiment.add(subexperiment)

  # Build the Simulation

  simulation = Simulation(network, experiment)

======================
Running a Simulation
======================

In every script or session, before you can run the simulator, you must first 
call :func:`run.setup`::

  setup()

To run the simulator on a :class:`create.Simulation` object, use the function
:func:`run.run`::

  results = run(simulation)

The :func:`run.run` function returns a NumPy array containing recorder data
from the simulation. The format and contents of this array will be described
below.

At the end of every script or session, you must shut down the simulator
by calling :func:`run.end`::

  end()

.. warning::

  Make a habit of calling ``end()`` at the end of each script or session.
  Failure to do so will lead to Java errors the next time you try to call
  ``setup()``.

---------------
Multiple Hosts
---------------

Read this section only if you want to run the simulator in parallel on multiple
machines.

In ERNST, host machines communicate with each other through a Java library
called Java Parallel Virtual Machine (JPVM).  A JPVM daemon must be launched
on any machine before ERNST can be run there.  What :func:`run.setup` really
does is launch a JPVM daemon, and what :func:`run.end` really does is terminate
the daemon.  

To run a simulation on multiple machines, follow these steps:

  1) Launch JPVM on each machine (including the master machine) with
     :func:`run.setup`.  On each machine, pass ``setup()`` a unique "port
     number" to identify the machine.  For example::

         setup(10001)

  2) Use :func:`jpvm.addHost` on the master host to add the other machines
     to its JPVM session::

         addHost('hostnamegoeshere', 10001)

  3) When you run the simulation, pass to :func:`run.run` a ``num_hosts``
     argument equal to the number of host machines you want to run the
     simulation on::

        run(simulation, num_hosts=2) 

  4) When finished, close JPVM daemons on all machines with :func:`run.end`::

        end()

**Note**: This functionality has not yet been tested.

=======================
Analyzing Recorded Data
=======================

------------------------
The Results Record Array
------------------------

The function :func:`run.run` returns a NumPy recarray_ that contains recorded
data from the simulation.  Each row in the results array corresponds to a
"recording" --- recorded data from a particular neuron in a particular trial.
As such, a recording is indexed by five variables: x-coordinate of the neuron
(``x``), y-coordinate of the neuron (``y``), label of the population of the neuron
(``population``), label of the subexperiment (``subexperiment``), and number of the trial 
(``trial``).  Note that this is why populations and subexperiments must be
given a string label.  The results array is sorted by subexperiment
(alphabetically), trial, population (alphabetically), x, and y, in that order.  

.. _recarray: http://www.scipy.org/RecordArrays

For example::

  results[0]

will return a row of recorded data from the first subexperiment, the first trial, the 
first population, the first x-coordinate, and the first y-coordinate.

::

  results[i]['population']

will print the population of the ``i``-th recording, and

::

  results['trial']

will return an array of all the ``trial`` variables in the results array.

Each row contains a field named ``"spike_train"`` --- a list of spike times::

  results[i]['spike_train']

as well as three fields that carry intracellular recorder data::

  results[i]['intracellular_time']
  results[i]['intracellular_voltage']
  results[i]['intracellular_current']

``"intracellular_time"`` and ``"intracellular_voltage"`` are one-dimensional
arrays of time and corresponding recorded voltage, respectively.
``intracellular_current`` is a two-dimensional array; rows represent time and
columns represent channel.

Note that spike recorder data and intracellular recorder data for a given
neuron-trial are both stored in the same row: if the neuron was not
being intracellularly recorded during the trial, the row's intracellular
data fields are empty arrays; if the neuron was not being spike recorded
during the trial, the row's ``spike_train`` field is an empty array.  If the
neuron was not being recorded at all during the trial, then it will not have a
field in the results array.

If you prefer working in MATLAB, you can export the NumPy record array to a 
MATLAB structure array with :func:`results.savemat`::

  savemat(results, 'datafile.mat')

You can either analyze the recorder data yourself using the results recarray
described above, or you can use functionality provided by ERNST for making
common types of plots.

The simplest spike-time-based plot is the :class:`results.SingleUnitPlot` ---
a raster plot of the spikes from a single neuron in a single subexperiment
across multiple trials::

    plot = SingleUnitPlot(results, "Training", "Output", (5, 5))
    plot.show()

To save the plot to a file instead of showing it on the screen, use
:func:`results.SingleUnitPlot.save`.

Another useful built-in plot is the :class:`results.FieldPlot`, which plots the
spike times for an entire population of neurons from a single subexperiment
averaged over multiple trials across multiple time bins::

    plot = FieldPlot(results, "Training", "Output", begin=0, end=10, step=.2)
    plot.show()

If you want to plot data from intracellular recordings, use
:class:`results.IntracellularPlot`, which plots the voltage and currents
from a single subexperiment, from a single trial, from a single neuron::

    plot = IntracellularPlot(results,    # results array
                             "Training", # subexperiment 
                             0,          # trial
                             "Input",    # population
                             (5, 5))     # coordinate
    plot.show()

:class:`results.IntracellularPlot` plots *all* currents --- synaptic and,
in the case of MiNi neurons, spike-induced.

For more examples, see the ``demos`` folder of the ERNST distribution.

