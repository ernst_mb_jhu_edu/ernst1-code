\select@language {english}
\contentsline {chapter}{\numberline {1}User Guide}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Installation}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}The ERNST Workflow}{1}{section.1.3}
\contentsline {section}{\numberline {1.4}Importing ERNST}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Creating a Simulation}{2}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Defining the Network}{2}{subsection.1.5.1}
\contentsline {subsubsection}{Creating Populations}{2}{subsubsection*.3}
\contentsline {subsubsection}{Connecting Populations}{3}{subsubsection*.4}
\contentsline {subsubsection}{Recording Neurons}{3}{subsubsection*.5}
\contentsline {subsubsection}{Building the Network}{4}{subsubsection*.6}
\contentsline {subsection}{\numberline {1.5.2}Defining the Experiment}{4}{subsection.1.5.2}
\contentsline {subsection}{\numberline {1.5.3}Building the Simulation}{5}{subsection.1.5.3}
\contentsline {section}{\numberline {1.6}Running a Simulation}{6}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}Multiple Hosts}{6}{subsection.1.6.1}
\contentsline {section}{\numberline {1.7}Analyzing Recorded Data}{7}{section.1.7}
\contentsline {subsection}{\numberline {1.7.1}The Results Record Array}{7}{subsection.1.7.1}
\contentsline {chapter}{\numberline {2}ERNST Python Reference}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}create}{9}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Cell classes}{11}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Connectors}{13}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Stimuli}{15}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}run}{16}{section.2.2}
\contentsline {section}{\numberline {2.3}results}{16}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}The Results NumPy Array}{17}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Results Classes and Functions}{17}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}jpvm}{19}{section.2.4}
\contentsline {chapter}{Index}{21}{section*.58}
