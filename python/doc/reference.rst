***********************
ERNST Python Reference
***********************

This page is a complete reference of the modules, classes, and functions in the
ERNST Python package.

=====================
create
=====================

This module contains classes that create the specifications for a simulation
run.

.. autoclass:: create.Simulation
   :members:
.. autoclass:: create.Network
   :members:
.. autoclass:: create.Population
   :members:
.. autoclass:: create.Projection
   :members:
.. autoclass:: create.Experiment
   :members:
.. autoclass:: create.Subexperiment
   :members:
.. autoclass:: create.Input
   :members:

.. _cellclasses:

-------------
Cell classes
-------------

A cell class defines the behavior and parameters of the neurons in a
population.  Each cell class has a dictionary, ``default_parameters``, which
contains its parameter names and their default values. Do not change the values
in this dictionary; the proper way to set cell parameters is to pass a
``cell_params`` dictionary to the Population constructor.

.. autoclass:: create.SIF
.. autoclass:: create.MiNi
.. autoclass:: create.SpikeSourcePoisson
.. autoclass:: create.SpikeSourceFile

.. _connectors:

------------
Connectors
------------

A Connector defines the synaptic connection scheme for a Projection.

.. autoclass:: create.OneToOneConnector
.. autoclass:: create.AllToAllConnector
.. autoclass:: create.PointsConnector
.. autoclass:: create.ProbabilityPointsConnector
.. autoclass:: create.PatternConnector
.. autoclass:: create.ProbabilityPatternConnector

.. _stimuli:

---------
Stimuli
---------

A Stimulus specifies which neurons in the target Population will receive input
current from an Input.

.. autoclass:: create.BackgroundStimulus
.. autoclass:: create.PointStimulus
.. autoclass:: create.LineStimulus
.. autoclass:: create.SquareStimulus
.. autoclass:: create.FilledSquareStimulus
.. autoclass:: create.MatrixStimulus
.. autoclass:: create.ImageStimulus

=====================
run
=====================

This module contains functions for running the simulator.

.. autofunction:: run.setup
.. autofunction:: run.end
.. autofunction:: run.run

.. _results:

=====================
results
=====================

This module contains functions and classes for plotting and otherwise
interacting with data from recorders.

-----------------------
The Results NumPy Array
-----------------------

:func:`run.run` returns a NumPy recarray containing recorded data, both spike
count and intracellular.  Each element of the returned array corresponds to
recorded data from a single neuron, from a single population, from a single
trial, from a single subexperiment.  The array has eight fields--five of which
identify the recording, one of which contains spike count data, and three of
which contain intracellular data.

+------------------------+----------------------------------------------------------------+                                    
| Field name             | Type                                                           |
+========================+================================================================+
| subexperiment          | string                                                         | 
+------------------------+----------------------------------------------------------------+
| trial                  | int                                                            |
+------------------------+----------------------------------------------------------------+
| population             | string                                                         |
+------------------------+----------------------------------------------------------------+
| x                      | int                                                            |
+------------------------+----------------------------------------------------------------+
| y                      | int                                                            |
+------------------------+----------------------------------------------------------------+
| spike_train            | 1-D NumPy array                                                |
+------------------------+----------------------------------------------------------------+
| intracellular_time     | 1-D NumPy array                                                |
+------------------------+----------------------------------------------------------------+
| intracellular_voltage  | 1-D NumPy array                                                |
+------------------------+----------------------------------------------------------------+
| intracellular_current  | n-D NumPy array, where n is the number of current channels     |
+------------------------+----------------------------------------------------------------+

To view and analyze data, you can either manipulate the results array yourself,
or use one of the built-in plotting classes.

------------------------------
Results Classes and Functions
------------------------------

.. autoclass:: results.SingleUnitPlot
   :members:
.. autoclass:: results.FieldPlot
   :members:
.. autoclass:: results.IntracellularPlot
   :members:
.. autofunction:: results.show
.. autofunction:: results.savemat
.. autofunction:: results.print_spikes
.. autofunction::  results.print_intracellular

==============================
jpvm
==============================

This module contains functions for interacting with the Java Parallel Virtual
Machine, which is a Java daemon that must be running in order for the simulator
to work.  You shouldn't worry about the functions in this module unless you
want to run the simulator using more than one host machine, and even then you
only need to use :func:`jpvm.addHost`.

.. autofunction:: jpvm.startJPVM
.. autofunction:: jpvm.stopJPVM
.. autofunction:: jpvm.killJPVM
.. autofunction:: jpvm.addHost
