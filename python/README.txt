HTML documentation is available in the *doc* subdirectory of this folder.

To configure, edit ernst/conf.py such that SIMULATOR_PATH points to the folder that contains this README documents.

This package requires the Python packages NumPy, SciPy, matplotlib, and LXML.

To install, run:
  sudo python setup.py install

To test, navigate to the *demos* subdirectory and run:
  python test.py
