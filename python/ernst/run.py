import os
import signal
import tempfile
import java
from results import parse_netcdf
from jpvm import startJPVM, stopJPVM

def setup(port = 10001):
    """
    Set up the simulator.  

    This method must be called before each session in which the simulator is 
    run.

    *port* is the port number that uniquely identifies this machine for JPVM.
    This is used for parallel computing.

    You **must** call end() after the simulator is finished running.

    """
    startJPVM(port)


def end():
    """
    Shut down the simulator.
  
    This method must be called after each session in which the simulator is 
    run.

    .. warning ::

        Failure to call :func:`run.end` at the conclusion of a session will
        result in the accumulation of orphan Java processes.  You will see a
        ``java.net.BindException`` error message (which is mostly harmless)
        the next time you call :func:`run.setup`. To fix this, call
        :func:`jpvm.stopJPVM` or, if that fails, :func:`jpvm.killJPVM`.
    
    """
    stopJPVM()


def run (simulation, num_trials=1, num_hosts=1, seed=-3,
         avalanche=False, xml_filename=None, results_filename=None,
         java_heap_size=512, host_heap_size=256):
    """
    Run the simulator.

    *simulation* is the :class:`create.Simulation` that should be run.

    *num_trials* is the number of trials for which the entire simulation will
    be run.  Do not confuse this with the number of repetitions of each 
    subexperiment.

    *num_hosts* is the number of host machines available to the simulator.
    All of the available host machines must already have been added, using 
    :func:`jpvm.addHosts()`, after :func:`run.setup` is called but before
    :func:`run.run` is called.

    *seed* is the seed of the simulator's random number generator.  Two
    simulations run with the same seed will return the exact same results.

    Set *avalanche* to ``True`` if you want the simulator to count avalanches.

    If *results_filename* is the name of a file, then the direct output of the
    Java simulator (a NetCDF file) will be saved to that location.  Otherwise,
    the direct output is saved to a temporary file and deleted after its
    its contents have been processed.

    If *xml_filename* is the name of a file, then the intermediate XML file
    that is passed as direct input to the Java simulator will be saved there.
    Otherwise, the intermediate XML file is saved to a temporary file and
    deleted as soon as it is no longer needed.  This argument should only be
    used by advanced users, for debugging.

    *java_heap_size* is the size of the heap of the main simulator process.
    Increase it if the simulator is crashing from inadequate memory.

    *host_heap_size* is the size of the heap of each NetHost process.
    Increase it if the simulator is crashing from inadequate memory.

    This function returns a NumPy recarray, the format of which is described in
    :ref:`results`.

    """

    is_temp_results_file = False
    if results_filename == None:
        results_filename = tempfile.mktemp()
        is_temp_results_file = True

    is_temp_xml_file = False
    if xml_filename:
        xml_file = open(xml_filename, 'w')
    else:
        model_path = java.join('model')
        xml_file = tempfile.NamedTemporaryFile(dir=model_path, delete=False)
       
    input_xml = simulation.finalize(java.relpath(results_filename,'results'),
                num_trials = num_trials, num_hosts = num_hosts)
    xml_file.write(input_xml)
    xml_file.close()

    options = ['-Xmx%dm' % java_heap_size]

    args = [java.relpath(xml_file.name, 'model'), str(seed), str(host_heap_size)]

    if avalanche:
        args += '-a'

    simulation = java.execute('starter', args, options)

    # Idiot-proofing. Intercept SIGINT so that CTRL-C will kill JPVM too. 
    def signal_handler(signal, frame):
        stopJPVM()
        exit(0)
    signal.signal(signal.SIGINT, signal_handler)

    simulation.wait()

    if is_temp_xml_file:
        os.unlink(xml_file.name)

    results = parse_netcdf(results_filename)
    if is_temp_results_file:
        os.unlink(results_filename)       

    return results

