import numpy
import matplotlib.colors as colors
import matplotlib.colorbar as colorbar
import matplotlib.cm as colormap
import matplotlib.pyplot as plt
import matplotlib.widgets as widgets
import matplotlib.ticker as ticker
import matplotlib.mlab as mlab
import scipy.io.netcdf as netcdf
import scipy.io

def parse_netcdf (filename):
    # Parses a NetCDF results file and returns a NumPy recarray containing
    # recorder data.

    # Converts a list of characters to a string.
    def to_string (chararray):
        return ''.join (chararray)

    results_file = open (filename)
    results_netcdf = netcdf.netcdf_file(results_file)
    variables = results_netcdf.variables
    results_file.close()

    population_list = []
    x_list = []
    y_list = []
    subexperiment_list = []
    trial_list = []
    spike_train_list = []
    intracellular_time_list = []
    intracellular_voltage_list = []
    intracellular_current_list = []

    subexperiment_names = map(to_string, variables['subexpNames'].data)
    subexperiment_ids = range (len(subexperiment_names))

    has_intracellular = 'intraBinSize' in variables
    has_spikes = 'suNames' in variables

    intracellular_neuron_names = []
    single_unit_neuron_names = []

    if has_intracellular:
        intracellular_time_bin_size = variables['intraBinSize'].data
        intracellular_neuron_names = map(to_string, variables['intraNames'].data)

    if has_spikes:
        single_unit_neuron_names = map(to_string, variables['suNames'].data)

    neuron_names = set(intracellular_neuron_names + single_unit_neuron_names)

    for subexperiment_id in subexperiment_ids:
        num_trials = variables [ 'SubExp%d_trialNum' % subexperiment_id ].data
        for trial_id in range (num_trials):
          for neuron_name in neuron_names:
              (population, x, y, _) = neuron_name.split (',')

              spike_train_key = ('SubExp%d_trial%d_%s,%s,%s,%s' %
                  (subexperiment_id, trial_id, population, x, y, population))
              voltage_key = ('SubExp%d_trial%d_%s,%s,%s,%s_V' %
                  (subexperiment_id, trial_id, population, x, y, population))
              current_key = ('SubExp%d_trial%d_%s,%s,%s,%s_C' %
                  (subexperiment_id, trial_id, population, x, y, population))

              spike_train = []
              if has_spikes:
                  if spike_train_key in variables:
                      spike_train = variables [spike_train_key].data

              voltage = []
              current = []
              times = []
              if has_intracellular:
                  if voltage_key in variables:
                      voltage = variables[voltage_key].data
                      current = variables[current_key].data
                      times = (numpy.arange(len(voltage)) 
                               * intracellular_time_bin_size)
               
              population_list.append(population)
              x_list.append(x)
              y_list.append(y)
              subexperiment_list.append(subexperiment_names [subexperiment_id])
              trial_list.append(trial_id)
              spike_train_list.append(numpy.array(spike_train, dtype='float64'))
              intracellular_time_list.append(numpy.array(times, dtype='float64'))
              intracellular_voltage_list.append(numpy.array(voltage, dtype='float64'))
              intracellular_current_list.append(numpy.array(current, dtype='float64'))


    num_results = len (population_list)

    results = numpy.zeros (num_results, dtype = [('subexperiment',  'a50'), 
                                                 ('trial', 'i'), 
                                                 ('population', 'a50'), 
                                                 ('x', 'i'), 
                                                 ('y', 'i'), 
                                                 ('spike_train', numpy.ndarray),
                                                 ('intracellular_time', numpy.ndarray),
                                                 ('intracellular_current', numpy.ndarray),
                                                 ('intracellular_voltage', numpy.ndarray)])

    results['subexperiment'] = subexperiment_list
    results['trial'] = trial_list
    results['population'] = population_list
    results['x'] = x_list
    results['y'] = y_list
    results['spike_train'] = spike_train_list
    results['intracellular_time'] = intracellular_time_list
    results['intracellular_current'] = intracellular_current_list
    results['intracellular_voltage'] = intracellular_voltage_list

    results = results.view(numpy.recarray)

    results = numpy.sort(results, order=['subexperiment', 'trial',
                                         'population', 'x', 'y'])

    return results


def print_spikes(filename, results, subexperiment, trial, population, coordinate):
    """
    Write a spike recording to a file.

    After a few #comment lines at the top, each line will contain a spike time.

    *filename* is the name of the file to write to.
 
    *results* is the results NumPy recarray.

    *subexperiment*, *trial*, *population*, *x*, and *y* are identifying
    information for the spike recording.

    """

    (x, y) = coordinate

    data = results [(results.subexperiment == subexperiment) * 
                    (results.population == population)  * 
                    (results.x == x) * 
                    (results.y == y)]

    if len(data) == 0:
         print "Warning: no recordings with those details were found."

    spike_train = map(str, list(data.spike_train[0]))
    spike_text = '\n'.join(spike_train)

    spike_file = open(filename, 'w')
    spike_file.write('# Subexperiment: %s\n' % subexperiment)
    spike_file.write('# Trial: %d\n' % trial)
    spike_file.write('# Population: %s\n' % population)
    spike_file.write('# Position: (%d, %d)\n' % (x, y))
    spike_file.write(spike_text)
    spike_file.close()

def print_intracellular(filename, results, subexperiment, trial, population, x, y):
    """
    Write an intracellular recording to a file.

    After a few #comment lines at the top, each line will contain the following
    data points, separated by spaces: time, voltage, excitatory current,
    inhibitory current.
    
    *filename* is the name of the file to write to.
 
    *results* is the results NumPy recarray.

    *subexperiment*, *trial*, *population*, *x*, and *y* are identifying
    information for the intracellular recording.

    """
    # TODO: currently, this function only works with 2 currents.

    data = results [(results.subexperiment == subexperiment) * 
                    (results.population == population)  * 
                    (results.x == x) * 
                    (results.y == y)]

    if len(data) == 0:
         print "Warning: no recordings with those details were found."

    time = map(str, data.intracellular_time[0])
    voltage = map(str, data.intracellular_voltage[0])
    currents = [' '.join(map(str, curr)) for curr in data.intracellular_current[0].T]

    print [curr for curr in data.intracellular_current[0].T]

    lines = zip(time, voltage, currents)
    text = '\n'.join([' '.join(line) for line in lines])

    spike_file = open(filename, 'w')
    spike_file.write('# Subexperiment: %s\n' % subexperiment)
    spike_file.write('# Trial: %d\n' % trial)
    spike_file.write('# Population: %s\n' % population)
    spike_file.write('# Position: (%d, %d)\n' % (x, y))
    spike_file.write('# Format: time voltage current[i]\n')
    spike_file.write(text)
    spike_file.close()

def savemat(results, filename, varname='results'):
    """Save a results array to a MATLAB .mat file.

    *results* is the NumPy array to save.

    *filename* is the name of the MATLAB data file to save to.
    It should end in ``.mat``.

    *varname* is the name of the variable under which the array will be stored.
    It defaults to ``"results"``.

    """

    scipy.io.savemat(filename, {varname : results})

class SingleUnitPlot:
    """Raster plot of spike times for a single neuron across all trials."""

    def __init__(self, results, subexperiment, population, coordinate):
        """

        *results* is the results NumPy recarray.

        *subexperiment*, is the label of the subexperiment of the desired spike
        recording.

        *population* is the label of the population of the desired spike
        recording.
       
        *coordinate*, a tuple of the form ``(x,  y)``, is the position of the
        neuron of the desired spike recording.

        """

        (x, y) = coordinate

        self.single_unit_data = results[(results.subexperiment == subexperiment) * 
                                        (results.population == population)  * 
                                        (results.x == x) * 
                                        (results.y == y)]

        if len(self.single_unit_data) == 0:
            print "Warning: no recordings with those details were found."

        self.subexperiment = subexperiment
        self.population = population
        self.x = x
        self.y = y

        self.single_unit_data.sort(order='trial')

    def show(self, block=True):
        """Show the plot.

        By default, this method "blocks", meaning that it stays in control of
        the execution thread until the window is manually closed.  If you want
        to show more than one plot on the screen at once, set *block* to
        ``False``.  In that case, no plots will appear until you call
        :func:`results.show`, at which point all plots will appear.

        """

        self.figure = plt.figure(facecolor='white', figsize=(10, 8), dpi=80 )
        self.plot_axes = self.figure.add_axes([.1, .15, .8, .75])
        self.draw()
        if block:
            plt.show()

    def save (self, filename):
        """Save the plot."""

        self.figure = plt.figure(facecolor='white', figsize=(10, 8), dpi=80 )
        self.plot_axes = self.figure.add_axes([ .1, .15, .8, .75 ])
        self.draw()
        self.figure.savefig(filename)

    def draw (self):
        ticklabels = []

        for i in range(len(self.single_unit_data)):
            recording = self.single_unit_data[i]
            x = recording['spike_train']
            y = numpy.zeros_like(x) + i
            self.plot_axes.scatter(x, y, s=30, color='r', marker='s')
            ticklabels.append('Trial %s' % recording['trial'])

        self.plot_axes.set_axis_bgcolor('.8')
        self.plot_axes.set_title('Single-Unit\n%s: %s (%d, %d)' % (self.subexperiment, self.population, self.x, self.y), fontsize=20)
        self.plot_axes.set_xlabel('Time (s)', fontsize=20)
        self.plot_axes.xaxis.grid(b=True)
        self.plot_axes.set_ylim(-.5, len(self.single_unit_data) - .5)
        self.plot_axes.set_xlim(left=0)
        self.plot_axes.yaxis.set_major_locator(ticker.FixedLocator(range(len(ticklabels))))
        self.plot_axes.yaxis.set_ticklabels(ticklabels, fontsize=15)


class MultiUnitPlot:
    """"Line plot of average spike times over multiple neurons in a population."""    

    def __init__(self, results, subexperiment, population, coordinates, step=.1, begin=0, end=None):
        """

        *results* is the results NumPy recarray.

        *subexperiment*, is the label of the subexperiment of the desired spike
        recordings.

        *population* is the label of the population of the desired spike
        recordings.
       
        *coordinates*, a list of tuples of the form ``(x, y)``, are the positions
        of the neurons of the desired spike recordings.
       
        *begin* and *end* are the beginning and ending times to plot.

        *step* is the length of the time bins.

        """

        self.population = population
        self.subexperiment = subexperiment
        
        if end == None:
            end = max ( map ( _safe_max, results.spike_train ) )

        data = results[(results.subexperiment == subexperiment) &
                       (results.population == population) &
                       (numpy.logical_or.reduce(
                           [(results.x == x) & (results.y == y)
                           for (x, y) in coordinates]))]
                        
        all_spikes = numpy.sort(numpy.hstack(data.spike_train))

        self.times = numpy.arange(begin, end, step)

        time_bin_edges = numpy.append(self.times, end)
                                        
        self.spike_counts = numpy.histogram(all_spikes, time_bin_edges)[0] / float(len(data))
        
    def show(self, block=True):
        """Show the plot.

        By default, this method "blocks", meaning that it stays in control of
        the execution thread until the window is manually closed.  If you want
        to show more than one plot on the screen at once, set *block* to
        ``False``.  In that case, no plots will appear until you call
        :func:`results.show`, at which point all plots will appear.

        """

        self.figure = plt.figure(facecolor='white', figsize=(10, 8), dpi=80 )
        self.plot_axes = self.figure.add_axes([.1, .15, .8, .75])
        self.draw()
        if block:
            plt.show()

    def save (self, filename):
        """Save the plot."""

        self.figure = plt.figure(facecolor='white', figsize=(10, 8), dpi=80 )
        self.plot_axes = self.figure.add_axes([ .1, .15, .8, .75 ])
        self.draw()
        self.figure.savefig(filename)
               
    def draw(self):
        self.plot_axes.plot(self.times, self.spike_counts, linewidth=4, color='r') 
        
        self.plot_axes.set_xlabel('Time (s)', fontsize=20)
        self.plot_axes.set_ylabel('Number of spikes', fontsize=20)
        self.plot_axes.set_title('Multi-Unit Plot', fontsize=20)
        self.plot_axes.set_axis_bgcolor('.8')

# A maximum function that won't raise an exception when passed an empty array.
def _safe_max(array):
    if len(array) == 0:
        return 0
    else:
        return array.max()

class FieldPlot(object):
    """Image plot of the spike counts of an entire population."""

    def __init__(self, results, subexperiment, population, step=None, begin=0, end=None ):
        """

        *results* is the results NumPy recarray.

        *subexperiment*, is the label of the subexperiment of the desired spike
        recordings.

        *population* is the label of the population of the desired spike
        recordings.
       
        *begin* and *end* are the beginning and ending times to plot.

        If *step* is not ``None``, the data is plotted in frames, each
        containing spikes within a time interval of *step*.
 
        """

        # Calculate the end time, if one wasn't specified.

        if end == None:
            last_spike_time = max(map(_safe_max, results.spike_train))
            if step:
                end = last_spike_time - (last_spike_time % step) + step
            else:
                end = last_spike_time

        step = step or (end - begin)

        self.begin = begin
        self.end = end
        self.step = step
        self.population = population
        self.subexperiment = subexperiment

        # Keep only spike data from our desired population and subexperiment.

        field_data = results[(results.population == population) *
                             (results.subexperiment == subexperiment)]

        if len(field_data) == 0:
            print "Warning: no recordings with those details were found."
  
        times = numpy.arange(begin, end, step)

        time_bin_edges = numpy.append(times, end)

        spike_counts = numpy.array([numpy.histogram(spikes, time_bin_edges)[0] for spikes in field_data.spike_train]).T

        neurons = numpy.unique(zip(field_data.x, field_data.y)) 
 
        masks = [(field_data.x == neuron[0]) * (field_data.y == neuron[1]) for neuron in neurons]

        mean_spike_counts = numpy.array([[spike_count[mask].mean() for mask in masks] for spike_count in spike_counts]) 

        # Construct the frames

        x_coords = neurons [:, 0]
        y_coords = neurons [:, 1]

        self.frames = numpy.zeros(len(times), dtype=[('time', float), 
                                                     ('frame', float, (y_coords.max() + 1, x_coords.max() + 1))])

        for i in range (0, len(self.frames)):
            self.frames[i]['time'] = times[i]
            for x, y, mean_spike_count in zip(x_coords, y_coords, mean_spike_counts[i]):
                self.frames [i]['frame'][y, x] = mean_spike_count

        self.normalization = colors.Normalize(vmin = 0, vmax=mean_spike_counts.max())

    def show (self, block=True):
        """Show the plot.

        By default, this method "blocks", meaning that it stays in control of
        the execution thread until the window is manually closed.  If you want
        to show more than one plot on the screen at once, set *block* to
        ``False``.  In that case, no plots will appear until you call
        :func:`results.show`, at which point all plots will appear.

        """

        self.figure = plt.figure(facecolor=(.7, .7, 1), figsize=(10, 8), dpi=80)
        self.plot_axes = self.figure.add_axes([.05, .15, .8, .75])
        self.colorbar_axes = self.figure.add_axes([.9, .15, .02, .75])
        self.prev_button_axes = self.figure.add_axes([.4, .02, .05, .03])
        self.next_button_axes = self.figure.add_axes([.45, .02, .05, .03])

        prev_button = widgets.Button(self.prev_button_axes, "Prev")
        next_button = widgets.Button(self.next_button_axes, "Next")

        prev_button.on_clicked(self.previous)
        next_button.on_clicked(self.next)
        
        self.figure.canvas.mpl_connect('key_press_event', self.key_press)

        self.frame_index = 0        
        self.draw(self.frame_index)

        if block:
            plt.show()

    def save (self, filename):
        """Save the plot."""

        self.figure = plt.figure(facecolor=(.7, .7, 1 ), figsize=(10, 8), dpi=80)
        self.plot_axes = self.figure.add_axes([.05, .15, .8, .75])
        self.colorbar_axes = self.figure.add_axes([.9, .15, .02, .75])
        self.draw()
        self.figure.savefig(filename)

    def key_press(self, event):
        if event.key == 'right':
            self.next()
        if event.key == 'left':
            self.previous()
 
    def next(self, event=None):
        self.frame_index += 1
        if self.frame_index == len(self.frames):
            self.frame_index = 0

        self.draw(self.frame_index)

    def previous(self, event=None):
        self.frame_index -= 1
        if self.frame_index == -1:
            self.frame_index = len(self.frames) - 1

        self.draw(self.frame_index)

    def draw(self, frame_index=0):
        self.draw_plot(frame_index)
        self.draw_colorbar()
        self.draw_title(frame_index)

        self.figure.canvas.draw()

    def draw_title(self, frame_index):
        time = self.frames  ['time'] [ frame_index ]
        title = str ( self.population ) + ' ' + str ( time ) + ' - ' + str ( time + self.step ) + ' s'  
        self.plot_axes.set_title ( title, fontsize = '20' )

    def draw_plot(self, frame_index):
        self.plot_axes.clear()

        self.plot_axes.imshow (self.frames[frame_index]['frame'], 
                               aspect='auto', cmap=colormap.gray, 
                               norm=self.normalization, origin='upper',
                               interpolation='nearest')

        self.plot_axes.locator_params(integer=True)

    def draw_colorbar(self):
        self.colorbar_axes.clear()
        self.colorbar_axes.set_title("Scale")
   
        cb = colorbar.ColorbarBase(self.colorbar_axes, cmap=colormap.gray, norm=self.normalization)
        cb.set_label('Number of Spikes')


class IntracellularPlot(object):
    """A line plot of intracellular voltage and currents for a single cell over
    time."""

    def __init__(self, results, subexperiment, trial, population, coordinate):
        """

        *results* is the results NumPy recarray.

        *results* is the results NumPy recarray.

        *subexperiment*, is the label of the subexperiment of the desired
        intracellular recording.

        *population* is the label of the population of the desired
        intracellular recording.
       
        *coordinate*, a tuple of the form ``(x, y)``, is the position of the
        neuron of the desired intracellular recording.

        """

        (x, y) = coordinate

        data = results [(results.subexperiment == subexperiment) *
                        (results.population == population) *
                        (results.trial == trial) *
                        (results.x == x) *
                        (results.y == y)]

        if len(data) == 0:
            print "Warning: no recordings with those details were found."

        self.subexperiment = subexperiment
        self.population = population
        self.trial = trial
        self.x = x
        self.y = y

        self.time = data[0]['intracellular_time']
        self.voltage = data[0]['intracellular_voltage']
        self.currents = data[0]['intracellular_current']

    def show(self, block=True):
        """Show the plot.

        By default, this method "blocks", meaning that it stays in control of
        the execution thread until the window is manually closed.  If you want
        to show more than one plot on the screen at once, set *block* to
        ``False``.  In that case, no plots will appear until you call
        :func:`results.show`, at which point all plots will appear.

        """

        self.figure = plt.figure(facecolor='white', figsize=(10, 8), dpi=80)
        self.voltage_axes = self.figure.add_subplot(2, 1, 1)
        self.current_axes = self.figure.add_subplot(2, 1, 2, sharex=self.voltage_axes)
        self.draw()
        if block:
            plt.show()

    def save(self, filename ):
        """Save the plot."""
        self.figure = plt.figure(facecolor='white', figsize=(10, 8), dpi=80)
        self.voltage_axes = self.figure.add_subplot(2, 1, 1)
        self.current_axes = self.figure.add_subplot(2, 1, 2, sharex=self.voltage_axes)
        self.draw()
        self.figure.savefig(filename)

    def draw(self):
        self.voltage_axes.plot(self.time, self.voltage, label='Voltage')
        for i in range(len(self.currents)):
            self.current_axes.plot(self.time, self.currents[i], label='Current' + str(i))

        self.figure.suptitle('Intracellular Plot\n %s #%d, %s, (%d, %d)' %
           (self.subexperiment, self.trial, self.population, self.x, self.y)
          , fontsize = 20)
        self.current_axes.set_xlabel('Time (s)', fontsize = 20)
        self.current_axes.set_ylabel('Current  (A)', fontsize = 20)
        self.voltage_axes.set_ylabel('Voltage (V)', fontsize = 20)
        self.voltage_axes.legend()
        self.current_axes.legend()
        self.current_axes.set_axis_bgcolor('.8')
        self.voltage_axes.set_axis_bgcolor('.8')
        self.current_axes.xaxis.grid(b = True)
        self.voltage_axes.xaxis.grid(b = True)

def show():
    """Display all plots that were shown with *block* set to ``False``."""
    plt.show()

