# Functions for interacting with the Java simulator.

import os
from subprocess import Popen
from conf import *

def execute(command, args=[], options=[]):
    os.environ['CLASSPATH'] = os.path.join(SIMULATOR_DIRECTORY, 'simulator.jar')
    cwd = os.getcwd()
    os.chdir(SIMULATOR_DIRECTORY)
    process = Popen(['java']  + options + [command] + args)
    os.chdir(cwd)
    return process

def join(path):
    return os.path.join(SIMULATOR_DIRECTORY, path)

def relpath(filename, subdirectory):
    # Returns the relative path from *subdirectory* in the simulation folder
    # to *filename*.

    return os.path.relpath(filename, join(subdirectory))
