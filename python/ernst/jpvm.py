import os
import signal
import subprocess
import java


def addHost(name, port):
    """Add another host machine to this JPVM daemon.

    *name* is the name of the host to connect to.  You can match host names
    to IP addresses in ``/etc/hosts`` (Linux), ``/private/etc/hosts``
    (Mac), or ``%SystemRoot%\system32\drivers\etc`` (Windows).

    *port* is the port number.  Each host must be assigned a different port
    number.
    """
    java.execute('jpvm.jpvmAddHosts', [name, str(port)])


def startJPVM (port):
    """Start the Java Parallel Virtual Machine daemon."""

    java.execute('jpvm.jpvmDaemon', [str(port)])


def stopJPVM ():
    """Stop the Java Parallel Virtual Machine daemon."""

    java.execute('jpvm.jpvmHaltDaemon')


def killJPVM ( ):
    """Kill all instances of the JPVM daemon currently running.

    Use this function if there are orphan JPVM daemon processes running on your
    machine.

    Alternatively, you can replicate this function manually by killing all
    Java processes named "jpvmDaemon", "TrialHost", or "NetHost."  You can
    see the names and pids of all ongoing Java processes with the shell command
    ``jps``.

    """

    # Use the jps commanded provided by Java to get a list of current Java processes.
    javaProcessesText = subprocess.check_output ( 'jps' )
    javaProcesses = javaProcessesText.split ( )

    # If the Java process is a JPVM daemon, terminate it.
    for i in range (0, len(javaProcesses) - 1, 2):
        processName = javaProcesses[i+1]
        processId = int ( javaProcesses[i])
        if (processName == "jpvmDaemon" 
         or processName == "TrialHost" 
         or processName == "NetHost"):
            os.kill(processId , signal.SIGKILL)

    # TODO: this function currently doesn't work properly if one or more of the
    # processes is unnamed.
