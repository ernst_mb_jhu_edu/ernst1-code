"""
Functions and classes for creating a simulation.

User classes:
    Simulation
    Population
    Projection
    Subexperiment
    Input
    
Connectors:
    OneToOneConnector
    PointConnector
    ProbabilityPointConnector
    PatternConnector
    ProbabilityPatternConnector

Stimuli:
    BackgroundStimulus
    PointStimulus
    LineStimulus
    SquareStimulus
    FilledSquareStimulus
    MatrixStimulus
    ImageStimulus

Cell classes:
    SIF
    MiNi
    SpikeSourcePoisson
    SpikeSourceFile
"""

from lxml import etree 
import numpy as np
from math import ceil

#=============================================================================#
# Utility Functions
#=============================================================================#

def _vector_to_comma_string(vector):
    # Converts a NumPy array or a Python list into a comma-separated string 
    # appropriate for ERNST XML parameters.
    return ','.join (map(str, vector))


_matrix_count = 0

def _add_matrix (root, matrix):
    # Adds a matrix to the ConstantDefinition section.
    # 
    # Arguments:
    #    root: the root element of the XML tree.
    #    matrix: a NumPy array to be added as a matrix.
    global _matrix_count
    matrix_id = 'matrix-' + str(_matrix_count)
    _matrix_count += 1

    constant_definition_element = root.find("ConstantDefinition")
    matrix_element = etree.SubElement(constant_definition_element, "Matrix", 
                                      id = matrix_id)

    row_element = etree.SubElement(matrix_element, 'Row')
    try:
        row_element.text = _vector_to_comma_string (matrix[0,:])
        for ii in range ( np.shape(matrix)[0] ) [1:]:
            row_element = etree.SubElement(matrix_element, 'Row')
            row_element.text = _vector_to_comma_string(matrix[ii,:])
    except IndexError:
        row_element.text = _vector_to_comma_string (matrix)

    return matrix_id

#=============================================================================#
# Dictionaries to convert between ErnstPy parameters and ERNST XML parameters.
#=============================================================================#

_SYNAPSE_TYPE = {
    'excitatory':'Glutamate',
    'inhibitory':'GABA'
}

_CONNECT_STYLE = {
    'convergent':'Convergent',
    'divergent':'Divergent'
}

#=============================================================================#
# Types of neurons.
#=============================================================================#

class Neuron(object):
    """The superclass of all neuron type classes."""

    _name = None  # The ERNST XML name of this neuron.
    default_parameters = {} 


class SIF(Neuron):
    """A simple integrate-and-fire neuron.

    SIF neurons have two synaptic input channels: one excitatory and one
    inhibitory.

    +--------------------+---------------+-----------------------------------------------+
    | Parameter name     | Default value | Description                                   |
    +====================+===============+===============================================+
    | Capacitance        |   3e-11       |                                               |
    +--------------------+---------------+-----------------------------------------------+
    | LeakConductance    |   1.5e-9      |                                               |
    +--------------------+---------------+-----------------------------------------------+
    | DecayConstantE     |   .005        | The decay constant for the excitatory channel |
    +--------------------+---------------+-----------------------------------------------+
    | DecayConstantI     |   .025        | The decay constant for the inhibitory channel |
    +--------------------+---------------+-----------------------------------------------+
    | RestingPotential   | -0.07         |                                               |
    +--------------------+---------------+-----------------------------------------------+
    | ResetPotential     | -0.07         |                                               |
    +--------------------+---------------+-----------------------------------------------+
    | Threshold          | -0.05         |                                               |
    +--------------------+---------------+-----------------------------------------------+
    | RefractoryCurrent  | 0             |                                               |
    +--------------------+---------------+-----------------------------------------------+
    | RefractoryPeriod   | 0.002         |                                               |
    +--------------------+---------------+-----------------------------------------------+
    | MinRiseTime        | 0.0001        |                                               |
    +--------------------+---------------+-----------------------------------------------+
    | MembraneVoltage    | -0.07         |                                               |
    +--------------------+---------------+-----------------------------------------------+
    | VarMembraneVoltage | 0.005         | The initial membrane voltage variance         |
    +--------------------+---------------+-----------------------------------------------+

    """

    _name = "SIFNeuron"

    default_parameters = {'Capacitance':3e-11, 
                          'LeakConductance':1.5e-9,
                          'DecayConstantE':0.005,
                          'DecayConstantI':0.025,
                          'RestingPotential':-0.07,
                          'ResetPotential':-0.07,
                          'Threshold':-0.05, 
                          'RefractoryCurrent':0,
                          'RefractoryPeriod':0.002, 
                          'MinRiseTime':0.0001,
                          'MembraneVoltage':-0.07, 
                          'VarMembraneVoltage':0.005}

class VSICLIFV2(Neuron):
    _name = "VSICLIFNeuronV2"
    default_parameters =  {'Capacitance':3e-11, 'LeakConductance':1.5e-9,
                       'RestingPotential':-0.07, 'ResetPotential':-0.07,
                       'AsymptoticThreshold':-0.05, 'RefractoryPeriod':0.002,
                       'MinRiseTime':0.0001, 'MembraneVoltage':-0.07, 
                       'VarMembraneVoltage':0.005, 'ThresholdAdaptation':0,
                       'ThresholdRebound':10, 'ThresholdReset':-0.06,
                       'ThresholdAddition':0, 'ExternalCurrent':0,
                       'InitThreshold':-0.07, 'Decays':[0.005,0.025],
                       'CurrentsAddition':[0,0], 'CurrentsRatio':[0,1],
                       'InitCurrents':[0,0]}

class MiNi(Neuron):
    """A Mihalas-Niebur neuron.

    MiNi neurons have two input synapse channels and an arbitrary number of
    spike-induced current channels, determined by the lengths of the
    spike-induced current-related parameters. 

    +-----------------------+---------------+------------------------------------+
    | Parameter name        | Default value | Description                        |
    +=======================+===============+====================================+
    | Capacitance           |   3e-11       | *C* in the paper                   |
    +-----------------------+---------------+------------------------------------+
    | LeakConductance       |   1.5e-9      | *G* in the paper                   |
    +-----------------------+---------------+------------------------------------+
    | RestingPotential      | -0.07         | *E_L* in the paper                 |
    +-----------------------+---------------+------------------------------------+
    | ResetPotential        | -0.07         | *V_r* in the paper                 |
    +-----------------------+---------------+------------------------------------+
    | AsymptoticThreshold   | -0.05         | *theta_infinity* in the paper      |
    +-----------------------+---------------+------------------------------------+
    | RefractoryPeriod      | 0.002         |                                    |
    +-----------------------+---------------+------------------------------------+
    | MembraneVoltage       | -0.07         | *V_0* in the paper                 |
    +-----------------------+---------------+------------------------------------+
    | VarMembraneVoltage    | 0.005         | Initial membrane voltage variance  |
    +-----------------------+---------------+------------------------------------+
    | ThresholdAdaptation   | 0             | *a* in the paper                   |
    +-----------------------+---------------+------------------------------------+
    | ThresholdRebound      | 10            | *b* in the paper                   |
    +-----------------------+---------------+------------------------------------+
    | ThresholdReset        | -0.06         | *theta_r* in the paper             |
    +-----------------------+---------------+------------------------------------+
    | ThresholdAddition     | 0             | *A_j* in the paper                 |
    +-----------------------+---------------+------------------------------------+
    | ExternalCurrent       | 0             | *I_E* in the paper                 |
    +-----------------------+---------------+------------------------------------+
    | InitThreshold         | -0.05         | *theta_0* in the paper             |
    +-----------------------+---------------+------------------------------------+
    | SpikeDecays           | [0.005,0.025] | *k_j* in the paper                 |
    +-----------------------+---------------+------------------------------------+
    | SynapseDecays         | [0.005,0.025] | Decay tau's for synaptic currents  |
    +-----------------------+---------------+------------------------------------+
    | SpikeCurrentsAddition | [0,0]         | *A_j* in the paper                 |
    +-----------------------+---------------+------------------------------------+
    | SpikeCurrentsRatio    | [1,1]         | *R_j* in the paper                 |
    +-----------------------+---------------+------------------------------------+
    | InitSpikeCurrents     | [0,0]         | *i_j0* in the paper                |
    +-----------------------+---------------+------------------------------------+
    | K_LTD                 | 50            | Long-term depression exponent      |
    +-----------------------+---------------+------------------------------------+
    | K_LTP                 | 200           | Long-term potentiation exponent    |
    +-----------------------+---------------+------------------------------------+
    | ALPHA_LTD             | 0.01          | Long-term depression multiplier    |
    +-----------------------+---------------+------------------------------------+
    | ALPHA_LTP             | 0.01          | Long-term potentiation multiplier  |
    +-----------------------+---------------+------------------------------------+
    
    .. seealso::
       `A Generalized Linear Integrate-and-Fire Neural Model Produces Diverse
       Spiking Behaviors <http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2954058/>`_.
          The original paper describing the neuron model.
    """

    _name = "MiNiNeuron"
    default_parameters = {'Capacitance':3e-11, 
                          'LeakConductance':1.5e-9,
                          'RestingPotential':-0.07, 
                          'ResetPotential':-0.07,
                          'AsymptoticThreshold':-0.05, 
                          'RefractoryPeriod':0.002,
                          'MembraneVoltage':-0.07, 
                          'VarMembraneVoltage':0.005,
                          'ThresholdAdaptation':0,
                          'ThresholdRebound':10,
                          'ThresholdReset':-0.06,
                          'ThresholdAddition':0,
                          'ExternalCurrent':0,
                          'InitThreshold':-0.05,
                          'SpikeDecays':[0.005,0.025],
                          'SynapseDecays':[0.005,0.025],
                          'SpikeCurrentsAddition':[0,0], 
                          'SpikeCurrentsRatio':[1,1],
                          'InitSpikeCurrents':[0,0],
                          'K_LTD':50,
                          'K_LTP':200, 
                          'Alpha_LTD':0.01, 
                          'Alpha_LTP':0.01}   


class SpikeSourcePoisson(Neuron):
    """A neuron that fires according to a Poisson process.

    +---------------------------+---------------+-----------------------------------------------+
    | Parameter name            | Default value | Description                                   |
    +===========================+===============+===============================================+
    | Frequency                 | 50            |                                               |
    +---------------------------+---------------+-----------------------------------------------+
    | SaturationFrequency       | 30            |                                               |
    +---------------------------+---------------+-----------------------------------------------+
    | InverseFrequencyDecayTime | 0             | Inverse time constant of frequency decay      |
    +---------------------------+---------------+-----------------------------------------------+

    """

    _name = "BKPoissonNeuron"
    default_parameters = {'Frequency':50,
                          'SaturationFrequency':30,
                          'InverseFrequencyDecayTime':0}


class SpikeSourceFile(Neuron):
    """A neuron that fires according to spike times read from a data file.

    +---------------------------+---------------+-----------------------------------------------+
    | Parameter name            | Default value | Description                                   |
    +===========================+===============+===============================================+
    | DataFile                  | inputData.txt | A file containing spike times.                |
    +---------------------------+---------------+-----------------------------------------------+
    
    The path to ``DataFile`` should be relative to the ``input`` folder, and
    the file should contain spike times in the following format:

    Spike times for each neuron should occupy one line.  Delimited by spaces,
    the line should first include the subexperiment number, then the trial
    number, then, separated by commas, the population label, the neuron
    x-coordinate, the neuron y-coordinate, the population label again, and
    finally, separated by spaces, a list of spike times::

        0 0 AA,0,0,AA 0.00049314 0.00065926 0.004612 0.018346 0.029955 \
0.055654 0.063001 0.08199 0.082669 0.087991 0.10277 0.10769 0.12964

    """

    _name = "UserSenNeuron"
    default_parameters = {'DataFile':'inputData.txt'}

#=============================================================================#
# User classes.
#=============================================================================#

class Simulation(object):
    """Consists of a Network and an Experiment, which together comprise all
    the information the simulator needs in order to run a simulation."""

    def __init__(self, network, experiment):
        """
        
        *network* is a :class:`create.Network`, which encapsulates the neurons
        and the connections between them.

        *experiment* is an :class:`create.Experiment`, which encapsulates the
        operations performed upon the Network in the course of the Simulation.

        """

        self.network = network
        self.experiment = experiment
        
    def finalize (self, data_file, num_trials = 1, num_hosts = 1):
        # Add some last-minute parameters and write to XML.

        self.data_file = data_file
        self.num_trials = num_trials
        self.num_hosts = num_hosts

        # The simulator requires that each subexperiment have at least one
        # input, so create dummy inputs as necessary.
        for subexperiment in self.experiment.subexperiments:
            if len(subexperiment.inputs) == 0:
                subexperiment.add( 
                  Input(self.network._get_dummy_population(), 0, 0,
                        BackgroundStimulus(), weight=0))

        root = self._write_xml()

        return etree.tostring(root, pretty_print = True)
        
    def _write_xml (self):

        XML_NS = 'http://cnslab.mb.jhu.edu'
        NS = 'http://www.w3.org/2001/XMLSchema-instance'
        location_attribute = '{%s}schemaLocation' % NS
        root = etree.Element('network_declaration', attrib={'xmlns': XML_NS,
                             location_attribute: XML_NS + ' simulator.xsd'})

        overall_configuration_element = etree.SubElement(root, 'OverallConfiguration')

        data_file_element = etree.SubElement (overall_configuration_element,
                                             'DataFileName')
        data_file_element.text = self.data_file

        trial_host_element = etree.SubElement(
                                   overall_configuration_element, 'TrialHost')
        trial_host_element.text = str(self.num_trials)

        net_host_element = etree.SubElement(
                                   overall_configuration_element, 'NetHost')
        net_host_number_element = etree.SubElement(net_host_element, 'Number')
        net_host_number_element.text = str(self.num_hosts)
        net_host_map_method_element = etree.SubElement(
                                          net_host_element, 'MapMethod',
                                          A='1', B='1')
        net_host_map_method_element.text = 'abmap'


        neuron_definition_element = etree.SubElement(
                                        root, 'NeuronDefinition')
        constant_definition_element = etree.SubElement(
                                          root, 'ConstantDefinition')
        layers_element = etree.SubElement(root, 'Layers')
        connections_element = etree.SubElement(root, 'Connections')
        experiment_element = etree.SubElement(root, 'Experiment')
        recorder_element = etree.SubElement(root, 'Recorder', plot='false')

        self.network._write_xml(root)
        self.experiment._write_xml(root)

        return root

class Network(object):
    """Encapsulates the populations and projections that comprise a network of
       neurons."""

    def __init__(self, populations=[], projections=[], base_size=(1,1)):
        """        
        
        *populations* is a list of :class:`create.Population` objects.  Populations
        can either be passed to the network here as a list or added one at a time
        using ``add()``.

        *projections* is a list of :class:`create.Projection` objects.  Projections can
        either be passed to the network here as a list or added one at a time
        using ``add()``.

        *base_size* is a tuple of the form ``(width, height)`` which
        determines the base size of the grid.

        """

        self.components = populations + projections
        self.base_size = base_size
        self.dummy_population = None

    def add(self, component):
        """Add a population or a projection."""
        self.components.append(component)

    def _write_xml(self, root):
        if len(self.components) == 0:
            raise ValueError("Network must include at least one Population.")

        overall_configuration_element = root.find('OverallConfiguration')

        layer_size_element = etree.SubElement(
                                    overall_configuration_element, 'LayerSize')

        x_edge_length_element = etree.SubElement(
                                   layer_size_element, 'xEdgeLength')
        x_edge_length_element.text = str(self.base_size[1])

        y_edge_length_element = etree.SubElement(
                                   layer_size_element, 'yEdgeLength')
        y_edge_length_element.text = str(self.base_size[0])        

        for component in self.components:
            component._write_xml(root)

    def _get_dummy_population(self):
        if self.dummy_population == None:
            self.dummy_population = Population("-DUMMY-", (1, 1), SIF)
            self.add(self.dummy_population)
        return self.dummy_population


class Population(object):
    """A group of neurons with the same parameters."""

    def __init__(self, label, size, cellclass, cellparams=None):
        """
 
        *label* is a string that uniquely identifies this population for the 
        purposes of recording.  It must be unique.

        *size*, a tuple of the form ``(width, height)``, is the size of this
        population.  The number of neurons is ``width x height``.

        *cellclass* is a cell class (see :ref:`cellclasses`) that represents
        one of the four available neuron types.  

        *cellparams* is a dictionary of parameter names and values for the
        neurons in this population.  All parameters that aren't passed as
        keys to *cellparams* will be set to their default values.

        For example::

            pop1 = Population('V1', (100, 100), SIF, cellparams=\
{'Threshold': -.06})

        will create a population ``pop1`` of 10,000 simple integrate-and-fire
        neurons laid out in a 100x100 grid with their voltage thresholds set to
        ``-.06`` and all other parameters set to default values.
        """

        self.label = label
        self.cellclass = cellclass
        self.recorders = []
        
        if len (size) != 2:
            raise ValueError("size must be a tuple of the following form:"
                             "(width, height)")
        self.size = size

        self.cellparams = cellclass.default_parameters.copy()
        if cellparams != None:
            for key, value in cellparams.iteritems():
                if key in self.cellparams:
                    self.cellparams[key] = value
                else:
                    raise ValueError (" '%s' is not a valid parameter."
                                      " Valid parameters are %s." % 
                                      ( key, str(self.cellparams.keys()) ))

    def record(self, coordinates=None):
        """Record the spike times of neurons in this population.
        
        If *coordinates* is a tuple of the form ``(x, y)``, then this function
        will record the neuron at that position.  If *coordinates* is a list
        of tuples, this function will record all neurons in the list.  If no
        *coordinates* argument is passed, this function will record all neurons
        in the population.

        Each recorded neuron slows down the simulator, so only record as many
        neurons as is necessary.

        """
        if coordinates == None:
            for x in range(self.size [0]):
                for y in range(self.size[1]):
                    self.recorders.append(
                        _SpikeRecorder("Line", self, x, y, x, y))
        elif type(coordinates) == list:
            for coordinate in coordinates:
                (x, y) = coordinate
                if x < 0 or y < 0 or x >= self.size[0] or y >= self.size[1]:
                    raise ValueError("Attempted to record neuron that does not exist.")
                self.recorders.append(_SpikeRecorder("Line", self, x, y, x, y))
        else:
            (x, y) = coordinates
            if x < 0 or y < 0 or x >= self.size[0] or y >= self.size[1]:
                raise ValueError("Attempted to record neuron that does not exist.")
            self.recorders.append(_SpikeRecorder("Line", self, x, y, x, y))

    def record_line(self, x1, y1, x2, y2):
        """Record the spike times of a line of neurons in this population.

        The arguments are the end coordinates of the line.

        """

        if ((x1 < 0 or x2 < 0 or x1 >= self.size[0] or x2 >= self.size[0]) or
            (y1 < 0 or y2 < 0 or y1 >= self.size[1] or y2 >= self.size[1])):
                raise ValueError("Attempted to record neuron that does not exist.")

        self.recorders.append(_SpikeRecorder("Line", self, x1, y1, x2, y2))

    def record_square(self, x1, y1, x2, y2):
        """Record the spike times of a square of neurons in this population.

        The arguments are the corner coordinates of the square.

        """

        if ((x1 < 0 or x2 < 0 or x1 >= self.size[0] or x2 >= self.size[0]) or
            (y1 < 0 or y2 < 0 or y1 >= self.size[1] or y2 >= self.size[1])):
                raise ValueError("Attempted to record neuron that does not exist.")

        self.recorders.append(_SpikeRecorder("Square", self, x1, y1, x2, y2))

    def record_intracellular(self, coordinates=None):
        """Record the intracellular voltages and currents of neurons in this
        population.
        
        If *coordinates* is a tuple of the form ``(x, y)``, then this function
        will record the neuron at that position.  If *coordinates* is a list
        of tuples, this function will record all neurons in the list.  If no
        *coordinates* argument is passed, this function will record all neurons
        in the population.

        Each recorded neuron slows down the simulator, so only record as many
        neurons as is necessary.

        """
        if coordinates == None:
            for x in range(self.size [0]):
                for y in range(self.size[1]):
                    self.recorders.append(
                        _IntracellularRecorder(self, x, y))
        elif type(coordinates) == list:
            for coordinate in coordinates:
                (x, y) = coordinate
                if x < 0 or y < 0 or x >= self.size[0] or y >= self.size[1]:
                    raise ValueError("Attempted to record neuron that does not exist.")
                self.recorders.append(
                    _IntracellularRecorder(self, x, y))
        else:
            (x, y) = coordinates
            if x < 0 or y < 0 or x >= self.size[0] or y >= self.size[1]:
                raise ValueError("Attempted to record neuron that does not exist.")
            self.recorders.append(_IntracellularRecorder(self, x, y))
   
    def _write_xml(self, root):
        neuron_id = "neuron-" + self.label

        # Alter the NeuronDefinition section.

        neuron_definition_element = root.find("NeuronDefinition")
        neuron_element = etree.SubElement(neuron_definition_element, "Neuron")
    
        neuron_name_element = etree.SubElement(neuron_element, "Name")
        neuron_name_element.text = neuron_id

        neuron_model_element = etree.SubElement(neuron_element, "Model")
        neuron_model_element.text = self.cellclass._name
    
        for param, value in self.cellparams.iteritems():
            param_element = etree.SubElement (neuron_element, param)
            if type(value) is list:
                param_element.text = _vector_to_comma_string (value)
            else:
                param_element.text = str(value)

        # Alter the Layers section.

        layers_element = root.find ("Layers")
        layer_element = etree.SubElement(layers_element, "Layer")
        prefix_element = etree.SubElement(layer_element, "Prefix")
        prefix_element.text = self.label
        suffix_element = etree.SubElement(layer_element, "Suffix")
        suffix_element.text = self.label;

        xBase = float(root.findtext(
                          'OverallConfiguration/LayerSize/xEdgeLength'))
        yBase = float(root.findtext(
                          'OverallConfiguration/LayerSize/yEdgeLength'))

        if self.size[0] >= xBase:
            xMult = ceil(self.size[0]/ xBase)
        else:
            xMult = - ceil(xBase/self.size[0])

        if self.size [1] >= yBase:
            yMult = ceil (self.size [1] / yBase )
        else:
            yMult = -ceil ( yBase / self.size[1])

        mult_x_element = etree.SubElement(layer_element, 'MultiX')
        mult_x_element.text = str(int(xMult))

        mult_y_element = etree.SubElement (layer_element, 'MultiY')
        mult_y_element.text = str(int(yMult))
    
        neuron_type_element = etree.SubElement(layer_element, 'NeuronType')
        neuron_type_element.text = neuron_id

        # Alter the Recorder section.

        recorder_element = root.find("Recorder")
        for recorder in self.recorders:
            recorder._write_xml(recorder_element)

#=============================================================================#
# Recorder classes: for internal use only.
#=============================================================================#

class _SpikeRecorder(object):

    def __init__(self, shape, population, x1, y1, x2, y2):
        self.shape = shape
        self.label = population.label
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def _write_xml(self, recorder_element):
        single_unit_element = etree.SubElement(recorder_element,
                                                  "SingleUnit")

        neuron_element = etree.SubElement(single_unit_element, "Neuron")

        line_element = etree.SubElement(neuron_element, "Line", 
                                            pre=self.label, suf=self.label)
        line_element.text = _vector_to_comma_string(
                                       [self.x1, self.y1, self.x2, self.y2])        


class _IntracellularRecorder(object):

    def __init__(self, population, x, y):      
        self.label = population.label
        self.x = x
        self.y = y

        if population.cellclass == SIF:
            self.channels = [0, 1]
        elif population.cellclass == MiNi:
            self.channels = range(len(population.cellparams['SpikeDecays']) + 
                                  len(population.cellparams['SynapseDecays']))
        elif population.cellclass == VSICLIFV2:
            self.channels = [0, 1]
        else:
            raise ValueError("This cell class cannot be intracellularly recorded.")

    def _write_xml(self, recorder_element):
        intracellular_element = etree.SubElement(recorder_element,
                                                  "Intracellular")

        name_element = etree.SubElement(intracellular_element, "Name")
        name_element.text = '%s,%d,%d,%s' % (self.label, self.x, self.y, self.label)

        neuron_element = etree.SubElement(intracellular_element, "Neuron")

        line_element = etree.SubElement(neuron_element, "Line", 
                                            pre=self.label, suf=self.label)
        line_element.text = _vector_to_comma_string(
                                       [self.x, self.y, self.x, self.y]) 

        for channel in self.channels:
            channel_element = etree.SubElement(intracellular_element,
                                                   "Channel")
            channel_element.text = str(channel)    
        

#=============================================================================#
        

class Projection(object):
    """A set of synaptic connections between two populations."""

    def __init__(self, from_population, to_population, connector,
                 target='excitatory'):
        """

        *from_population* and *to_population* are the pre-synaptic and
        post-synaptic populations, respectively.

        *connector* is a Connector object (see :ref:`connectors`) that
        determines the connection scheme between the two populations.

        *target* specifies the target input synapse of the post-synaptic neurons.
        Its possible values are ``"excitatory"`` and ``"inhibitory"``.
        Excitatory synapses correspond to glutmate channels, and inhibitory
        synapses correspond to GABA channels.  If a projection is excitatory,
        all synaptic weights must be positive; if a projection is inhibitory,
        all synaptic weights must be negative.

        """

        if target not in _SYNAPSE_TYPE:
            raise ValueError ("Invalid target.  Target must be in %s." %
                              str(_SYNAPSE_TYPE.keys()))

        if connector.weight < 0 and target == 'excitatory':
            raise ValueError("Synapse to excitatory target must have positive weight.")
        if connector.weight > 0 and target == 'inhibitory':
            raise ValueError("Synapse to inhibitory target must have negative weight.")

        self.from_population = from_population
        self.to_population = to_population
        self.connector = connector
        self.target = target

    def _write_xml (self, root):
        connections_element = root.find("Connections")
         
        # This tag name is later replaced by the Connector in its _write_xml() function
        connection_element = etree.SubElement(connections_element, "Connection") 

        from_layer_element = etree.SubElement(connection_element, "FromLayer", 
                                             prefix=self.from_population.label,
                                             suffix=self.from_population.label)

        to_layer_element = etree.SubElement(connection_element, "ToLayer",
                                            prefix=self.to_population.label, 
                                            suffix=self.to_population.label)

        rotation_element = etree.SubElement(connection_element, "Rotation")
        rotation_element.text = '0'
          
        synapse_type_element = etree.SubElement(connection_element, "Type")
        synapse_type_element.text = _SYNAPSE_TYPE[self.target]

        self.connector._write_xml(root, connection_element)


class Connector(object):

    def __init__(self, weight, delay):
        if delay == 0:
            raise ValueError("Connection delay cannot be zero.")

        self.weight = weight
        self.delay = delay

    def _write_xml(self, root, connection_element): 
        strength_element = etree.SubElement(connection_element, "Strength")
        strength_element.text = str(self.weight)

        delay_element = etree.SubElement(connection_element, "Delay")
        delay_element.text = str(self.delay)

        offset_element = etree.SubElement(connection_element, "Offset")
        offset_element.text = '0'

        multiplier_element = etree.SubElement(connection_element, "Multiplier")
        multiplier_element.text = '1'
         

class PointsConnector(Connector):
    """Connects two populations according to an adjacency matrix."""

    def __init__ (self, adjacency_matrix, weight=1e-8, delay=2e-3):
        """

        *adjacency_matrix* is a NumPy array which, when multiplied by
        *weight*, specifies the weight of each synapse in amps as an
        adjacency matrix.

        The rows of the adjacency matrix correspond to neurons in the
        pre-synaptic population; the columns correspond to those in the
        post-synaptic population.  In assigning a linear index to every neuron
        in a population, the y values are indexed first, then the x values.

        For example, if the size of the pre-synaptic population is a x b and 
        the size of the post-synaptic population is m x n, then the adjacency
        matrix must be of size (a * b) x (m * n). 

        For example, if we have two populations, both of size 2 x 2, then
        setting the Connector of the Projection between them to::

          PointsConnector(np.array([[ 1 , 0 , 0 , 1 ],
                                    [ 0 , 0 , 0 , 0 ],
                                    [ 0 , 0 , 0 , 0 ],
                                    [ 0 , 1 , 1 , 0 ]], weight=1e-8 ))

        will establish the following connections, each with weight ``1e-8``.

        +---------+--------+
        | From    | To     |
        +=========+========+
        | (0, 0)  | (0, 0) |
        +---------+--------+                    
        | (0, 0)  | (1, 1) |
        +---------+--------+
        | (1, 1)  | (0, 1) |
        +---------+--------+
        | (1, 1)  | (1, 0) |
        +---------+--------+

        If the adjacency matrix has fewer elements, in either dimension, than
        the corresponding population, it will be tiled to match.  This means
        that an adjacency matrix of ``array([1])`` is an all-to-all connector.

        *delay* is the time interval, in seconds, between when a pre-synaptic
        neuron generates a spike and when a post-synaptic neuron receives it.
        It cannot be set to ``0``.

        """

        Connector.__init__(self, weight, delay)
        self.adjacency_matrix = adjacency_matrix

    def _write_xml (self, root, connection_element):
        Connector._write_xml(self, root, connection_element)

        connection_element.tag = "GConnection"

        connect_matrix_id = _add_matrix (root, self.adjacency_matrix)

        connect_matrix_element = etree.SubElement(connection_element, "Matrix")
        connect_matrix_ref_element = etree.SubElement(connect_matrix_element, 
                                                      "Ref")
        connect_matrix_ref_element.text = connect_matrix_id


class PatternConnector(Connector):
    """Connects two populations according to a repeated connection matrix."""

    def __init__ (self, connect_matrix, connect_style='convergent',
                  periodic=True, weight=1e-8, delay=2e-3):
        """

        *connect_matrix* is a NumPy array which, when multiplied by
        *weight*, is repeated over one population like a 2D convolution
        kernel to specify the weight of each synapse in amps.

        If *connect_style* is ``"convergent"``, then one-to-many connections
        will be made from the pre-synaptic to the post-synaptic population; if
        *connect_style* is ``"divergent"``, then many-to-one connections will
        be made.

        *periodic* sets whether periodic boundary conditions are used.

        For example, setting the Connector of a Projection to::

          PatternConnector(np.array([ 1, .5 ], weight=1e-8, \
connect_style='convergent', periodic=True))

        will connect each neuron in the pre-synaptic population to two neurons
        in the post-synaptic population -- one directly across from it and one
        directly across and to the right -- with weights of ``1e-8`` and
        ``.5e-8``, respectively.  Because periodic boundary conditions are on,
        neurons at the right edge of the pre-synaptic population will be
        connected to neurons at the left edge of the post-synaptic population.

        *delay* is the time interval, in seconds, between when a pre-synaptic
        neuron generates a spike and when a post-synaptic neuron receives it.
        It cannot be set to ``0``.

        """

        Connector.__init__(self, weight, delay)
        self.connect_matrix = connect_matrix
        self.connect_style = connect_style
        self.periodic = periodic

    def _write_xml (self, root, connection_element):
        Connector._write_xml(self, root, connection_element)

        connection_element.tag = "LConnection"

        connect_matrix_id = _add_matrix(root, self.connect_matrix)

        connect_matrix_element = etree.SubElement(connection_element, "Matrix")
        connect_matrix_ref_element = etree.SubElement(connect_matrix_element, 
                                                      "Ref")
        connect_matrix_ref_element.text = connect_matrix_id

        style_element = etree.SubElement(connection_element, "Style")
        style_element.text = _CONNECT_STYLE[self.connect_style]

        connection_element.set("periodic", str(self.periodic).lower())


class ProbabilityPointsConnector(Connector):
    """Connects two populations probabilistically
    according to an adjacency matrix."""

    def __init__ (self, adjacency_matrix, probability_matrix,
                  weight = 1e-8, delay = 2e-3):
        """

        *connect_matrix* is a NumPy array which, when multiplied by
        *weight*, specifies the weight of each synapse in amps
        (see :class:`create.PointsConnector`).

        *probability_matrix* is a NumPy array of the same size as
        *connect_matrix* which specifies the probability (from 0 to 1) that
        each connection will be made.

        *delay* is the time interval, in seconds, between when a pre-synaptic
        neuron generates a spike and when a post-synaptic neuron receives it.
        It cannot be set to ``0``.
        
        """

        Connector.__init__(self, weight, delay)
        self.adjacency_matrix = adjacency_matrix
        self.probability_matrix = probability_matrix

    def _write_xml (self, root, connection_element):
        Connector._write_xml(self, root, connection_element)

        connection_element.tag = "GPConnection"

        connect_matrix_id = _add_matrix (root, self.adjacency_matrix)

        connect_matrix_element = etree.SubElement(connection_element, 
                                                  "SMatrix")
        connect_matrix_ref_element = etree.SubElement(connect_matrix_element,
                                                      "Ref")
        connect_matrix_ref_element.text = connect_matrix_id

        probability_matrix_id = _add_matrix(root, self.probability_matrix)

        probability_matrix_element = etree.SubElement(connection_element,
                                                      "PMatrix")
        probability_matrix_ref_element = etree.SubElement (
                                             probability_matrix_element, "Ref")
        probability_matrix_ref_element.text = probability_matrix_id


class ProbabilityPatternConnector(Connector):
    """Connects two populations probabilistically according to a repeated
    connection matrix."""

    def __init__ (self, connect_matrix, probability_matrix, 
                  connect_style='convergent', periodic = True, weight = 1e-8,
                  delay = 2e-3):
        """

        *connect_matrix* is a NumPy array which, when multiplied by
        *weight*, is repeated over one population like a 2D convolution
        kernel to specify the weight of each synapse in amps
        (see :class:`create.PatternConnector`).

        *probability_matrix* is a NumPy array of the same size as
        *connect_matrix* which specifies the probability (from 0 to 1) that
        each connection will be made.

        If *connect_style* is ``"convergent"``, then one-to-many connections
        will be made from the pre-synaptic to the post-synaptic population; if
        *connect_style* is ``"divergent"``, then many-to-one connections will
        be made.

        *periodic* sets whether periodic boundary conditions are used.

        *delay* is the time interval, in seconds, between when a pre-synaptic
        neuron generates a spike and when a post-synaptic neuron receives it.
        It cannot be set to ``0``.
        
        """

        Connector.__init__(self, weight, delay)
        self.connect_matrix = connect_matrix
        self.probability_matrix = probability_matrix
        self.connect_style = 'convergent'
        self.periodic = periodic

    def _write_xml (self, root, connection_element):
        Connector._write_xml(self, root, connection_element)

        connection_element.tag = "PConnection"

        connect_matrix_id = _add_matrix(root, self.connect_matrix)

        connect_matrix_element = etree.SubElement(connection_element, 
                                                  "SMatrix")
        connect_matrix_ref_element = etree.SubElement(connect_matrix_element,
                                                      "Ref")
        connect_matrix_ref_element.text = connect_matrix_id

        probability_matrix_id = _add_matrix(root, self.probability_matrix)

        probability_matrix_element = etree.SubElement(connection_element, 
                                                      "PMatrix")
        probability_matrix_ref_element = etree.SubElement(
                                             probability_matrix_element, "Ref")
        probability_matrix_ref_element.text = probability_matrix_id

        style_element = etree.SubElement(connection_element, "Style")
        style_element.text = _CONNECT_STYLE[self.connect_style]

        connection_element.set ("periodic", str(self.periodic).lower())


class OneToOneConnector (PatternConnector):
    """Connects each neuron in the pre-synaptic population to one neuron in the
    post-synaptic population."""

    def __init__ (self, weight=1e-8, delay=2e-3):
        """
        *weight* is the weight of every synapse, in amps.

        *delay* is the time interval, in seconds, between when a pre-synaptic
        neuron generates a spike and when a post-synaptic neuron receives it.
        It cannot be set to ``0``.

        Note that a OneToOneConnector is equivalent to a PatternConnector with
        a connection matrix of ``array([1]).``

        """

        PatternConnector.__init__(self, connect_matrix=np.array([1]),
                                weight=weight, delay=delay)

class AllToAllConnector(PointsConnector):
    """Connects each neuron in the pre-synaptic population to each neuron in
       the post-synaptic population."""

    def __init__(self, weight=1e-8, delay=2e-3):
        """
        *weight* is the weight of every synapse, in amps.

        *delay* is the time interval, in seconds, between when a pre-synaptic
        neuron generates a spike and when a post-synaptic neuron receives it.
        It cannot be set to ``0``.

        Note that an AllToAllConnector is equivalent to a PointsConnector with
        a connection matrix of ``array([1]).``

        """

        PointsConnector.__init__(self, connect_matrix=np.array([1]),
                                 weight=weight, delay=delay)


class Experiment(object):
    """An electrophysiological experiment, which is divided into a set of 
    Subexperiments."""

    def __init__(self, subexperiments=[], background_strength=0,
                 background_frequency=0, background_target='excitatory'):

        """

        *subexperiments* is a list of :class:`create.Subexperiment` objects.

        *background_strength*, *background_frequency*, and
        *background_target* are parameters for an optional input that
        stimulates every neuron in every population.

        """

        self.subexperiments = subexperiments
        self.background_strength = background_strength
        self.background_frequency = background_frequency
        self.background_target = background_target

    def add(self, subexperiment):
        """Add a Subexperiment."""

        self.subexperiments.append(subexperiment)
    
    def _write_xml(self, root):
        overall_configuration_element = root.find('OverallConfiguration')

        background_strength_element = etree.SubElement(
                                          overall_configuration_element,
                                          'BackgroundStrength')
        background_strength_element.text = str(self.background_strength)
        
        background_synapse_type_element = etree.SubElement(
                                              overall_configuration_element, 
                                              'BackgroundType')
        background_synapse_type_element.text = _SYNAPSE_TYPE [
                                                   self.background_target]

        background_frequency_element = etree.SubElement(
                                           overall_configuration_element, 
                                           'BackgroundFrequency')
        background_frequency_element.text = str(self.background_frequency)

        for subexperiment in self.subexperiments:
            if len(subexperiment.inputs) == 0:
                raise ValueError("Subexperiment must have at least one Input.")
            subexperiment._write_xml(root)

class Subexperiment(object):
    """Encapsulates a series of Inputs to be presented to the network."""
    
    def __init__ (self, label, length, num_repetitions, inputs=[]):
        """

        *label* is a string that uniquely identifies this Subexperiment for the
        purposes of recording.

        *length* is the duration, in seconds, of this Subexperiment.

        *num_repetitions* is the number of trials for which this Subexperiment
        is repeated.

        *inputs* is a list of :class:`create.Input` objects to be presented to
        the network.  Inputs can also be added using the methods below.


        For example::

            subexp = Subexperiment('Training', 10, 5)

        will create a Subexperiment ``subexp`` with the label ``"Training"``
        that lasts 10 seconds and will be repeated 5 times.  It does not yet
        have any inputs.

        """
    
        self.label = label
        self.length = length
        self.num_repetitions = num_repetitions
        self.inputs = inputs

    def add (self, input):
        """Adds an Input to this Subexperiment."""

        if input.off_time > self.length:
            raise ValueError(
                "Input time is out of range of subexperiment duration.")

        self.inputs.append (input)

    def _write_xml (self, root):
        experiment_element = root.find("Experiment")
        subexperiment_element = etree.SubElement(experiment_element, 
                                                 "SubExperiment",
                                                 name=str(self.label),
                                                 length=str(self.length),
                                                 repetition=
                                                     str(self.num_repetitions))
        
        for input in self.inputs:
            input._write_xml(root, subexperiment_element)

class Input(object):

    """An Input regularly injects a group of neurons with stimulatory 
    currents."""
    
    def __init__(self, population, on_time, off_time, stimulus, weight=0,
                  frequency=0, decay=0, target='excitatory', channel=-1):
        """

        *population* is the target Population.
 
        *on_time* and *off_time* are the starting and ending times.

        *stimulus* is a Stimulus (see :ref:`stimuli`) that determines which
        neurons in the Population receive input.

        *weight* is the amount of current, in amps, that is injected into the
        neurons each cycle.
        
        *frequency* is the frequency of this Input, in cycles per second.

        *decay* is the inverse time constant of the decay of the weight.  A
        *decay* of ``0`` mans that the weight of this Input does not decay.

        *target* specifies the target input synapse of the targeted neurons.
        Its possible values are ``"excitatory"`` and ``"inhibitory"``.  Excitatory 
        synapses correspond to glutmate channels, and inhibitory synapses
        correspond to GABA channels.  If *target* is ``"excitatory"``, *weight*
        must be positive; if *target* is ``"inhibitory"``, *weight* must be
        negative.

        *channel* can be used to facilitate the generation of
        orientation-selective responses.  Setting it to ``-1``  means that it
        will be ignored.

        For example, adding the following Input to a Subexperiment::

            Input(input_layer, 1, 3, BackgroundStimulus(), weight=1e-8, \
frequency=40, decay=0)

        will stimulate all neurons in the Population *input_layer* from time =
        1 second to time = 3 seconds with 1e-8 amps of current every 1/40
        seconds.

        """

        self.population = population
        self.on_time = on_time
        self.off_time = off_time
        self.stimulus = stimulus
        self.weight = weight
        self.frequency = frequency
        self.decay = decay
        self.target = target
        self.channel = -1

    def _write_xml(self, root, subexperiment_element):
        input_element = etree.SubElement (subexperiment_element, "Input", 
                                          on=str(self.on_time), 
                                          off=str(self.off_time))
        
        prefix_element = etree.SubElement(input_element, "Prefix")
        prefix_element.text = self.population.label

        suffix_element = etree.SubElement(input_element, "Suffix")
        suffix_element.text = self.population.label

        weight_element = etree.SubElement(input_element, "Strength")
        weight_element.text = str(self.weight)

        frequency_element = etree.SubElement(input_element, "Frequency")
        frequency_element.text = str(self.frequency)

        decay_element = etree.SubElement(input_element, "Decay")
        decay_element.text = str(self.decay)

        synapse_type_element = etree.SubElement(input_element, "Type")
        synapse_type_element.text = _SYNAPSE_TYPE[self.target]

        channel_element = etree.SubElement(input_element, "Channel")
        channel_element.text = str(self.channel)

        stimulus_element = etree.SubElement(input_element, "Stimulus")
        self.stimulus._write_xml(root, stimulus_element)


class Stimulus(object):
    
    def _write_xml(self, root, stimulus_element):
        pass


class BackgroundStimulus(Stimulus):
    """Stimulates every neuron in the population."""
    
    def __init__(self):
        pass

    def _write_xml(self, root, stimulus_element):
        etree.SubElement(stimulus_element, "Background")


class PointStimulus(Stimulus):
    """Stimulates a single neuron."""

    def __init__(self, x, y):
        """

        *x* and *y* are the x- and y-coordinates of the neuron.

        """

        return LineStimulus(x, y, x, y)


class LineStimulus(Stimulus):
    """Stimulates a line of neurons."""

    def __init__(self, x1, y1, x2, y2):
        """

        (*x1*, *y1*) and (*x2*, *y2*) are the end points of the line.

        For example::

            LineStimulus(1,1,3,1)

       will stimulate three neurons in the target population: (1,1), (1,2),
       and (1,3).

        """

        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def _write_xml(self, root, stimulus_element):
        line_element = etree.SubElement(stimulus_element, "Line")
        line_element.text = _vector_to_comma_string([self.x1, self.y1, self.x2, self.y2])


class SquareStimulus(Stimulus):
    """Stimulates a hollow square of neurons."""

    def __init__(self, x1, y1, x2, y2):
        """

        (*x1*, *y1*) and (*x2*, *y2*) are the corner points of the square.        

        """

        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def _write_xml (self, root, stimulus_element):
        square_element = etree.SubElement(stimulus_element, "Square")
        square_element.text = _vector_to_comma_string([self.x1, self.y1, self.x2, self.y2])


class FilledSquareStimulus(Stimulus):
    """Stimulates a filled square of neurons."""

    def __init__(self, x1, y1, x2, y2):
        """

        (*x1*, *y1*) and (*x2*, *y2*) are the corner points of the square.        

        """

        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def _write_xml(self, root, stimulus_element):
        filled_square_element = etree.SubElement(stimulus_element, "FilledSquare")
        filled_square_element.text = _vector_to_comma_string([self.x1, self.y1, self.x2, self.y2])


class ImageStimulus(Stimulus):
    """Stimulates the neurons in the population according to the intensity
    values of an image.

    Note: this function is known to have problems.  You may be better off using
    MatrixStimulus and manually populating the matrix with intensity values
    from an image.

    """

    def __init__(self, filename, filter, reverse=False, gamma=-1.0, threshold=.02):
        """

        *filename* is the path to the image file, relative to the `imagelib`
        folder of your ERNST installation.  JPEG, BMP, and GIF formats
        are supported.

        *filter* is a matrix through which the image is filtered.

        If *reverse* is ``True``, the image will be reversed.

        """

        self.filename = filename
        self.filter = filter
        self.reverse = reverse
        self.gamma = gamma
        self.threshold = threshold

    def _write_xml(self, root, stimulus_element):
        image_element = etree.SubElement(stimulus_element, "Image",
                                         reverse=str(self.reverse).lower(), 
                                         gamma=str(self.gamma),
                                         threshold=str(self.threshold))
        image_element.text = self.filename

        matrix_id = _add_matrix(root, self.filter)
        
        filter_element = etree.SubElement(stimulus_element, "Filter")
        matrix_element = etree.SubElement(filter_element, "Matrix")
        matrix_ref_element = etree.SubElement(matrix_element, "Ref")
        matrix_ref_element.text = matrix_id


class MatrixStimulus(Stimulus):
    """Stimulates the neurons in the population according to a matrix."""

    def __init__(self, matrix, center_coordinate):
        """

        *matrix* is a two-dimensional NumPy array that specifies which neurons
        in the population will receive input.

        *center_coordinate* is the location in the population to which the
        center of the matrix should correspond.

        """

        self.center_x, self.center_y = center_coordinate
        self.matrix = matrix

    def _write_xml (self, root, stimulus_element):
        matrix = self.matrix.T

        matrix_element = etree.SubElement(stimulus_element, "PlotMatrix")

        matrix_element.set('centerX', str(self.center_x))
        matrix_element.set('centerY', str(self.center_y))

        matrix_id = _add_matrix(root, matrix)
        
        matrix_ref_element = etree.SubElement(matrix_element, "Ref")
        matrix_ref_element.text = matrix_id

