from distutils.core import setup

setup(name='ernst',
      version='1.4',
      author='Jeremy Cohen',
      url='ernst.mb.jhu.edu',
      packages=['ernst'],
      requires=['numpy', 'scipy', 'matplotlib', 'lxml'],
      description="""
      Python interface for the Event-Related Neuronal Simulation Tool
      """
      )
