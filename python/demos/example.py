"""
This script provides examples of common tasks in ERNST Python.
"""


from ernst import *

#=============================================================================#
# Define the Simulation                                                       #
#=============================================================================#

# Define the Network:

network = Network()

# Add a population labeled "Input" that consists of 10 x 10 Mihalas-Niebur
# neurons with default values for cell parameters:
pop1 = Population("Input", (10, 10), MiNi)
network.add(pop1)

# Add a population labeled "Input" that consists of 10 x 10 simple
# integrate-and-fire neurons with default values for cell parameters:
# pop1 = Population("Input", (10, 10), SIF)
# network.add(pop1)

# Record the neuron in pop1 at (5, 5) intracellularly:
pop1.record_intracellular((5, 5))

# Record the neuron in pop1 at (5, 5) for spikes:
pop1.record((5, 5))

# Add a population labeled "Output" that consists of 10 x 10 Mihalas-Niebur
# neurons with threshold adaptation and large spike-induced current decay time
# constants:
pop2 = Population("Output", (10, 10), MiNi, cellparams={'ThresholdAdaptation':10,
                                                        'SpikeDecays':[.05, .0025]})
network.add(pop2)

# Record all neurons in pop2:
pop2.record()

# Connect the populations according to a convolved connection matrix:
connect_matrix = numpy.array([[.25, .5, .25],
                              [.5,   1, .5 ],
                              [.25, .5, .25]])
connector = PatternConnector(connect_matrix, weight=1e-9, delay=1e-4, periodic=True)
projection = Projection(pop1, pop2, connector)
network.add(projection)

# Connect the populations according to an inhibitory one-to-one connection scheme:
# projection = Projection(pop1, pop2, OneToOneConnector(weight=-1e-9, delay=1e-4), target='inhibitory')
# network.add(projection)

# Connect the populations according to an all-to-all connection scheme:
# projection = Projection(pop1, pop2, AllToAllConnector(weight=1e-9, delay=1e-4))
# network.add(projection)

# Define the Experiment:

experiment = Experiment()

# Create a subexperiment labeled "Training" that lasts for 10 seconds and is
# repeated 3 times.
subexperiment = Subexperiment("Training", 10, 1)

# Add an input that runs from time = 2 to time = 5 seconds and stimulates all
# all neurons in pop1:
background_input = Input(pop1, 2, 5, BackgroundStimulus(), weight=1e-9, frequency=2)
subexperiment.add(background_input)

# Add an input that runs from time = 5 to time = 8 seconds and stimulates a
# filled square of neurons in pop1 with decaying frequency:
square_input = Input(pop1, 5, 8, FilledSquareStimulus(3, 3, 7, 7), weight=1e-9, 
                     frequency=3, decay=2)
subexperiment.add(square_input)

# Add an input that runs from time = 5 to time = 8 and stimulates a line
# of neurons in pop1 from (4, 4) to (4, 7):
# line_input = Input(pop1, 5, 8, LineStimulus(4, 4, 4, 7), weight=-1e-9, frequency=3)
# subexperiment.add(line_input)

experiment.add(subexperiment)

# Build the Simulation:
simulation = Simulation(network, experiment)

#=============================================================================#
# Run the Simulation                                                          #
#=============================================================================#

setup()
results = run(simulation, seed=-5)
end()

#=============================================================================#
# Analyze the Results                                                         #
#=============================================================================#

# Save the results to a MATLAB file:
# savemat(results, 'example.mat')

# Print spike data to a file:
# print_spike_data('example_spikes.txt', results, "Training", "Input", (5, 5))

# Make a single unit plot:
single_unit_plot = SingleUnitPlot(results, "Training", "Input", (5, 5))

# Save the single unit plot:
# single_unit_plot.save('example_singleunit.png')

# Show the single unit plot:
single_unit_plot.show()

# Make field plots:
field_plot1 = FieldPlot(results, "Training", "Output", begin=2, end=5)
field_plot2 = FieldPlot(results, "Training", "Output", begin=5, end=8)

# Make field plots in frames:
# field_plot1 = FieldPlot(results, "Training", "Output", begin=2, end=5, step=.2)
# field_plot2 = FieldPlot(results, "Training", "Output", begin=5, end=8, step=.2)

# Save the field plots to image files:
# field_plot1.save('example_field1.png')
# field_plot2.save('example_field2.png')

# Show the field plots one at a time:
field_plot1.show()
field_plot2.show()

# Show the field plots all at once:
# field_plot1.show(block=False)
# field_plot2.show(block=False)
# show()

# Make an intracellular plot:
intra_plot = IntracellularPlot(results, "Training", 0, "Input", (5,5))

# Save the intracellular plot to an image file:
# intra_plot.save('example_intracellular.png')

# Show the intracellular plot:
intra_plot.show()
