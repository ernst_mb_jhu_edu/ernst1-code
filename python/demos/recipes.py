"""
This script creates MiNi neurons of several different behaviour patterns and
saves to the disk images of their intracellular plots.
"""

from ernst import *
import numpy

# Creating the simulation

models  = {
  'tonic-spiking' : {
                 'ThresholdAdaptation':0, 
                 'ExternalCurrent' : 1.5 * 3e-11,
                },
  'spike-frequency-adaptation' : {
                 'ThresholdAdaptation':5, 
                 'ExternalCurrent' : 2 * 3e-11,
                },
  'hyperpolarizing-spiking' : {
                 'ThresholdAdaptation':30,
                 'SpikeCurrentsAddition':[0 * 3e-11, 0 * 3e-11],
                 'SpikeCurrentsRatio':[0, 1],
                 'SpikeDecays':[.005, .05],
                 'ExternalCurrent':-1 * 3e-11
                 },
  'tonic-bursting' : {
                 'ThresholdAdaptation':5,
                 'SpikeCurrentsAddition':[10 * 3e-11, -0.6 * 3e-11],
                 'SpikeCurrentsRatio':[0, 1],
                 'SpikeDecays':[.005, .05],
                 'ExternalCurrent':2 * 3e-11
                 },
  'mixed-mode' : {
                 'ThresholdAdaptation':5,
                 'SpikeCurrentsAddition':[5 * 3e-11, -0.3 * 3e-11],
                 'SpikeCurrentsRatio':[0, 1],
                 'SpikeDecays':[.005, .05],
                 'ExternalCurrent':2 * 3e-11
                 },
         }

network = Network()

for model_name, model_params in models.items():
    pop = Population(model_name, (1, 1), MiNi, cellparams=model_params)
    pop.record_intracellular((0,0))
    network.add(pop)

subexp1 = Subexperiment("SubExp1", .5, 1)

experiment = Experiment()
experiment.add(subexp1)

simulation = Simulation(network, experiment)

# Running the simulation

setup()
results = run(simulation)
end()

# Plotting the results
for model_name in models:
    IntracellularPlot(results, "SubExp1", 0, model_name, (0, 0)).save(model_name + '.png')
