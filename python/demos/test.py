"""
Run this script to make sure that your installation is working.
You'll know if it is.
"""

from ernst import *
import numpy as np

# Creating the simulation

network = Network()

population = Population("Welcome", (23, 7), MiNi)
population.record()

network.add(population)

ernst = np.array([
  [ 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ],
  [ 0 , 1 , 1 , 1 , 0 , 1 , 1 , 1 , 0 , 1 , 0 , 0 , 0 , 1 , 0 , 1 , 1 , 1 , 0 , 1 , 1 , 1 , 0 ],
  [ 0 , 1 , 0 , 0 , 0 , 1 , 0 , 1 , 0 , 1 , 1 , 0 , 0 , 1 , 0 , 1 , 0 , 0 , 0 , 0 , 1 , 0 , 0 ],
  [ 0 , 1 , 1 , 1 , 0 , 1 , 1 , 0 , 0 , 1 , 0 , 1 , 0 , 1 , 0 , 1 , 1 , 1 , 0 , 0 , 1 , 0 , 0 ],
  [ 0 , 1 , 0 , 0 , 0 , 1 , 0 , 1 , 0 , 1 , 0 , 0 , 1 , 1 , 0 , 0 , 0 , 1 , 0 , 0 , 1 , 0 , 0 ],
  [ 0 , 1 , 1 , 1 , 0 , 1 , 0 , 1 , 0 , 1 , 0 , 0 , 0 , 1 , 0 , 1 , 1 , 1 , 0 , 0 , 1 , 0 , 0 ],
  [ 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ],
])

input = Input(population, 0, 1, MatrixStimulus(ernst, (11, 3)), weight=1e-8, frequency=50)

subexp = Subexperiment("SubExp", 1, 1)
subexp.add(input)

experiment = Experiment()
experiment.add(subexp)

simulation = Simulation(network, experiment)

# Running the simulation

setup()
results = run(simulation)
end()

# Plotting the results

FieldPlot(results, "SubExp", "Welcome", begin=0, end=1).show()
