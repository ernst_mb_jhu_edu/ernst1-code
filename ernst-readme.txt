Purpose ..........:  ERNST readme.txt
Version.Date .....:  $Date: 2012-03-03 16:05:09 -0600 (Sat, 03 Mar 2012) $
Version.Rev ......:  $Rev: 380 $
Version.Author ...:  $Author: croft $
Author ...........:  Yi Dong
Author ...........:  David Wallace Croft, M.Sc.

Additional documentation is in subdirectory doc/.

Requirements for running the simulator: Linux/MacOS/Windows XP
operation system, JAVA JRE version 6.

If you are using Windows XP , run ernst.bat, otherwise run ernst.sh
(that is, the command is "./ernst.sh")

   There are 3 tabs in the ERNST simulator. The first one is for the
   Java Parallel Virtual Machine (JPVM) panel, the second one is the
   simulator control panel, and the last one is for the visualization
   of the simulation result.

Click on the JPVM button (ignore the warning about the daemon). You
can then choose the port that the JVPM daemon will listen to (default
is 10000, or you can use a random port). It is safe to stay with the
default. 

Click on 'Start'. You will notice a 'Stop' button appearing, this is
to stop the daemon. If you are running on just one computer, you will
see the name of this machine listed below (see below for using
multiple machines). 

To start the simulator, click on the 'Ernst' button at the top. You
will see some information about the memory (heap) size which is
associated
with the simulator. For very large networks, you can increase them. If
you are just playing, leave it as it is. 'Seed' is obviously the
random seed. 

(there is a button called 'Avalanche' which has been used in the Niebur
lab for a specific project. Just ignore it; we will either remove it
for a 'nicer' version of the simulator or document it properly)

You now need to select one 'Model' (i.e. network) to simulate. A model
is an XML file in which you described your network in a format
described elsewhere. All such files need to be in the
subdirectory 'model'. The simulator comes with one example called
test001.xml. 

You can create the XML file describing your model simply in a text
editor or you can go to http://ernst.mb.jhu.edu/ where you will find
an online XML file generator.  Note that format verification is done
when the simulator is started, i.e. it will not execute if the XML
file is syntactically incorrect.

When you select a Model, the 'Start' and 'Stop' buttons appears, as
well as the 'Show model' button. The latter displays the model file
and also allows you to modify it. When you click on 'Start', the
simulation is started and information about it is displayed in the
window below the buttons. Obviously, when you click on 'Stop' it will
kill the simulation. 


   When closing the simulator window, the jpvm daemon is running in
   the background. To stop the daemon, run ernst again and click on
   the "Stop" button in the jpvm tab.


   The XML file should be put in a subdirectory called "model", the
   simulation result is stored in the  "results" subdirectory in
   network
   Common Data Format (netcdf
   http://www.unidata.ucar.edu/software/netcdf/). Some
   example files are provided, e.g. the file test001.xml. 

   The input image are put in the "imagelib" directory.

   The "bin" subdirectory contains a few scripts to help to kill the
   zombie java processes, which happens when program is terminated
   unexpected.

   This note is only of interest if you run a large simulation in
   which you use several machines simultaneously; ignore it if you run
   it on only one machine. JPVM can combine different machines into
   one "super" virtual machine.
   The simulator is running on top of java parallel virtual machine,
   so you need to
   start JPVM before you can run any simulation. To setup the JPVM,
   you need to:

           a. Start the daemon in each of the machines you want to
           include
              for the simulation by starting the simulator on each of
              the machines and then clicking the "start" button in the
           Jpvm tab.
           b. Decide which machine you would like to use as the
              control machine. On this machine,
              choose "Add a host" in Jpvm tab and fill in the host
           name and port
              number. Repeat this  till you have added all
           hosts. Note, you can only
              fill in the host name but not  not the  IP address. So
           you
           need to edit
              /etc/hosts(Linux) or /private/etc/hosts(Mac) or
              %SystemRoot%\system32\drivers\etc\ (Windows) to
              make sure the machines can ping each other by host name.
              Another alternative to adding hosts, you can edit a host
           file "myhosts".
              Each machine occupies one line,  write host name first
              then port number, separated by space.
              Then instead of "Add a host", you can use "Add hosts
              from file" command.
   
The following note is only for non-gui fans:
	To start the jpvm daemon:
		bin/jpvmd   (Linux)
           or   bin/jpvmd.bat (Windows)
	To run control the jpvm:
		bin/jpvm    (Linux)
           or   bin/jpvm.bat (Windows)
		in the jpvm environment, type 'help' to get the list of commands.
	To run the simulator without gui
		java -cp simulator.jar starter modelfile.xml [seed] [heap size]
        for example 
	  	java -cp simulator.jar starter test001.xml -3 512
        will run the model test001.xml with seed number -3 and heapsize
        512mb for the netHost.
	if seed number and heap size are ignored, the default will be used.