# purpose ...:  installs libraries into Maven local repository
# since .....:  2012-05-05
# author ....:  Jeremy Cohen, David Wallace Croft
#
# I use version zero (0) where I do not know what the real version is.

mvn install:install-file -Dfile=lib/freehep-graphics2d-2.1.1.jar -DgroupId=org.freehep -DartifactId=freehep-graphics2d -Dversion=2.1.1 -Dpackaging=jar

mvn install:install-file -Dfile=lib/freehep-graphicsio-2.1.1.jar -DgroupId=org.freehep -DartifactId=freehep-graphicsio -Dversion=2.1.1 -Dpackaging=jar

mvn install:install-file -Dfile=lib/freehep-graphicsio-ps-2.1.1.jar -DgroupId=org.freehep -DartifactId=freehep-graphicsio-ps -Dversion=2.1.1 -Dpackaging=jar

mvn install:install-file -Dfile=lib/Jaxe.jar -DgroupId=net.sourceforge.jaxe -DartifactId=jaxe -Dversion=0 -Dpackaging=jar

mvn install:install-file -Dfile=lib/netcdf-2.2.22.jar -DgroupId=edu.ucar -DartifactId=netcdf -Dversion=2.2.22 -Dpackaging=jar

mvn install:install-file -Dfile=lib/thinlet.jar -DgroupId=net.sourceforge.thinlet -DartifactId=thinlet -Dversion=0 -Dpackaging=jar

