#!/bin/bash
#
# author ...:  Yi Dong
# author ...:  David Wallace Croft
# author ...:  Jeremy Cohen

mkdir log

mkdir results

mkdir pics

export CLASSPATH=.:$PWD/simulator.jar

java -jar simulator.jar

