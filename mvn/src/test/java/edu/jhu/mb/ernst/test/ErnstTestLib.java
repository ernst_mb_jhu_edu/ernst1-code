    package edu.jhu.mb.ernst.test;
    
    import java.io.*;
    import java.util.*;
    import org.w3c.dom.*;
    import org.w3c.dom.bootstrap.*;
    import org.w3c.dom.ls.*;

    import jpvm.jpvmEnvironment;
    import jpvm.jpvmTaskId;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import cnslab.cnsmath.Seed;
    import cnslab.cnsnetwork.Axon;
    import cnslab.cnsnetwork.BKPoissonNeuron;
    import cnslab.cnsnetwork.BKPoissonNeuronPara;
    import cnslab.cnsnetwork.MiNiNeuronPara;
    import cnslab.cnsnetwork.Network;
    import cnslab.cnsnetwork.Neuron;
    import cnslab.cnsnetwork.SimulatorParser;
    import cnslab.cnsnetwork.VSICLIFNeuronPara;

    import edu.jhu.mb.ernst.net.PeerInfo;

    /***********************************************************************
    * Library of static methods to support unit testing of ERNST.
    *
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @since
    *   2011-07-23
    * @author
    *   David Wallace Croft, M.Sc.
    * @author
    *   Jeremy Cohen
    ***********************************************************************/
    public final class  ErnstTestLib
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    private static final Class<ErnstTestLib>
      CLASS = ErnstTestLib.class;
      
    private static final ClassLoader
      CLASS_LOADER = CLASS.getClassLoader ( );
    
//    private static final String
//      CLASS_NAME = CLASS.getName ( );
    
    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public static DOMImplementationLS  createDomImplementationLs ( )
      throws ClassCastException, ClassNotFoundException,
        InstantiationException, IllegalAccessException
    ////////////////////////////////////////////////////////////////////////
    {
      // http://www.informit.com/guides/content.aspx?g=xml&seqNum=28
      
      final DOMImplementationRegistry
        registry = DOMImplementationRegistry.newInstance ( );
      
      final DOMImplementation
        domImplementation = registry.getDOMImplementation( "LS" );
      
      if ( domImplementation == null )
      {
        // If this is null, it might be caused by an old copy of Xerces JAR
        // file in the classpath.
        
        LOGGER.warn ( "domImplementation is null" );
        
        return null;
      }
      
      LOGGER.info ( domImplementation.getClass ( ).getName ( ) );
        
      return ( DOMImplementationLS ) domImplementation; 
    }
    
    public static Network  createTestNetwork ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final Seed  seed = createTestSeed ( );
      
      final Neuron  neuron0 = new BKPoissonNeuron (
        seed,
        new BKPoissonNeuronPara ( ) );
      
      final Neuron [ ]  neurons = new Neuron [ ] { neuron0 };
      
      final Map<Integer, Axon>  axonMap = new HashMap<Integer, Axon> ( );
      
      final SimulatorParser  simulatorParser = new SimulatorParser ( );
      
      final Seed  networkSeed = createTestSeed ( );
      
      final Network  network = new Network (
        simulatorParser.getModelFactory ( ),
        simulatorParser.getDiscreteEventQueue ( ),
        simulatorParser.getModulatedSynapseSeq ( ),
        neurons,
        axonMap,
        0, // minDelay
        simulatorParser,
        networkSeed );
      
      return network;
    }
    
    public static PeerInfo  createTestPeerInfo (
      final jpvmEnvironment  jpvmEnvironmentInstance )
      throws IOException
    ////////////////////////////////////////////////////////////////////////
    {
      final byte [ ]
        byteArray = toByteArray ( ErnstTestConstants.TEST_MODEL_FILENAME );
      
      final int  numTasks = 1;
      
      final int [ ]  endIndices = { 0 };
      
      final jpvmTaskId
        myJpvmTaskId = jpvmEnvironmentInstance.pvm_mytid ( );
      
      if ( myJpvmTaskId == null )
      {
        LOGGER.warn ( "myJpvmTaskId is null" );
      }
      else
      {
        LOGGER.info ( "myJpvmTaskId:  " + myJpvmTaskId );
      }
      
      final jpvmTaskId [ ]
        jpvmTaskIds = { myJpvmTaskId };
      
// TODO:  Will using the current task id as the parent work for testing?
      
      final jpvmTaskId  parentJpvmTaskId = myJpvmTaskId;
      
      final int  seedInt = ErnstTestConstants.TEST_SEED_INT;
      
      return new PeerInfo (
        byteArray,
        endIndices,
        jpvmTaskIds,
        numTasks,
        parentJpvmTaskId,
        seedInt );
    }
    
    public static Seed  createTestSeed ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return new Seed ( -3 );
    }
    
    public static VSICLIFNeuronPara  createTestVsiclifNeuronPara ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final VSICLIFNeuronPara
        vsiclifNeuronPara = new VSICLIFNeuronPara ( );
      
// TODO:  Not sure what these methods do or in which order they are called.
// They appear to be required for initialization.      
  
      vsiclifNeuronPara.assignExp ( );
  
      vsiclifNeuronPara.calConstants ( );
      
      return vsiclifNeuronPara;
    }
    
    public static MiNiNeuronPara createTestMiNiNeuronPara ( ) 
    {
      final MiNiNeuronPara miniNeuron = new MiNiNeuronPara();
      
      miniNeuron.A = 1;
      
      miniNeuron.SPIKE_ADD = new double [ ] { 1e-10, 0 };
      miniNeuron.SPIKE_RATIO = new double [ ] { 2 , 0 };
            
      miniNeuron.calConstants();
      
      return miniNeuron;
    }
    
    public static boolean  equivalent (
      final double [ ] [ ]  matrix0,
      final double [ ] [ ]  matrix1 )
    ////////////////////////////////////////////////////////////////////////
    {
      if ( matrix0 == null )
      {
        return matrix1 == null;
      }
      
      if ( matrix1 == null )
      {
        return false;
      }
      
      if ( matrix0.length != matrix1.length )
      {
        return false;
      }
      
      for ( int  i = 0; i < matrix0.length; i++ )
      {
        if ( !Arrays.equals ( matrix0 [ i ], matrix1 [ i ] ) )
        {
          return false;
        }
      }
      
      return true;
    }
    
    public static byte [ ]  toByteArray ( final String  resourceName )
      throws IOException
    ////////////////////////////////////////////////////////////////////////
    {
      InputStream  inputStream = null;
      
      try
      {      
        inputStream = CLASS_LOADER.getResourceAsStream ( resourceName );
        
        if ( inputStream == null )
        {
          // If this is null, check that the resources directory is in the
          // classpath.

          LOGGER.info ( "inputStream is null" );
          
          return null;
        }
        
        final ByteArrayOutputStream
          byteArrayOutputStream = new ByteArrayOutputStream ( );
        
        int  i;
        
        while ( ( i = inputStream.read ( ) ) != -1 )
        {
          byteArrayOutputStream.write ( i );
        }
        
        return byteArrayOutputStream.toByteArray ( );
      }
      finally
      {
        if ( inputStream != null )
        {
          try
          {
            inputStream.close ( );
          }
          catch ( final Exception  e )
          {
            // ignore
          }
        }
      }      
    }
      
    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
      
    private ErnstTestLib ( )
    ////////////////////////////////////////////////////////////////////////
    {
      // private constructor
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }