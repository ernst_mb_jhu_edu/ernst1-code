    package edu.jhu.mb.ernst.test;

    import static org.junit.Assert.*;
    import static org.junit.Assert.assertEquals;
    import static org.junit.Assert.assertFalse;
    import static org.junit.Assert.assertNotNull;

    import java.util.Arrays;

    import org.junit.Test;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import cnslab.cnsnetwork.MiNiNeuron;
    import cnslab.cnsnetwork.MiNiNeuronPara;
    import cnslab.cnsnetwork.Neuron;
    import cnslab.cnsnetwork.VSICLIFNeuron;
    import cnslab.cnsnetwork.VSICLIFNeuronPara;
    import edu.jhu.mb.ernst.model.ModelFactory;
    import edu.jhu.mb.ernst.model.Synapse;
    import edu.jhu.mb.ernst.model.factory.DefaultModelFactoryImp;

    /***********************************************************************
    * Unit testing of class MiNiNeuron.
    *
    * @version
    *   $Date: 2012-08-28 17:02:53 +0200 (Tue, 28 Aug 2012) $
    *   $Rev: 128 $
    *   $Author: jmcohen27 $
    * @author
    *   Jeremy Cohen
    * @author
    *   David Wallace Croft, M.Sc.
    ***********************************************************************/
    public final class  ErnstMiNiNeuronTest
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    private static final Class<ErnstMiNiNeuronTest>
      CLASS = ErnstMiNiNeuronTest.class;

    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );

    private static final ModelFactory
      MODEL_FACTORY = DefaultModelFactoryImp.INSTANCE;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    /***********************************************************************
    * Test of interface Neuron accessor methods.
    ***********************************************************************/
    @Test
    public void  testNeuronAccessorMethods ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final MiNiNeuronPara para = ErnstTestLib.createTestMiNiNeuronPara ( );

      final Neuron neuron = new MiNiNeuron ( para );

      neuron.init (
        1, // expId
        1, // trialId
        ErnstTestLib.createTestSeed ( ), // idum
        ErnstTestLib.createTestNetwork ( ), // net
        0 ); // id

      // interface Neuron accessor methods

      // VSICLIFNeuron is not a sensory neuron

      assertFalse ( neuron.isSensory ( ) );

      final double [ ]  currents = neuron.getCurr ( 0 );

      assertNotNull ( currents );

      assertEquals ( para.DECAY_SYNAPSE.length + para.DECAY_SPIKE.length, currents.length );      

      // Initially the currents should be zero.

      for ( int i = 0; i < currents.length; i++ )
      {
        assertEquals ( currents [ i ] ,  0 , 1e-15 );
      }

      // The membrane voltage should be within the bounds determined 
      // by its initial jitter.

      final double  membraneVoltage = neuron.getMemV ( 0 );

      assertTrue ( membraneVoltage > para.ini_mem 
        && membraneVoltage < para.ini_mem + para.ini_memVar );

      final boolean  record = neuron.getRecord ( );

      assertFalse ( record );

      final long  targetHost = neuron.getTHost ( );

      assertEquals ( 0, targetHost );

      final double  timeOfNextFire = neuron.getTimeOfNextFire ( );

      assertEquals ( -1, timeOfNextFire, 0 );

      final boolean  realFire = neuron.realFire ( );

      assertFalse ( realFire );
    }

    /***********************************************************************
    * Test of interface Neuron lifecycle methods.
    ***********************************************************************/
    @Test
    public void  testNeuronLifecycleMethods ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final MiNiNeuronPara para = ErnstTestLib.createTestMiNiNeuronPara ( );

      final Neuron neuron = new MiNiNeuron ( para );

      neuron.init (
        1, // expId
        1, // trialId
        ErnstTestLib.createTestSeed ( ), // idum
        ErnstTestLib.createTestNetwork ( ), // net
        0 ); // id

      final double [ ]  currents0 = neuron.getCurr ( 0 );

      assertNotNull ( currents0 );

      // Initially the currents should be zero

      assertEquals ( 0, currents0 [ 0 ], 0 );

      assertEquals ( 0, currents0 [ 1 ], 0 );

      final double  membraneVoltage0 = neuron.getMemV ( 0 );

      assertTrue ( membraneVoltage0 != 0 );

      final double  timeOfNextFire0 = neuron.getTimeOfNextFire ( );
    
      LOGGER.debug ( "time of next fire ....:  {}", timeOfNextFire0 );
      
      LOGGER.debug ( "real fire? " + neuron.realFire());

      assertEquals ( -1, timeOfNextFire0, 0 );

      final double  weight = 1e-9;   

      final Synapse  synapse = MODEL_FACTORY.createSynapse (
        0,   // to
        ( byte ) 0, // type (0 = excitatory)
        ( float ) weight );

      double  timeOfNextFire = neuron.updateInput (
        0, // time
        synapse );

      neuron.setTimeOfNextFire(timeOfNextFire);

      for (int i = 0; i < 100; i++)
      {
        timeOfNextFire = neuron.updateFire();
      
        LOGGER.debug ( "time of next fire ....:  {}", timeOfNextFire );
        
        LOGGER.debug ( "real fire? " + neuron.realFire());
        
        neuron.setTimeOfNextFire(timeOfNextFire);
      }
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }