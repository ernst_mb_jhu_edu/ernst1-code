    package edu.jhu.mb.ernst.test;
    
    import java.io.*;
    import java.net.*;
    import java.util.*;

    import org.junit.Test;

    import cnslab.cnsmath.Seed;
    import cnslab.cnsnetwork.Experiment;
    import cnslab.cnsnetwork.Layer;
    import cnslab.cnsnetwork.LayerStructure;
    import cnslab.cnsnetwork.Para;
    import cnslab.cnsnetwork.Recorder;
    import cnslab.cnsnetwork.SimulatorParser;
    import cnslab.cnsnetwork.Stimulus;
    import cnslab.cnsnetwork.SubExp;
    import cnslab.cnsnetwork.VSICLIFNeuronParaV2;
    import edu.jhu.mb.ernst.ErnstLib;

    import static org.junit.Assert.*;

    /***********************************************************************
    * Unit testing of class SimulatorParser.
    *
    * @version
    *   $Date: 2012-08-04 21:09:25 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 106 $
    *   $Author: croft $
    * @since
    *   2011-06-04
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  ErnstSimulatorParserTest
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static final Class<ErnstSimulatorParserTest>
      CLASS = ErnstSimulatorParserTest.class;
      
    private static final ClassLoader
      CLASS_LOADER = CLASS.getClassLoader ( );
    
    private static final String
      TEST_MODEL_FILENAME = ErnstTestConstants.TEST_MODEL_FILENAME;    
      
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
      
    @Test
    public void  testConstructor ( )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      new SimulatorParser (
        new Seed ( -3 ),
        toFile ( TEST_MODEL_FILENAME ) );
    }
    
    @Test
    public void  testParse ( )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      final SimulatorParser  simulatorParser = new SimulatorParser (
        new Seed ( -3 ),
        toFile ( TEST_MODEL_FILENAME ) );
      
      assertNotNull ( simulatorParser.document );
      
      assertNotNull ( simulatorParser.rootElement );
      
      assertNull ( simulatorParser.documentType );
      
      simulatorParser.parseMapCells ( );
      
      assertEquals ( 10, simulatorParser.xEdgeLength );
      
      assertEquals ( 10, simulatorParser.yEdgeLength );
      
      assertEquals ( "ava.nc", simulatorParser.outFile );
      
      assertEquals ( 1, simulatorParser.numOfHosts );
      
      assertEquals ( 1, simulatorParser.aSec );
      
      assertEquals ( 1, simulatorParser.bSec );
      
      assertEquals ( LayerStructure.ABMAP, simulatorParser.mapType );
      
      assertNotNull ( simulatorParser.layerStructure );
      
      assertEquals ( 5, simulatorParser.backgroundFrequency, 0 );
      
      assertEquals ( 1e-9, simulatorParser.backgroundStrength, 0 );
      
      assertTrue (
        ErnstLib.equivalent (
          new Layer ( "AA", "AA", 1, 1, "exc" ),
          simulatorParser.layerStructure.getLayer ( "AA", "AA" ) ) );
      
      assertTrue (
        ErnstLib.equivalent (
          new Layer ( "BB", "BB", 1, 1, "exc" ),
          simulatorParser.layerStructure.getLayer ( "BB", "BB" ) ) );
      
      assertEquals (
        200,
        simulatorParser.layerStructure.totalNumNeurons ( ) );
      
      simulatorParser.parseNeuronDef ( );
      
      final Map<String, Para>  modelParaMap = simulatorParser.modelParaMap;
      
      final Para  excPara = modelParaMap.get ( "exc" );
      
      assertNotNull ( excPara );
      
      final String  excModel = excPara.getModel ( );
      
      assertEquals ( "VSICLIFNeuronV2", excModel );
      
      final VSICLIFNeuronParaV2
        vsiclifNeuronParaV2 = ( VSICLIFNeuronParaV2 ) excPara;
      
      assertEquals (
        -0.070,
        vsiclifNeuronParaV2.VREST,
        0 );
      
      assertEquals (
        100,
        vsiclifNeuronParaV2.K_LTD,
        0 );
      
      assertEquals (
        1 / 0.005,
        vsiclifNeuronParaV2.DECAY [ 0 ],
        0 );
      
      assertEquals (
        1 / 0.025,
        vsiclifNeuronParaV2.DECAY [ 1 ],
        0 );
      
      assertEquals (
        200,
        vsiclifNeuronParaV2.K_LTP,
        0 );
      
      assertEquals (
        0.1,
        vsiclifNeuronParaV2.Alpha_LTD,
        0 );
      
      assertEquals (
        0,
        vsiclifNeuronParaV2.Alpha_LTP,
        0 );
      
      final Map<String, Double>
        doubleDataMap = simulatorParser.doubleDataMap;
      
      assertEquals (
        5e-10,
        doubleDataMap.get ( "str_exc" ).doubleValue ( ),
        0 );
        
      assertEquals (
        0.01,
        doubleDataMap.get ( "del" ).doubleValue ( ),
        0 );
        
      assertEquals (
        -5e-10,
        doubleDataMap.get ( "str_inhib" ).doubleValue ( ),
        0 );
      
      final Map<String, double [ ] [ ]>
        matrixDataMap = simulatorParser.matrixDataMap;
        
      assertTrue (
        ErnstTestLib.equivalent (
          new double [ ] [ ] {
            { 0, 0, 0 },
            { 1, 5, 1 },
            { 0, 0, 0 } },
          matrixDataMap.get ( "conn" ) ) );
      
      simulatorParser.parseExperiment ( );
      
//      final ArrayList<String>  subexpNames = simulatorParser.subexpNames;
//      
//      assertNotNull ( subexpNames );
//      
//      assertEquals (
//        1,
//        subexpNames.size ( ) );
//      
//      assertEquals (
//        "test",
//        subexpNames.get ( 0 ) );
      
      final Experiment  experiment = simulatorParser.experiment;
      
      assertNotNull ( experiment );
      
      final SubExp [ ]  subExpArray = experiment.subExp;
      
      assertNotNull ( subExpArray );
      
      assertEquals (
        1,
        subExpArray.length );
      
      final SubExp  subExp = subExpArray [ 0 ];
      
      assertNotNull ( subExp );
      
      assertEquals (
        "test",
        subExp.name );
      
      assertEquals (
        10.0,
        subExp.trialLength,
        0 );
      
      assertEquals (
        2,
        subExp.repetition );
        
      final Stimulus [ ]  stimuli = subExp.stimuli;
      
      assertNotNull ( stimuli );
      
      assertEquals (
        2,
        stimuli.length );
      
      final Stimulus  stimulus0 = stimuli [ 0 ];
      
      final Stimulus  stimulus1 = stimuli [ 1 ];
      
      assertNotNull ( stimulus0 );
      
      assertNotNull ( stimulus1 );
      
      assertNull ( stimulus0.simulatorParser );
      
      assertNull ( stimulus1.simulatorParser );
      
      // strength
      
      assertEquals (
        1e-10,
        stimulus0.getStrength ( ),
        0 );
      
      assertEquals (
        1e-10,
        stimulus1.getStrength ( ),
        0 );
      
      // type
      
      assertEquals (
        0,
        stimulus0.getType ( ) );
      
      assertEquals (
        0,
        stimulus1.getType ( ) );
      
      // onTime
      
      assertEquals (
        0.0,
        stimulus0.getOnTime ( ),
        0 );
      
      assertEquals (
        0.0,
        stimulus1.getOnTime ( ),
        0 );
      
      // offTime
      
      assertEquals (
        10.0,
        stimulus0.getOffTime ( ),
        0 );
      
      assertEquals (
        10.0,
        stimulus1.getOffTime ( ),
        0 );
      
      // freq
      
      assertEquals (
        40.0,
        stimulus0.getFreq ( ),
        0 );
      
      assertEquals (
        40.0,
        stimulus1.getFreq ( ),
        0 );
      
      // decay
      
      assertEquals (
        0.0,
        stimulus0.getDecay ( ),
        0 );
      
      assertEquals (
        0.0,
        stimulus1.getDecay ( ),
        0 );
      
      // seed
      
      assertNotNull ( stimulus0.seed );
      
      assertNotNull ( stimulus1.seed );
      
      // channel
      
      assertEquals (
        -1,
        stimulus0.getChannel ( ) );
      
      assertEquals (
        -1,
        stimulus1.getChannel ( ) );
      
      // TODO:  For prefix and suffix parsing, see Stimulus.background()
      
      final Recorder  recorder = experiment.recorder;
      
      assertNotNull ( recorder );
      
      assertFalse ( recorder.plot );
      
      assertNull ( recorder.simulatorParser );
      
// TODO:  continue adding asserts here      
      
      simulatorParser.findMinDelay ( );
    }

//    @Test
//    public void  testParseConnection ( )
//      throws Exception
//    ////////////////////////////////////////////////////////////////////////
//    {
//      final SimulatorParser  simulatorParser = new SimulatorParser (
//        new Seed ( -3 ),
//        toFile ( TEST_MODEL_FILENAME ) );
//      
//      simulatorParser.parseConnection ( );
//    }
    
    @Test
    public void  testReferenceCheck ( )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      final SimulatorParser  simulatorParser = new SimulatorParser (
        new Seed ( -3 ),
        toFile ( TEST_MODEL_FILENAME ) );
      
      simulatorParser.referenceCheck ( );
    }
    
    @Test
    public void  testValidate ( )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      assertEquals (
        "",
        new SimulatorParser ( ).validate (
          toFile ( TEST_MODEL_FILENAME ) ) );
    }
/*    
    @Test
    public void  testConstructor ( )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      final URL  modelURL = CLASS_LOADER.getResource ( "test001.xml" );
      
      final URI  modelURI = modelURL.toURI ( );
      
      final File  modelFile = new File ( modelURI );
      
      final SimulatorParser
        simulatorParser = new SimulatorParser (
          new Seed ( -3 ),
          modelFile );
      
      // pas.parseMapCells();
      
      simulatorParser.parseMapCells ( 5 );
      
      System.out.println (
        simulatorParser.ls.base + " " + simulatorParser.ls.neuron_end );
      
      simulatorParser.parsePopNeurons ( 5 );
      
      simulatorParser.parseConnection ( 5 );
      
      simulatorParser.parseExp ( );
      
      simulatorParser.findMinDelay ( );
      
      System.out.println ( simulatorParser.minDelay );
    }
*/    
    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
    
    private static final File  toFile ( final String  filename )
      throws URISyntaxException
    ////////////////////////////////////////////////////////////////////////
    {
      final URL  modelURL = CLASS_LOADER.getResource ( filename );
      
      final URI  modelURI = modelURL.toURI ( );
      
      return new File ( modelURI );      
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }