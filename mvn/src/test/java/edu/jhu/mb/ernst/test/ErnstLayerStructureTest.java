    package edu.jhu.mb.ernst.test;
    
    import static org.junit.Assert.*;

    import org.junit.Test;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import cnslab.cnsnetwork.LayerStructure;
    import edu.jhu.mb.ernst.model.ModelFactory;
    import edu.jhu.mb.ernst.model.factory.DefaultModelFactoryImp;

    /***********************************************************************
    * Unit testing of class LayerStructure.
    *
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @since
    *   2012-01-28
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  ErnstLayerStructureTest
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static final Class<ErnstLayerStructureTest>
      CLASS = ErnstLayerStructureTest.class;
      
    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );
    
    private static final ModelFactory
      MODEL_FACTORY = DefaultModelFactoryImp.INSTANCE;
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Test
    public void  testCellMapSlow ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final int
        edgeLengthX      = 100,
        edgeLengthY      = 200,
        layerMultiplierX = 2,
        layerMultiplierY = 3,
        sectionsInAxisX  = 4,
        sectionsInAxisY  = 5,
        nodeCount        = sectionsInAxisX * sectionsInAxisY,
        layerCoordinateX = 147,
        layerCoordinateY = 363;
      
      final String
        layerPrefix = "AA",
        layerSuffix = "BB";
      
      final LayerStructure
        layerStructure = new LayerStructure (
          MODEL_FACTORY,
          nodeCount,
          edgeLengthX,
          edgeLengthY );
      
      layerStructure.addLayer (
        layerPrefix,
        layerSuffix,
        layerMultiplierX,
        layerMultiplierY,
        "neuronType" );
      
      layerStructure.abmapcells_Main (
        sectionsInAxisX,
        sectionsInAxisY );
      
      final int  neuronIndexActual = layerStructure.cellmap_slow (
        layerPrefix,
        layerSuffix,
        layerCoordinateX,
        layerCoordinateY );
      
      final int
        neuronsPerSectionX
          = edgeLengthX * layerMultiplierX / sectionsInAxisX,
        neuronsPerSectionY
          = edgeLengthY * layerMultiplierY / sectionsInAxisY;
      
      final int
        expectedSectionX = 2,
        expectedSectionY = 3;
      
      final int  neuronIndexExpected
        = ( expectedSectionX * sectionsInAxisY + expectedSectionY )
        * edgeLengthX * layerMultiplierX
        * edgeLengthY * layerMultiplierY
        / sectionsInAxisX
        / sectionsInAxisY
        + neuronsPerSectionY
          * ( layerCoordinateX - ( expectedSectionX * neuronsPerSectionX ) )
        + layerCoordinateY - ( expectedSectionY * neuronsPerSectionY );
      
      LOGGER.debug ( "neuronIndexExpected ...:  {}", neuronIndexExpected );
      
      LOGGER.debug ( "neuronIndexActual .....:  {}", neuronIndexActual );
      
      assertEquals (
        neuronIndexExpected,
        neuronIndexActual );
    }    
      
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }