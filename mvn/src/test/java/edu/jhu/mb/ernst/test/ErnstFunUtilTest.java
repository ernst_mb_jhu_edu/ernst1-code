    package edu.jhu.mb.ernst.test;

    import org.junit.Assert;
    import org.junit.Test;

    import cnslab.cnsnetwork.FunUtil;

    /***********************************************************************
    * Unit testing of class FunUtil.
    *
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @since
    *   2011-06-04
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  ErnstFunUtilTest
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static double
      TEST_MEAN_DATA [ ]   = { -2, -1, 0, 1, 2 },
      TEST_MEAN_DELTA      = 1e-9,
      TEST_MEAN_EXPECTED_1 = 0,
      TEST_MEAN_EXPECTED_2 = 0.5;
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
      
    @Test
    public void  testMean ( )
    ////////////////////////////////////////////////////////////////////////
    {
      Assert.assertEquals (
        TEST_MEAN_EXPECTED_1,
        FunUtil.mean ( TEST_MEAN_DATA ),
        TEST_MEAN_DELTA );
      
      Assert.assertEquals (
        TEST_MEAN_EXPECTED_2,
        FunUtil.mean ( 1, TEST_MEAN_DATA ),
        TEST_MEAN_DELTA );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }