    package edu.jhu.mb.ernst.test;
    
    import java.util.*;
    import java.util.concurrent.CyclicBarrier;

    import org.junit.Test;

    import jpvm.jpvmTaskId;

    import cnslab.cnsmath.Seed;

    import cnslab.cnsnetwork.Axon;
    import cnslab.cnsnetwork.BKPoissonNeuron;
    import cnslab.cnsnetwork.BKPoissonNeuronPara;
    import cnslab.cnsnetwork.Experiment;
    import cnslab.cnsnetwork.JpvmInfo;
    import cnslab.cnsnetwork.NetRecordSpike;
    import cnslab.cnsnetwork.Network;
    import cnslab.cnsnetwork.Neuron;
    import cnslab.cnsnetwork.PRun;
    import cnslab.cnsnetwork.RecordBuffer;
    import cnslab.cnsnetwork.Recorder;
    import cnslab.cnsnetwork.SimulatorParser;
    import cnslab.cnsnetwork.Stimulus;
    import cnslab.cnsnetwork.SubExp;

    import static org.junit.Assert.*;

    /***********************************************************************
    * Unit testing of class PRun.
    *
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @since
    *   2011-07-16
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  ErnstPRunTest
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
//    private static final Class<ErnstPRunTest>
//      CLASS = ErnstPRunTest.class;
//      
//    private static final ClassLoader
//      CLASS_LOADER = CLASS.getClassLoader ( );
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Test
    public void  test ( )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      final Network  network = createNetwork ( );
      
      assertNull ( network.getFireEventSlot ( ) );
      
      network.initNet ( );
      
      assertNotNull ( network.getFireEventSlot ( ) );
      
      network.init ( );
      
      final CyclicBarrier  cyclicBarrier = new CyclicBarrier ( 1 );
      
      final PRun  pRun = new PRun (
        network.getDiscreteEventQueue ( ),
        network,
        new Object ( ), // lock
        cyclicBarrier );
      
      new Thread ( pRun ).start ( );
      
      try
      {
        Thread.sleep ( 3000 );
      }
      catch ( final InterruptedException  ie )
      {
        // ignore
      }
      
      network.stop = true;
      
      final RecordBuffer  recordBuffer = network.recordBuff;
      
      final List<NetRecordSpike>  netRecordSpikeList = recordBuffer.buff;
      
      assertTrue ( netRecordSpikeList.size ( ) > 0 );
      
//      for ( final NetRecordSpike  netRecordSpike : netRecordSpikeList )
//      {
//        System.out.println ( netRecordSpike );
//      }
    }
    
    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
    
    private static JpvmInfo  createJpvmInfo ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final JpvmInfo  jpvmInfo = new JpvmInfo ( );
      
      jpvmInfo.numTasks = 1;
      
      jpvmInfo.parentJpvmTaskId = new jpvmTaskId ( );
      
      return jpvmInfo;      
    }
    
    private static Network  createNetwork ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final Seed  seed = new Seed ( -3 );
      
      final Neuron  neuron0 = new BKPoissonNeuron (
        seed,
        new BKPoissonNeuronPara ( ) );
      
      neuron0.setRecord ( true );
      
      final Neuron [ ]  neurons = new Neuron [ ] { neuron0, null };
      
      final Map<Integer, Axon>  axonMap = new HashMap<Integer, Axon> ( );
      
      final SimulatorParser  simulatorParser = new SimulatorParser ( );
      
      final Seed  networkSeed = new Seed ( -3 );
      
      final Stimulus [ ]  stimuli = new Stimulus [ ] { };
      
      final SubExp [ ]  subexperiments = new SubExp [ ] {
        new SubExp (
          1, // repetition
          1.0, // trialLength
          stimuli,
          null ) }; // discreteEventCollection
      
      final Recorder  recorder = new Recorder (
        simulatorParser,
        false ); // plot
      
      final Experiment  experiment = new Experiment (
        subexperiments,
        recorder );
      
      final JpvmInfo  jpvmInfo = createJpvmInfo ( );
      
      final Network  network = new Network (
        simulatorParser.getModelFactory ( ),
        simulatorParser.getDiscreteEventQueue ( ),
        simulatorParser.getModulatedSynapseSeq ( ),
        neurons,
        axonMap,
        jpvmInfo,
        0,    // base
        0,    // minDelay
        simulatorParser,
        networkSeed,
        experiment );
      
      return network;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }