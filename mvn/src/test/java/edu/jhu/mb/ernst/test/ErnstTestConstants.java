    package edu.jhu.mb.ernst.test;
    
    /***********************************************************************
    * Constants to support ERNST unit testing.
    *
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @since
    *   2011-07-23
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  ErnstTestConstants
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    public static final String
      TEST_MODEL_FILENAME = "model/test001.xml";
    
    public static final String
      MODEL_UNIT_TEST_001_XML = "model/unit-test-001.xml";
    
    public static final int
      TEST_SEED_INT = -3;    
      
    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
      
    private  ErnstTestConstants ( )
    ////////////////////////////////////////////////////////////////////////
    {
      // private constructor
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }