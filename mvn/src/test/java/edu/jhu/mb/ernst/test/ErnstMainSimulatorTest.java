    package edu.jhu.mb.ernst.test;
    
    import java.io.*;
    
    import org.junit.Test;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    
    import cnslab.cnsmath.Seed;
    import cnslab.cnsnetwork.MainSimulator;

    import static org.junit.Assert.*;
    
    /***********************************************************************
    * Unit testing of class MainSimulator.
    *
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @since
    *   2011-07-10
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  ErnstMainSimulatorTest
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static final Class<ErnstMainSimulatorTest>
      CLASS = ErnstMainSimulatorTest.class;
      
    private static final ClassLoader
      CLASS_LOADER = CLASS.getClassLoader ( );
    
//    private static final String
//      CLASS_NAME = CLASS.getName ( );
      
    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );
    
    private static final String
      TEST_MODEL_FILENAME = ErnstTestConstants.TEST_MODEL_FILENAME;
    
    private static final int
      TEST_HEAP_SIZE = 512,
      TEST_SEED      = -3;
      
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
      
    @Test
    public void  test ( )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      final String  modelFilename
        = CLASS_LOADER.getResource ( TEST_MODEL_FILENAME ).getFile ( );
      
      LOGGER.info ( modelFilename );
      
      final MainSimulator  mainSimulator = new MainSimulator (
        new Seed ( TEST_SEED ),
        new File ( modelFilename ),
        TEST_HEAP_SIZE );
      
      // TODO:  This test seems to fail when run from the mvn directory,
      // probably because jpvm requires running from the main directory,
      // but it is useful to run this test within the debugger with the
      // working directory set to the main directory.
      
      mainSimulator.run ( );
    }
    
    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }