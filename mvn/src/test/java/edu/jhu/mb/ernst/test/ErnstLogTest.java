    package edu.jhu.mb.ernst.test;
    
    
    import org.junit.Test;
    
    import ch.qos.logback.classic.Level; 
    import ch.qos.logback.classic.Logger; 
    import ch.qos.logback.classic.LoggerContext; 
    import ch.qos.logback.classic.spi.ILoggingEvent; 
    import ch.qos.logback.core.ConsoleAppender; 
    import org.slf4j.LoggerFactory;
    
    /***********************************************************************
    * Unit testing of ERNST logging.
    *
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @since
    *   2011-09-10
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  ErnstLogTest
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private static final Class<ErnstLogTest>
      CLASS = ErnstLogTest.class;
    
    private static final Level
      LEVEL = Level.ALL;
      
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
      
    @Test
    public void  test ( )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      final Logger  rootLogger
        = ( Logger ) LoggerFactory.getLogger ( Logger.ROOT_LOGGER_NAME );
      
      rootLogger.setLevel ( LEVEL );
      
//      rootLogger.detachAndStopAllAppenders ( );
      
      final ConsoleAppender<ILoggingEvent>
        consoleAppender = new ConsoleAppender<ILoggingEvent> ( );
      
      consoleAppender.setTarget ( "System.err" );
      
      consoleAppender.setContext (
        ( LoggerContext ) LoggerFactory.getILoggerFactory ( ) );
      
//    outputStreamAppender.setLevel ( level );
      
      rootLogger.addAppender ( consoleAppender );
      
      rootLogger.info ( "Logging level set to " + LEVEL );
      
//      logger.debug ( "test" );
//      
//      logger.debug ( logger.getClass ( ).getName ( ) );
      
    }
    
    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }