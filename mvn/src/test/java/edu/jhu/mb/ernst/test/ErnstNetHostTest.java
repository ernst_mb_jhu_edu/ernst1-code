    package edu.jhu.mb.ernst.test;

    import static org.junit.Assert.*;

    import static edu.jhu.mb.ernst.test.ErnstTestConstants.*;

    import java.util.concurrent.CyclicBarrier;
    import org.w3c.dom.ls.*;

    import jpvm.jpvmException;    

    import org.junit.Test;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import cnslab.cnsmath.Seed;
    import cnslab.cnsnetwork.JpvmInfo;
    import cnslab.cnsnetwork.NetHost;
    import cnslab.cnsnetwork.Network;
    import cnslab.cnsnetwork.PRun;
    import cnslab.cnsnetwork.SimulatorParser;

    /***********************************************************************
    * Unit testing of class NetHost.
    *
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @since
    *   2011-07-16
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  ErnstNetHostTest
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static final Class<ErnstNetHostTest>
      CLASS = ErnstNetHostTest.class;
      
//    private static final String
//      CLASS_NAME = CLASS.getName ( );
    
    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );
    
//    private static final String
//      METHOD_NAME_TEST_CREATE_SIMULATOR_PARSER
//        = "testCreateSimulatorParser"; 
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Test
    public void  testCreateSimulatorParser ( )
      throws Exception, jpvmException
    ////////////////////////////////////////////////////////////////////////
    {
// TODO:  This method has expanded beyond its original test goal.
      
      final DOMImplementationLS
        impl = ErnstTestLib.createDomImplementationLs ( );
      
      if ( impl == null )
      {
        LOGGER.error ( "impl == null" );
      }
      
      assertNotNull ( impl );
      
      final JpvmInfo  jpvmInfo = new JpvmInfo ( );
      
      jpvmInfo.numTasks = 1;
      
      final byte [ ]
        byteArray = ErnstTestLib.toByteArray ( MODEL_UNIT_TEST_001_XML );
      
      assertNotNull ( byteArray );
      
//      System.out.println ( new String ( byteArray, "UTF-8" ) );

      final SimulatorParser
        simulatorParser = NetHost.createSimulatorParser (
          impl,
          byteArray,
          TEST_SEED_INT,
          jpvmInfo );
      
      assertNotNull ( simulatorParser );
      
      final Seed  idum = new Seed ( -2 );
      
      final Network  network = new Network (
        simulatorParser.getModelFactory ( ),
        simulatorParser.getDiscreteEventQueue ( ),
        simulatorParser.getModulatedSynapseSeq ( ),
        simulatorParser.layerStructure.neurons,
        simulatorParser.layerStructure.axons,
        jpvmInfo,
        simulatorParser.layerStructure.base,
        simulatorParser.minDelay,
        simulatorParser,              
        idum,
        simulatorParser.experiment );
      
      assertNotNull ( network );
      
      network.initNet ( );
      
      final Object
        lockObject = new Object ( );
      
      final CyclicBarrier
        cyclicBarrier = new CyclicBarrier ( 2 );
      
      final PRun
        pRun = new PRun (
          network.getDiscreteEventQueue ( ),
          network,
          lockObject,
          cyclicBarrier );
      
      final Runnable  runnable = new Runnable ( )
      {
        @Override
        public void run ( )
        {
          try
          {
            cyclicBarrier.await ( );
            
            Thread.sleep ( 5 * 60 * 1000 );            
          }
          catch ( Exception e )
          {
            e.printStackTrace();
          }
          finally
          {
            network.stop = true;
            
            network.trialDone = true;
          }
        }
      };
      
      new Thread ( runnable ).start ( );
      
      pRun.run ( );
    }
/*    
    @Test
    public void  testMain ( )
      throws jpvmException, Exception
    ////////////////////////////////////////////////////////////////////////
    {
      final jpvmEnvironment
        jpvmEnvironmentInstance = new jpvmEnvironment ( );
      
      final PeerInfo  peerInfo
        = ErnstTestLib.createTestPeerInfo ( jpvmEnvironmentInstance ); 
      
      NetHost.launch ( jpvmEnvironmentInstance, peerInfo );
    }
*/    
    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }