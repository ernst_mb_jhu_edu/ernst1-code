    package edu.jhu.mb.ernst.test;
    
    import static org.junit.Assert.*;
    
    import org.junit.Test;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import cnslab.cnsnetwork.Neuron;
    import cnslab.cnsnetwork.VSICLIFNeuron;
    import cnslab.cnsnetwork.VSICLIFNeuronPara;
    import edu.jhu.mb.ernst.model.ModelFactory;
    import edu.jhu.mb.ernst.model.Synapse;
    import edu.jhu.mb.ernst.model.factory.DefaultModelFactoryImp;

    /***********************************************************************
    * Unit testing of class VSICLIFNeuron.
    *
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @since
    *   2011-10-25
    * @author
    *   David Wallace Croft, M.Sc.
    * @author
    *   Jeremy Cohen
    ***********************************************************************/
    public final class  ErnstVsiclifNeuronTest
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static final Class<ErnstVsiclifNeuronTest>
      CLASS = ErnstVsiclifNeuronTest.class;
    
    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );
    
    private static final ModelFactory
      MODEL_FACTORY = DefaultModelFactoryImp.INSTANCE;
  
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
      
    /***********************************************************************
    * Test of interface Neuron accessor methods.
    ***********************************************************************/
    @Test
    public void  testNeuronAccessorMethods ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final VSICLIFNeuronPara
        vsiclifNeuronPara = ErnstTestLib.createTestVsiclifNeuronPara ( );
  
      final Neuron  neuron = new VSICLIFNeuron ( vsiclifNeuronPara );
      
      neuron.init (
        1, // expId
        1, // trialId
        ErnstTestLib.createTestSeed ( ), // idum
        ErnstTestLib.createTestNetwork ( ), // net
        0 ); // id
      
      // interface Neuron accessor methods
      
      // VSICLIFNeuron is not a sensory neuron
      
      assertFalse ( neuron.isSensory ( ) );
      
      final double [ ]  currents = neuron.getCurr ( 0 );
      
      assertNotNull ( currents );
      
      // VSICLIFNeuron has 2 currents:  excitatory and inhibitory
      
      assertEquals ( 2, currents.length );      
      
//      LOGGER.debug ( "excitatory current ...:  {}", currents [ 0 ] );
//      
//      LOGGER.debug ( "inhibitory current ...:  {}", currents [ 1 ] );
      
      // Initially the currents should be zero
      
      assertEquals ( 0, currents [ 0 ], 0 );
      
      assertEquals ( 0, currents [ 1 ], 0 );
      
//      final double  membraneVoltage = neuron.getMemV ( 0 );
      
//      LOGGER.debug ( "membrane voltage .....:  {}", membraneVoltage );
      
//       Initially the membrane voltage should be the reset voltage
//      Edit: no, it shouldn't.  In the init() method of the VSICLIF neuron,
//      the membrane voltage is initialized to a random value.
//      assertEquals ( vsiclifNeuronPara.VRESET, membraneVoltage, 0 );
      
      final boolean  record = neuron.getRecord ( );
      
      assertFalse ( record );
      
      final long  targetHost = neuron.getTHost ( );
      
      assertEquals ( 0, targetHost );
      
      final double  timeOfNextFire = neuron.getTimeOfNextFire ( );
      
//      LOGGER.debug ( "time of next fire ....:  {}", timeOfNextFire );
      
      assertEquals ( -1, timeOfNextFire, 0 );
      
      final boolean  realFire = neuron.realFire ( );
      
      assertFalse ( realFire );
    }
    
    /***********************************************************************
    * Test of interface Neuron accessor methods.
    ***********************************************************************/
    @Test
    public void  testNeuronLifecycleMethods ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final VSICLIFNeuronPara
        vsiclifNeuronPara = ErnstTestLib.createTestVsiclifNeuronPara ( );  
      
      final VSICLIFNeuron
        vsiclifNeuron = new VSICLIFNeuron ( vsiclifNeuronPara );
      
      // creating interface reference
      
      final Neuron  neuron = vsiclifNeuron;
      
      neuron.init (
        1, // expId
        1, // trialId
        ErnstTestLib.createTestSeed ( ), // idum
        ErnstTestLib.createTestNetwork ( ), // net
        0 ); // id
      
      final double [ ]  currents0 = neuron.getCurr ( 0 );
      
      assertNotNull ( currents0 );
      
//      LOGGER.debug ( "excitatory current ...:  {}", currents0 [ 0 ] );
//      
//      LOGGER.debug ( "inhibitory current ...:  {}", currents0 [ 1 ] );
      
      // Initially the currents should be zero
      
      assertEquals ( 0, currents0 [ 0 ], 0 );
      
      assertEquals ( 0, currents0 [ 1 ], 0 );
      
      final double  membraneVoltage0 = neuron.getMemV ( 0 );
      
//      LOGGER.debug ( "membrane voltage .....:  {}", membraneVoltage0 );
      
      assertTrue ( membraneVoltage0 != 0 );
      
      final double  timeOfNextFire0 = neuron.getTimeOfNextFire ( );
      
//      LOGGER.debug ( "time of next fire ....:  {}", timeOfNextFire0 );
      
      assertEquals ( -1, timeOfNextFire0, 0 );
      
      final double  weight = VSICLIFNeuron.maxWeight / 10;   
      
      final Synapse  synapse = MODEL_FACTORY.createSynapse (
        0,   // to
        ( byte ) 0, // type (0 = excitatory)
        ( float ) weight );
      
      final double  timeOfNextFire1 = neuron.updateInput (
        0, // time
        synapse );
      
//      LOGGER.debug ( "time of next fire ....:  {}", timeOfNextFire1 );
      
      // Excitatory current should drive it to fire
      
      assertTrue ( timeOfNextFire1 > 0 );
      
      final double [ ]  currents1 = neuron.getCurr ( 0 );
      
      assertNotNull ( currents1 );
      
//      LOGGER.debug ( "excitatory current ...:  {}", currents1 [ 0 ] );
//      
//      LOGGER.debug ( "inhibitory current ...:  {}", currents1 [ 1 ] );
      
      // Excitatory current should be equal to weight
      
      assertEquals ( weight, currents1 [ 0 ], weight * 1e-6 );
      
      // Inhibitory current should still be zero
      
      assertEquals ( 0, currents1 [ 1 ], 0 );
      
//      LOGGER.debug (
//        "time of last update ..:  {}",
//        vsiclifNeuron.getTimeOfLastUpdate ( ) );
      
      final double  membraneVoltage1 = neuron.getMemV ( 0 );
      
//      LOGGER.debug ( "membrane voltage .....:  {}", membraneVoltage1 );
      
      // Excitatory current should increase membrane voltage
      
//      assertTrue ( membraneVoltage1 > membraneVoltage0 );
//      
//      final int  steps = 10;
//      
//      final double  stepTimeDelta = timeOfNextFire1 / steps;
//      
//      double  previousMembraneVoltage = membraneVoltage1;
//      
//      double  previousExcitatoryCurrent = currents1 [ 0 ];
//      
//      for ( int  i = 1; i <= steps; i++ )
//      {
//        final double  time = i * stepTimeDelta;
//        
//        final double  membraneVoltage2 = neuron.getMemV ( time );
//        
//        LOGGER.debug ( "membrane voltage .....:  {}", membraneVoltage2 );
//        
//        final double [ ]  currents2 = neuron.getCurr ( time );
//        
//        final double  excitatoryCurrent = currents2 [ 0 ];
//        
//        LOGGER.debug ( "excitatory current ...:  {}", excitatoryCurrent );
//        
//        assertTrue ( membraneVoltage2 > previousMembraneVoltage );
//        
//        previousMembraneVoltage = membraneVoltage2;
//        
//        assertTrue ( excitatoryCurrent < previousExcitatoryCurrent );
//        
//        previousExcitatoryCurrent = excitatoryCurrent;
//        
//        assertEquals ( 0, currents2 [ 1 ], 0 );
//      }
//      
//      neuron.setTimeOfNextFire ( timeOfNextFire1 );
//      
//      double  timeOfNextFire2 = neuron.updateFire ( );
//      
//      while ( timeOfNextFire2 > 0 )
//      {      
//        LOGGER.debug ( "time of next fire ....:  {}", timeOfNextFire2 );
//        
//        neuron.setTimeOfNextFire ( timeOfNextFire2 );
//        
//        timeOfNextFire2 = neuron.updateFire ( );
//      }
//      
//      LOGGER.debug ( "time of next fire ....:  {}", timeOfNextFire2 );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }