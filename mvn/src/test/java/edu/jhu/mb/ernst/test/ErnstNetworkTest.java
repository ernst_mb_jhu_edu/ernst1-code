    package edu.jhu.mb.ernst.test;
    
    import java.util.*;

    import jpvm.jpvmException;
    import jpvm.jpvmTaskId;

    import org.junit.Test;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import cnslab.cnsmath.Seed;

    import cnslab.cnsnetwork.Axon;
    import cnslab.cnsnetwork.BKPoissonNeuron;
    import cnslab.cnsnetwork.BKPoissonNeuronPara;
    import cnslab.cnsnetwork.Experiment;
    import cnslab.cnsnetwork.FireEvent;
    import cnslab.cnsnetwork.JpvmInfo;
    import cnslab.cnsnetwork.Network;
    import cnslab.cnsnetwork.Neuron;
    import cnslab.cnsnetwork.Recorder;
    import cnslab.cnsnetwork.SimulatorParser;
    import cnslab.cnsnetwork.Stimulus;
    import cnslab.cnsnetwork.SubExp;

    import static org.junit.Assert.*;

    /***********************************************************************
    * Unit testing of class Network.
    *
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @since
    *   2011-06-25
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  ErnstNetworkTest
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static final Class<ErnstNetworkTest>
      CLASS = ErnstNetworkTest.class;
    
    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );
      
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Test
    public void  testFire ( )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      final Seed  seed = new Seed ( -3 );
      
      final Neuron  neuron0 = new BKPoissonNeuron (
        seed,
        new BKPoissonNeuronPara ( ) );
      
      final Neuron [ ]  neurons = new Neuron [ ] { neuron0 };
      
      final Map<Integer, Axon>  axonMap = new HashMap<Integer, Axon> ( );
      
      final SimulatorParser  simulatorParser = new SimulatorParser ( );
      
      final Seed  networkSeed = new Seed ( -3 );
      
      final Network  network = new Network (
        simulatorParser.getModelFactory ( ),
        simulatorParser.getDiscreteEventQueue ( ),
        simulatorParser.getModulatedSynapseSeq ( ),        
        neurons,
        axonMap,
        0,      // minDelay
        simulatorParser,
        networkSeed );
      
      assertNull ( network.getFireEventSlot ( ) );
      
      network.initNet ( );
      
      assertNotNull ( network.getFireEventSlot ( ) );
      
      assertNull ( network.getFirstFireEvent ( ) );
      
      neuron0.init (
        0, // expId
        0, // trialId
        seed,
        network,
        1234 );
      
      final FireEvent  fireEvent = network.getFirstFireEvent ( );
      
      assertNotNull ( fireEvent );
      
//      network.fireQueue.deleteItem ( fireEvent );
//      
//      assertNull ( network.getFirstFireEvent ( ) );
//      
//      System.out.println ( fireEvent );
      
      assertEquals ( 1234, fireEvent.index );
    }
    
    @Test
    public void  testInit ( )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      final Seed  seed = new Seed ( -3 );
      
      final Neuron  neuron0 = new BKPoissonNeuron (
          seed,
          new BKPoissonNeuronPara ( ) );
      
      final Neuron [ ]  neurons = new Neuron [ ] { neuron0 };
      
      final Map<Integer, Axon>  axonMap = new HashMap<Integer, Axon> ( );
      
      final SimulatorParser  simulatorParser = new SimulatorParser ( );
      
      final Seed  networkSeed = new Seed ( -3 );
      
      final Stimulus [ ]  stimuli = new Stimulus [ ] { };
      
      final SubExp [ ]  subexperiments = new SubExp [ ] {
        new SubExp (
          1, // repetition
          1.0, // trialLength
          stimuli,
          null ) }; // discreteEventCollection
      
      final Recorder  recorder = new Recorder (
        simulatorParser,
        false ); // plot
      
      final Experiment  experiment = new Experiment (
        subexperiments,
        recorder );
      
      final Network  network = new Network (
        simulatorParser.getModelFactory ( ),
        simulatorParser.getDiscreteEventQueue ( ),
        simulatorParser.getModulatedSynapseSeq ( ),
        neurons,
        axonMap,
        null, // info
        0,    // base
        0,    // minDelay
        simulatorParser,
        networkSeed,
        experiment );
      
      assertNull ( network.getFireEventSlot ( ) );
      
      network.initNet ( );
      
      assertNotNull ( network.getFireEventSlot ( ) );
      
      network.init ( );
    }
    
    /***********************************************************************
    * Test of method pFireProcess().
    ***********************************************************************/
    @Test
    public void  testParallelFireProcess ( )
      throws Exception, jpvmException
    ////////////////////////////////////////////////////////////////////////
    {
      final Seed  seed = new Seed ( -3 );
      
      final Neuron  neuron0 = new BKPoissonNeuron (
          seed,
          new BKPoissonNeuronPara ( ) );
      
      final Neuron [ ]  neurons = new Neuron [ ] { neuron0, null };
      
      final Map<Integer, Axon>  axonMap = new HashMap<Integer, Axon> ( );
      
      final SimulatorParser  simulatorParser = new SimulatorParser ( );
      
      final Seed  networkSeed = new Seed ( -4 );
      
      final Stimulus [ ]  stimuli = new Stimulus [ ] { };
      
      final SubExp [ ]  subexperiments = new SubExp [ ] {
        new SubExp (
          1, // repetition
          1.0, // trialLength
          stimuli,
          null ) }; // discreteEventCollection
      
      final Recorder  recorder = new Recorder (
        simulatorParser,
        false ); // plot
      
      final Experiment  experiment = new Experiment (
        subexperiments,
        recorder );
      
      final JpvmInfo  jpvmInfo = new JpvmInfo ( );
      
      jpvmInfo.numTasks = 1;
      
      final jpvmTaskId
        parentJpvmTaskId = new jpvmTaskId ( "localhost", 1968 );
      
      jpvmInfo.parentJpvmTaskId = parentJpvmTaskId;
      
      final Network  network = new Network (
        simulatorParser.getModelFactory ( ),
        simulatorParser.getDiscreteEventQueue ( ),
        simulatorParser.getModulatedSynapseSeq ( ),
        neurons,
        axonMap,
        jpvmInfo, // info
        0,    // base
        0,    // minDelay
        simulatorParser,
        networkSeed,
        experiment );
      
      assertNull ( network.getFireEventSlot ( ) );
      
      network.initNet ( );
      
      assertNotNull ( network.getFireEventSlot ( ) );
      
      network.init ( );
      
      int  spikeCount = 0;
      
      FireEvent  firstFireEvent = null;
      
      while ( ( firstFireEvent = network.getFirstFireEvent ( ) ) != null )
      {      
//        LOGGER.debug (
//          "index {} time {}",
//          firstFireEvent.index,
//          firstFireEvent.time );
        
        if ( firstFireEvent.index >= 1 )
        {
          break;
        }
        
        spikeCount++;
      
        network.pFireProcess ( firstFireEvent );
      }
      
//      LOGGER.debug ( "spike count {}", spikeCount );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }