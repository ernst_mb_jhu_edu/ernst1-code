    package edu.jhu.mb.ernst.test;

    import static org.junit.Assert.assertEquals;
    import static org.junit.Assert.assertFalse;
    import static org.junit.Assert.assertNotNull;
    import static org.junit.Assert.assertTrue;

    import java.util.List;

    import org.junit.Test;

    import cnslab.cnsmath.Seed;
    import cnslab.cnsnetwork.LayerStructure;
    import cnslab.cnsnetwork.PInputEvent;
    import cnslab.cnsnetwork.SimulatorParser;
    import cnslab.cnsnetwork.Stimulus;
    import edu.jhu.mb.ernst.model.ModelFactory;
    import edu.jhu.mb.ernst.model.factory.DefaultModelFactoryImp;

    /***********************************************************************
    * Unit testing of class Stimulus.
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Jeremy Cohen
    * @author
    *   David Wallace Croft, M.Sc.
    ***********************************************************************/
    public final class  ErnstStimulusTest
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static final ModelFactory
      MODEL_FACTORY = DefaultModelFactoryImp.INSTANCE;
    
    private static final int SIZE = 10;
    
    // TODO: Test getNextTime()
    
    /**
     * Test of geometric plotting functions.
     */
    @Test
    public final void testPlotShapes ( )
    ////////////////////////////////////////////////////////////////////////
    {
    	makeSquare(2, 2, 8, 8);
   	
    	for (int x1 = 0; x1 < SIZE; x1++)
    	{
    		for (int y1 = 0; y1 < SIZE; y1++)
    		{
    			for (int x2 = 0; x2 < SIZE; x2++)
    			{
    				for (int y2 = 0; y2 < SIZE; y2++) 
    				{
    					List<PInputEvent> line = makeLine ( x1 , y1 , x2 , y2 );
    					
    			    checkDifferentTargets ( line );
    					
    					if ( x1 == x2 || y1 == y2)
    					{
    						checkEqualWeights ( line );
    					}
    					
    					if (x1 != x2 && y1 != y2)
    					{	
	    					List<PInputEvent> square = makeSquare ( x1 , y1 , x2 , y2 );
	    					
	    			    checkDifferentTargets ( square );
	    			    	
	    					List<PInputEvent>
	    					  filledSquare = makeFilledSquare ( x1 , y1 , x2 , y2 );
	    					
	    			    checkDifferentTargets ( filledSquare );
    					}
    				}
    			}
    		}
    	}
    }
    
    
    private final void checkEqualWeights ( List<PInputEvent> inputList )
    ////////////////////////////////////////////////////////////////////////
    {    	
    	if ( inputList.size( ) > 0 )
    	{
    		double weight = inputList.get( 0 ).synapse.getWeight( ); 
    		
    		for ( PInputEvent p : inputList )
    		{
    			assertEquals ( weight , p.synapse.getWeight( ) , 1e15 );
    		}
    	}
    }
    
    private final void checkDifferentTargets ( List<PInputEvent> inputList )
    ////////////////////////////////////////////////////////////////////////
    {    	    		
    	for ( int i = 0; i < inputList.size( ); i ++ )
    	{
    		for (int j = 0; j < inputList.size( ); j ++ )
    		{
    			PInputEvent event1 = inputList.get( i );
    			PInputEvent event2 = inputList.get( j );
    			
    			if (event1 != event2)
    			{
    				assertFalse( event1.synapse.getTargetNeuronIndex( )
    						== event2.synapse.getTargetNeuronIndex( ) );
    			}
    		}
    	}
    }
    
    public final List<PInputEvent>  makeLine (
      int x1,
      int y1,
      int x2,
      int y2)
    ////////////////////////////////////////////////////////////////////////
    {
    	Stimulus stimulus = createStimulus ( );
    	
    	stimulus.plotLine ( x1 , y1, x2, y2, "AA", "BB" );

    	final List<PInputEvent> poissonInputList = stimulus.init ( -1 );
    	
    	assertNotNull ( poissonInputList );
    	    	
    	int deltaX = Math.abs ( x2 - x1 );
    	int deltaY = Math.abs ( y2 - y1 );
    	int numExpectedInputs = Math.max ( deltaX , deltaY ) + 1;
    	assertTrue ( poissonInputList.size() == numExpectedInputs );
    	
    	return poissonInputList;
    }
    
    public final List<PInputEvent>  makeSquare (
      int x1,
      int y1,
      int x2,
      int y2)
    ////////////////////////////////////////////////////////////////////////
    {
    	Stimulus stimulus = createStimulus ( );
    	    	
    	stimulus.plotSquare ( x1 , y1, x2, y2, "AA", "BB" );

    	final List<PInputEvent> poissonInputList = stimulus.init ( -1 );
    	
    	assertNotNull ( poissonInputList );
    	    	
    	int deltaX = Math.abs ( x2 - x1 );
    	int deltaY = Math.abs ( y2 - y1 );
    	int numExpectedInputs = 2 * (deltaX + deltaY);
    	assertTrue ( poissonInputList.size() == numExpectedInputs );
    	
    	return poissonInputList;
    }
    
    public final List<PInputEvent>  makeFilledSquare (
      int x1,
      int y1,
      int x2,
      int y2)
    ////////////////////////////////////////////////////////////////////////
    {
    	Stimulus stimulus = createStimulus ( );
    	
    	stimulus.plotFilledSquare ( x1 , y1, x2, y2, "AA", "BB" );

    	final List<PInputEvent> poissonInputList = stimulus.init ( -1 );
    	
    	assertNotNull ( poissonInputList );
    	    	
    	int deltaX = Math.abs ( x2 - x1 );
    	int deltaY = Math.abs ( y2 - y1 );
    	int numExpectedInputs = (deltaX + 1) * (deltaY + 1);

    	assertTrue ( poissonInputList.size() == numExpectedInputs );
    	
    	return poissonInputList;
    }
    
    private final Stimulus createStimulus ( )
    ////////////////////////////////////////////////////////////////////////
    {
    	final SimulatorParser simulatorParser = createSimulatorParser 
    			( SIZE , SIZE , 1 , 1 );
    	final Stimulus stimulus = new Stimulus (
    			MODEL_FACTORY,
    			simulatorParser,
    			1e-10, // strength
    			0, // type
    			0, // on time
    			5, // off time
    			20, // frequency
    			0, // decay
    			new Seed(-3),
    			-1 // channel
    			);
    	return stimulus;
    }
    
    
    private final SimulatorParser createSimulatorParser( int edgeLengthX,
    		int edgeLengthY, int layerMultiplierX, int layerMultiplierY )
    ////////////////////////////////////////////////////////////////////////
    {
    	final SimulatorParser simulatorParser = new SimulatorParser();
    	
    	simulatorParser.layerStructure = createLayerStructure(edgeLengthX,
    			edgeLengthY, layerMultiplierX, layerMultiplierY);
    	
    	simulatorParser.xEdgeLength = edgeLengthX;
    	simulatorParser.yEdgeLength = edgeLengthY;
    	
    	return simulatorParser;
    }
    
    private final LayerStructure createLayerStructure( int edgeLengthX,
    		int edgeLengthY, int layerMultiplierX, int layerMultiplierY)
    ////////////////////////////////////////////////////////////////////////
    {
        final int
        sectionsInAxisX  = 1,
        sectionsInAxisY  = 1,
        nodeCount        = 1;
      
      final String
        layerPrefix = "AA",
        layerSuffix = "BB";
      
      final LayerStructure
        layerStructure = new LayerStructure (
          MODEL_FACTORY,
          nodeCount,
          edgeLengthX,
          edgeLengthY );
      
      layerStructure.addLayer (
        layerPrefix,
        layerSuffix,
        layerMultiplierX,
        layerMultiplierY,
        "neuronType" );
      
      layerStructure.abmapcells_Main (
        sectionsInAxisX,
        sectionsInAxisY );
      
      return layerStructure;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }