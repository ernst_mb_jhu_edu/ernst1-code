    import java.io.*;
    
    import cnslab.cnsmath.*;
    import cnslab.cnsnetwork.*;

    /***********************************************************************
    * @version
    *   $Date: 2011-09-10 20:48:53 -0500 (Sat, 10 Sep 2011) $
    *   $Rev: 226 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  starter
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    public static void  main ( final String [ ]  args )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      if ( args.length > 4
        || args.length < 1
        || args [ 0 ].equals ( "-help" )
        || args [ 0 ].equals ( "--help" ) ) 
      {
        System.out.println (
          "Usage:  java starter xmlfile.xml [seednum] [heapsize]megabyte" );
        
        System.out.println (
          "        default seednum -3, default heapsize 512m" );
        
        System.out.println (
          "        java --help to show this help information" );
      }
      else
      {
        final File  filename = new File ( "model/" + args [ 0 ] );
        
        if ( !filename.exists ( ) )
        {
          System.out.println ( "xml file doesn't exist, "
            + "please put it in the model directory" );
          
          System.exit ( 0 );
        }
        
        int  seed = -3;
        
        int  heapsize = 512;
        
        if ( args.length >= 2 )
        {
          seed = Integer.parseInt ( args [ 1 ] );
          
          if ( seed > 0 )
          {
            seed = -seed;
          }
        }
        
        if ( args.length == 3 )
        {
          heapsize = Integer.parseInt ( args [ 2 ] );
          
          if ( heapsize < 0 )
          {
            heapsize = -heapsize;
          }
        }
        
        System.out.println (
          "file:"
          + args [ 0 ]
          + " seed:"
          + seed
          + " heapsize:"
          + heapsize );

        final Seed  idum = new Seed ( seed );
        
        MainSimulator  main;
        
        if ( args.length == 4
          && args [ 3 ].equals ( "-a" ) )
        {
          main = new AMainSimulator (
            idum,
            args [ 0 ],
            heapsize );
        }
        else
        {
          main = new MainSimulator (
            idum,
            args [ 0 ],
            heapsize );
        }
        
        main.run ( );
      }
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }