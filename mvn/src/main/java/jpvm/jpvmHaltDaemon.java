package jpvm;

public class jpvmHaltDaemon
{

  /**
   * Halts the JPVM daemon currently running on this computer.
   */
  public static void main(String[] args)
  {
    try
    {
      jpvmEnvironment jpvm  = new jpvmEnvironment();
      jpvm.pvm_halt();
    } catch (jpvmException e)
    {
      e.printStackTrace();
    }

  }

}
