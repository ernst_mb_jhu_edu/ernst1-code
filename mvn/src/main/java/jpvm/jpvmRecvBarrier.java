/* jpvmRecvConnection.java
 *
 * The jpvmRecvConnection class implements objects that represent
 * connections to remote jpvm processes from which messages may be
 * received.
 *
 * Adam J Ferrari
 * Sun 05-26-1996
 *
 * Copyright (C) 1996  Adam J Ferrari
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge,
 * MA 02139, USA.
 */

package jpvm;

import java.net.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public
class jpvmRecvBarrier extends Thread {
	private InputStream	instrm;
	public OutputStream	out;

//	public static Map<Integer,Integer> key; 
	public static Map<Integer,CyclicBarrier> number; 
//	public static Map<Integer,Integer> totalNumber; 
	public int tagId; //current tagId



	public jpvmRecvBarrier(Socket sock) {
		if(sock==null) return;
//		if(key == null) key = new HashMap<Integer,Integer>();
		if(number == null) number = new HashMap<Integer,CyclicBarrier>();
//		if(totalNumber == null) totalNumber = new HashMap<Integer,Integer>();
		try {
			instrm = sock.getInputStream();
			out = sock.getOutputStream();
			//System.out.println(sock.getLocalSocketAddress());	
			//System.out.println(sock.getRemoteSocketAddress());	
//			//System.out.println(instrm.read());
			/*

			if(!sock.isConnected())System.out.println("not connected");
			if(instrm ==null) System.out.println("empty sock input");
			BufferedInputStream intrm = new BufferedInputStream(instrm);
			if(intrm ==null) System.out.println("empty buffer input");

			strm = new DataInputStream(intrm);
			if(strm ==null) System.out.println("empty datainputstream input");
			*/
			tagId = instrm.read(); //get tag id
			//System.out.println("receve tagid"+tagId);
			int total = instrm.read(); //get tag id
			//System.out.println("receve total"+total);
			/*
			if(!key.containsKey(tagId))
			{
			 key.put(tagId,new Integer(tagId));
			}
			*/
			CyclicBarrier cyc = number.get(tagId);
			if(cyc==null) 
			{
				number.put(tagId,new CyclicBarrier(total));
			}
			/*
			else
			{
				count = count+1;
				number.put(key.get(tagId),count);
			}
			*/
			//totalNumber.put(key.get(tagId),total);
		}
		catch (IOException ioe) {
			//strm  = null;
			ioe.printStackTrace();
		}
//		if(strm == null) return;
	}

	public void run() {
		//System.out.println("tag id"+tagId);
		try{

			//System.out.println("now"+number.get(key.get(tagId))+" fu:"+totalNumber.get(key.get(tagId)));
			/*
			synchronized(key.get(tagId))
			{
				if(number.get(key.get(tagId)) == totalNumber.get(key.get(tagId))) key.get(tagId).notifyAll();
				while( number.get(key.get(tagId)) != totalNumber.get(key.get(tagId)) )
				{
					key.get(tagId).wait();
				}

			}
			*/
			number.get(tagId).await();

			out.write(0); // read the last element to un block the sender;
			out.flush();
			//System.out.println("read the last one");
			out.close();
			instrm.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		/*
		synchronized(key.get(tagId))
		{
			Integer count = number.get(key.get(tagId));
			count=0;
			number.put(key.get(tagId),count);
		}
		*/
		number.remove(tagId);
	}

};
