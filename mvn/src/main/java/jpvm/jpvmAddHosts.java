package jpvm;

public class jpvmAddHosts
{

  /**
   * Adds hosts to the JPVM daemon running on this computer.
   * @param args hostname1 portnumber1 hostname2 portnumber2 ...
   */
  public static void main(String[] args)
  {
    try {
      jpvmEnvironment jpvm  = new jpvmEnvironment();
      
      int numHosts = args.length / 2;
      
      String hostNames [ ] = new String [ numHosts ];
      jpvmTaskId taskIds [ ] = new jpvmTaskId [ numHosts ];    
      
      for (int i = 0; i < numHosts; i ++) {
        String hostName = args [ i * 2 ];
        int portNumber = Integer.parseInt ( args [ i * 2 + 1] );
        jpvmTaskId tid = new jpvmTaskId ( hostName, portNumber );
        
        hostNames [ i ] = hostName;
        taskIds [ i ] = tid;
      }
      
      jpvm.pvm_addhosts( numHosts , hostNames , taskIds );
      
    } catch ( jpvmException e ){
      e.printStackTrace();
    }
  }

}
