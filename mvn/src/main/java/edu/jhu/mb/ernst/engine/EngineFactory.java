    package edu.jhu.mb.ernst.engine;

    import java.util.Comparator;
    
    /***********************************************************************
    * Factory for creating engine instances.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public interface  EngineFactory
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    Comparator<DiscreteEvent>  createDiscreteEventComparator ( );
    
    DiscreteEventQueue  createDiscreteEventQueue ( );
    
    DiscreteEventQueue  createDiscreteEventQueue (
      Comparator<DiscreteEvent>  discreteEventComparator );
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }