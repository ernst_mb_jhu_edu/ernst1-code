    package edu.jhu.mb.ernst.util.seq;

    /***********************************************************************
    * Read-only access to a sequence such as an Object array or a List.
    * 
    * Pronounced "seek".
    * 
    * For a description, please see the online tutorial
    * <a target="_blank"
    *   href="http://www.CroftSoft.com/library/tutorials/seq">
    * Interface Seq</a>.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2007-04-29
    * @author
    *   <a href="http://www.CroftSoft.com/">David Wallace Croft</a>
    ***********************************************************************/

    public interface  Seq<E>
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    public int  size ( );
    
    public E  get ( int  index );

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }