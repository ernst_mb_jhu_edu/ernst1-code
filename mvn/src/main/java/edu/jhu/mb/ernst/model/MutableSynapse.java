    package edu.jhu.mb.ernst.model;
    
    /***********************************************************************
    * Mutable extension of interface Synapse.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public interface  MutableSynapse
      extends Synapse
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    void  setTargetNeuronIndex ( int  targetNeuronIndex );
    
    void  setType ( byte  type );
    
    void  setWeight ( float  weight );
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }