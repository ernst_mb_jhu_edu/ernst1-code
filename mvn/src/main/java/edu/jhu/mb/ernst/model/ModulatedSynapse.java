    package edu.jhu.mb.ernst.model;
    
    /***********************************************************************
    * A Synapse that can have neuromodulation factors applied.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public interface  ModulatedSynapse
      extends Synapse      
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    /***********************************************************************
    * The weight is immediately restored to the unmodulated value.
    * 
    * All previous modulation factors are permanently forgotten. 
    ***********************************************************************/
    void  clearModulationFactors ( );
      
    /***********************************************************************
    * The modulation factor is applied immediately to the weight.
    ***********************************************************************/
    void  setModulationFactor (
      final String  name,
      final Double  peakFactor );
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }