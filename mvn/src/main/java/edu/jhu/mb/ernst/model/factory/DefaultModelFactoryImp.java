    package edu.jhu.mb.ernst.model.factory;
    
    import edu.jhu.mb.ernst.engine.DiscreteEvent;
    import edu.jhu.mb.ernst.model.*;
    import edu.jhu.mb.ernst.model.event.DefaultModulationDiscreteEventImp;
    import edu.jhu.mb.ernst.model.synapse.MutableSynapseImp;
    import edu.jhu.mb.ernst.model.synapse.ModulatedSynapseImp;
    import edu.jhu.mb.ernst.util.seq.Seq;
    
    /***********************************************************************
    * Default implementation of interface ModelFactory.
    * 
    * @version
    *   $Date: 2012-04-15 13:41:48 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 8 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public class  DefaultModelFactoryImp
      implements ModelFactory
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    public static final DefaultModelFactoryImp
      INSTANCE = new DefaultModelFactoryImp ( );
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  DefaultModelFactoryImp ( )
    ////////////////////////////////////////////////////////////////////////
    {
      // public no-argument constructor to support dynamic class loading
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public ModulatedSynapse  createModulatedSynapse (
      final int    targetNeuronIndex,
      final byte   type,
      final float  weight )      
    ////////////////////////////////////////////////////////////////////////
    {
      return new ModulatedSynapseImp (
        targetNeuronIndex,
        type,
        weight );
    }
    
    @Override
    public DiscreteEvent  createModulationDiscreteEvent (
      final double                time,
      final String                key,
      final double                factor,
      final Seq<ModulatedSynapse>  modulatedSynapseSeq )
    ////////////////////////////////////////////////////////////////////////
    {
      return new DefaultModulationDiscreteEventImp (
        time,
        key,
        factor,
        modulatedSynapseSeq );
    }
    
    @Override
    public MutableSynapse  createMutableSynapse (
      final int    targetNeuronIndex,
      final byte   type,
      final float  weight )      
    ////////////////////////////////////////////////////////////////////////
    {
      return new MutableSynapseImp (
        targetNeuronIndex,
        type,
        weight );
    }
    
    @Override
    public Synapse  createSynapse (
      final int    targetNeuronIndex,
      final byte   type,
      final float  weight )      
    ////////////////////////////////////////////////////////////////////////
    {
      return new ModulatedSynapseImp (
        targetNeuronIndex,
        type,
        weight );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }