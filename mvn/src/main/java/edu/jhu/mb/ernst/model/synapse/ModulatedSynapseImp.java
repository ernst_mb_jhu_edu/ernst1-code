    package edu.jhu.mb.ernst.model.synapse;
    
    import java.util.*;
    
    import edu.jhu.mb.ernst.model.*;
    
    /***********************************************************************
    * Modulated Synapse implementation.
    * 
    * <p>
    * This implementation optimizes read performance at the expense of write
    * performance.  The accessor methods are fast but the modulation factor
    * mutator methods are slow.  Use this implementation when the modulation
    * factors are changed infrequently relative to the number of times the
    * modulated weight is accessed.
    * </p>
    * 
    * <p>
    * This implementation conserves memory by using lazy initialization and
    * by releasing memory allocated to internal data structures when empty.
    * </p>
    * 
    * <p>
    * ModulatedSynapseImp can be used as the default implementation for
    * accessor interface Synapse because its memory consumption and read
    * access performance is similar to MutableSynapseImp when modulation is
    * not used.
    * </p>
    * 
    * @see
    *   MutableSynapseImp
    * 
    * @version
    *   $Date: 2012-04-15 13:47:26 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 9 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  ModulatedSynapseImp
      implements ModulatedSynapse
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private final int
      targetNeuronIndex;

    private final byte
      type;

    private final double
      unmodulatedWeight;
    
    //
    
    private Map<String, Double>
      modulationFactorMap;
  
    private float
      modulatedWeight;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  ModulatedSynapseImp (
      final int    targetNeuronIndex,
      final byte   type,
      final float  unmodulatedWeight )
    ////////////////////////////////////////////////////////////////////////
    {
      this.targetNeuronIndex = targetNeuronIndex;
      
      this.type = type;
      
      this.unmodulatedWeight = unmodulatedWeight;
      
      modulatedWeight = unmodulatedWeight;
    }

    ////////////////////////////////////////////////////////////////////////
    // accessor methods
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public int  getTargetNeuronIndex ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return targetNeuronIndex;
    }
    
    @Override
    public byte  getType ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return type;
    }
    
    @Override
    public float  getWeight ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return modulatedWeight;
    }
    
    ////////////////////////////////////////////////////////////////////////
    // mutator methods
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public void  clearModulationFactors ( )
    ////////////////////////////////////////////////////////////////////////
    {
      if ( modulationFactorMap != null )
      {
        modulationFactorMap = null;
      }
      
      modulatedWeight = ( float ) unmodulatedWeight;
    }      
    
    @Override
    public void  setModulationFactor (
      final String  name,
      final Double  peakFactor )
    ////////////////////////////////////////////////////////////////////////
    {      
      if ( peakFactor == null )
      {
        if ( modulationFactorMap == null )
        {
          // The weight does not need to be recomputed.
          
          return;
        }
        
        final Double  oldValue = modulationFactorMap.remove ( name );
        
        if ( modulationFactorMap.size ( ) == 0 )
        {
          modulationFactorMap = null;
        }
        
        if ( oldValue == null )
        {
          // The weight does not need to be recomputed.
          
          return;
        }
      }
      else
      {      
        if ( modulationFactorMap == null )
        {
          modulationFactorMap = new HashMap<String, Double> ( );
        }
        
        modulationFactorMap.put ( name, peakFactor );
      }
      
      modulatedWeight = ( float ) unmodulatedWeight;
      
      if ( modulationFactorMap != null )
      {      
        for ( final Double  value : modulationFactorMap.values ( ) )
        {
          modulatedWeight *= value.doubleValue ( );
        }
      }
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public int  compareTo ( final Synapse  other )
    ////////////////////////////////////////////////////////////////////////
    {
      return SynapseLib.compareTo ( this, other );
    }
    
    @Override
    public String  toString ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return SynapseLib.toString ( this );
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }