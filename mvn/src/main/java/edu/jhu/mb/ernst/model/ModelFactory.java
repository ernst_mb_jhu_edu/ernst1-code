    package edu.jhu.mb.ernst.model;
    
    import edu.jhu.mb.ernst.engine.DiscreteEvent;
    import edu.jhu.mb.ernst.util.seq.Seq;

    /***********************************************************************
    * Factory for creating model instances.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public interface  ModelFactory
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    ModulatedSynapse  createModulatedSynapse (
      int    targetNeuronIndex,
      byte   type,
      float  weight );
    
    DiscreteEvent  createModulationDiscreteEvent (
      double                 time,
      String                 name,
      double                 peakFactor,
      Seq<ModulatedSynapse>  modulatedSynapseSeq );
    
    MutableSynapse  createMutableSynapse (
      int    targetNeuronIndex,
      byte   type,
      float  weight );
    
    Synapse  createSynapse (
      int    targetNeuronIndex,
      byte   type,
      float  weight );
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }