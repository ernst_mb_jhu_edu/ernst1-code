    package edu.jhu.mb.ernst;

    import cnslab.cnsnetwork.Layer;

    /***********************************************************************
    * Static method library for ERNST.
    *
    * @version
    *   $Date: 2011-12-17 16:00:56 -0600 (Sat, 17 Dec 2011) $
    *   $Rev: 312 $
    *   $Author: croft $
    * @since
    *   2011-06-04
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  ErnstLib
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    public static boolean  equivalent (
      final Layer  layer0,
      final Layer  layer1 )
    ////////////////////////////////////////////////////////////////////////
    {
      if ( layer0 == null )
      {
        return layer1 == null;
      }
      
      if ( layer1 == null )
      {
        return false;
      }
        
      return equivalent (
          layer0.getPrefix ( ),
          layer1.getPrefix ( ) )
        && equivalent (
          layer0.getSuffix ( ),
          layer1.getSuffix ( ) )
        && equivalent (
          layer0.getNeuronType ( ),
          layer1.getNeuronType ( ) )
        && layer0.getMultiplierX ( ) ==
           layer1.getMultiplierX ( )
        && layer0.getMultiplierY ( ) ==
           layer1.getMultiplierY ( );
    }
    
    public static boolean  equivalent (
      final String  string0,
      final String  string1 )
    ////////////////////////////////////////////////////////////////////////
    {
      return string0 == null
        ? string1 == null
        : string0.equals ( string1 );
    }

    /***********************************************************************
    * Placeholder method for JUnit test setup validation.
    ***********************************************************************/
    public static int  negate ( final int  i )
    ////////////////////////////////////////////////////////////////////////
    {
      return -i;
    }
      
    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
    
    private  ErnstLib ( )
    ////////////////////////////////////////////////////////////////////////
    {
      // private constructor
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }