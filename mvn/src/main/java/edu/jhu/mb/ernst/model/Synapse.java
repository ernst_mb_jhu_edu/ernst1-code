    package edu.jhu.mb.ernst.model;
    
    /***********************************************************************
    * Accessor interface for the neuron synaptic structure.
    * 
    * @version
    *   $Date: 2012-04-15 13:41:48 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 8 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public interface  Synapse
      extends Comparable<Synapse>
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    int  getTargetNeuronIndex ( );
    
    byte  getType ( );
    
    float  getWeight ( );
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }