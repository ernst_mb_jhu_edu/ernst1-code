    package edu.jhu.mb.ernst.net;

    import cnslab.cnsnetwork.JpvmInfo;
    
    import jpvm.jpvmException;
    import jpvm.jpvmMessage;
    import jpvm.jpvmTaskId;

    /***********************************************************************
    * Parses network message containing peer information.
    * 
    * Created by NetHost.
    *
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2011-07-23
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  PeerInfo
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    public final int
      numTasks,
      seedInt;
    
    public final int [ ]
      endIndices;
    
    public final jpvmTaskId
      parentJpvmTaskId;
    
    public final jpvmTaskId [ ]
      jpvmTaskIds;
    
    public final byte [ ]
      byteArray;
      
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
      
    public  PeerInfo ( final jpvmMessage  message )
      throws jpvmException
    ////////////////////////////////////////////////////////////////////////
    {
      numTasks = message.buffer.upkint ( );
      
      jpvmTaskIds = new jpvmTaskId [ numTasks ];

      message.buffer.unpack (
        jpvmTaskIds,
        numTasks,
        1 );
      
      endIndices = new int [ numTasks ];

      message.buffer.unpack (
        endIndices,
        numTasks,
        1 );

      seedInt = message.buffer.upkint ( );

      // trialHost

      parentJpvmTaskId = message.buffer.upktid ( );

      final int  baLength = message.buffer.upkint ( );

      byteArray = new byte [ baLength ];

      message.buffer.unpack ( byteArray, baLength, 1 );
    }
    
    public PeerInfo (
      final byte [ ]        byteArray,
      final int [ ]         endIndices,
      final jpvmTaskId [ ]  jpvmTaskIds,
      final int             numTasks,
      final jpvmTaskId      parentJpvmTaskId,
      final int             seedInt )
    ////////////////////////////////////////////////////////////////////////
    {
      this.numTasks = numTasks;
      
      this.seedInt = seedInt;
      
      this.endIndices = endIndices;
      
      this.parentJpvmTaskId = parentJpvmTaskId;
      
      this.jpvmTaskIds = jpvmTaskIds;
      
      this.byteArray = byteArray;
    }
      
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public void  populateJpvmInfo ( final JpvmInfo  jpvmInfo )
    ////////////////////////////////////////////////////////////////////////
    {
      jpvmInfo.numTasks = numTasks;

      jpvmInfo.tids = jpvmTaskIds;
      
      jpvmInfo.endIndex = endIndices;
      
      int  index;
        
      for ( index = 0; index < numTasks; index++ )
      {
        if ( jpvmInfo.myJpvmTaskId.equals ( jpvmTaskIds [ index ] ) )
        {
          break;
        }
      }

      jpvmInfo.idIndex = index;
      
      // trialHost
        
      jpvmInfo.parentJpvmTaskId = parentJpvmTaskId;
      
      // self id should not be stored, change to grandpa's id
        
      jpvmInfo.tids [ index ] = jpvmInfo.jpvm.pvm_parent ( );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }