    package edu.jhu.mb.ernst.engine.queue;

    import java.util.Collection;
    import java.util.Comparator;
    import java.util.PriorityQueue;

    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import edu.jhu.mb.ernst.engine.DiscreteEvent;
    import edu.jhu.mb.ernst.engine.DiscreteEventQueue;

    /***********************************************************************
    * Default implementation of DiscreteEventQueue.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  DefaultDiscreteEventQueueImp
      implements DiscreteEventQueue
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private static final Class<DefaultDiscreteEventQueueImp>
      CLASS = DefaultDiscreteEventQueueImp.class;

    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );
    
    //
  
    private final PriorityQueue<DiscreteEvent>  priorityQueue;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  DefaultDiscreteEventQueueImp (
      final Comparator<DiscreteEvent>  discreteEventComparator )
    ////////////////////////////////////////////////////////////////////////
    {
      priorityQueue
        = new PriorityQueue<DiscreteEvent> ( 1, discreteEventComparator );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    @Override
    public void  add ( final DiscreteEvent...  discreteEvents )
    ////////////////////////////////////////////////////////////////////////
    {
      for ( final DiscreteEvent  discreteEvent : discreteEvents )
      {
        priorityQueue.add ( discreteEvent );
      }
    }
    
    @Override
    public void  addAll (
      final Collection<DiscreteEvent>  discreteEventCollection )
    ////////////////////////////////////////////////////////////////////////
    {
      LOGGER.debug (
        "discreteEventCollection size:  {},",
        Integer.valueOf ( discreteEventCollection.size ( ) ) );
      
      LOGGER.debug (
        "priorityQueue size before addAll:  {},",
        Integer.valueOf ( priorityQueue.size ( ) ) );
      
      priorityQueue.addAll ( discreteEventCollection );
      
      LOGGER.debug (
        "priorityQueue size after addAll:  {},",
        Integer.valueOf ( priorityQueue.size ( ) ) );
    }
    
    @Override
    public void  clear ( )
    ////////////////////////////////////////////////////////////////////////
    {
      priorityQueue.clear ( );
    }
    
    @Override
    public boolean  offer ( DiscreteEvent  discreteEvent )
    ////////////////////////////////////////////////////////////////////////
    {
      return priorityQueue.add ( discreteEvent );
    }
    
    @Override
    public DiscreteEvent  peek ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return priorityQueue.peek ( );
    }
    
    @Override
    public DiscreteEvent  poll ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return priorityQueue.poll ( );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }