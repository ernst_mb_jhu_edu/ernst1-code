    package edu.jhu.mb.ernst.model.synapse;

    import edu.jhu.mb.ernst.model.Synapse;
    
    /***********************************************************************
    * Static method library to manipulate Synapse instances.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  SynapseLib
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    public static int  compareTo (
      final Synapse  synapse0,
      final Synapse  synapse1 )
    ////////////////////////////////////////////////////////////////////////
    {
      final int
        targetNeuronIndex0 = synapse0.getTargetNeuronIndex ( ),
        targetNeuronIndex1 = synapse1.getTargetNeuronIndex ( );
      
      if ( targetNeuronIndex0 < targetNeuronIndex1 )
      {
        return -1;
      }
      
      if ( targetNeuronIndex0 > targetNeuronIndex1 )
      {
        return 1;
      }
      
      return 0;
    }
    
    public static String  toString ( final Synapse  synapse )
    ////////////////////////////////////////////////////////////////////////
    {
      return "To:" + synapse.getTargetNeuronIndex ( )
        + " Weight:" + synapse.getWeight ( )
        + " Type:" + synapse.getType ( );
    }    
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    private  SynapseLib ( )
    ////////////////////////////////////////////////////////////////////////
    {
      // private constructor
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }
