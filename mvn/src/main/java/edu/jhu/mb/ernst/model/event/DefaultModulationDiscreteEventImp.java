    package edu.jhu.mb.ernst.model.event;
    
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import edu.jhu.mb.ernst.engine.DiscreteEvent;
    import edu.jhu.mb.ernst.model.ModulatedSynapse;
    import edu.jhu.mb.ernst.util.seq.Seq;

    /***********************************************************************
    * Default implementation of a Modulation DiscreteEvent.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  DefaultModulationDiscreteEventImp
      implements DiscreteEvent
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private static final Class<DefaultModulationDiscreteEventImp>
      CLASS = DefaultModulationDiscreteEventImp.class;
 
    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );
    
    //
  
    private final double  time;
    
    private final String  key;
    
    private final double  factor;
    
    private final Seq<ModulatedSynapse>  modulatedSynapseSeq;
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  DefaultModulationDiscreteEventImp (
      final double  time,
      final String  key,
      final double  factor,
      final Seq<ModulatedSynapse>  modulatedSynapseSeq )
    ////////////////////////////////////////////////////////////////////////
    {
      this.time = time;
      
      this.key = key;
      
      this.factor = factor;
      
      this.modulatedSynapseSeq = modulatedSynapseSeq;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public double  getTime ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return time;
    }

    @Override
    public boolean  process ( )
    ////////////////////////////////////////////////////////////////////////
    {
      LOGGER.trace ( "process() entered" );
      
      final int  size = modulatedSynapseSeq.size ( );

      for ( int  i = 0; i < size; i++ )
      {
        final ModulatedSynapse
          modulatedSynapse = modulatedSynapseSeq.get ( i );
        
        modulatedSynapse.setModulationFactor ( key, factor );
      }
      
      LOGGER.trace ( "process() exiting" );

      return false;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }