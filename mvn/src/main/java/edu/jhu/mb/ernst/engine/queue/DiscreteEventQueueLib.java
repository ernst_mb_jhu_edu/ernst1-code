    package edu.jhu.mb.ernst.engine.queue;

    import java.util.Comparator;
    import java.util.PriorityQueue;

    import edu.jhu.mb.ernst.engine.DiscreteEvent;
    import edu.jhu.mb.ernst.engine.DiscreteEventQueue;

    /***********************************************************************
    * Static method library to manipulate DiscreteEventQueue instances.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  DiscreteEventQueueLib
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    public static void  process (
      final DiscreteEventQueue  discreteEventQueue )
    ////////////////////////////////////////////////////////////////////////
    {
      final DiscreteEvent  discreteEvent = discreteEventQueue.poll ( );
      
      if ( discreteEvent == null )
      {
        return;
      }
      
      final boolean  reinsert = discreteEvent.process ( );
      
      if ( reinsert )
      {
        discreteEventQueue.offer ( discreteEvent );
      }
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    private  DiscreteEventQueueLib ( )
    ////////////////////////////////////////////////////////////////////////
    {
      // private constructor
    }    
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }