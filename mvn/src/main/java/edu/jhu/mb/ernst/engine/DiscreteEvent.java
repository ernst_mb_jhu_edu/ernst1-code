    package edu.jhu.mb.ernst.engine;
    
    /***********************************************************************
    * A generic interface for discrete event simulation.
    * 
    * At the designated event time, the event is processed.  The event can
    * then be reinserted into the time-sorted event queue with a new time.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public interface  DiscreteEvent
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    /***********************************************************************
    * Gets the event time. 
    ***********************************************************************/ 
    double  getTime ( );
    
    /***********************************************************************
    * Called at the event time to process the discrete event.
    * 
    * @return
    *   Returns true if the event should be reinserted into the queue. 
    ***********************************************************************/ 
    boolean  process ( );
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }