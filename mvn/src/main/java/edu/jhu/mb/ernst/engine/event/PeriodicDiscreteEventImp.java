    package edu.jhu.mb.ernst.engine.event;

    import edu.jhu.mb.ernst.engine.DiscreteEvent;
    
    /***********************************************************************
    * DiscreteEvent implementation that runs periodically.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  PeriodicDiscreteEventImp
      implements DiscreteEvent
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private final Runnable  runnable;
    
    private final double    period;
    
    private final double    stop;
    
    //
    
    private double  time;
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  PeriodicDiscreteEventImp (
      final Runnable  runnable,
      final double    period,
      final double    start,
      final double    stop )
    ////////////////////////////////////////////////////////////////////////
    {
      this.runnable = runnable;
      
      this.period = period;
      
      this.stop = stop;
      
      time = start;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public double  getTime ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return time;
    }

    @Override
    public boolean  process ( )
    ////////////////////////////////////////////////////////////////////////
    {
      runnable.run ( );
      
      // TODO:  subject to drift; eliminate using integer tick
      
      time += period;
      
      return time < stop;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }