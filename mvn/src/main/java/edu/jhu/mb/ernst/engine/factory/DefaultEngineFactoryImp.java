    package edu.jhu.mb.ernst.engine.factory;
    
    import java.util.Comparator;

    import edu.jhu.mb.ernst.engine.DiscreteEvent;
    import edu.jhu.mb.ernst.engine.DiscreteEventQueue;
    import edu.jhu.mb.ernst.engine.EngineFactory;
    import edu.jhu.mb.ernst.engine.event.DefaultDiscreteEventComparatorImp;
    import edu.jhu.mb.ernst.engine.queue.DefaultDiscreteEventQueueImp;

    /***********************************************************************
    * Default implementation of interface EngineFactory.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public class  DefaultEngineFactoryImp
      implements EngineFactory
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    public static final DefaultEngineFactoryImp
      INSTANCE = new DefaultEngineFactoryImp ( );
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  DefaultEngineFactoryImp ( )
    ////////////////////////////////////////////////////////////////////////
    {
      // public no-argument constructor to support dynamic class loading
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public Comparator<DiscreteEvent>  createDiscreteEventComparator ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return DefaultDiscreteEventComparatorImp.INSTANCE;
    }
    
    @Override
    public DiscreteEventQueue  createDiscreteEventQueue ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return new DefaultDiscreteEventQueueImp (
        createDiscreteEventComparator ( ) );
    }
    
    @Override
    public DiscreteEventQueue  createDiscreteEventQueue (
      final Comparator<DiscreteEvent>  discreteEventComparator )
    ////////////////////////////////////////////////////////////////////////
    {
      return new DefaultDiscreteEventQueueImp ( discreteEventComparator );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }