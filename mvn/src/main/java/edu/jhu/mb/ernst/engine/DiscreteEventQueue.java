    package edu.jhu.mb.ernst.engine;

    import java.util.Collection;

    import edu.jhu.mb.ernst.util.slot.Slot;
    
    /***********************************************************************
    * Factory for creating engine instances.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public interface  DiscreteEventQueue
      extends Slot<DiscreteEvent>
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    void  add ( DiscreteEvent...  discreteEvents );
    
    void  addAll ( Collection<DiscreteEvent>  discreteEventCollection );
    
    void  clear ( );
    
    DiscreteEvent  peek ( );
    
    DiscreteEvent  poll ( );
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }