    package edu.jhu.mb.ernst.util.slot;

    /***********************************************************************
    * Mail slot for receiving messages to be processed, stored, or relayed. 
    *
    * Please see the online tutorial <a target="_blank"
    * href="http://www.CroftSoft.com/library/tutorials/slot/">
    * Interface Slot</a>.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2007-01-28
    * @author
    *   David Wallace Croft
    ***********************************************************************/

    public interface  Slot<E>
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    boolean  offer ( E  e );

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }
