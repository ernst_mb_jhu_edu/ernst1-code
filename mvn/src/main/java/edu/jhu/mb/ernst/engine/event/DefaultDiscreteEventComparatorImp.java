    package edu.jhu.mb.ernst.engine.event;
    
    import java.util.Comparator;

    import edu.jhu.mb.ernst.engine.DiscreteEvent;
    
    /***********************************************************************
    * Default implementation of a DiscreteEvent Comparator.
    * 
    * @version
    *   $Date: 2012-04-15 13:06:25 -0500 (Sun, 15 Apr 2012) $
    *   $Rev: 7 $
    *   $Author: croft $
    * @since
    *   2012-02-19
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  DefaultDiscreteEventComparatorImp
      implements Comparator<DiscreteEvent>
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    public static final DefaultDiscreteEventComparatorImp
      INSTANCE = new DefaultDiscreteEventComparatorImp ( );
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  DefaultDiscreteEventComparatorImp ( )
    ////////////////////////////////////////////////////////////////////////
    {
      // public no-argument constructor to support dynamic class loading
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public int  compare (
      final DiscreteEvent  discreteEvent0,
      final DiscreteEvent  discreteEvent1 )
    ////////////////////////////////////////////////////////////////////////
    {
      final double
        time0 = discreteEvent0.getTime ( ),
        time1 = discreteEvent1.getTime ( );
      
      if ( time0 < time1 )
      {
        return -1;
      }
      
      if ( time0 > time1 )
      {
        return 1;
      }
      
      return 0;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }