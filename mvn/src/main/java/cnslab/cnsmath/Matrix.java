package cnslab.cnsmath;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
public class Matrix<T>
{
	private T[][] A;
	private int m, n;

	public Matrix (int m, int n) {
		this.m = m;
		this.n = n;
		A = (T[][]) new Object[m][n];
	}

	public Matrix (int m, int n, T s) {
		this.m = m;
		this.n = n;
		A = (T[][]) new Object[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				A[i][j] = s;
			}
		}
	}

	/** Construct a matrix from a 2-D array.
	  @param A    Two-dimensional array of doubles.
	  @exception  IllegalArgumentException All rows must have the same length
	  @see        #constructWithCopy
	  */

	public Matrix (T[][] A) {
		m = A.length;
		n = A[0].length;
		for (int i = 0; i < m; i++) {
			if (A[i].length != n) {
				throw new IllegalArgumentException("All rows must have the same length.");
			}
		}
		this.A = A;
	}


	/** Construct a matrix from a one-dimensional packed array
	  @param vals One-dimensional array of doubles, packed by rows (ala C++).
	  @param m    Number of rows.
	  @exception  IllegalArgumentException Array length must be a multiple of m.
	  */

	public Matrix (T vals[], int m) {
		this.m = m;
		n = (m != 0 ? vals.length/m : 0);
		if (m*n != vals.length) {
			throw new IllegalArgumentException("Array length must be a multiple of m.");
		}
		A = (T[][]) new Object[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				A[i][j] = vals[i*m+j];
			}
		}
	}


	/** Print the matrix to stdout.  Line the elements up in columns.
	 * Use the format object, and right justify within columns of width
	 * characters.                                                                                             
	 * Note that is the matrix is to be read back in, you probably will want                                             
	 * to use a NumberFormat that is set to US Locale.                                                                   
	 @param format A  Formatting object for individual elements.
	 @param width     Field width for each column.
	 @see java.text.DecimalFormat#setDecimalFormatSymbols
	 */
	public void print (PrintWriter output, NumberFormat format, int width) {
		output.println();  // start on new line.
		for (int i = 0; i < m; i++) {  
			for (int j = 0; j < n; j++) {
				String s = format.format(A[i][j]); // format the number
				int padding = Math.max(1,width-s.length()); // At _least_ 1 space
				for (int k = 0; k < padding; k++)
					output.print(' ');
				output.print(s);
			}
			output.println();
		}
		output.println();   // end with blank line.
	}

	/** Print the matrix to the output stream.   Line the elements up in
	 * columns with a Fortran-like 'Fw.d' style format.
	 @param output Output stream.
	 @param w      Column width.
	 @param d      Number of digits after the decimal.
	 */
	public void print (PrintWriter output, int w, int d) {
		DecimalFormat format = new DecimalFormat();
		format.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
		format.setMinimumIntegerDigits(1);
		format.setMaximumFractionDigits(d);
		format.setMinimumFractionDigits(d);
		format.setGroupingUsed(false);
		print(output,format,w+2);
	}

	/** Print the matrix to stdout.   Line the elements up in columns
	 * with a Fortran-like 'Fw.d' style format.
	 @param w    Column width.
	 @param d    Number of digits after the decimal.
	 */
	public void print (int w, int d) {
		print(new PrintWriter(System.out,true),w,d); }

	public void print () {
		print(new PrintWriter(System.out,true),2,1); }
}
