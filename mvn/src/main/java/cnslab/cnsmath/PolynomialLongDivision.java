package cnslab.cnsmath;
import java.math.BigDecimal;
import java.math.RoundingMode;
public class PolynomialLongDivision
{
//	public static final double  ACC = 1e-15; // accurace for polynomial division
	public static final double  ACC = 0.0; // accurace for polynomial division

	//returns the remainder of the polynomial long division:
	public static Polynomial getRemainder(Polynomial numerator, Polynomial denominator)
	{
		double[][] table=new double[numerator.coefficients.length-denominator.coefficients.length+3][numerator.coefficients.length];//row,col
		//fill in first row:
		for(int a=0;a<denominator.coefficients.length;a++)
			table[0][a]=denominator.coefficients[denominator.coefficients.length-a-1];
		//fill in second row:
		for(int a=0;a<numerator.coefficients.length;a++)
		      	table[1][a]=numerator.coefficients[numerator.coefficients.length-a-1];
		
		//fill in the rest of the rows:
		for(int row=2;row<table.length;row++)
		{
			for(int col=0;col<table[0].length-row+1;col++)
			{
//				table[row][col]=(table[0][0]*table[row-1][col+1]-table[0][1+col]*table[row-1][0])/table[0][0];
				table[row][col]=(table[row-1][col+1]-table[0][1+col]*table[row-1][0]/table[0][0]);
//				if(Math.abs(table[row][col])<1e-15)table[row][col]=0.0;
			}
		}
		
		//last row of table makes the coefficients, in reverse order!
		int highestDegree=denominator.coefficients.length-2;
		for(int a=0;a<denominator.coefficients.length-1;a++)
		{
//			if(table[table.length-1][a]==0.0)
			if(Math.abs(table[table.length-1][a])<=ACC)
				highestDegree--;
			else
				break;
		}
		if(highestDegree<0)
			highestDegree=0;
		//System.out.println("highestDegree: " +highestDegree);
		double[] remainder=new double[highestDegree+1];
		for(int a=0;a<remainder.length;a++)
			remainder[a]=(Math.abs(table[table.length-1][denominator.coefficients.length-2-a])<=ACC?0:table[table.length-1][denominator.coefficients.length-2-a]);
//			remainder[a]=table[table.length-1][denominator.coefficients.length-2-a];
		
		//print table:
		//System.out.println("Table:");
		//for(int a=0;a<table.length;a++)
		//{
		//	for(int b=0;b<table[0].length;b++)
		//		System.out.print(table[a][b] + " ");
		//	System.out.println("");
		//}
		//table=null;
		return new Polynomial(remainder);
	}
	//old implementation
/*
	public static Polynomial getNegRemainder(Polynomial numerator, Polynomial denominator)
	{
		double[][] table=new double[numerator.coefficients.length-denominator.coefficients.length+3][numerator.coefficients.length];//row,col
		//fill in first row:
		for(int a=0;a<denominator.coefficients.length;a++)
			table[0][a]=denominator.coefficients[denominator.coefficients.length-a-1]/denominator.coefficients[denominator.coefficients.length-1];
		//fill in second row:
		for(int a=0;a<numerator.coefficients.length;a++)
		      	table[1][a]=numerator.coefficients[numerator.coefficients.length-a-1];
		
		//fill in the rest of the rows:
		for(int row=2;row<table.length;row++)
		{
			if(table[row-1][0]==0) //copy the line row-1 to row, left shift by 1
			{
				System.arraycopy(table[row-1],1,table[row],0,table[0].length-row+1);
			}
			else
			{
				for(int col=0;col<table[0].length-row+1;col++)
				{
					//			table[row][col]=(table[0][0]*table[row-1][col+1]-table[0][1+col]*table[row-1][0])/table[0][0];
					//				table[row][col]=(table[row-1][col+1]-table[0][1+col]*table[row-1][0]/table[0][0]);
					table[row][col]=(table[row-1][col+1]-table[0][1+col]*table[row-1][0]);
					//				if(Math.abs(table[row][col])<1e-15)table[row][col]=0.0;
				}
			}
		}
		
		//last row of table makes the coefficients, in reverse order!
		int highestDegree=denominator.coefficients.length-2;
		for(int a=0;a<denominator.coefficients.length-1;a++)
		{
//			if(table[table.length-1][a]==0.0)
			if(Math.abs(table[table.length-1][a])<=ACC)
				highestDegree--;
			else
				break;
		}
		if(highestDegree<0)
			highestDegree=0;
		//System.out.println("highestDegree: " +highestDegree);
		double[] remainder=new double[highestDegree+1];
		for(int a=0;a<remainder.length;a++)
			remainder[a]=(Math.abs(table[table.length-1][denominator.coefficients.length-2-a])<=ACC?0:-table[table.length-1][denominator.coefficients.length-2-a]);
//			remainder[a]=table[table.length-1][denominator.coefficients.length-2-a];
		
		//print table:
		//System.out.println("Table:");
		//for(int a=0;a<table.length;a++)
		//{
		//	for(int b=0;b<table[0].length;b++)
		//		System.out.print(table[a][b] + " ");
		//	System.out.println("");
		//}
		//table=null;
		return new Polynomial(remainder);
	}
*/

	//new one from nr
	/*
	public static Polynomial getNegRemainder(Polynomial numerator, Polynomial denominator)
	{
		int k,j;
		int n=numerator.coefficients.length-1;
		int nv=denominator.coefficients.length-1;
		BigDecimal [] r = new BigDecimal [n+1];
		BigDecimal [] q = new BigDecimal [n+1];
		double [] out;

		for (j=0;j<=n;j++) {
			r[j]=new BigDecimal(numerator.coefficients[j]);
			q[j]=new BigDecimal(0.0);
		}
		for (k=n-nv;k>=0;k--) {
			q[k]=new BigDecimal(r[nv+k].doubleValue()/denominator.coefficients[nv]);
			for (j=nv+k-1;j>=k;j--) r[j] = r[j].subtract(q[k].multiply(new BigDecimal(denominator.coefficients[j-k])));
		}
	// we need negative Remainder
		for (j=0;j<nv;j++) r[j]=r[j].negate();
		for(int a=nv-1;a>=0;a--)
		{
			if(Math.abs(r[a].doubleValue())!=0)
			{
				out=new double[a+1];
				for (int e=0;e<a+1;e++) out[e]=r[a].doubleValue();
				return new Polynomial(out);
			}
		}
		return new Polynomial(new double [] {0.0});
	}
	*/
	//new one from nr for doesfire
	public static Polynomial getNegRemainder(Polynomial numerator, Polynomial denominator)
	{
		int k,j;
		int n=numerator.coefficients.length-1;
		int nv=denominator.coefficients.length-1;
		double [] r = new double [n+1];
		double [] q = new double [n+1];
		double [] out;

		for (j=0;j<=n;j++) {
			r[j]=numerator.coefficients[j];
			q[j]=0.0;
		}
		for (k=n-nv;k>=0;k--) {
			q[k]=r[nv+k]/denominator.coefficients[nv];
			for (j=nv+k-1;j>=k;j--) r[j] -= q[k]*denominator.coefficients[j-k];
		}
	// we need negative Remainder
		for (j=0;j<nv;j++) r[j]=-r[j];
		for(int a=nv-1;a>=0;a--)
		{
//			if(Math.abs(r[a])!=0)
			if(Math.abs(r[a])>1e-9)
			{
				out=new double[a+1];
				System.arraycopy(r,0,out,0,a+1);
				return new Polynomial(out);
			}
		}
		return new Polynomial(new double [] {0.0});
	}

	//new one from nr for time of fire
	public static Polynomial getNegRemainderA(Polynomial numerator, Polynomial denominator)
	{
		int k,j;
		int n=numerator.coefficients.length-1;
		int nv=denominator.coefficients.length-1;
		double [] r = new double [n+1];
		double [] q = new double [n+1];
		double [] out;

		for (j=0;j<=n;j++) {
			r[j]=numerator.coefficients[j];
			q[j]=0.0;
		}
		for (k=n-nv;k>=0;k--) {
			q[k]=r[nv+k]/denominator.coefficients[nv];
			for (j=nv+k-1;j>=k;j--) r[j] -= q[k]*denominator.coefficients[j-k];
		}
	// we need negative Remainder
		for (j=0;j<nv;j++) r[j]=-r[j];
		for(int a=nv-1;a>=0;a--)
		{
			if(Math.abs(r[a])!=0)
//			if(Math.abs(r[a])>1e-13)
			{
				out=new double[a+1];
				System.arraycopy(r,0,out,0,a+1);
				return new Polynomial(out);
			}
		}
		return new Polynomial(new double [] {0.0});
	}
}
