package cnslab.cnsmath;
public class KCelement implements Comparable<KCelement>
{
	public KCelement(int c ,int k) {
		if(k>32)throw new RuntimeException("the k level is too deep");
		this.c =c ;
		this.k =k ;
	}

	public int k;
	public int c;
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object) compareTo
	 */
	public int compareTo(KCelement arg0) {
		if((double)c/(double)(1<<k)==(double)arg0.c/((double)(1<<(arg0.k))))
		{
			if(k<arg0.k)
				return -1;
			else if(k>arg0.k)
				return 1;
			else return 0;
		}
		else if((double)c/(double)(1<<k)<(double)arg0.c/((double)(1<<(arg0.k)))) 
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}

	/**
	 * @see java.lang.Object#toString() toString
	 */
	public String toString() {
		return new String("KC: c"+c+",k"+k+" interval:["+((double)c/((double)(1<<k)))+","+(((double)c+1)/((double)(1<<k)))+"]");
	}
}
