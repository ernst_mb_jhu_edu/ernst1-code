package cnslab.cnsmath;
public class Cnsran
{
	public  static int idum2=123456789,iy=0;
	public  static int[] iv=new int[32];
	private	static int iset=0;
	private	static double gset;

	public static double ran2(Seed idum)
	{       
		int IM1=2147483563,IM2=2147483399;
		int IA1=40014,IA2=40692,IQ1=53668,IQ2=52774;
		int IR1=12211,IR2=3791,NTAB=32,IMM1=IM1-1;
		int NDIV=1+IMM1/NTAB;
		double EPS=3.0e-16,RNMX=1.0-EPS,AM=1.0/(double)(IM1);
		int j,k;
		double temp;

		if (idum.seed <= 0) {
			idum.seed=(idum.seed==0 ? 1 : -idum.seed);
			idum2=idum.seed;
			for (j=NTAB+7;j>=0;j--) {
				k=idum.seed/IQ1;
				idum.seed=IA1*(idum.seed-k*IQ1)-k*IR1;
				if (idum.seed < 0) idum.seed += IM1;
				if (j < NTAB) iv[j] = idum.seed;
			}
			iy=iv[0];
		}
		k=idum.seed/IQ1;
		idum.seed=IA1*(idum.seed-k*IQ1)-k*IR1;
		if (idum.seed < 0) idum.seed += IM1;
		k=idum2/IQ2;
		idum2=IA2*(idum2-k*IQ2)-k*IR2;
		if (idum2 < 0) idum2 += IM2;
		j=iy/NDIV;
		iy=iv[j]-idum2;
		iv[j] = idum.seed;
		if (iy < 1) iy += IMM1;
		if ((temp=AM*iy) > RNMX) return RNMX;
		else return temp;
	}


	public static double ran1(Seed idum)
	{       
		int IA=16807,IM=2147483647,IQ=127773,IR=2836,NTAB=32;
		int NDIV=(1+(IM-1)/NTAB);
		double EPS=3.0e-16,AM=1.0/IM,RNMX=(1.0-EPS);
		int j,k;
		double temp;

		if (idum.seed <= 0 || iy==0) {
			if (-idum.seed < 1) idum.seed=1;
			else idum.seed = -idum.seed;
			for (j=NTAB+7;j>=0;j--) {
				k=idum.seed/IQ;
				idum.seed=IA*(idum.seed-k*IQ)-IR*k;
				if (idum.seed < 0) idum.seed += IM;
				if (j < NTAB) iv[j] = idum.seed;
			}
			iy=iv[0];
		}
		k=idum.seed/IQ;
		idum.seed=IA*(idum.seed-k*IQ)-IR*k;
		if (idum.seed < 0) idum.seed += IM;
		j=iy/NDIV;
		iy=iv[j];
		iv[j] = idum.seed;
		if ((temp=AM*iy) > RNMX) return RNMX;
		else return temp;
	}

	public static double gasdev(Seed idum)
	{       
		double fac,rsq,v1,v2;

		if (idum.seed < 0) iset=0;
		if (iset == 0) {
			do {    
				v1=2.0*ran1(idum)-1.0;
				v2=2.0*ran1(idum)-1.0;
				rsq=v1*v1+v2*v2;
			} while (rsq >= 1.0 || rsq == 0.0);
			fac=Math.sqrt(-2.0*Math.log(rsq)/rsq);
			gset=v1*fac;
			iset=1;
			return v2*fac;
		} else {
			iset=0;
			return gset;
		}
	}
}
