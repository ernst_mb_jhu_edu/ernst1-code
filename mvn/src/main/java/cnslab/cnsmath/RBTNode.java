package cnslab.cnsmath;
public class RBTNode<T extends Comparable>
{

        /**
        * The satellite data in the node.
        *
        */  
	public T data;

    /**
         * The color node of current node.
         *
         * @see #setBlack()
         * @see #setRed()
     */ 

        protected boolean isRed;

    /**
     * The parent node of current node.
     *
     * @see #parent()
     * @see #parentTo
     */ 
        protected RBTNode<T> p;

        /**
         * The left child node of current node.
         *
         * @see #left()
         * @see #leftTo
     */ 
        protected RBTNode<T> left;


        /**
         * The right child node of current node.
         *
         * @see #right()
         * @see #rightTo
     */ 
        protected RBTNode<T> right;

	/**       
         * Sets the data to o.
         *
         * @param o             The new data of the node.
         */
        public void objectTo(T o)
        {       
                this.data = o;
        }

        /**
         * Returns the satellite data of the node.
         *
         * @return    The satellite object of the node.
         */
        public T object()
        {       
                return this.data;
        }

        public RBTNode(T data)
        {       
		this.data=data;
                this.isRed = true ;
		p = null;
		left = null;
		right = null;
	} 

        public RBTNode()
        {       
		this.data=null;
                this.isRed = true ;
		p = null;
		left = null;
		right = null;
	} 


         /**
         * Returns the parent of the node.
         *
         * @return    The parent of the node.
         */
        public RBTNode<T> parent()
        {
                return this.p;
        }

         /**
         * Returns the right child of the node.
         *
         * @return    The right child of the node.
         */
        public RBTNode<T> right()
        {
                return this.right;
        }

         /**
         * Returns the left child of the node.
         *
         * @return    The left child of the node.
         */
        public RBTNode<T> left()
        {
                return this.left;
        }

        /**
         * Returns the node as a <code>String</code>.
         *
         * @return    The node as a <code>String</code>.
         */
        public String toString()
        {
		String color;
		if(isRed)
			color="red";
		else
			color="black";
                //return new String("data: "+data+", color: "+color+", parent: "+p+";");
		return new String("data: "+data+", color: "+color+" ;");
        }
        /**
         * Sets the color of the node to black.
         *
         */
        public void setBlack()
        {
                this.isRed = false;
        }

        /**
         *  color of the node to black.
         *
         */
        public boolean isBlack()
        {
                return !(this.isRed);
        }


        /**
         * Sets the color of the node to red.
         *
         */
        public void setRed()
        {
                this.isRed = true;
        }

        /**
         * Sets the parent node to parent.
         *
         * @param parent        The new parent node of the node.
         */
        public void parentTo(RBTNode<T> parent)
        {
                this.p = parent;
        }

        /**
         * Sets the right child node to parent.
         *
         * @param r             The new right child node of the node.
         */
        public void rightTo(RBTNode<T> r)
        {
                this.right = r;
        }

        /**
         * Sets the left child node to parent.
         *
         * @param l             The new left child node of the node.
         */
        public void leftTo(RBTNode<T> l)
        {
                this.left = l;
        }
}
