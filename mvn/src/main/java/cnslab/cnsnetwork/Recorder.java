    package cnslab.cnsnetwork;
    
    import java.io.Serializable;
    import java.util.ArrayList;

    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    /***********************************************************************
    * The class used to record neuron activities.
    *
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  Recorder
      implements Serializable, Transmissible
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    private static final Class<Recorder>
      CLASS = Recorder.class;

    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );
    
    private static final long  serialVersionUID = 0L;
    
    //

    public String outputFile;

    public boolean plot;

    public SimulatorParser simulatorParser;
    
    //type 1 single unit electrode.
    /**
     *  Single unit array, stored the array of recorded index of neurons
     */
    public ArrayList<Integer>  singleUnit;
    
    /**
     *  Single unit name array
     */
    public ArrayList<String>  suNames;

    //type 2 multiunit electrode.
    /**
     *  Multi unit array, stored the array of recorded index of neurons
     */
    public ArrayList<ArrayList<Integer>>  multiUnit;
    
    /**
     *  Multi unit array, stored the array of recorded neuron names
     */
    public ArrayList<ArrayList<String>>  neuronNames;
    
    /**
     *  Multi unit recorder names 
     */
    public ArrayList<String>  multiNames;
    
    /**
     *  time resolution
     */
    public double timeBinSize; 

    //type 3a field electrode. 
    public ArrayList<Layer>  fieldEle;
    
    public ArrayList<String>  fieldNames;
    
    public double fieldTimeBinSize; 

    //type 3b vector electrode.
    public ArrayList<VComponents>  vectorUnit;
    
    public ArrayList<String>  vectorNames;
    
    public double vectorTimeBinSize;

    //type 4 curr and memV intracellular electrode.
    /**
     * Intracelluar electrodes, pool the inner arraylist of neurons info
     * together
     */
    public ArrayList<ArrayList<Integer>> intraEle;
    
    /**
     * Intracelluar electrodes, the current channels that needs to be
     * recorded.
     */
    public ArrayList<ArrayList<Integer>> currChannel;
    
    public ArrayList<String> intraNames;
    
    public double intraTimeBinSize;

    ////////////////////////////////////////////////////////////////////////
    // constructor methods
    ////////////////////////////////////////////////////////////////////////
    
    public  Recorder (
      final SimulatorParser  simulatorParser,
      final boolean          plot )
    ////////////////////////////////////////////////////////////////////////
    {
      this.simulatorParser = simulatorParser;
      
      this.plot = plot;
      
      singleUnit = new ArrayList<Integer> ( );
      
      suNames = new ArrayList<String> ( );

      multiUnit = new ArrayList< ArrayList<Integer> >();
      neuronNames = new ArrayList< ArrayList<String> >();
      multiNames = new ArrayList< String >();

      fieldEle = new ArrayList< Layer >();
      fieldNames = new  ArrayList< String >();

      vectorUnit = new ArrayList< VComponents >();
      vectorNames = new ArrayList< String > ();

      intraEle = new ArrayList< ArrayList<Integer> >();
      currChannel = new ArrayList< ArrayList<Integer> >();
      intraNames = new  ArrayList< String >();
      outputFile="";
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public int  intraIndex ( final int  neuron )
    ////////////////////////////////////////////////////////////////////////
    {
      for ( int  i = 0; i < intraEle.size ( ); i++ )
      {
        for ( int  j = 0; j < intraEle.get ( i ).size ( ); j++ )
        {
          if ( intraEle.get ( i ).get ( j ).intValue ( ) == neuron )
          {
            return i;
          }
        }
      }
      
      throw new RuntimeException ( "recorded wrong cell" );
    }

    public int [ ]  intraNeurons ( )
    ////////////////////////////////////////////////////////////////////////
    {
      ArrayList<Integer> allNeurons = new ArrayList<Integer>();
      
      for(int i =0 ; i< intraEle.size(); i++)
      {
        allNeurons.addAll(intraEle.get(i));
      }
      
      int [] tmp = new int[allNeurons.size()];
      
      int i=0;
      
      for( int x : allNeurons)
      {
        tmp[i] = x;
        
        i++;
      }
      
      return tmp;
    }

    public VComponents addComs()
    ////////////////////////////////////////////////////////////////////////
    {
      VComponents vs;
      vectorUnit.add(vs=new VComponents());
      return vs;
    }

    /***********************************************************************
    * Set the layer of neurons which needs to be recorded.
    * 
    * @param layer
    ***********************************************************************/
    public void setLayerNeurons(Layer layer, int hostId)
    ////////////////////////////////////////////////////////////////////////
    {
      int xTotal=0;           
      int yTotal=0;
      int xstep=0;
      int ystep=0;

      int xMul = layer.getMultiplierX();
      int yMul = layer.getMultiplierY();

      if(xMul < 0 )
      {           
        xTotal=simulatorParser.xEdgeLength;
        xstep=-xMul;
      }
      else
      {       
        xTotal=simulatorParser.xEdgeLength*xMul;
        xstep=1;
      } 
      if(yMul < 0 )
      {       
        yTotal=simulatorParser.yEdgeLength;
        ystep=-yMul;
      }
      else
      {       
        yTotal=simulatorParser.yEdgeLength*yMul;
        ystep=1;
      }

      for( int x = 0; x< xTotal; x+=xstep)
      {
        for(int y = 0; y< yTotal; y+=ystep)
        {
          // String neu
          //   = layer.getPrefix()+","+x+","+y+","+layer.getSuffix();

          int  id = simulatorParser.layerStructure.cellmap_slow (
            layer.getPrefix(),
            layer.getSuffix(),x,
            y);
          
          if(id!=-1)
          {
            if ( FunUtil.hostId (
              simulatorParser.layerStructure.nodeEndIndices, id )
              == hostId )
            {
              simulatorParser.layerStructure.neurons [
                id - simulatorParser.layerStructure.base ]
                  .setRecord ( true );
            }
          }
          else
          {
            // throw new RuntimeException("not in the grid");
          }
        }
      }
    }

    /***********************************************************************
    * Set the layer of neurons which needs to be recorded.
    ***********************************************************************/
    public void  setLayerNeurons ( final Layer  layer )
    ////////////////////////////////////////////////////////////////////////
    {
      int xTotal=0;           
      int yTotal=0;
      int xstep=0;
      int ystep=0;

      int xMul = layer.getMultiplierX();
      int yMul = layer.getMultiplierY();

      if(xMul < 0 )
      {           
        xTotal=simulatorParser.xEdgeLength;
        xstep=-xMul;
      }
      else
      {       
        xTotal=simulatorParser.xEdgeLength*xMul;
        xstep=1;
      } 
      if(yMul < 0 )
      {       
        yTotal=simulatorParser.yEdgeLength;
        ystep=-yMul;
      }
      else
      {       
        yTotal=simulatorParser.yEdgeLength*yMul;
        ystep=1;
      }

      for( int x = 0; x< xTotal; x+=xstep)
      {
        for(int y = 0; y< yTotal; y+=ystep)
        {
          // String neu
          //   = layer.getPrefix()+","+x+","+y+","+layer.getSuffix();

          //        if(pas.ls.cellmap.containsKey(neu))
          //        {
          
          int id;
          
          id = simulatorParser.layerStructure.cellmap_slow (
            layer.getPrefix(),layer.getSuffix(),x,y );
          
          if(simulatorParser.layerStructure.neurons != null)
          {
            simulatorParser.layerStructure.neurons[
              id - simulatorParser.layerStructure.base].setRecord(true);
          }
          
          //        }
          //        else
          //        {
          //          throw new RuntimeException("not in the grid");
          //        }

        }
      }
    }

    /***********************************************************************
    * Add a line of neurons to be recorded
    ***********************************************************************/
    public  ArrayList<Integer>  addLine (
      final int                x1,
      final int                y1,
      final int                x2,
      final int                y2,
      final String             pre,
      final String             suf,
      final ArrayList<String>  names )
    ////////////////////////////////////////////////////////////////////////
    {
      final ArrayList<Integer>  out = new ArrayList<Integer> ( );
      
//    int xTotal=0;
//      
//    int yTotal=0;
      
      int
        xstep = 0,
        ystep = 0;
      
      final int
        xMul = simulatorParser.layerStructure.getXmultiplier ( pre, suf ),
        yMul = simulatorParser.layerStructure.getYmultiplier ( pre, suf );

      if ( xMul < 0 )
      {           
//      xTotal=simulatorParser.xEdgeLength;
        
        xstep = -xMul;
      }
      else
      {       
//      xTotal=simulatorParser.xEdgeLength*xMul;
        
        xstep = 1;
      } 
      
      if ( yMul < 0 )
      {       
//      yTotal=simulatorParser.yEdgeLength;
        
        ystep = -yMul;
      }
      else
      {       
//      yTotal=simulatorParser.yEdgeLength*yMul;
        
        ystep = 1;
      }

      if ( x1 == x2
        && y1 == y2 ) 
      {
        final String  neu = pre + "," + x1 + "," + y1 + "," + suf;
        
        final int  id = simulatorParser.layerStructure.cellmap_slow (
          pre,
          suf,
          x1,
          y1 );
        
        if ( id != -1 )
        {
          out.add ( id );
          
          names.add ( neu );
          
          if ( simulatorParser.layerStructure.neurons != null )
          {
            simulatorParser.layerStructure.neurons [ id ].setRecord (
              true );
          }
        }
        
        return out;
      }

      int  xBegin = x1 < x2 ? x1 : x2;
      
      if ( xBegin % xstep != 0 )
      {
        xBegin += xstep - xBegin % xstep;
      }

      int  yBegin = y1 < y2 ? y1 : y2;
      
      if ( yBegin % ystep != 0 )
      {
        yBegin += ystep - yBegin % ystep;
      }

      if ( Math.abs ( x1 - x2 ) < Math.abs ( y1 - y2 ) )
      {
        for ( int y = yBegin; y <= ( y1 > y2 ? y1 : y2 ); y += ystep )
        {
          final double  k = ( double ) ( x2 - x1 ) / ( double ) ( y2 - y1 );
          
          String neu = pre+","+ (int)(x1 + (double)(y-y1)*k)+","+y+","+suf;
          
          int id;
          
          if ( ( id = simulatorParser.layerStructure.cellmap_slow (
            pre,
            suf,
            ( int ) ( x1 + ( double ) ( y - y1 ) *k ),
            y ) ) != -1 )
          {
            out.add ( id );
            
            names.add ( neu );
            
            if ( simulatorParser.layerStructure.neurons != null )
            {
              simulatorParser.layerStructure.neurons [ id ].setRecord (
                true );
            }
          }
        }
      }
      else
      {
        for ( int  x = xBegin; x <= ( x1 > x2 ? x1 : x2 ); x += xstep )
        {
          final double  k = ( double ) ( y2 - y1 ) / ( double ) ( x2 - x1 );
          
          final String  neu
            = pre
            + ","
            + x
            + ","
            + ( int ) ( y1 + ( double ) ( x - x1 ) * k )
            + ","
            + suf;
          
          int id;
          
          if ( ( id = simulatorParser.layerStructure.cellmap_slow (
            pre,
            suf,
            x,
            ( int ) ( y1 + ( double ) ( x - x1 ) * k ) ) ) !=-1 )
          {
            out.add ( id );
            
            names.add ( neu );
            
            if ( simulatorParser.layerStructure.neurons != null )
            {
              simulatorParser.layerStructure.neurons [ id ].setRecord (
                true );
            }
          }
        }
      }
      
      return out;
    }

    /***********************************************************************
    * Add a square of neurons to be recorded. 
    * 
    * @param x1
    * @param y1
    * @param x2
    * @param y2
    * @param pre
    * @param suf
    * @param names
    ***********************************************************************/
    public ArrayList<Integer>  addSquare (
      final int                x1,
      final int                y1,
      final int                x2,
      final int                y2,
      final String             pre,
      final String             suf,
      final ArrayList<String>  names )
    ////////////////////////////////////////////////////////////////////////
    {
      // LOGGER.trace ( "addSquare() entered" );
      
      final ArrayList<Integer>  out = new ArrayList<Integer> ( );
      
      int xTotal=0;
      
      int yTotal=0;
      
      int xstep=0;
      
      int ystep=0;
      
      int  xMul = simulatorParser.layerStructure.getXmultiplier(pre,suf);
      
      int  yMul = simulatorParser.layerStructure.getYmultiplier(pre,suf);

      if(xMul < 0 )
      {           
        xTotal=simulatorParser.xEdgeLength;
        
        xstep=-xMul;
      }
      else
      {       
        xTotal=simulatorParser.xEdgeLength*xMul;
        
        xstep=1;
      } 
      
      if(yMul < 0 )
      {       
        yTotal=simulatorParser.yEdgeLength;
        
        ystep=-yMul;
      }
      else
      {       
        yTotal=simulatorParser.yEdgeLength*yMul;
        
        ystep=1;
      }

      int xBegin =  (x1<x2 ? x1: x2);
      
      if(xBegin % xstep !=0) xBegin+= xstep - xBegin % xstep ;

      int yBegin =  (y1<y2 ? y1: y2);
      
      if(yBegin % ystep !=0) yBegin+= ystep - yBegin % ystep ;

      for( int x = xBegin  ; x <= (x1>x2?x1:x2); x+=xstep)
      {
        for( int y = yBegin  ; y <= (y1>y2?y1:y2); y+=ystep)
        {
          int id;
          
          if ( ( id = simulatorParser.layerStructure.cellmap_slow (
            pre,
            suf,
            x,
            y ) ) != -1 )
          {
            out.add(id);
            
            final String  neu = pre + "," + x + "," + y + "," + suf;

            names.add ( neu );
            
            if ( simulatorParser.layerStructure.neurons != null )
            {
              simulatorParser.layerStructure.neurons [ id ].setRecord (
                true );
            }
            
            // LOGGER.trace ( "Recording neuron id {}", id );
          }
        }
      }
      
      return out;
    }

    /***********************************************************************
    * Add a line of neurons to be recorded 
    * 
    * @param x1
    * @param y1
    * @param x2
    * @param y2
    * @param pre
    * @param suf
    * @param hostId
    * @param names
    ***********************************************************************/
    public ArrayList<Integer>  addLine (
      final int                x1,
      final int                y1,
      final int                x2,
      final int                y2,
      final String             pre,
      final String             suf,
      final int                hostId,
      final ArrayList<String>  names )
    ////////////////////////////////////////////////////////////////////////
    {
      final ArrayList<Integer> out = new ArrayList<Integer>();
      
      int xTotal=0;
      
      int yTotal=0;
      
      int xstep=0;
      
      int ystep=0;
      
      int xMul=simulatorParser.layerStructure.getXmultiplier(pre,suf);
      
      int yMul=simulatorParser.layerStructure.getYmultiplier(pre,suf);

      if(xMul < 0 )
      {           
        xTotal=simulatorParser.xEdgeLength;
        
        xstep=-xMul;
      }
      else
      {       
        xTotal=simulatorParser.xEdgeLength*xMul;
        
        xstep=1;
      } 
      
      if(yMul < 0 )
      {       
        yTotal=simulatorParser.yEdgeLength;
        
        ystep=-yMul;
      }
      else
      {       
        yTotal=simulatorParser.yEdgeLength*yMul;
        
        ystep=1;
      }

      if(x1==x2 && y1==y2) 
      {
        String neu = pre+","+ x1+","+y1+","+suf;
        
        int id = simulatorParser.layerStructure.cellmap_slow(pre,suf,x1,y1);
        
        if(id!=-1)
        {
          if ( FunUtil.hostId (
            simulatorParser.layerStructure.nodeEndIndices, id )
            == hostId )
          {
            out.add(id);
            
            names.add(neu);
            
            simulatorParser.layerStructure.neurons [
              id - simulatorParser.layerStructure.base].setRecord(true);
          }
        }
        
        return out;
      }

      int xBegin =  (x1<x2 ? x1: x2);
      
      if(xBegin % xstep !=0) xBegin+= xstep - xBegin % xstep ;

      int yBegin =  (y1<y2 ? y1: y2);
      
      if(yBegin % ystep !=0) yBegin+= ystep - yBegin % ystep ;

      if( Math.abs(x1-x2)<Math.abs(y1-y2))
      {
        for( int y = yBegin  ; y <= (y1>y2?y1:y2); y+=ystep)
        {
          double k=(double)(x2-x1)/(double)(y2-y1);
          
          String neu = pre+","+ (int)(x1 + (double)(y-y1)*k)+","+y+","+suf;
          
          int id;
          
          if ( ( id = simulatorParser.layerStructure.cellmap_slow (
            pre,
            suf,
            ( int ) ( x1 + ( double ) ( y - y1 ) * k ),
            y ) ) !=-1 )
          {
            if ( FunUtil.hostId (
              simulatorParser.layerStructure.nodeEndIndices, id )
              == hostId )
            {
              out.add(id);
              
              names.add(neu);
              
              simulatorParser.layerStructure.neurons[
                id - simulatorParser.layerStructure.base].setRecord(true);
            }
          }
        }
      }
      else
      {
        for( int x = xBegin  ; x <= (x1>x2?x1:x2); x+=xstep)
        {
          double k=(double)(y2-y1)/(double)(x2-x1);
          
          String neu = pre+","+ x+","+ (int)(y1 + (double)(x-x1)*k)+","+suf;
          
          int id;
          
          if ( ( id = simulatorParser.layerStructure.cellmap_slow (
            pre,
            suf,
            x,
            ( int ) ( y1 + ( double ) ( x - x1 ) * k ) ) ) !=-1 )
          {
            // id=pas.ls.cellmap.get(neu);
            
            if ( FunUtil.hostId (
              simulatorParser.layerStructure.nodeEndIndices, id )
              == hostId )
            {
              out.add(id);
              
              names.add(neu);
              
              simulatorParser.layerStructure.neurons [
                id-simulatorParser.layerStructure.base ].setRecord ( true );
            }
          }
        }
      }
      
      return out;
    }

    /***********************************************************************
    * Add a square of neurons to be recorded. 
    * 
    * @param x1
    * @param y1
    * @param x2
    * @param y2
    * @param pre
    * @param suf
    * @param hostId
    * @param names
    ***********************************************************************/
    public ArrayList<Integer>  addSquare (
      final int                x1,
      final int                y1,
      final int                x2,
      final int                y2,
      final String             pre,
      final String             suf,
      final int                hostId,
      final ArrayList<String>  names )
    ////////////////////////////////////////////////////////////////////////
    {
      // LOGGER.trace ( "addSquare(hostId) entered" );
      
      final ArrayList<Integer>  out = new ArrayList<Integer>();
      
      int xTotal=0;
      
      int yTotal=0;
      
      int xstep=0;
      
      int ystep=0;
      
      int xMul=simulatorParser.layerStructure.getXmultiplier(pre,suf);
      
      int yMul=simulatorParser.layerStructure.getYmultiplier(pre,suf);

      if(xMul < 0 )
      {           
        xTotal=simulatorParser.xEdgeLength;
        
        xstep=-xMul;
      }
      else
      {       
        xTotal=simulatorParser.xEdgeLength*xMul;
        
        xstep=1;
      } 
      
      if(yMul < 0 )
      {       
        yTotal=simulatorParser.yEdgeLength;
        
        ystep=-yMul;
      }
      else
      {       
        yTotal=simulatorParser.yEdgeLength*yMul;
        
        ystep=1;
      }

      int xBegin =  (x1<x2 ? x1: x2);
      
      if(xBegin % xstep !=0) xBegin+= xstep - xBegin % xstep ;

      int yBegin =  (y1<y2 ? y1: y2);
      
      if(yBegin % ystep !=0) yBegin+= ystep - yBegin % ystep ;

      for( int x = xBegin  ; x <= (x1>x2?x1:x2); x+=xstep)
      {
        for( int y = yBegin  ; y <= (y1>y2?y1:y2); y+=ystep)
        {
          String neu = pre+","+ x+","+y+","+suf;

          int id = simulatorParser.layerStructure.cellmap_slow(pre,suf,x,y);
          
          if(id!=-1)
          {
            if ( FunUtil.hostId (
              simulatorParser.layerStructure.nodeEndIndices, id )
              == hostId )
            {
              out.add(id);
              
              names.add(neu);
              
              simulatorParser.layerStructure.neurons[
                id-simulatorParser.layerStructure.base].setRecord(true);
            }
          }
        }
      }
      
      return out;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }