    package cnslab.cnsnetwork;
    
    import java.io.*;
import java.util.*;
import org.w3c.dom.bootstrap.*;
import org.w3c.dom.ls.*;

    import jpvm.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cnslab.cnsmath.*;
    
    /***********************************************************************
    * The class for main host, which  spawn all the trial hosts and
    * NetNosts, setup timer to check progress of the simulation and collect
    * recorded data from all NetHosts.
    * 
    * @version
    *   $Date: 2012-08-21 23:46:51 +0200 (Tue, 21 Aug 2012) $
    *   $Rev: 125 $
    *   $Author: jmcohen27 $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft, M.Sc.
    * @author
    *   Jeremy Cohen
    ***********************************************************************/
    public class  MainSimulator
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static final Class<MainSimulator>
      CLASS = MainSimulator.class;

//    private static final String
//      CLASS_NAME = CLASS.getName ( );
  
    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );
    
    protected static final String
      RESULTS_DIR = "results",
      RESULTS_DIR_PREFIX = RESULTS_DIR + File.separator;
    
    // final instance variables
      
    /** Seed number used in simulation. */
    public final Seed  idum;

    /** Simulation XML file */
    public final File  modelFile;

    /** Heapsize for each of the nethosts. */
    public final int  heapSize;

    // non-final instance variables
      
    /** Seed integer provided by user */
    public int  seedInt;
    
    /** Stores all the nethosts, first dimension is trialhosts, second
    dimension is nethosts. */
    public jpvmTaskId [ ] [ ]  netJpvmTaskIds;
    
    /** Information of JPVM */
    public JpvmInfo  info;

    /** Neuron end index for each of the Net Hosts correponding to one trial
    host, endIndex.size is the number of nethost for one trial host */
    public int [ ]  endIndex;

    //  public Neuron [] neurons;

    /**  Experiment instance. */
    public Experiment  experiment;

    /** Minimum synaptic delay */
    public double  minDelay;

    @Deprecated
    public double  backFire;
    
    // private final instance variables
    
    private final RecorderData
      recorderData = new RecorderData ( );    

    // private non-final instance variables
    
    private int
      aliveHost,
      countDis,
      countHosts,
      expId   = 0,
      totalTrials,
      trialId = -1;
    
    // TODO:  What calls the toDo.run() method?
    
    private ToDo
      toDo;

    /** Each LinkedList corresponds to the data from a given intracellular 
     * recorder within a given trial within a given subexperiment. */
    private LinkedList<IntraInfo> [ ]
      intraReceiver;
    
    private double [ ]
      per;
    
    private int [ ]
      eeId,
      ttId;    

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    /***********************************************************************
    * Main method with default values for step debugging.
    ***********************************************************************/
    public static void  main ( final String [ ]  args )
    ////////////////////////////////////////////////////////////////////////
    {
      final MainSimulator  mainSimulator = new MainSimulator (
        new Seed ( -2 ),
         "test001.xml",
        512 );
      
      mainSimulator.run ( );
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    /** Timer to check progress of the nethosts periodically. */
    public final class  ToDo
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private final Timer  timer;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  ToDo ( final int  seconds )
    ////////////////////////////////////////////////////////////////////////
    {
      timer = new Timer ( ) ;
      
      timer.schedule (
        new ToDoTask (  ),
        seconds * 1000,
        seconds * 1000 ) ;
    }

    final class  ToDoTask
      extends TimerTask
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    @Override
    public void  run ( )
    ////////////////////////////////////////////////////////////////////////
    {
      jpvmBuffer  buf;

      try
      {
        if ( info.endIndex.length == 1
          && experiment.recorder.intraEle.size ( ) == 0 )
        {
          for ( int  i = 0; i < info.numTasks; i++ )
          {
            buf = new jpvmBuffer ( );
            
            buf.pack ( i );
            
            // LOGGER.finer("pack "+i);
            
            info.jpvm.pvm_mcast (
              buf,
              netJpvmTaskIds [ i ],
              info.endIndex.length,
              NetMessageTag.checkTime );
          }
        }
        else
        {
          buf = new jpvmBuffer ( );
          
          info.jpvm.pvm_mcast (
            buf,
            info.tids,
            info.numTasks,
            NetMessageTag.checkTime );
        }
      }
      catch ( jpvmException  ex )
      {
        ex.printStackTrace ( );
        
        System.exit ( -1 );
      }
    }
    
    ////////////////////////////////////////////////////////////////////////
    // end of class ToDoTask
    ////////////////////////////////////////////////////////////////////////
    }

    public void  stop ( )
    ////////////////////////////////////////////////////////////////////////
    {
      timer.cancel ( );
    }
    
    ////////////////////////////////////////////////////////////////////////
    // end of class ToDo
    ////////////////////////////////////////////////////////////////////////
    }
    
    ////////////////////////////////////////////////////////////////////////
    // constructor methods
    ////////////////////////////////////////////////////////////////////////
    
    public  MainSimulator (
      final Seed  idum,
      final File  modelFile,
      final int   heapSize )
    ////////////////////////////////////////////////////////////////////////
    {
      this.idum      = idum;
      
      this.modelFile = modelFile;
      
      this.seedInt   = idum.seed;
      
      this.heapSize  = heapSize;
    }
    
    public  MainSimulator (
      final Seed    idum,
      final String  modelFilename,
      final int     heapSize )
    ////////////////////////////////////////////////////////////////////////
    {
      this (
        idum,
        new File ( "model/" + modelFilename ),
        heapSize );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    /***********************************************************************
    * Organize the whole simulation in following steps:
    * 1. start to parse the XML file
    * 2. map cells, finding the numbers of neuron IDs.
    * 3. parse neuronal definitions
    * 4. parse the experiment
    * 5. find the minimum synaptic delay
    * 6. startup the JPVM
    * 7. spawn all the trial hosts
    * 8. spawn all the net hosts
    * 9. fill up the jpvm info structure
    * 10. setup the timing to send check progress message to all the
    *     NetHosts periodically.
    * 11. listing in background to communicate with TrialHosts and NetHosts
    *     to initialize new trials, collect recorded data etc.
    * 12. organize collected data and record it
    * 13. end the simulation
    ***********************************************************************/
    public void  run ( )
    ////////////////////////////////////////////////////////////////////////
    {
      try
      {
        // read network DOM info
        
        final long  simulationStartTime = System.currentTimeMillis ( );
        
        final SimulatorParser
          simulatorParser = new SimulatorParser ( idum, modelFile );
        
        LOGGER.trace ( "begin to map cells" );
        
        simulatorParser.parseMapCells ( );
        
        simulatorParser.parseNeuronDef ( );
        
        LOGGER.trace ( "begin to parseexp" );
        
        simulatorParser.parseExperiment ( );
        
        LOGGER.trace ( "experiment is parsed" );
        
        // experiment information
        
        experiment = simulatorParser.experiment;
        
        simulatorParser.findMinDelay ( );
        
        if ( simulatorParser.documentType != null )
        {
          simulatorParser.document.removeChild (
            simulatorParser.documentType );
        }
        
        final DOMImplementationRegistry  domImplementationRegistry
          = DOMImplementationRegistry.newInstance ( );
        
        final DOMImplementationLS  domImplementationLs
          = ( DOMImplementationLS )
            domImplementationRegistry.getDOMImplementation ( "LS" );
        
        final LSSerializer  lsSerializer
          = domImplementationLs.createLSSerializer ( );
        
        final LSOutput  output = domImplementationLs.createLSOutput ( );
        
        final ByteArrayOutputStream  bArray = new ByteArrayOutputStream ( );
        
        output.setByteStream ( bArray );
        
        lsSerializer.write (
          simulatorParser.document,
          output );
        
        if ( simulatorParser.documentType != null )
        {
          simulatorParser.document.appendChild (
            simulatorParser.documentType );
        }
        
        final byte [ ]  ba = bArray.toByteArray ( );

        info = new JpvmInfo ( );
        
        // Enroll in the parallel virtual machine...
        
        info.jpvm = new jpvmEnvironment ( );

        // Get my task id...
        info.myJpvmTaskId = info.jpvm.pvm_mytid ( );
        
        LOGGER.trace ( "Task Id: " + info.myJpvmTaskId.toString ( ) );
        
        // total number of trial hosts

        info.numTasks = simulatorParser.parallelHost;
        
        // root id
        // not used at all
        
        info.idIndex = info.numTasks;
        
        info.tids = new jpvmTaskId [ info.numTasks ];

        // Spawn some trialHosts
        
        info.jpvm.pvm_spawn (
          "cnslab.cnsnetwork.TrialHost", // task_name
          info.numTasks,                 // num
          info.tids,                     // jpvmTaskId [ ]
          48 );                          // heapSize
        
        LOGGER.trace ( "spawn successful" );

        final jpvmBuffer  jpvmBufferInstance2 = new jpvmBuffer ( );
        
        // buf.pack(ba.length);   //just send the 
        // buf.pack(ba, ba.length, 1);

        jpvmBufferInstance2.pack ( info.numTasks );
        
        jpvmBufferInstance2.pack (
          info.tids,
          info.numTasks,
          1 );
        
        jpvmBufferInstance2.pack ( simulatorParser.minDelay );

        info.jpvm.pvm_mcast (
          jpvmBufferInstance2,
          info.tids,
          info.numTasks,
          NetMessageTag.sendTids );
        
        //send the different seed to the trialHost
        /*
        for(int i =0 ; i < info.numTasks; i++)
        {
          buf = new jpvmBuffer();
          buf.pack(seedInt);
          seedInt--;
          info.jpvm.pvm_send(buf,info.tids[i],NetMessageTag.sendSeed);
        }
        */

        LOGGER.trace ( "All sent" );

        // generate all the nethosts
        
        info.endIndex = simulatorParser.layerStructure.nodeEndIndices;
        
        netJpvmTaskIds
          = new jpvmTaskId [ info.numTasks ] [ info.endIndex.length ] ;

        for ( int i = 0 ; i < info.numTasks; i++ )
        {
          LOGGER.trace ( "generate child for trialHost " + i );
          
          // Net Host is to separate large network into small pieces
          
          info.jpvm.pvm_spawn (
            "cnslab.cnsnetwork.NetHost",
            info.endIndex.length,
            netJpvmTaskIds [ i ],
            heapSize );
                    
          final jpvmBuffer  jpvmBufferInstance = new jpvmBuffer ( );
          
          jpvmBufferInstance.pack ( info.endIndex.length );
          
          jpvmBufferInstance.pack (
            netJpvmTaskIds [ i ],
            info.endIndex.length,
            1 );
          
          jpvmBufferInstance.pack (
            info.endIndex,
            info.endIndex.length,
            1 );
          
          seedInt = seedInt - info.endIndex.length;
          
          jpvmBufferInstance.pack ( seedInt );
          
          // parent's TID
          
          jpvmBufferInstance.pack ( info.tids [ i ] );
          
          jpvmBufferInstance.pack ( ba.length );
          
          jpvmBufferInstance.pack ( ba, ba.length, 1 );
          
          info.jpvm.pvm_mcast (
            jpvmBufferInstance,
            netJpvmTaskIds [ i ],
            info.endIndex.length,
            NetMessageTag.sendTids );
        }

        for ( int i = 0; i < info.numTasks; i++ )
        {
          final jpvmBuffer  jpvmBufferInstance = new jpvmBuffer ( );
          
          jpvmBufferInstance.pack ( info.endIndex.length );
          
          jpvmBufferInstance.pack (
            netJpvmTaskIds [ i ],
            info.endIndex.length,
            1 );
          
          // send trial Host the child TIDs
          
          info.jpvm.pvm_send (
            jpvmBufferInstance,
            info.tids [ i ],
            NetMessageTag.sendTids2 );
        }

        aliveHost = info.numTasks;
        
        totalTrials = 0;
        
        for ( int  j = 0; j < experiment.subExp.length; j++ )
        {
          totalTrials += experiment.subExp [ j ].repetition;
        }

        totalTrials = totalTrials * simulatorParser.numOfHosts;
        
        int numIntraRecordings = 0;
        for ( SubExp subexperiment : experiment.subExp )
        {
          numIntraRecordings += subexperiment.repetition;
        }
        numIntraRecordings *= experiment.recorder.intraEle.size ( );
            
        intraReceiver = new LinkedList [ numIntraRecordings ];

        /*
        HashMap<String, Integer> multiCounter
          = new HashMap<String, Integer>(); // don't consider trials
          
        HashMap<String, Integer> multiCounterAll
          = new HashMap<String, Integer>(); // considering separate trials

        HashMap<String, Integer> fieldCounter
          = new HashMap<String, Integer>();
          // different time section , neuron 

        HashMap<String, Double> vectorCounterX
          = new HashMap<String, Double>();
          // different time section , neuron for X axis
         
        HashMap<String, Double> vectorCounterY
          = new HashMap<String, Double>();
          // different time section , neuron for Y axis
        */

        boolean  stop = false;

        // Barrier Sync
        
        for (
          int i = 0;
          i < info.numTasks * info.endIndex.length + info.numTasks;
          i++ )
        {
          // Receive a message...
          
          final jpvmMessage
            message = info.jpvm.pvm_recv ( NetMessageTag.readySig );
          
          // Unpack the message...
          
          final String  str = message.buffer.upkstr ( );
          
          LOGGER.trace ( str );
        }
        
        /*
        for(int i = 0 ; i < info.numTasks; i++)
        {
          info.jpvm.pvm_mcast (
            buf,
            netTids[i],
            info.endIndex.length,
            NetMessageTag.readySig );
        }
        */
        
        //info.jpvm.pvm_mcast(
        //  buf,info.tids,info.numTasks,NetMessageTag.readySig);
        
        //Barrier Sync
        
        final long
          estimateTime1 = System.currentTimeMillis ( ) - simulationStartTime;
        
        LOGGER.trace ( String.format (
          "****** Simulation is starting --- %dm:%ds"
          + " was spent for connection\n",
          new Long ( estimateTime1 / 60000 ),
          new Long ( ( estimateTime1 % 60000 ) / 1000 ) ) );
        
        // LOGGER.trace(
        //   "************ simulation is starting *********************");
        
        // 5 seconds 
        
        toDo = new ToDo ( 5 );

        per = new double [ info.numTasks ];
        
        eeId = new int [ info.numTasks ];
        
        ttId = new int [ info.numTasks ];

        countDis = 0;
        
        countHosts = 0;

        while ( !stop )
        {
          // receive info from others
          
          final jpvmMessage  m = info.jpvm.pvm_recv ( );
          
          switch ( m.messageTag )
          {
            case NetMessageTag.checkTime:
              
              processCheckTime ( m );              
              
              break;
              
            case NetMessageTag.trialReady:
              
              processTrialReady ( m );              
              
              break;
              
            case NetMessageTag.getBackData:
              
              processBackData ( m );
              
              break;
              
            case NetMessageTag.trialDone:
              
              processTrialDone ( m );
              
              break;
          }
          
          //  LOGGER.trace("alive hosts"+aliveHost);

          if ( aliveHost   == 0
            && totalTrials == 0 ) 
          {
            stop = true;
            
            break;
          }
        }

        LOGGER.trace ( "Recording and exiting" );
        
        System.gc ( );
        
        // For every recorder for every trial for every subexperiment, 
        // average the voltages and currents across all neurons being 
        // recorded by that recorder.
        
        for ( int  j = 0; j < experiment.subExp.length; j++ )
        {
          for ( int k = 0; k < experiment.subExp [j].repetition; k++)
          {
            for ( int  i = 0; i < experiment.recorder.intraEle.size ( ); i++ )
            {
              final LinkedList<IntraInfo>  currList
                = intraReceiver [ i +  
                                  k * experiment.recorder.intraEle.size ( ) + 
                                  j * experiment.recorder.intraEle.size ( )
                                    * experiment.subExp [ j ].repetition
                                  ];
            
              final Iterator<IntraInfo>  intraData = currList.iterator ( );
            
              while ( intraData.hasNext ( ) )
              {
                intraData.next ( ).divide (
                    ( experiment.recorder.intraEle.get ( i ).size ( ) ) );
              }
            }
          }
        }

        final RecWriter  recWriter = new RecWriter (
          experiment,
          simulatorParser,
          intraReceiver,
          recorderData );
        
        //      Thread.sleep(0);
        
        //network.ncfile.close();
        
        LOGGER.trace ( "\n" );

        if ( !experiment.recorder.outputFile.equals ( "" ) )
        {
          LOGGER.trace (
            "output to file "
              + RESULTS_DIR_PREFIX
              + experiment.recorder.outputFile );
          
          final RecorderData  outputRecorderData = new RecorderData ( );
          
          // TODO:  Is this necessary?
          
          RecorderDataLib.insertExperimentRecorderDataIntoRecorderData (
            outputRecorderData,
            experiment,
            recorderData );
          
          final File
            binaryFile = new File (
              RESULTS_DIR,
              experiment.recorder.outputFile ); 
          
          RecorderDataLib.writeRecorderDataToBinaryFile (
            outputRecorderData,
            binaryFile );
        }
        else
        {
          recWriter.record ( simulatorParser.outFile );
          
          PlotResult.init ( RESULTS_DIR_PREFIX + simulatorParser.outFile );
          
          if ( experiment.recorder.plot )
          {
            // plot is allowed
            
            PlotResult.suPlot ( );
            
            PlotResult.muPlot ( );
            
            PlotResult.fieldPlot ( );
            
            PlotResult.vectorPlot ( );
            
            PlotResult.intraPlot ( );
            
            // PlotResult.exportFile ( );
          }
          
          PlotResult.stop ( );
        }
        
        /*
      String filename = "results/datainput";
      FileOutputStream fos = null;
      ObjectOutputStream out = null;
      try {

        RecorderData outdata = new RecorderData();
        for( int eId=0; eId<exp.subExp.length; eId++)
        {
          for( int tId=0; tId <exp.subExp[eId].repetition; tId++)
          {
            for( int sID= 0; sID < exp.rec.singleUnit.size(); sID++) 
            {
              outdata.receiver.put(
                "Exp"+eId+"Tri"+tId+"/"+exp.rec.suNames.get(sID),
                rdata.receiver.get(
                  "E"+eId+"T"+tId+"N"+exp.rec.singleUnit.get(sID)));
            }
          }
        }

        fos = new FileOutputStream(filename);
        out = new ObjectOutputStream(fos);
        out.writeObject(outdata);
        out.close();
        LOGGER.trace("Object Persisted");
        } catch (IOException ex) {
          ex.printStackTrace();
        }
        */

        final long
          estimateTime2 = System.currentTimeMillis ( ) - simulationStartTime;
        
        LOGGER.trace ( String.format (
          "****** Simulation is done --- %dm:%ds was "
            + "spent for the whole simulation\n",
          new Long ( estimateTime2 / 60000 ),
          new Long ( ( estimateTime2 % 60000 ) / 1000 ) ) );
        
        info.jpvm.pvm_exit ( );
      } 
      catch ( final jpvmException  jpe )
      {
        LOGGER.error ( "Error - mainhost jpvm exception" );
        
        LOGGER.error ( jpe.toString ( ) );
      }
      catch ( final Exception  a )
      {
        a.printStackTrace ( );
      }
    }
    
    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
    
    private void  processBackData (
      final jpvmMessage  jpvmMessageInstance )
      throws jpvmException
    ////////////////////////////////////////////////////////////////////////
    {
      countHosts++;

      if ( countHosts == info.endIndex.length )
      {
        aliveHost--;

        countHosts = 0;
      }

      // LOGGER.trace("Receiving data from ");

      final RecorderData
        spikes = ( RecorderData ) jpvmMessageInstance.buffer.upkcnsobj ( );

      // LOGGER.trace(m.buffer.upkint()+"\n");

      // combine for single unit

      final Set<Map.Entry<String, LinkedList<Double>>>
        entries1 = spikes.receiver.entrySet ( );

      final Iterator<Map.Entry<String, LinkedList<Double>>>
        entryIter1 = entries1.iterator ( );

      while ( entryIter1.hasNext ( ) )
      {
        final Map.Entry<String, LinkedList<Double>>
          entry = entryIter1.next ( );

        // Get the key from the entry.

        final String  key = entry.getKey ( );

        // Get the value.

        final LinkedList<Double>  value = entry.getValue ( );

        LinkedList<Double>
          tmp1 = recorderData.receiver.get ( key );

        if ( tmp1 == null )
        {
          recorderData.receiver.put (
            key,
            tmp1 = ( new LinkedList<Double> ( ) ) );
        }

        //put received info into memory
        //if( spike.time !=0.0 ) tmp.add(spike.time);
        
        // put received info into memory
        
        tmp1.addAll(value);
      }

      ///combine for multi unit

      final Set<Map.Entry<String, Integer>>
        entries2 = spikes.multiCounter.entrySet ( );

      final Iterator<Map.Entry<String, Integer>>
        entryIter2 = entries2.iterator();

      while (entryIter2.hasNext())
      {
        final Map.Entry<String, Integer>
          entry = entryIter2.next ( );
        
        // Get the key from the entry.

        final String key = entry.getKey();
        
        // Get the value.

        final Integer value = entry.getValue();

        Integer  tmp2 = recorderData.multiCounter.get ( key );

        if ( tmp2 == null)
        {
          recorderData.multiCounter.put (
            key,
            tmp2 = ( new Integer ( 0 ) ) );
        }

        tmp2 += value;

        recorderData.multiCounter.put (
          key,
          tmp2 );
      }

      final Set<Map.Entry<String, Integer>>
        entries3 = spikes.multiCounterAll.entrySet();

      final Iterator<Map.Entry<String, Integer>>
        entryIter3 = entries3.iterator ( );

      while ( entryIter3.hasNext ( ) )
      {
        final Map.Entry<String, Integer>
          entry = entryIter3.next ( );
        
        // Get the key from the entry.

        final String  key = entry.getKey ( );
        
        // Get the value.

        final Integer  value = entry.getValue ( );

        Integer  tmp3 = recorderData.multiCounterAll.get ( key );

        if ( tmp3 == null )
        {
          recorderData.multiCounterAll.put (
            key,
            tmp3 = ( new Integer ( 0 ) ) );
        }

        tmp3 += value;

        recorderData.multiCounterAll.put (
          key,
          tmp3 );
      }

      //combine for field ele

      final Set<Map.Entry<String, Integer>>
        entries4 = spikes.fieldCounter.entrySet ( );

      final Iterator<Map.Entry<String, Integer>>
        entryIter4 = entries4.iterator ( );

      while ( entryIter4.hasNext ( ) )
      {
        final Map.Entry<String, Integer>
          entry = entryIter4.next ( );

        // Get the key from the entry.

        final String  key = entry.getKey ( );

        // LOGGER.trace(key);

        // Get the value.

        final Integer  value = entry.getValue ( );

        Integer  tmp4 = recorderData.fieldCounter.get ( key );

        if ( tmp4 == null )
        {
          recorderData.fieldCounter.put (
            key,
            tmp4 = ( new Integer ( 0 ) ) );
        }

        tmp4 += value;

        recorderData.fieldCounter.put (
          key,
          tmp4 );
      }

      //combine for vector ele

      final Set<Map.Entry<String, Double>>
        entries5 = spikes.vectorCounterX.entrySet ( );

      final Iterator<Map.Entry<String, Double>>
        entryIter5 = entries5.iterator ( );

      while  ( entryIter5.hasNext ( ) )
      {
        final Map.Entry<String, Double>
          entry = entryIter5.next ( );
        
        // Get the key from the entry.

        final String  key = entry.getKey ( );
        
        // Get the value.

        final Double  value = entry.getValue();

        Double  tmp5 = recorderData.vectorCounterX.get ( key );

        if ( tmp5 == null )
        {
          recorderData.vectorCounterX.put (
            key,
            tmp5 = ( new Double ( 0.0 ) ) );
        }

        tmp5 += value;

        recorderData.vectorCounterX.put (
          key,
          tmp5 );
      }

      final Set<Map.Entry<String, Double>>
        entries6 = spikes.vectorCounterY.entrySet ( );

      final Iterator<Map.Entry<String, Double>>
        entryIter6 = entries6.iterator ( );

      while ( entryIter6.hasNext ( ) )
      {
        final Map.Entry<String, Double>
          entry = entryIter6.next ( );
        
        // Get the key from the entry.

        final String  key = entry.getKey ( );
        
        // Get the value.

        final Double value = entry.getValue ( );

        Double  tmp6 = recorderData.vectorCounterY.get ( key );

        if ( tmp6 == null )
        {
          recorderData.vectorCounterY.put (
            key,
            tmp6 = ( new Double ( 0.0 ) ) );
        }

        tmp6 += value;

        recorderData.vectorCounterY.put (
          key,
          tmp6 );
      }
    }
    
    private void  processCheckTime (
      final jpvmMessage  jpvmMessageInstance )
      throws jpvmException
    ////////////////////////////////////////////////////////////////////////
    {
      countDis++;
      
      int hostId = jpvmMessageInstance.buffer.upkint();
      
      int eId = jpvmMessageInstance.buffer.upkint();
      
      double root_time = jpvmMessageInstance.buffer.upkdouble();
      
      int tId = jpvmMessageInstance.buffer.upkint();
      
      per [ hostId ]
        = root_time / experiment.subExp [ eId ].trialLength * 100;
      
      eeId [ hostId ] = eId;
      
      ttId [ hostId ] = tId;
      
      if(countDis == info.numTasks)
      {
        countDis=0;
        
        for ( int ii=0; ii < info.numTasks; ii++ )
        {
          LOGGER.trace ( String.format (
            "E%dT%d: %.1f%% ",
            new Integer ( eeId [ ii ] ),
            new Integer ( ttId [ ii ] ),
            new Double  ( per  [ ii ] ) ) );
        }
      }
    }
    
    private void  processTrialDone (
      final jpvmMessage  jpvmMessageInstance )
      throws jpvmException
    ////////////////////////////////////////////////////////////////////////
    {
      totalTrials--;
      
      int res_trial = jpvmMessageInstance.buffer.upkint ( );
      
      int res_exp = jpvmMessageInstance.buffer.upkint ( );
      
      final IntraRecBuffer
        intra = ( IntraRecBuffer ) jpvmMessageInstance.buffer.upkcnsobj ( );
      

      for ( int  i = 0; i < intra.neurons.length; i++ )
      {
        final int  neu = intra.neurons [ i ];
        
        final int  eleId = experiment.recorder.intraIndex ( neu );
        
        final LinkedList<IntraInfo>  info = intra.buff.get ( i );
        
        final int index = eleId + 
                          res_trial *
                            experiment.recorder.intraEle.size ( ) + 
                          res_exp *
                            experiment.recorder.intraEle.size ( ) * 
                            experiment.subExp [ res_exp ].repetition;
        
        final LinkedList<IntraInfo>  currList = intraReceiver [ index ];
        
        if ( currList != null )
        {
          final Iterator<IntraInfo>  intraData = info.iterator ( );
          
          final Iterator<IntraInfo>  thisData = currList.iterator ( );
          
          while ( intraData.hasNext ( ) )
          {
            thisData.next ( ).plus ( intraData.next ( ) );
          }
        }
        else
        { 
          intraReceiver [ index ] = info;  
        }
      }
    }
    
    private void  processTrialReady (
      final jpvmMessage  jpvmMessageInstance )
      throws jpvmException
    ////////////////////////////////////////////////////////////////////////
    {
      // get free host id
      
      final int  freeId = jpvmMessageInstance.buffer.upkint ( );
      
      trialId++;
      
      if ( expId < experiment.subExp.length )
      {
        if ( trialId == experiment.subExp [ expId ].repetition )
        {
          trialId = 0;
          
          expId++;
        }
      }
      
      // game is still on
      
      if ( expId < experiment.subExp.length )
      {
        LOGGER.trace (
          "Subexp "    + expId
          + " trial "  + trialId
          + " freeId " + freeId );

        final jpvmBuffer  jpvmBufferInstance = new jpvmBuffer ( );      
        
        jpvmBufferInstance.pack ( trialId );
        
        jpvmBufferInstance.pack ( expId );
        
        info.jpvm.pvm_send (
          jpvmBufferInstance,
          info.tids [ freeId ],
          NetMessageTag.oneTrial );
      }
      else if ( aliveHost != 0 )
      {
        // If all of the work is done and some hosts are not killed
        
        if ( toDo != null )
        {
          toDo.stop ( );
          
          toDo = null;
        }

        LOGGER.trace ( "host " + freeId + " is killed" );
        
        final jpvmBuffer  jpvmBufferInstance = new jpvmBuffer ( );
        
        jpvmBufferInstance.pack ( 0 );
        
        info.jpvm.pvm_send (
          jpvmBufferInstance,
          info.tids [ freeId ],
          NetMessageTag.stopSig );
      }
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }