    package cnslab.cnsnetwork;
    
    import jpvm.*;
    import java.util.Scanner;

    /***********************************************************************
    * Deprecated.
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    @Deprecated 
    public class KeyInput implements Runnable
    {
    Thread t;
    Scanner keyscan = new Scanner(System.in);
    String keyPressed="";
    Network network;

    public KeyInput(Network network) {
      this.network = network;
      t = new Thread(this);
      t.setDaemon(true);
      t.start();
    }

    public void run() {
      try {
        while(!keyPressed.equals("q"))
        {
          keyPressed = keyscan.nextLine();
          Thread.sleep(1);
          if(keyPressed.equals("q"))
          {
            if(network.info!=null)
            {
              jpvmBuffer buff = new jpvmBuffer();
              buff.pack(0);
              network.info.jpvm.pvm_mcast(buff,network.info.tids,network.info.numTasks,NetMessageTag.stopSig); //send stop signal
              Thread.sleep(2000);
              network.info.jpvm.pvm_send(buff,network.info.myJpvmTaskId,NetMessageTag.stopSig); //send stop signal
            }
            else
            {
              network.stopRun();
            }
          }
          //			System.out.println(keyPressed);
        }
      }
      catch (jpvmException ex) {
        System.out.println("jpvm error keyinput...");
      }
      catch (InterruptedException e) {
        System.out.println("Listener interrupted...");
      }
      //System.out.println ("Stop signal received - finishing sequence ...");
    }
    }
