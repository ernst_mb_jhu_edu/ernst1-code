    package cnslab.cnsnetwork;

    import java.io.*;

    /***********************************************************************
    * An array of simulation progress times of each of the NetHosts
    * corresponding to one trial host. 
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  NetTime
      implements Serializable, Transmissible
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private static final long  serialVersionUID = 0L;
    
    //

    public final double [ ]  localTime;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  NetTime ( final double [ ]  localTime )
    ////////////////////////////////////////////////////////////////////////
    {
      this.localTime = localTime;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public String  toString ( )
    ////////////////////////////////////////////////////////////////////////
    {
      String  tmp = "";
      
      for ( int  i = 0; i < localTime.length; i++ )
      {
        tmp = tmp + " T" + i + " " + localTime [ i ] + " ";
      }
      
      return tmp;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }