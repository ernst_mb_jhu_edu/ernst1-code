    package cnslab.cnsnetwork;
    
    import jpvm.*;

    /***********************************************************************
    * A class to store all the necessary information about Java Parallel
    * virtual machine.
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  JpvmInfo
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    /** Jpvm Environment object */
    public jpvmEnvironment  jpvm;

    /** Array of task ids */
    public jpvmTaskId [ ]  tids;

    /** The end neuron index for each of the net hosts */
    public int [ ]  endIndex;

    /** total number of trial hosts */
    public int  numTasks;

    /** tid of current machine */
    public jpvmTaskId  myJpvmTaskId;

    /** tid of parent machine, trial Host id. */
    public jpvmTaskId  parentJpvmTaskId;

    /** index of this machine(Nethost) in an array of ids */
    public int  idIndex;
  
    ////////////////////////////////////////////////////////////////////////
    // constructor methods
    ////////////////////////////////////////////////////////////////////////
    
    public  JpvmInfo (
      final int                  idIndex,
      final jpvmEnvironment      jpvm,
      final jpvmTaskId           mytid,
      final int                  numTasks,
      final jpvmTaskId           parent,
      final jpvm.jpvmTaskId [ ]  tids,
      final int [ ]              endIndex )
    ////////////////////////////////////////////////////////////////////////
    {
      this.idIndex = idIndex;
      
      this.jpvm = jpvm;
      
      this.myJpvmTaskId = mytid;
      
      this.numTasks = numTasks;
      
      this.parentJpvmTaskId = parent;
      
      this.tids = tids;
      
      this.endIndex = endIndex;
    }

    public  JpvmInfo ( )
    ////////////////////////////////////////////////////////////////////////
    {
      //
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }
