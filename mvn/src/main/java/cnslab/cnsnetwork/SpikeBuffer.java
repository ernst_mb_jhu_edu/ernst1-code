    package cnslab.cnsnetwork;
    
    import java.io.*;
    import java.util.*;
    
    /***********************************************************************
    * Buffer of spikes between min synaptic delay, send all the buffer once.
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  SpikeBuffer
      implements Serializable, Transmissible
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private static final long  serialVersionUID = 0L;
    
    //
    
    public ArrayList<NetMessage>  buff;
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  SpikeBuffer ( )
    ////////////////////////////////////////////////////////////////////////
    {
      buff = new ArrayList<NetMessage> ( );
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }