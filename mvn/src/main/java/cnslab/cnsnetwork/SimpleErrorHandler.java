    package cnslab.cnsnetwork;
    
    import org.xml.sax.ErrorHandler;
    import org.xml.sax.SAXParseException;

    /***********************************************************************
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  SimpleErrorHandler
      implements ErrorHandler
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    @Override
    public void  error ( final SAXParseException  exception )
    ////////////////////////////////////////////////////////////////////////
    {
      throw new RuntimeException (
        "error: " + exception.getMessage ( )
        + " at line" + exception.getLineNumber ( )
        + "," + exception.getColumnNumber ( ) );
    }

    @Override
    public void  fatalError ( final SAXParseException  exception )
    ////////////////////////////////////////////////////////////////////////
    {
      throw new RuntimeException (
        "fatalError: " + exception.getMessage ( )
        + " at line" + exception.getLineNumber ( )
        + "," + exception.getColumnNumber ( ) );
    }

    @Override
    public void  warning ( final SAXParseException  exception )
    ////////////////////////////////////////////////////////////////////////
    {
      throw new RuntimeException (
        "warning: "+ exception.getMessage ( )
        + " at line" +exception.getLineNumber ( )
        + "," + exception.getColumnNumber ( ) );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }