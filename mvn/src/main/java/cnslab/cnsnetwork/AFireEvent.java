package cnslab.cnsnetwork;
/**
 * Same as FireEvent but AFireEvent stored the avalancheId
 * 
 * @author Yi Dong
 */
public class AFireEvent extends FireEvent
{
	public AFireEvent(int index ,double time, int sourceId, int avalancheId) {
		super(index,time);
		this.avalancheId =avalancheId;
		this.sourceId = sourceId;
	}

	/** 
	 *avalanche Id it belongs to 
	 */
	public int avalancheId;

	/** 
	 *the avalanche originated from which netHost; 
	 */
	public int sourceId;

}
