    package cnslab.cnsnetwork;
    
    /***********************************************************************
    * MiNiNeuron neuron parameters. 
    * 
    * @version
    *   $Date: 2012-08-28 18:41:55 +0200 (Tue, 28 Aug 2012) $
    *   $Rev: 129 $
    *   $Author: jmcohen27 $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft, M.Sc.
    * @author
    *   Jeremy Cohen
    ***********************************************************************/
    public final class  MiNiNeuronPara
      implements Para
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    /** capacitance of the neuron farad */
    public double CAP = 3E-11;
    
    /** leak conductance siemens */
    public double GL = 1.5E-9;
    
    /** reset potential V */
    public double VRESET = -0.07;

    /** resting potential V */
    public double VREST = -0.07;  

    /** threshold potential V (theta infinity in the paper) */
    public double THRESHOLD = -0.05;
    
    /** refractory current */
    public double REFCURR = 0E-10;

    /** Change of threshold when neuron fires */
    public double THRESHOLDADD=0.0;
    
    public double RRESET=-.06;
    
    /** Adaptation see Mihalas, Niebur 2009 */
    public double A = 0.0;
    
    /** Adaptation see Mihalas, Niebur 2009 */
    public double B=10;
    
    /** Decay rates for synaptic currents (inverse time constant) */
    public double [] DECAY_SYNAPSE = {1/0.005, 1/0.025};

    /** Decay rates for spike-induced currents (inverse time constant) */
    // the decay for excitation 5 ms
    public double [] DECAY_SPIKE = {1/0.005,1/0.025};
    
    /** Additive resets for spike-induced currents */
    public double [] SPIKE_ADD= {0,0};
    
    /** Multiplicative resets for spike-induced currents */
    public double[] SPIKE_RATIO= {1.0,1.0};
    
    /** Initial spike induced currents */
    public double [] ini_spike_curr = {0.0,0.0};

    /** external constant current */
    public double IEXT = 0;

    /** absolute refractory period */
    public double ABSREF = 0.002;

    /** initial membrane voltage */
    public double ini_mem = -0.07;
    
    /** initial membrane voltage jitter, uniformly distributed. */
    public double ini_memVar = 0.005;
    
    /** initial voltage threshold */
    public double ini_threshold = -0.05;
    
    // STDP constants.

    /** weight of strength of LTD */
    public double Alpha_LTD = 0.01;
    
    /** weight of strength of LTP (e.g. best timed spike doublet changes
    the synapse by 1%) */
    public double Alpha_LTP = 0.01;
    
    /** LTD inverse time constant */
    public double K_LTD = 1/0.020;
    
    /** LTP inverse time constant */
    public double K_LTP = 1/0.005;
    
    // Calculated constants.
   
    /** constant for membrane voltage */
    public double MEMBRANE_VOLTAGE_BASE;
    
    /** constant for threshold gap (membrane voltage - threshold)*/
    public double THRESHOLD_DIFF_BASE;
    
    /** the base of the NB term */
    public double NB_BASE;
    
    /** the base of the NG term */
    public double NG_BASE;
    
    /** all state variables' decays */
    public double [ ] [ ] allDecays;
    
    // Conversion factors
    
  /*
   * The state of a Mihalas-Niebur neuron is formally defined by a voltage, a
   * voltage threshold, and an arbitrary number of both spike-induced and
   * synaptic currents. However, the MNNeuron class does not keep track of those
   * "model variables" directly. Instead, the state of an MNNeuron is stored as
   * a set of "state variables": one NB term, one NG term, and an arbitrary
   * number of NJ_SPIKE and NJ_SYNAPSE terms. This novel storage scheme makes it
   * possible to compute the thresholdDiff function very quickly, but it also
   * makes it difficult to access or change the values of the model variables.
   * The program must use the conversion factors below to convert between model
   * variables and state variables.
   * 
   * Due to the definitions of the state variables, the following guidelines
   * apply when changing or accessing the value of a model variable:
   * 
   * To change a current, the program must change the NG term, the NB term, and
   * the corresponding NJ term.
   * 
   * To change the voltage, the program must change the NG term and the NB term.
   * 
   * To change the threshold, the program must change the NB term.
   * 
   * To access a current, the program must access the corresponding NJ term.
   * 
   * To access the voltage, the program must access the NG term and all NJ
   * terms. (This is done in membraneVoltage() ).
   * 
   * (The program never needs to access the threshold.)
   * 
   * For example, to add "x" (in amps) to current 0, the program would need to
   * change the state variables like this: nj_term_0 += CURRENT_TO_NJ[0] * x;
   * ng_term += CURRENT_TO_NG * x; nb_term += CURRENT_TO_NB * x;
   * 
   * The conversion factors are all derived Equation 3.2 and Equation 3.5.
   */

    /**
     * Conversion factor between a spike current and a spike NJ term. Used when
     * adding a spike current to a spike NJ term and when accessing a spike
     * current from a spike NJ term.
     */
    public double [ ] SPIKE_CURRENT_TO_SPIKE_NJ;
    
    /**
     * Conversion factor between a spike current and the NG term.  Used when changing
     * a spike current in the NG term.
     */
    public double [ ] SPIKE_CURRENT_TO_NG;
    
    /**
     * Conversion factor between a spike current and the NB term.  Used when 
     * changing a spike current in the NB term.
     */
    public double [ ] SPIKE_CURRENT_TO_NB;
    
    /**
     * Conversion factor between the voltage and a spike NJ term.  Used when 
     * accessing the voltage from a spike NJ term.
     */
    public double [ ] VOLTAGE_TO_SPIKE_NJ;
    
    /**
     * Conversion factor between a synapse current and a synapse NJ term. Used when
     * adding a synapse current to a synapse NJ term and when accessing a synapse
     * current from a synapse NJ term.
     */
    public double [ ] SYNAPSE_CURRENT_TO_SYNAPSE_NJ;
    
    /**
     * Conversion factor between a synapse current and the NG term.  Used when changing
     * a synapse current in the NG term.
     */
    public double [ ] SYNAPSE_CURRENT_TO_NG;
    
    /**
     * Conversion factor between a synapse current and the NB term.  Used when 
     * changing a synapse current in the NB term.
     */
    public double [ ] SYNAPSE_CURRENT_TO_NB;
    
    /**
     * Conversion factor between the voltage and a synapse NJ term.  Used when 
     * accessing the voltage from a synapse NJ term.
     */
    public double [ ] VOLTAGE_TO_SYNAPSE_NJ;
    
    /** 
     * Conversion factor between the voltage and the NG term.  Used when
     * changing voltage in the NG term and when accessing the voltage from the
     * NG term.
     */
    public double VOLTAGE_TO_NG;
    
    /** 
     * Conversion factor between the voltage and the NB term.  Used when
     * changing the voltage in the NB term.
     */
    public double VOLTAGE_TO_NB;
    
    /**
     * Conversion factor between the threshold and the NB term.  Used when
     * changing the threshold in the NG term. 
     */
    public double THRESHOLD_TO_NB;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public String  getModel ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return "MiNiNeuron";
    }

 
    /**
     * Pre-calculate constants.
     */
    public void  calConstants ( )
    ////////////////////////////////////////////////////////////////////////
    {           
      if ( DECAY_SPIKE.length != SPIKE_ADD.length
        || DECAY_SPIKE.length != SPIKE_RATIO.length
        || DECAY_SPIKE.length != ini_spike_curr.length )
      {
        throw new RuntimeException (
          "number of spike-induced currents doesn't match "
          + "decay.length:"     + DECAY_SPIKE.length
          + " iadd.length:"     + SPIKE_ADD.length
          + " iratio.length:"   + SPIKE_RATIO.length
          + " ini_curr.length:" + ini_spike_curr.length );
      } 
      
      SPIKE_CURRENT_TO_SPIKE_NJ = new double [ DECAY_SPIKE.length ];
      
      for (int a = 0; a < DECAY_SPIKE.length; a ++)
      {
        SPIKE_CURRENT_TO_SPIKE_NJ [ a ] = (1 - ( A / ( B - DECAY_SPIKE [ a ]
                ) ) )  / ( GL- CAP * DECAY_SPIKE [ a ] );
      }
      
      SPIKE_CURRENT_TO_NG = new double [ DECAY_SPIKE.length ];
      
      for (int a = 0; a < DECAY_SPIKE.length; a ++)
      {
        SPIKE_CURRENT_TO_NG [ a ] = - (1 - ( A / (B - GL / CAP ) ) ) /
                ( GL- CAP * DECAY_SPIKE [ a ] );
      }
      
      SPIKE_CURRENT_TO_NB = new double [ DECAY_SPIKE.length ];
      
      for (int a = 0; a < DECAY_SPIKE.length; a ++)
      {
          SPIKE_CURRENT_TO_NB [ a ] = - ( A / (B - GL / CAP ) )  /
                  (B * CAP - CAP * DECAY_SPIKE [ a ] );
      }
      
      SYNAPSE_CURRENT_TO_SYNAPSE_NJ = new double [ DECAY_SYNAPSE.length ];
      
      for (int a = 0; a < DECAY_SYNAPSE.length; a ++)
      {
          SYNAPSE_CURRENT_TO_SYNAPSE_NJ [ a ] = (1 - ( A / 
              ( B - DECAY_SYNAPSE [ a ] ) ) )
              / ( GL- CAP * DECAY_SYNAPSE [ a ] );
      }
      
      SYNAPSE_CURRENT_TO_NG = new double [ DECAY_SYNAPSE.length ];
      
      for (int a = 0; a < DECAY_SYNAPSE.length; a ++)
      {
          SYNAPSE_CURRENT_TO_NG [ a ] = - (1 - ( A / (B - GL / CAP ) ) )
                  / ( GL- CAP * DECAY_SYNAPSE [ a ] );
      }
      
      SYNAPSE_CURRENT_TO_NB = new double [ DECAY_SYNAPSE.length ];
      
      for (int a = 0; a < DECAY_SYNAPSE.length; a ++)
      {
          SYNAPSE_CURRENT_TO_NB [ a ] = - ( A / (B - GL / CAP ) ) 
                  / (B * CAP - CAP * DECAY_SYNAPSE [ a ] );
      }
      
      VOLTAGE_TO_SPIKE_NJ = new double [ DECAY_SPIKE.length ];
      
      for (int a = 0; a < DECAY_SPIKE.length; a ++)
      {
          VOLTAGE_TO_SPIKE_NJ [ a ] = 1 - ( A / ( B - DECAY_SPIKE [ a ] ) );
      }
      
      VOLTAGE_TO_SYNAPSE_NJ = new double [ DECAY_SYNAPSE.length ];
      
      for (int a = 0; a < DECAY_SYNAPSE.length; a ++)
      {
          VOLTAGE_TO_SYNAPSE_NJ [ a ] = 
              1 - ( A / ( B - DECAY_SYNAPSE [ a ] ) );
      }
      
      VOLTAGE_TO_NG = 1 - ( A / (B - GL / CAP ) );
      
      VOLTAGE_TO_NB = ( A / (B - GL / CAP ) );
      
      THRESHOLD_TO_NB = -1;
      
      // Set up allDecays.
      
      allDecays = new double [ 4 ] [ ];
      
      // Decay for NG term.
      
      allDecays [ 0 ] = new double [] { GL / CAP  };
      
      // Decay for NB term.
      
      allDecays [ 1 ] = new double [] { B };
      
      // Decay for NJ_SPIKE terms.
      
      allDecays [ 2 ] = new double [ DECAY_SPIKE.length ];
      
      for ( int i = 0; i < DECAY_SPIKE.length ; i++)
      {
        allDecays [ 2 ] [ i ] = DECAY_SPIKE [ i ];
      }
      
      // Decay for NJ_SYNAPSE terms.
      
      allDecays [ 3 ] = new double [ DECAY_SYNAPSE.length ];
      
      for ( int i = 0; i < DECAY_SYNAPSE.length ; i++)
      {
        allDecays [ 3 ] [ i ] = DECAY_SYNAPSE [ i ];
      }
      
      // Compute the base values before state variables are considered.
      
      NG_BASE = (1 - ( A / ( B - GL / CAP)))
              * (- VREST - IEXT / GL);
      
      NB_BASE = THRESHOLD + ( A / ( B -  GL / CAP))
              * (- VREST - IEXT / ( B * CAP ) );
      
      MEMBRANE_VOLTAGE_BASE = IEXT / GL;
      
      THRESHOLD_DIFF_BASE = VREST - THRESHOLD + (1 - A / B) * (IEXT / GL);
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }