    package cnslab.cnsnetwork;
    
    import java.io.*;
    import java.util.*;
    import org.w3c.dom.bootstrap.DOMImplementationRegistry;
    import org.w3c.dom.ls.DOMImplementationLS;
    import org.w3c.dom.ls.LSSerializer;
    import org.w3c.dom.ls.LSOutput;
    
    import jpvm.*;
    
    import cnslab.cnsmath.*;

    /***********************************************************************
    * Same as MainSimulator but deals with avalanche
    * 
    * @version
    *   $Date: 2012-08-27 18:47:48 +0200 (Mon, 27 Aug 2012) $
    *   $Rev: 126 $
    *   $Author: jmcohen27 $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft, M.Sc.
    * @author
    *   Jeremy Cohen
    ***********************************************************************/
    public class AMainSimulator extends MainSimulator
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    public AMainSimulator( Seed idum, String modelFile, int heapSize)
    ////////////////////////////////////////////////////////////////////////
    {
      super(idum, modelFile, heapSize);
    }
    
    /*
	public class ToDo  {
		Timer timer;

		public ToDo ( int seconds )   {
			timer = new Timer (  ) ;
			timer.schedule ( new ToDoTask (  ) , seconds*1000, seconds*1000) ;
		}


		class ToDoTask extends TimerTask  {
			public void run (  )   {
				jpvmBuffer buf;
				try {
					if(info.endIndex.length==1 && exp.rec.intraEle.size()==0)
					{
						for(int i = 0 ; i < info.numTasks; i++)
						{
							buf = new jpvmBuffer();
							buf.pack(i);
				//			System.out.println("pack "+i);
							info.jpvm.pvm_mcast(buf,netTids[i],info.endIndex.length,NetMessageTag.checkTime);
						}
					}
					else
					{
					buf = new jpvmBuffer();
					info.jpvm.pvm_mcast(buf,info.tids,info.numTasks,NetMessageTag.checkTime);
					}
				}
				catch(jpvmException ex) {
					ex.printStackTrace();
					System.exit(-1);
				}
			}
		}

		public void stop()
		{
			timer.cancel() ;
		}
	}
     */
    public void run()
    ////////////////////////////////////////////////////////////////////////
    {
      try {

        //read network dom info
        long simulationTime= System.currentTimeMillis();
        SimulatorParser pas = new SimulatorParser(idum, modelFile);
        System.out.println("begin to map cells");
        pas.parseMapCells();
        pas.parseNeuronDef();
        System.out.println("begin to parseexp");
        pas.parseExperiment();
        System.out.println("experiment is parsed");
        experiment = pas.experiment; // experiment infomation

        pas.findMinDelay();
        System.out.println("the mindelay is found");

        if(pas.documentType!=null)
        {
          pas.document.removeChild(pas.documentType);
        }
        DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
        DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
        LSSerializer writer = impl.createLSSerializer();
        LSOutput output = impl.createLSOutput();
        ByteArrayOutputStream bArray = new ByteArrayOutputStream();
        output.setByteStream(bArray);
        writer.write(pas.document, output);
        if(pas.documentType!=null)
        {
          pas.document.appendChild(pas.documentType);
        }
        byte [] ba = bArray.toByteArray();


        info= new JpvmInfo();
        // Enroll in the parallel virtual machine...
        info.jpvm = new jpvmEnvironment();

        // Get my task id...
        info.myJpvmTaskId = info.jpvm.pvm_mytid();
        System.out.println("Task Id: "+info.myJpvmTaskId.toString());

        info.numTasks= pas.parallelHost; // total number of trial hosts; 
        info.idIndex=info.numTasks; // root id; //not used at all
        info.tids = new jpvmTaskId[info.numTasks];

        // Spawn some  trialHosts
        info.jpvm.pvm_spawn("cnslab.cnsnetwork.TrialHost",info.numTasks,info.tids,48);
        System.out.println("spawn successfully");

        jpvmBuffer buf2 = new jpvmBuffer();
        //		buf.pack(ba.length);   //just send the 
        //		buf.pack(ba, ba.length, 1);

        buf2.pack(info.numTasks);
        buf2.pack(info.tids, info.numTasks, 1);
        buf2.pack(pas.minDelay);

        info.jpvm.pvm_mcast(buf2,info.tids,info.numTasks,NetMessageTag.sendTids);
        //send the different seed to the trialHost
        /*
			for(int i =0 ; i < info.numTasks; i++)
			{
				buf = new jpvmBuffer();
				buf.pack(seedInt);
				seedInt--;
				info.jpvm.pvm_send(buf,info.tids[i],NetMessageTag.sendSeed);
			}
         */

        System.out.println("All sent");

        //gernerate all the nethosts :
        //
        info.endIndex=pas.layerStructure.nodeEndIndices;
        netJpvmTaskIds =  new jpvmTaskId [info.numTasks][info.endIndex.length] ;

        for(int i = 0 ; i < info.numTasks; i++)
        {
          System.out.println("generate child for trialHost "+i);
          info.jpvm.pvm_spawn("cnslab.cnsnetwork.ANetHost",info.endIndex.length,netJpvmTaskIds[i],heapSize); //Net Host is to seperate large network into small pieces;
          jpvmBuffer buf = new jpvmBuffer();
          buf.pack(info.endIndex.length);
          buf.pack(netJpvmTaskIds[i],info.endIndex.length,1);
          buf.pack(info.endIndex,info.endIndex.length,1);
          seedInt = seedInt - info.endIndex.length;
          buf.pack(seedInt);
          buf.pack(info.tids[i]); //parent's tid;
          buf.pack(ba.length);
          buf.pack(ba, ba.length, 1);
          info.jpvm.pvm_mcast(buf,netJpvmTaskIds[i],info.endIndex.length,NetMessageTag.sendTids);
        }

        for(int i = 0 ; i < info.numTasks; i++)
        {
          jpvmBuffer buf = new jpvmBuffer();
          buf.pack(info.endIndex.length);
          buf.pack(netJpvmTaskIds[i],info.endIndex.length,1);
          info.jpvm.pvm_send(buf,info.tids[i],NetMessageTag.sendTids2); //send trial Host the child tids
        }


        int trialId = -1;
        int expId = 0;
        int aliveHost = info.numTasks;
        int totalTrials=0;
        for(int j=0;j< experiment.subExp.length; j++)
        {
          totalTrials += experiment.subExp[j].repetition;
        }

        totalTrials = totalTrials * pas.numOfHosts; 
        
        int numIntraRecordings = 0;
        for ( SubExp subexperiment : experiment.subExp )
        {
          numIntraRecordings += subexperiment.repetition;
        }
        numIntraRecordings *= experiment.recorder.intraEle.size ( );
            
        LinkedList[] intraReceiver = new LinkedList[numIntraRecordings];

        RecorderData rdata = new RecorderData();
        AvalancheCounter aData = new AvalancheCounter(experiment);

        /*
			HashMap<String, Integer> multiCounter = new HashMap<String, Integer>(); // don't consider trials
			HashMap<String, Integer> multiCounterAll = new HashMap<String, Integer>(); // considering seperate trials

			HashMap<String, Integer> fieldCounter = new HashMap<String, Integer>(); // different time section , neuron 

			HashMap<String, Double> vectorCounterX = new HashMap<String, Double>(); // different time section , neuron for X axis
			HashMap<String, Double> vectorCounterY = new HashMap<String, Double>(); // different time section , neuron for Y axis
         */



        boolean stop = false;

        //Barrier Sync
        for (int i=0;i<info.numTasks*info.endIndex.length+info.numTasks; i++) {
          // Receive a message...
          jpvmMessage message = info.jpvm.pvm_recv(NetMessageTag.readySig);
          // Unpack the message...
          String str = message.buffer.upkstr();
          System.out.println(str);
        }
        jpvmBuffer buf = new jpvmBuffer();
        /*
			for(int i = 0 ; i < info.numTasks; i++)
			{
				info.jpvm.pvm_mcast(buf,netTids[i],info.endIndex.length,NetMessageTag.readySig);
			}
         */
        //info.jpvm.pvm_mcast(buf,info.tids,info.numTasks,NetMessageTag.readySig);
        //Barrier Sync
        long estimateTime = System.currentTimeMillis() - simulationTime;
        System.out.format("****** Simulation is starting --- %dm:%ds was spent for connection\n" , estimateTime/60000, (estimateTime % 60000)/1000);
        //			System.out.println("************ simulation is starting *********************");
        ToDo toDo = new ToDo (5); //5 seconds 

        double [] per = new double[info.numTasks];
        int [] eeId = new int[info.numTasks];
        int [] ttId = new int[info.numTasks];

        int countDis =0;
        int countHosts=0;

        while(!stop)
        {
          jpvmMessage m =	info.jpvm.pvm_recv(); //receive info from others
          switch(m.messageTag)
          {
            case NetMessageTag.checkTime:
              countDis ++;
              int hostId = m.buffer.upkint();
              int eId = m.buffer.upkint();
              double root_time = m.buffer.upkdouble();
              int tId = m.buffer.upkint();
              per[hostId] = root_time/experiment.subExp[eId].trialLength*100;
              eeId[hostId] = eId;
              ttId[hostId] = tId;
              if(countDis == info.numTasks)
              {
                countDis=0;
                for(int ii=0; ii<info.numTasks; ii++)System.out.format("E%dT%d: %.1f%% ",eeId[ii], ttId[ii], per[ii]);
                System.out.print("\r");
              }
              break;
            case NetMessageTag.trialReady:
              int freeId = m.buffer.upkint(); //get free host id;
              trialId++;
              if(expId < experiment.subExp.length)
              {
                if(trialId == experiment.subExp[expId].repetition)
                {
                  trialId =0;
                  expId++;
                }
              }
              if(expId < experiment.subExp.length)  // game is still on;
              {
                System.out.println();
                System.out.println("Subexp "+expId+" trial "+ trialId+" freeId "+freeId);
                buf = new jpvmBuffer();
                buf.pack(trialId);
                buf.pack(expId);
                info.jpvm.pvm_send(buf,info.tids[freeId],NetMessageTag.oneTrial);
              }
              else if( aliveHost != 0 ) // if all the work are done and some hosts are not killed
              {
                if(toDo!=null)
                {
                  toDo.stop();
                  toDo=null;
                  System.out.println();
                }

                System.out.println("host "+freeId+" is killed");
                buf = new jpvmBuffer();
                buf.pack(0);
                info.jpvm.pvm_send(buf,info.tids[freeId],NetMessageTag.stopSig);
              }
              break;
            case NetMessageTag.getBackData:
              countHosts++;
              if(countHosts== info.endIndex.length) {aliveHost--;countHosts=0;}
              //System.out.println("Receiving data from ");
              RecorderData spikes = (RecorderData)m.buffer.upkcnsobj();
              //System.out.print(m.buffer.upkint()+"\n");
              //comibne for single unit
              Set entries = spikes.receiver.entrySet();
              Iterator entryIter = entries.iterator();
              while (entryIter.hasNext()) {
                Map.Entry<String, LinkedList<Double> > entry = (Map.Entry<String, LinkedList<Double> >)entryIter.next();
                String key = entry.getKey();  // Get the key from the entry.
                LinkedList<Double> value = entry.getValue();  // Get the value.
                LinkedList<Double> tmp = rdata.receiver.get(key);
                if(tmp == null)
                {
                  rdata.receiver.put(key, tmp=(new LinkedList<Double>()));
                }
                //if( spike.time !=0.0 ) tmp.add(spike.time); //put received info into memory
                tmp.addAll(value); //put received info into memory
              }
              ///comibne for multi unit
              entries = spikes.multiCounter.entrySet();
              entryIter = entries.iterator();
              while (entryIter.hasNext()) {
                Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>)entryIter.next();
                String key = entry.getKey();  // Get the key from the entry.
                Integer value = entry.getValue();  // Get the value.
                Integer tmp = rdata.multiCounter.get(key);
                if(tmp == null)
                {
                  rdata.multiCounter.put(key, tmp=(new Integer(0)));
                }
                tmp+=value;
                rdata.multiCounter.put(key, tmp);
              }
              entries = spikes.multiCounterAll.entrySet();
              entryIter = entries.iterator();
              while (entryIter.hasNext()) {
                Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>)entryIter.next();
                String key = entry.getKey();  // Get the key from the entry.
                Integer value = entry.getValue();  // Get the value.
                Integer tmp = rdata.multiCounterAll.get(key);
                if(tmp == null)
                {
                  rdata.multiCounterAll.put(key, tmp=(new Integer(0)));
                }
                tmp+=value;
                rdata.multiCounterAll.put(key, tmp);
              }
              //combine for field ele
              entries = spikes.fieldCounter.entrySet();
              entryIter = entries.iterator();
              while (entryIter.hasNext()) {
                Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>)entryIter.next();
                String key = entry.getKey();  // Get the key from the entry.
                //							System.out.println(key);
                Integer value = entry.getValue();  // Get the value.
                Integer tmp = rdata.fieldCounter.get(key);
                if(tmp == null)
                {
                  rdata.fieldCounter.put(key, tmp=(new Integer(0)));
                }
                tmp+=value;
                rdata.fieldCounter.put(key, tmp);
              }
              //combine for vector ele
              entries = spikes.vectorCounterX.entrySet();
              entryIter = entries.iterator();
              while (entryIter.hasNext()) {
                Map.Entry<String, Double> entry = (Map.Entry<String, Double>)entryIter.next();
                String key = entry.getKey();  // Get the key from the entry.
                Double value = entry.getValue();  // Get the value.
                Double tmp = rdata.vectorCounterX.get(key);
                if(tmp == null)
                {
                  rdata.vectorCounterX.put(key, tmp=(new Double(0.0)));
                }
                tmp+=value;
                rdata.vectorCounterX.put(key, tmp);
              }
              entries = spikes.vectorCounterY.entrySet();
              entryIter = entries.iterator();
              while (entryIter.hasNext()) {
                Map.Entry<String, Double> entry = (Map.Entry<String, Double>)entryIter.next();
                String key = entry.getKey();  // Get the key from the entry.
                Double value = entry.getValue();  // Get the value.
                Double tmp = rdata.vectorCounterY.get(key);
                if(tmp == null)
                {
                  rdata.vectorCounterY.put(key, tmp=(new Double(0.0)));
                }
                tmp+=value;
                rdata.vectorCounterY.put(key, tmp);
              }
              //processing avalanche part.
              //						System.out.println("num elements: "+m.buffer.num_list_elts);
              AvalancheCounter avalanche = (AvalancheCounter)m.buffer.upkcnsobj();
              for(int i=0; i< avalanche.avalancheCounter.size(); i++)
              {
                for(int j=0; j< avalanche.avalancheCounter.get(i).size();j++)
                {
                  entries = avalanche.avalancheCounter.get(i).get(j).entrySet();
                  entryIter = entries.iterator();
                  while (entryIter.hasNext()) {
                    Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>)entryIter.next();
                    String key = entry.getKey();  // Get the key from the entry.
                    Integer value = entry.getValue();  // Get the value.
                    Integer tmp = aData.avalancheCounter.get(i).get(j).get(key);
                    if(tmp == null)
                    {
                      aData.avalancheCounter.get(i).get(j).put(key, tmp=(new Integer(0)));
                    }
                    tmp+=value;
                    aData.avalancheCounter.get(i).get(j).put(key, tmp);

                  }

                }
                //System.out.println(key+" --> "+tmp);
              } //DATA TRANSFERRED FROM PARALLEL THREADS TO HOST AVALANCHE COUNTER?

              break;
            case NetMessageTag.trialDone:
              totalTrials--;
              int res_trial = m.buffer.upkint();
              int res_exp = m.buffer.upkint();
              //	System.out.println("R: "+"E"+res_exp+"T"+res_trial);
              //	RecordBuffer spikes = (RecordBuffer)m.buffer.upkcnsobj();
              IntraRecBuffer intra = (IntraRecBuffer)m.buffer.upkcnsobj();
              //			System.out.println(intra);
              //	int totalsize = spikes.buff.size();
              //	int n =0;

              //	Iterator<NetRecordSpike> iter_spike = spikes.buff.iterator();
              //					System.out.println("spikes"+spikes.buff.size());
              /*
						while(iter_spike.hasNext())
						{
							n++;
							if( (n%(totalsize/1000)) == 0)System.out.print("recording..."+(double)n/(double)totalsize+"\r");
							NetRecordSpike spike = iter_spike.next();

							//single unit processing
							if(exp.rec.singleUnit.contains(spike.from)) //if neuron is in single unit recorder
							{


							LinkedList<Double> tmp = receiver.get("E"+res_exp+"T"+res_trial+"N"+spike.from);

							if(tmp == null)
							{
								receiver.put("E"+res_exp+"T"+res_trial+"N"+spike.from, tmp=(new LinkedList<Double>()));
							}
							//if( spike.time !=0.0 ) tmp.add(spike.time); //put received info into memory
							tmp.add(spike.time); //put received info into memory
							//System.out.println("fire: time:"+spike.time+ " index:"+spike.from);
							}
							//multiple unit processing
							for(int mID = 0 ; mID < exp.rec.multiUnit.size(); mID++)
							{
								if(exp.rec.multiUnit.get(mID).contains(spike.from))
								{
									int binIndex = (int)(spike.time/exp.rec.timeBinSize);
									Integer tmpInt = multiCounter.get("E"+res_exp+"N"+mID+"B"+binIndex);
									if(tmpInt == null)
									{
										multiCounter.put("E"+res_exp+"N"+mID+"B"+binIndex, tmpInt=(new Integer(0)));
									}
									tmpInt = tmpInt + 1;
									multiCounter.put("E"+res_exp+"N"+mID+"B"+binIndex, tmpInt);
									Integer tmpIntAll = multiCounterAll.get("E"+res_exp+"T"+res_trial+"N"+mID+"B"+binIndex);
									if(tmpIntAll == null)
									{
										multiCounterAll.put("E"+res_exp+"T"+res_trial+"N"+mID+"B"+binIndex, tmpIntAll=(new Integer(0)));
									}
									tmpIntAll = tmpIntAll + 1;
									multiCounterAll.put("E"+res_exp+"T"+res_trial+"N"+mID+"B"+binIndex, tmpIntAll);

								}
							}
							//field processing
							for(int fID = 0 ; fID < exp.rec.fieldEle.size(); fID++)
							{
								if(pas.ls.celllayer_test(spike.from,exp.rec.fieldEle.get(fID).getPrefix(),exp.rec.fieldEle.get(fID).getSuffix()))
								{
									int [] xy  = pas.ls.celllayer_cordinate(spike.from,exp.rec.fieldEle.get(fID).getPrefix(),exp.rec.fieldEle.get(fID).getSuffix());
									int binIndex = (int)(spike.time/exp.rec.fieldTimeBinSize);
									Integer tmpInt = fieldCounter.get("E"+res_exp+","+exp.rec.fieldEle.get(fID).getPrefix()+","+xy[0]+","+xy[1]+","+exp.rec.fieldEle.get(fID).getSuffix()+","+"B"+binIndex);
									if(tmpInt == null)
									{
										fieldCounter.put("E"+res_exp+","+exp.rec.fieldEle.get(fID)+","+xy[0]+","+xy[1]+","+exp.rec.fieldEle.get(fID).getSuffix()+","+"B"+binIndex, tmpInt=(new Integer(0)));
									}
									tmpInt = tmpInt + 1;
									fieldCounter.put("E"+res_exp+","+exp.rec.fieldEle.get(fID).getPrefix()+","+xy[0]+","+xy[1]+","+exp.rec.fieldEle.get(fID).getSuffix()+","+"B"+binIndex, tmpInt);
								}
							}
							//vector processing
							for(int vID = 0 ; vID < exp.rec.vectorUnit.size(); vID++)
							{
								for(int cID =0 ; cID < exp.rec.vectorUnit.get(vID).coms.size(); cID++)
								{
									if(pas.ls.celllayer_test(spike.from,exp.rec.vectorUnit.get(vID).coms.get(cID).layer.getPrefix(),exp.rec.vectorUnit.get(vID).coms.get(cID).layer.getSuffix()))
									{
										int [] xy  = pas.ls.celllayer_cordinate(spike.from,exp.rec.vectorUnit.get(vID).coms.get(cID).layer.getPrefix(),exp.rec.vectorUnit.get(vID).coms.get(cID).layer.getSuffix());
										int binIndex = (int)(spike.time/exp.rec.vectorTimeBinSize);
										Double tmpDX = vectorCounterX.get("E"+res_exp+"V"+vID+"B"+binIndex+"C"+xy[0]+","+xy[1]);
										Double tmpDY = vectorCounterY.get("E"+res_exp+"V"+vID+"B"+binIndex+"C"+xy[0]+","+xy[1]);

										if(tmpDX == null)
										{
											vectorCounterX.put("E"+res_exp+"V"+vID+"B"+binIndex+"C"+xy[0]+","+xy[1], tmpDX=(new Double(0.0)));
											vectorCounterY.put("E"+res_exp+"V"+vID+"B"+binIndex+"C"+xy[0]+","+xy[1], tmpDY=(new Double(0.0)));
										}

										tmpDX = tmpDX + Math.cos( Math.PI/180.0*(double)exp.rec.vectorUnit.get(vID).coms.get(cID).orientation);
										tmpDY = tmpDY + Math.sin( Math.PI/180.0*(double)exp.rec.vectorUnit.get(vID).coms.get(cID).orientation);
										vectorCounterX.put("E"+res_exp+"V"+vID+"B"+binIndex+"C"+xy[0]+","+xy[1], tmpDX);
										vectorCounterY.put("E"+res_exp+"V"+vID+"B"+binIndex+"C"+xy[0]+","+xy[1], tmpDY);
									}
								}
							}

						}
               */

              for(int i = 0; i < intra.neurons.length; i++)
              {
                int neu = intra.neurons[i];
                int eleId = experiment.recorder.intraIndex(neu);
                LinkedList<IntraInfo> info = intra.buff.get(i);
                LinkedList<IntraInfo> currList;
                final int index = eleId + 
                    res_trial *
                      experiment.recorder.intraEle.size ( ) + 
                    res_exp *
                      experiment.recorder.intraEle.size ( ) * 
                      experiment.subExp [ res_exp ].repetition;
                if((currList=(LinkedList<IntraInfo>)intraReceiver[index])!=null)
                {
                  Iterator<IntraInfo> intraData = info.iterator();
                  Iterator<IntraInfo> thisData = currList.iterator();
                  while(intraData.hasNext())
                  {
                    (thisData.next()).plus(intraData.next());
                  }
                }
                else
                {
                  intraReceiver[eleId+res_exp*experiment.recorder.intraEle.size()]=info;	
                }
              }
              break;
          }
          //	System.out.println("alive hosts"+aliveHost);

          if( aliveHost == 0 && totalTrials==0) 
          {
            stop = true;
            break;
          }
        }

        System.out.println("Recording and exiting");
        System.gc();
        for ( int  j = 0; j < experiment.subExp.length; j++ )
        {
          for ( int k = 0; k < experiment.subExp [j].repetition; k++)
          {
            for ( int  i = 0; i < experiment.recorder.intraEle.size ( ); i++ )
            {
              final LinkedList<IntraInfo>  currList
                = intraReceiver [ i +  
                                  k * experiment.recorder.intraEle.size ( ) + 
                                  j * experiment.recorder.intraEle.size ( )
                                    * experiment.subExp [ j ].repetition
                                  ];
            
              final Iterator<IntraInfo>  intraData = currList.iterator ( );
            
              while ( intraData.hasNext ( ) )
              {
                intraData.next ( ).divide (
                    ( experiment.recorder.intraEle.get ( i ).size ( ) ) );
              }
            }
          }
        }

        RecWriter wri= new RecWriter(experiment, pas, intraReceiver,rdata,aData);
        //			Thread.sleep(0);
        //network.ncfile.close();
        System.out.print("\n");

        if(!experiment.recorder.outputFile.equals(""))
        {
          System.out.println (
            "output to file "
              + RESULTS_DIR_PREFIX
              + experiment.recorder.outputFile );
          
          final RecorderData  outputRecorderData = new RecorderData ( );
          
          RecorderDataLib.insertExperimentRecorderDataIntoRecorderData (
            outputRecorderData,
            experiment,
            rdata );
          
          final File
            binaryFile = new File (
              RESULTS_DIR,
              experiment.recorder.outputFile );
          
          RecorderDataLib.writeRecorderDataToBinaryFile (
            outputRecorderData,
            binaryFile );
        }
        else
        {

          wri.record(pas.outFile);
          PlotResult.init("results/"+pas.outFile);
          if(experiment.recorder.plot) // plot is allowed
          {
            PlotResult.suPlot();
            PlotResult.muPlot();
            PlotResult.fieldPlot();
            PlotResult.vectorPlot();
            PlotResult.intraPlot();
            //				PlotResult.exportFile();
          }
          PlotResult.stop();
        }
        /*
			String filename = "results/datainput";
			FileOutputStream fos = null;
			ObjectOutputStream out = null;
			try {

				RecorderData outdata = new RecorderData();
				for( int eId=0; eId<exp.subExp.length; eId++)
				{
					for( int tId=0; tId <exp.subExp[eId].repetition; tId++)
					{
						for( int sID= 0; sID < exp.rec.singleUnit.size(); sID++) 
						{
							outdata.receiver.put("Exp"+eId+"Tri"+tId+"/"+exp.rec.suNames.get(sID),rdata.receiver.get("E"+eId+"T"+tId+"N"+exp.rec.singleUnit.get(sID)));
						}
					}
				}

				fos = new FileOutputStream(filename);
				out = new ObjectOutputStream(fos);
				out.writeObject(outdata);
				out.close();
				System.out.println("Object Persisted");
			} catch (IOException ex) {
				ex.printStackTrace();
			}
         */

        estimateTime = System.currentTimeMillis() - simulationTime;
        System.out.format("****** Simulation is done --- %dm:%ds was spent for the whole simulation\n" , estimateTime/60000, (estimateTime % 60000)/1000);
        info.jpvm.pvm_exit();
      } 
      catch (jpvmException jpe) {
        System.out.println("Error - mainhost jpvm exception");
        System.out.println(jpe);
      }
      catch (Exception a)
      {
        a.printStackTrace();
      }
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }