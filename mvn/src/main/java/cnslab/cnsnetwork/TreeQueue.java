    package cnslab.cnsnetwork;
    
    import java.io.PrintStream;
    import java.util.TreeSet;
    import java.util.Iterator;
    import java.util.Arrays;
    
    import edu.jhu.mb.ernst.model.ModelFactory;
    import edu.jhu.mb.ernst.model.Synapse;

    /***********************************************************************
    * Implement the tree queue by using Red black queue provided by Java.
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  TreeQueue<T extends Comparable<T>>
      implements Queue<T>
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private final ModelFactory  modelFactory;
    
    private TreeSet<T> treeQueue = new TreeSet<T>();

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    public  TreeQueue ( final ModelFactory  modelFactory )
    ////////////////////////////////////////////////////////////////////////
    {
      this.modelFactory = modelFactory;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    /**
     * @see cnslab.cnsnetwork.Queue#deleteItem(java.lang.Comparable) deleteItem
     */
    public synchronized void deleteItem(T item) {
      if(!treeQueue.isEmpty())
      {
        treeQueue.remove(item);
      }
    }

    /**
     * Second item from the queue 
     * 
     * @return
     */
    public synchronized T secondItem() {
      if(treeQueue.size()<2) {
        return null;
      }
      else
      {
        Iterator<T> iter= treeQueue.iterator();
        iter.next();
        return iter.next();
      }
    }

    /**
     * @see cnslab.cnsnetwork.Queue#insertItem(java.lang.Comparable) insertItem
     */
    public synchronized void insertItem(T item) {
      treeQueue.add(item);
    }


    /**
     * @see cnslab.cnsnetwork.Queue#firstItem() firstItem
     */
    public synchronized T firstItem() {
      if(!treeQueue.isEmpty())
      {
        return treeQueue.first();
      }
      else
      {
        return null;
      }
    }

    /**
     * @see cnslab.cnsnetwork.Queue#lastItem() lastItem
     */
    public synchronized T lastItem() {
      if(!treeQueue.isEmpty())
      {
        return treeQueue.last();
      }
      else
      {
        return null;
      }
    }

    /** 
     * show all the elements in the Queue in order 
     */
    public synchronized void show(PrintStream p) {
      Iterator<T> iter= treeQueue.iterator();
      while(iter.hasNext())
      {
        p.println(iter.next());
      }
    }

    /**
     * {@inheritDoc}
     * @see Queue#show()
     */
    public synchronized String show() {
      String out="";
      Iterator<T> iter= treeQueue.iterator();
      out=out+"<";
      while(iter.hasNext())
      {
        out=out+iter.next();
      }
      out=out+">";
      return out;
    }

    public synchronized boolean noTillFireEvent(FireEvent event)
    {
      Iterator iter= treeQueue.iterator();

      while( iter.hasNext())
      {
        InputEvent input= (InputEvent) (iter.next());

        if( input.time > event.time) return false;
        
        if ( Arrays.binarySearch (
          input.branch.synapses,
          modelFactory.createSynapse (event.index, ( byte ) 0, 0f ) ) >= 0 )
        {
          return true;
        }
        
        /*
      for(int i=0; i< input.branch.synapses.length; i++)
      {
        if (input.branch.synapses[i].to == event.index) { return true; }
      }
         */
      }
      /*
    treeQueue.iterExit = false;
    treeQueue.firstFire = event;
    //return treeQueue.treeNoTillInputEvent(treeQueue.root(),event.time, event.index);
    return treeQueue.treeNoTillInputEvent(treeQueue.root());
       */
      return false;
    }

    public synchronized boolean noTillInputEvent(InputEvent event)
    {
      Iterator iter= treeQueue.iterator();

      while( iter.hasNext())
      {
        FireEvent fire= (FireEvent) (iter.next());

        if( fire.time > event.time) return false;
        
        
        if ( Arrays.binarySearch (
          event.branch.synapses,
          modelFactory.createSynapse ( fire.index, ( byte ) 0, 0f ) ) >= 0 )
        {
          return true;
        }
        
        /*
      for(int i=0; i< event.branch.synapses.length; i++)
      {
        if (event.branch.synapses[i].to == fire.index) { return true; }
      }
         */
      }
      /*
    treeQueue.iterExit = false;
    treeQueue.firstInput = event;
    treeQueue.flags = flags;
    return treeQueue.treeNoTillFireEvent(treeQueue.root());
       */
      return true;
    }

    /**
     * {@inheritDoc}
     * @see Queue#clear()
     */
    public synchronized void clear(){
      treeQueue.clear();
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }