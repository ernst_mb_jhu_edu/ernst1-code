    package cnslab.cnsnetwork;
    
    import java.io.Serializable;
    import java.util.Collection;

    import edu.jhu.mb.ernst.engine.DiscreteEvent;

    /***********************************************************************
    * Subexperiment. 
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  SubExp
      implements Serializable, Transmissible
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    private static final long  serialVersionUID = 0L;
    
    //

    /** stimulus for this subexp */
    public Stimulus [ ]  stimuli;
    
    /** number of repetitions or trials */
    public int  repetition;

    /** the length of the trials */
    public double  trialLength;

    /** subexp name */
    public String  name;
    
// TODO:  DiscreteEvent not Serializable and Transmissible
    
    private final Collection<DiscreteEvent>  discreteEventCollection;

    ////////////////////////////////////////////////////////////////////////
    // constructor methods
    ////////////////////////////////////////////////////////////////////////
    
    public  SubExp (
      final int           repetition,
      final double        trialLength,
      final Stimulus [ ]  stimuli,
      final Collection<DiscreteEvent>  discreteEventCollection )
    ////////////////////////////////////////////////////////////////////////
    {
      this.repetition = repetition;
      
      this.trialLength = trialLength;
      
      this.stimuli = stimuli;
      
      this.discreteEventCollection = discreteEventCollection;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public Collection<DiscreteEvent>  getDiscreteEventCollection ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return discreteEventCollection;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    @Override
    public String  toString ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return new String (
        "Subexp trialLength:"
        + trialLength
        + " #Trials:"
        + repetition );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }