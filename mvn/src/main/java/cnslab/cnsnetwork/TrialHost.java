    package cnslab.cnsnetwork;

    import java.io.*;

    import jpvm.*;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    
    /***********************************************************************
    * Trial Host class.
    * 
    * It listens to the message from mainhost and nethosts; it synchronizes
    * the Nethosts at the beginning and end of the trial. 
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  TrialHost
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static final Class<TrialHost>
      CLASS = TrialHost.class;

    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );
    
    //
    
    private boolean
      stopthis = true;
    
    private double
      rootTime,
      tickTime;
    
    private double [ ]
      localTime;

    private int
      counting,
      expId,
      trialId = -1;
  
    private JpvmInfo
      info;
  
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public static void main ( final String [ ]  args )
    ////////////////////////////////////////////////////////////////////////
    {
      new TrialHost ( ).run ( );
    }
  
    public void  run ( )
    ////////////////////////////////////////////////////////////////////////
    {
      // Directing debug output to System.out since JPVM only prints to
      // console messages directed to standard output, not standard error.
      
//    LoggingLib.enableDebugLogging ( System.out );
      
      LOGGER.trace ( "entering" );
      
      try
      {
        // int seedInt;
        
        // Enroll in the parallel virtual machine...
        
        info = new JpvmInfo ( );
        
        info.jpvm = new jpvmEnvironment ( );

        info.myJpvmTaskId = info.jpvm.pvm_mytid ( );
        
        // Get my parent's task id...
        
        info.parentJpvmTaskId = info.jpvm.pvm_parent ( );

        // receive information about its peers
        
        jpvmMessage  m = info.jpvm.pvm_recv ( NetMessageTag.sendTids );
        
        LOGGER.trace ( "Received message" );

        // FileOutputStream outt = new FileOutputStream(
        //   "log/preinfo"+iter_int+".txt");
        //
        // PrintStream p = new  PrintStream(outt);

        int  peerNum = m.buffer.upkint ( ); // get # of peers
        
        final jpvmTaskId [ ]  peerTids = new jpvmTaskId [ peerNum ];
        
        m.buffer.unpack ( peerTids, peerNum, 1 );

        int iter_int;
        
        for ( iter_int = 0; iter_int < peerNum; iter_int++ )
        {
          if ( info.myJpvmTaskId.equals ( peerTids [ iter_int ] ) )
          {
            break;
          }
        }
        
        info.idIndex = iter_int;

        tickTime = m.buffer.upkdouble ( ) / 2.0;

        m = info.jpvm.pvm_recv ( NetMessageTag.sendTids2 );
        
        info.numTasks = m.buffer.upkint();
        
        info.tids = new jpvmTaskId [ info.numTasks ];
        
        m.buffer.unpack (
          info.tids,
          info.numTasks,
          1 );

        //
        
        localTime = new double [ info.numTasks ];
        
        // FileOutputStream outt = new FileOutputStream(
        //   "log/trialHost"+info.idIndex+".txt");
        //
        // PrintStream p = new  PrintStream(outt);

        // Barrier Sync
        
        jpvmBuffer  buff = new jpvmBuffer ( );
        
        // send out ready info
        
        buff.pack ( "TrialHost " + info.jpvm.pvm_mytid ( ).toString ( )
          +" is ready to go" );
        
        info.jpvm.pvm_send (
          buff,
          info.parentJpvmTaskId,
          NetMessageTag.readySig );
        
        // m = info.jpvm.pvm_recv(NetMessageTag.readySig);
        
        // Barrier Sync

        buff = new jpvmBuffer ( );
        
        // send the id who is available
        
        buff.pack ( info.idIndex );
        
        // p.println("T"+trialId+" is done");
        
        info.jpvm.pvm_send (
          buff,
          info.parentJpvmTaskId,
          NetMessageTag.trialReady );

        while ( stopthis )
        {
          // get info from root to start new trials
          
          m = info.jpvm.pvm_recv ( );
          
          switch ( m.messageTag )
          {
            case NetMessageTag.checkTime:
              
              processCheckTime ( );
              
              break;
              
            case NetMessageTag.oneTrial:
              
              processOneTrial ( m );
              
              break;
              
            case NetMessageTag.sendTick:
              
              processSendTick ( m );
              
              break;
              
            case NetMessageTag.stopSig:
              
              processStopSig ( );
              
              break;
              
            case NetMessageTag.tempStopSig:
              
              processTempStopSig ( );
              
              break;
              
            case NetMessageTag.trialDone:
              
              processTrialDone ( );
              
              break;
          }
        }
        
        // p.close();
        
        info.jpvm.pvm_exit ( );
      } 
      catch ( final jpvmException  jpe )
      {
        System.out.println ( "Error - jpvm exception" );
        
        try
        {
          FileOutputStream out = new FileOutputStream("log/myfile.txt");
          
          PrintStream p = new  PrintStream(out);
          
          p.println(jpe);
          
          p.close();
        }
        catch(Exception ex)
        {
          //
        }
      }
      catch (Exception a)
      {
        try
        {
          FileOutputStream out = new FileOutputStream("log/myfile.txt");
          
          PrintStream p = new  PrintStream(out);
          
          a.printStackTrace(p);
          
          p.close();
        }
        catch(Exception ex)
        {
          //
        }
      }
      
      LOGGER.trace ( "exiting" );      
    }
    
    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
    
    private void  processCheckTime ( )
      throws jpvmException
    ////////////////////////////////////////////////////////////////////////
    {
      final jpvmBuffer  buff = new jpvmBuffer ( );
      
      // send the available Host id
      
      buff.pack ( info.idIndex );
      
      buff.pack ( expId );
      
      buff.pack ( rootTime );
      
      buff.pack ( trialId );
      
      info.jpvm.pvm_send (
        buff,
        info.parentJpvmTaskId,
        NetMessageTag.checkTime );      
    }
    
    private void  processOneTrial ( final jpvmMessage  m )
      throws jpvmException
    ////////////////////////////////////////////////////////////////////////
    {    
      trialId = m.buffer.upkint ( );
    
      expId = m.buffer.upkint ( );
    
      final jpvmBuffer  buff = new jpvmBuffer ( );
    
      // buff.pack(pas.exp.subExp[expId].trialLength);
    
      buff.pack ( trialId );
    
      buff.pack ( expId );
    
      // p.println("E"+expId+" T"+trialId);
      // System.out.println("E"+expId+" T"+trialId);
    
      /*
      jpvmMessage backM;
      for(int i=0;i<info.numTasks;i++)
      {
      // get sig from sub nethosts for synchronization
      backM =  info.jpvm.pvm_recv(NetMessageTag.syncTrialHost); 
      }
      */
    
      //Thread.sleep(500);
    
      // wait up hosts for new trials
    
      info.jpvm.pvm_mcast (
        buff,
        info.tids,
        info.numTasks,
        NetMessageTag.trialDone );
    }
    
    private void  processSendTick ( final jpvmMessage  m )
      throws jpvmException
    ////////////////////////////////////////////////////////////////////////
    {    
      final double sysTime = m.buffer.upkdouble ( );
    
      final int hostId = m.buffer.upkint ( );
    
      boolean flag = true;
    
      localTime [ hostId ] = sysTime;
    
      for ( int i = 0; i < localTime.length; i++ )
      {
        if ( localTime [ i ] - tickTime < rootTime )
        {
          // if one of the host not satisfied, we can't broadcast
        
          flag = false; 
        }
      }
    
      if ( flag )
      {
        rootTime += tickTime;
      
        //buff = new jpvmBuffer();
        //buff.pack(rootTime);
        ////broadcast the root time
        //info.jpvm.pvm_mcast(
        //  buff,
        //  info.tids,
        //  info.numTasks,
        //  NetMessageTag.syncRoot);
      }
    }
    
    private void  processStopSig ( )
      throws jpvmException
    ////////////////////////////////////////////////////////////////////////
    {    
      final jpvmBuffer  buff = new jpvmBuffer ( );
    
      // broadcast the root time
    
      info.jpvm.pvm_mcast (
        buff,
        info.tids,
        info.numTasks,
        NetMessageTag.stopSig );
    
      stopthis = false;
    }
    
    private void  processTempStopSig ( )
      throws jpvmException
    ////////////////////////////////////////////////////////////////////////
    {
      final jpvmBuffer  buff = new jpvmBuffer ( );
    
      // p.println("tempStopSig is received");
      // p.flush();
    
      //broadcast the root time
    
      info.jpvm.pvm_mcast (
        buff,
        info.tids,
        info.numTasks,
        NetMessageTag.tempStopSig );
    
      // p.println("sending to"+info.numTasks
      //   + " tids"+info.tids[0]+" "+info.tids[1]);
      // p.flush();
    }
    
    private void  processTrialDone ( )
      throws jpvmException
    ////////////////////////////////////////////////////////////////////////
    {    
      counting++;
    
      // new trial should be begin
    
      if ( counting == info.numTasks )
      {
        // reset the counting
        
        counting = 0;
        
        // reset the root time;
      
        rootTime = 0.0;
      
        for ( int  i = 0; i < localTime.length; i++ )
        {
          //reset the hosts time
          
          localTime [ i ] = 0.0;
        }
      
        final jpvmBuffer  buff = new jpvmBuffer ( );
      
        // send the id who is available
      
        buff.pack ( info.idIndex );
      
        // p.println("T"+trialId+" is done");
      
        info.jpvm.pvm_send (
          buff,
          info.parentJpvmTaskId,
          NetMessageTag.trialReady );
      }
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }