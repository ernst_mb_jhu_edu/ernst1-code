    package cnslab.cnsnetwork;
    
    import java.io.*;
    
    /***********************************************************************
    * Message of recorded spikes transmitted from NetHosts to MainHost.
    *  
    * Similar to NetMessage, maybe use any one is fine.
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  NetRecordSpike
      implements Serializable, Transmissible
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private static final long  serialVersionUID = 0L;
    
    //

    /** From which the spike is generated */ 
    public int  from;
    
    /** The spike time */
    public double  time;
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  NetRecordSpike (
      final double  time,
      final int     from )
    ////////////////////////////////////////////////////////////////////////
    {
      this.time = time;
      
      this.from = from;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public String  toString ( )
    ////////////////////////////////////////////////////////////////////////
    {
      String  tmp = "";
      
      tmp = tmp + "From:" + from + " time:" + time + "\n";
      
      return tmp;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }