    package cnslab.cnsnetwork;
    
    import java.util.*;
    
    /***********************************************************************
    * VSICLIFNeuron parameters.
    *  
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft, M.Sc.
    * @author
    *   Jeremy Cohen
    ***********************************************************************/
    public final class  VSICLIFNeuronPara
      implements Para
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    /** constant for membrane voltage */
    public double CON_v;
    
    /** constant for membrane voltage - threshold */
    public double CON;

    /** capacitance of the neuron farad */
    public double CAP = 3E-11;
    
    /** leak conductance siemens */
    public double GL = 1.5E-9;
    
    @Deprecated
    /** inverse of the time constant */
    public int TIMECON =  (int)(GL/CAP);

    /** reset potential V */
    public double VRESET = -0.07;
    
    /** resting potential V */
    public double VREST = -0.07 ;  

    /** threshold potential V (theta infinity in the paper) */
    public double THRESHOLD = -0.05;
    
    /** refractory current */
    public double REFCURR = 0E-10;

    public double THRESHOLDADD=0.0;
    
    public double RRESET=-.06;
    
    public double A=0.0;

    public double K;
    
    public double B=10;

    /** must put 0, NG-term, NB-term, followed by NJs in same order as curr
    with highest exponent last! */
    public int[] expts;
    
    public int[] orderOfExpts;

    /** the decay for excitation 5 ms */
    public double [] DECAY = {1/0.005,1/0.025};
    
    public double [] IADD= {0,0};
    
    public double[] IRATIO= {1.0,1.0};
    
    public double [] ini_curr = {0.0,0.0};

    /** external constant current */
    public double IEXT = 0 ;

    /** some constants to boost up the computation */
    public double [] GLCAPDECAY;
    
    /** some constants to boost up the computation */
    public double [] BCAPDECAY;
    
    /** some constants to boost up the computation */
    public double [] ABDECAY;
    
    /** some constants to boost up the computation */
    public double ABGLCAP;

    /** all state variables' decays */
    public double [] allDecays;

    /** absolute refractory period */
    public double ABSREF = 0.002;

    @Deprecated
    /** minimum rising time for neuron to fire */
    public double MINRISETIME = 1e-4;

    public double ini_mem = -0.07;
    
    public double ini_memVar = 0.005;
    
    public double ini_threshold = -0.05;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    @Override
    public String  getModel ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return "VSICLIFNeuron";
    }

    @Deprecated
    public void assignExp()
    ////////////////////////////////////////////////////////////////////////
    {
      TIMECON =  (int)(GL/CAP);
      
      int currLen = DECAY.length;
      
      if ( currLen != IADD.length
        || currLen != IRATIO.length
        || currLen != ini_curr.length )
      {
        throw new RuntimeException ( "current length doesn't match "
          + "decay.length:"     + DECAY.length
          + " iadd.length:"     + IADD.length
          + " iratio.length:"   + IRATIO.length
          + " ini_curr.length:" + ini_curr.length );
      }
      
      expts = new int[currLen+3];
      
      orderOfExpts = new int[currLen+3];
      
      int [] gcdArray = new int[currLen+2];
      
      gcdArray[0]=TIMECON;
      
      gcdArray[1]=(int)B;

      for(int i=2; i < gcdArray.length ; i++)
      {
        gcdArray[i] = (int)DECAY[i-2];
      }

      //computes the greatest common divisor
      
      K = FunUtil.gcd(gcdArray);

      expts[0]=0;
      
      expts[1]=TIMECON/(int)K;
      
      expts[2]=((int)B)/(int)K;
      
      for(int i=3; i < expts.length; i++)
      {
        expts[i] =  ((int)DECAY[i-3])/(int)K;
      }

      System.arraycopy(expts, 0, orderOfExpts, 0, orderOfExpts.length);
      
      Arrays.sort(orderOfExpts);

      GLCAPDECAY = new double [currLen];
      
      for(int a=0; a <currLen; a++)
      {
        GLCAPDECAY[a]=(GL-CAP*DECAY[a]);
      }

      BCAPDECAY = new double [currLen];
      
      for(int a=0; a <currLen; a++)
      {
        BCAPDECAY[a]=(B*CAP-CAP*DECAY[a]);
      }
      
      ABGLCAP = A/(B-GL/CAP);

      ABDECAY = new double [currLen];
      
      for(int a=0; a <currLen; a++)
      {
        ABDECAY[a]= A/(B-DECAY[a]);
      }
    }

    public void calConstants()
    ////////////////////////////////////////////////////////////////////////
    {
      TIMECON =  (int)(GL/CAP);
      
      int currLen = DECAY.length;
      
      if ( currLen != IADD.length
        || currLen != IRATIO.length
        || currLen != ini_curr.length )
      {
        throw new RuntimeException ( "current length doesn't match "
          + "decay.length:"     + DECAY.length
          + " iadd.length:"     + IADD.length
          + " iratio.length:"   + IRATIO.length
          + " ini_curr.length:" + ini_curr.length );
      }

      GLCAPDECAY = new double [currLen];
      
      for(int a=0; a <currLen; a++)
      {
        GLCAPDECAY[a]=(GL-CAP*DECAY[a]);
      }

      BCAPDECAY = new double [currLen];
      
      for(int a=0; a <currLen; a++)
      {
        BCAPDECAY[a]=(B*CAP-CAP*DECAY[a]);
      }
      
      ABGLCAP = A/(B-GL/CAP);

      ABDECAY = new double [currLen];
      
      for(int a=0; a <currLen; a++)
      {
        ABDECAY[a]= A/(B-DECAY[a]);
      }

      allDecays = new double[currLen+2];
      
      allDecays[0]=GL/CAP;
      
      allDecays[1]=B;
      
      for(int i=2; i < allDecays.length ; i++)
      {
        allDecays[i] = DECAY[i-2];
      }
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }