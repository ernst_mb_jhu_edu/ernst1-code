    package cnslab.cnsnetwork;
    
    import cnslab.cnsmath.*;

    /***********************************************************************
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
@Deprecated
public class PMainBK implements Runnable
{

	private Network network;

	private Object lock;

	private double minDelay;

	private volatile boolean stop= false;

	private double freq;

	private double weight;

	private Seed idum;


	public PMainBK(Network network, Object lock, double minDelay, double freq, double weight, Seed idum)
	{
		this.network = network;
		this.lock = lock;
		this.minDelay = minDelay;
		this.freq=freq;
		this.weight=weight;
		this.idum = idum;
	}


	/** 
	 * @param fire from FireEvent queue
	 * @return true if the communication thread is allowed to proceed
	 */
	public boolean mainAllowed(FireEvent fire)
	{
		//other hosts not beyond safe time
		for( int i=0; i<network.info.numTasks; i++)
		{
			if(network.localTime[i]>=network.localTime[network.info.numTasks])
			return true;
		}
		return false;
	}

	/** 
	 *stop the thread 
	 */
	public void stopThread()
	{
		stop = true;
	}

	private boolean letGo;
	/**
	 * set a new value to letGo
	 * @param letGo the new value to be used
	 */
	public void setLetGo() {
		this.letGo=true;
	}

	/**
	 * @see java.lang.Runnable#run() run
	 */
	public void run() {
		FireEvent firstFire,tmpFire;
		double tmp_firetime;
		boolean isFire;
		letGo=false;
		while (!stop)
		{

			synchronized (lock)
			{
		//		while ( !communicationAllowed(firstFire=( ((tmpFire=network.fireQueue.firstItem())!=null) ? new FireEvent(tmpFire.index, tmpFire.time) : null )))
				while ( !mainAllowed(firstFire=network.getFirstFireEvent()) && !letGo )
				{
					try {
						//						System.out.println("Communication wait");
						//						lock.notify();
						lock.wait();
					}
					catch(InterruptedException ex) {
						//TODO: Add Exception handler here
						ex.printStackTrace();
					}
				}
			}
			letGo=false;
			//System.out.println("sent");
			network.pMainProcess(firstFire,freq,weight,idum);
			synchronized(lock)
			{
				lock.notify();
			}
		} // END: while
		synchronized (lock)
		{       
			lock.notify();
		}
	}
}

