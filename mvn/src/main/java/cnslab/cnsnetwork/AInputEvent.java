package cnslab.cnsnetwork;
/**
 * Same as InputEvent but, Avalanch InputEvent stored extra avlanch  id and sourceID
 * 
 * @author 
 */
public class AInputEvent extends InputEvent
{

	public AInputEvent(double time ,Branch branch, int from,int sourceId,int avalancheId) {
		super(time,branch,from);
		this.sourceId = sourceId;
		this.avalancheId = avalancheId;
	}
	/** 
	 *the spike belong to which avalancheID 
	 */
	public int avalancheId;

	/** 
	 *the avalance originated from which netHost. 
	 */
	public int sourceId;
}

