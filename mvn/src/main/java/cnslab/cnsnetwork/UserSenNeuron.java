    package cnslab.cnsnetwork;
    
    import java.io.*;
    import java.util.*;
    
    import cnslab.cnsmath.*;
    import edu.jhu.mb.ernst.model.Synapse;
    import edu.jhu.mb.ernst.util.slot.Slot;

    /***********************************************************************
    * Sensory neuron fire according to user provided file. 
    * 
    * <p>
    * If the data file ends in ".txt", the file will be assumed to be a text
    * data file in the format described in the documentation for class
    * RecorderDataLib and the data file path will be constructed relative to
    * the "model" subdirectory.
    * </p>
    * <p>
    * If the data file does not end in ".txt", the file will be assumed to
    * be a binary data file and the data file path will be constructed
    * relative to the "results" subdirectory.
    * </p>
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  UserSenNeuron
      implements Neuron, Serializable
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private static final long  serialVersionUID = 0L;
    
    private static final String
      BINARY_DATA_DIR = "results",
      TEXT_DATA_DIR = "model";
    
    //
    
    private boolean  record;
    
    //** Neuron axons  */
    //  public Axon axon;

    /** the seed for this neuron */

    private int currentP;
    
    private UserSenNeuronPara para;
    
    private String myId;
    
    private LinkedList<Double> spikeData;

    public long tHost;

    public double time=0.0;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    public  UserSenNeuron (
      final UserSenNeuronPara para,
      final String            pre,
      final String            suf,
      final int               x,
      final int               y )
    ////////////////////////////////////////////////////////////////////////
    {
      this.para = para;
      
      if ( !para.gIni )
      {
        try
        {
//        org.apache.log4j.BasicConfigurator.configure(
//          new FileAppender(new SimpleLayout(),"log/customncfile.log"));
//        String filename = "results/"+para.datafile;
//        para.ncfile = NetcdfFile.open(filename);
//        para.gIni = true;
          
          this.para.rdata = readRecorderDataFromFile ( para.datafile );
          
          para.gIni = true;
        }
        catch(Exception a)
        {
          try
          {   
            FileOutputStream
              out = new FileOutputStream("log/openerror.txt");
            
            PrintStream p = new  PrintStream(out);
            
            a.printStackTrace(p);
            
            p.close();
          }
          catch(Exception ex)
          {
            // TODO:  ignore?
          }
        }

      }
      
      myId = pre + "," + x + "," + y + "," + suf;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    @Override
    public void  setTimeOfNextFire ( final double  arg0 )
    ////////////////////////////////////////////////////////////////////////
    {
      this.time = arg0;
    }

    @Override
    public double updateFire()
    ////////////////////////////////////////////////////////////////////////
    {
      if(currentP<spikeData.size())
      {
        final double  out = spikeData.get(currentP) - this.time;
        
        currentP++;
        
        return out;
      }
      else
      {
        return -1.0;
      }
    }
    
    @Override
    public double updateInput(double time, Synapse input)
    ////////////////////////////////////////////////////////////////////////
    {
      throw new RuntimeException("sensory neuron won't recieve inputs");
    }
    
    @Override
    public double  getTimeOfNextFire ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return -1;
    }

    /*
    public Axon getAxon()
    {
      return axon;
    }
    */
   
    @Override
    public String  toString ( )
    ////////////////////////////////////////////////////////////////////////
    {
      String tmp="User defined Sensory Neuron\n";
      
//    tmp=tmp+axon;
      
      return tmp;
    }
    
    @Override
    public boolean isSensory()
    ////////////////////////////////////////////////////////////////////////
    {
      return true;
    }

    /***********************************************************************
    * Each sub experiment use the date from subexperiment 0, unless changed
    * the code by uncommenting the line.
    ***********************************************************************/
    @Override
    public void  init (
      final int      expid,
      final int      trialid,
      final Seed     idum,
      final Network  net,
      final int      id )
    ////////////////////////////////////////////////////////////////////////
    {
      this.time=0.0;
      
      currentP = 0;

//  if(para.rdata.receiver.containsKey("Exp"+expid+"Tri"+trialid+"/"+myId))
//  {
//    spikeData=para.rdata.receiver.get("Exp"+expid+"Tri"+trialid+"/"+myId);
      
      //only 1 sub experiment
      
      spikeData=para.rdata.receiver.get("Exp"+0+"Tri"+trialid+"/"+myId);
      
      if(spikeData!=null)
      {
        //throw new RuntimeException(
        //  "not found it"+"Exp"+expid+"Tri"+trialid+"/"+myId);
        
        if(currentP < spikeData.size())
        {
          final Slot<FireEvent>  fireEventSlot = net.getFireEventSlot ( );
          
          if(net.getClass().getName().equals("cnslab.cnsnetwork.ANetwork"))
          {
            fireEventSlot.offer (
              new AFireEvent (
                id,
                this.time = updateFire ( ),
                net.info.idIndex,
                ( ( ANetwork ) net).aData.getId ( ( ANetwork ) net ) ) );
          }
          else if ( net.getClass ( ).getName ( ).equals (
            "cnslab.cnsnetwork.Network" ) )
          {
            fireEventSlot.offer (
              new FireEvent ( id, this.time = updateFire ( ) ) );
          }
          else
          {
            throw new RuntimeException (
              "Other Network Class doesn't exist" );
          }
        }
      }
//    }
    }

    @Override
    public boolean getRecord()
    ////////////////////////////////////////////////////////////////////////
    {
      return this.record;
    }

    @Override
    public void setRecord(boolean record)
    ////////////////////////////////////////////////////////////////////////
    {
      this.record = record;
    }

    @Override
    public double getMemV(double currT)
    ////////////////////////////////////////////////////////////////////////
    {
      throw new RuntimeException("no memV for sensory neuron");
    }

    @Override
    public double [] getCurr(double currT)
    ////////////////////////////////////////////////////////////////////////
    {
      throw new RuntimeException("no curr for sensory neuron");
    }

    @Override
    public long getTHost()
    ////////////////////////////////////////////////////////////////////////
    {
      return tHost;
    }

    @Override
    public void setTHost(long id)
    ////////////////////////////////////////////////////////////////////////
    {
      this.tHost=id;
    }
    
    @Override
    public boolean realFire()
    ////////////////////////////////////////////////////////////////////////
    {
      return true;
    }

    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
    
    private static RecorderData  readRecorderDataFromFile (
      final String  dataFilename )
      throws ClassNotFoundException, IOException
    ////////////////////////////////////////////////////////////////////////
    {
      if ( dataFilename == null )
      {
        throw new IllegalArgumentException ( "null" );
      }
      
      if ( dataFilename.endsWith ( ".txt" ) )
      {
        final File  textFile = new File ( TEXT_DATA_DIR, dataFilename );
        
        return RecorderDataLib.readRecorderDataFromTextFile ( textFile );
      }
      
      final File  binaryFile = new File ( BINARY_DATA_DIR, dataFilename );
      
      return RecorderDataLib.readRecorderDataFromBinaryFile ( binaryFile );
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }