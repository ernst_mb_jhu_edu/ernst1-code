    package cnslab.cnsnetwork;
    
    import java.io.PrintStream;
    import java.io.FileOutputStream;
    import java.util.ArrayList;
    import java.util.Iterator;
    import java.util.Map;

    import jpvm.*;

    import cnslab.cnsmath.*;
    import edu.jhu.mb.ernst.engine.DiscreteEventQueue;
    import edu.jhu.mb.ernst.model.ModelFactory;
    import edu.jhu.mb.ernst.model.ModulatedSynapse;
    import edu.jhu.mb.ernst.util.seq.Seq;

    /***********************************************************************
    * Same as Network but Avalanche recorder is added. 
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public class  ANetwork
      extends Network
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    public AvalancheCounter aData;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  ANetwork (
      final ModelFactory               modelFactory,
      final DiscreteEventQueue         discreteEventQueue,
      final Seq<ModulatedSynapse>      modulatedSynapseSeq,
      final Neuron [ ]                 neurons,
      final Map<Integer, Axon>         axons,
      final double                     minDelay,
      final SimulatorParser            pas,
      final Seed                       idum )
    ////////////////////////////////////////////////////////////////////////
    {
      super (
        modelFactory,
        discreteEventQueue,
        modulatedSynapseSeq,
        neurons,
        axons,
        minDelay,
        pas,
        idum );
    }

    public  ANetwork (
      final ModelFactory               modelFactory,
      final DiscreteEventQueue         discreteEventQueue,
      final Seq<ModulatedSynapse>      modulatedSynapseSeq,
      final Neuron [ ]                 neurons,
      final Map<Integer, Axon>         axons,
      final JpvmInfo                   info,
      final int                        base,
      final double                     minDelay,
      final SimulatorParser            pas,
      final Seed                       idum,
      final Experiment                 exp )
    ////////////////////////////////////////////////////////////////////////
    {
      super (
        modelFactory,
        discreteEventQueue,
        modulatedSynapseSeq,
        neurons,
        axons,
        info,
        base,
        minDelay,
        pas,
        idum,
        exp );
      
      this.aData = new AvalancheCounter(exp);
    }

    public  ANetwork (
      final ModelFactory               modelFactory,
      final DiscreteEventQueue         discreteEventQueue,
      final Seq<ModulatedSynapse>      modulatedSynapseSeq,
      final Experiment                 exp,
      final JpvmInfo                   info,
      final int                        base,
      final double                     minDelay,
      final SimulatorParser            pas,
      final Seed                       idum )
    ////////////////////////////////////////////////////////////////////////
    {
      super (
        modelFactory,
        discreteEventQueue,
        modulatedSynapseSeq,
        exp,
        info,
        base,
        minDelay,
        pas,
        idum );
      
      this.aData = new AvalancheCounter(exp);
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public void initNet()
    ////////////////////////////////////////////////////////////////////////
    {
//    subExpId=0; //the first subExp
//    trialId=-1; // the first trial;

      double tmp_time;
      //    minDelay = 0.001; //minimum delay is 1ms
      //    inputQueue = new PriQueue<InputEvent>(); // assign the queue
      //  inputQueue = new FiboQueue<InputEvent>(); // assign the queue
      //    inputQueue = new TreeQueue<InputEvent>(); // assign the queue
      inputQueue = new MTreeQueue<InputEvent>(); // assign the queue

      //    fireQueue = new TreeQueue<FireEvent>();
      fireQueue = new MTreeQueue<FireEvent>();

      //    if(bFreq>0.0)
      //    {
      poissonQueue = new MTreeQueue<PInputEvent>(); // assign the queue
      //    }


      stop=false;
      if(  info !=null )
      {
        spikeBuffers = new SpikeBuffer[info.numTasks];
        neurons[neurons.length-1]=new SYNNeuron(this);//adding the clock neuron at the end of neuron list
        for(int i=0; i<info.numTasks;i++)
        {
          spikeBuffers[i]=new SpikeBuffer();
        }

        recordBuff = new RecordBuffer();

        received  = new int[info.numTasks];
        for(int iter=0;iter<info.numTasks;iter++)
        {       
          received[iter]= 1; //leave mark here
        }

        intraRecBuffers = new IntraRecBuffer(experiment.recorder.intraNeurons());

        //send initial input spikes from the sensory neuron to all the neurons in the network
        /*
         for(int i= 1; i< neurons.length; i++)
         {
         tmp_time=-Math.log(Cnsran.ran2(idum))/5.0; // 5Hz background input
         inputQueue.insertItem( new InputEvent(0,tmp_time,i,1e-10)); //sensory neuron send spikes to the other neurons
         }
         inputQueue.show();
         */
        /*
         if(bFreq>0.0)
         {
      //      poissonQueue = new PriQueue<PInputEvent>(); // assign the queue
      poissonQueue = new MTreeQueue<PInputEvent>(); // assign the queue
      //    poissonQueue = new TreeQueue<PInputEvent>(); // assign the queue
      for(int i=0; i<neurons.length; i++)
      {
      if(!neurons[i].isSensory())
      {
      poissonQueue.insertItem( new PInputEvent(-Math.log(Cnsran.ran2(idum))/bFreq , new Synapse(base+i,1e-10,0),0));
      }
      }
         }
         */
        /*

         for(int i=0; i<neurons.length; i++)
         {
         if(neurons[i].isSensory())
         {
         fireQueue.insertItem( new FireEvent(i+base,0.0)); //sensory neuron send spikes to the other neurons
         }
         }
         */

        try {
          FileOutputStream out = new FileOutputStream("log/"+info.parentJpvmTaskId.getHost()+"_myfile"+info.idIndex+".txt");
          p = new  PrintStream(out);
          //    p.println(base+neurons[neurons.length-1].toString());
          p.println("Logfile start");
        }
        catch(Exception ex) {
        }

        String curDir = System.getProperty("user.dir");
        p.println(curDir);
        //added
        /*
      for(int iter=0;iter<info.numTasks;iter++)
      {       
        while(!received[iter].empty())
        {
          received[iter].pop();
        }
        received[iter].push(new Object());
      }
         */
        //added over

      }
//    init();

    }

    public void init()
    ////////////////////////////////////////////////////////////////////////
    {
      double tmp_time;
//    minDelay = 0.001; //minimum delay is 1ms
      stop=false;

//    trialDone=false;

      spikeState=true;

      /* this modification make old simulator done's run any more

    trialId++;
    if(trialId == exp.subExp[subExpId].repetition)
    {       
      trialId =0;
      subExpId++;
    }
       */
//    p.println("sub "+subExpId+ "tri: "+trialId);


      if(subExpId < experiment.subExp.length) // only initilize when subExp is within the range
      {
        /*
         for(int iter=0;iter<info.numTasks;iter++)
         {       
         while(!received[iter].empty())
         {
         received[iter].pop();
         }
         received[iter].push(new Object());
         }
         */

        if(info !=null )
        {
          for(int i=0; i<info.numTasks;i++)
          {
            spikeBuffers[i].buff.clear();
          }
        }

        //    recordBuff.buff.clear();

        //      intraBuff.init(); //clear intracelluar info;

        if(simulatorParser.backgroundFrequency>0.0)
        {
          for(int i=0; i<neurons.length; i++)
          {
            if(!neurons[i].isSensory())
            {
              poissonQueue.insertItem (
                new PInputEvent (
                  -Math.log ( Cnsran.ran2 ( seed ) )
                    / simulatorParser.backgroundFrequency,
                  modelFactory.createSynapse (
                    base + i,
                    ( byte ) simulatorParser.bChannel,
                    ( float ) simulatorParser.backgroundStrength ),
                  -1 ) );
            }
          }
        }

        //put the other sources of inputs
        for(int i = 0 ; i< experiment.subExp[subExpId].stimuli.length; i++)
        {
          experiment.subExp[subExpId].stimuli[i].seed = seed; //unify the seed
          //        p.println("input seed:"+exp.subExp[subExpId].sti[i].idum.seed);
          ArrayList<PInputEvent> tmp = experiment.subExp[subExpId].stimuli[i].init(i);
          Iterator<PInputEvent> iter = tmp.iterator();
          while(iter.hasNext())
          {
            //          PInputEvent pin;
            poissonQueue.insertItem(iter.next());
            //          p.println(pin);
          }
        }

        for(int i=0; i<neurons.length-1; i++)
        {

          //  p.println("init neuron seed:"+idum.seed);
          neurons[i].init(subExpId,trialId, seed,this,i+base); //all neurons will be initiazed;

          //    if(neurons[i].isSensory())
          //    {
          //        fireQueue.insertItem( new FireEvent(i+base,0.0)); //sensory neuron send spikes to the other neurons
          //      fireQueue.insertItem( new FireEvent(i+base, neurons[i].updateFire() )); //sensory neuron send spikes to the other neurons

          //    }
          //  else
          //  {
          //  }
        }
        //      p.println("neurons number: "+neurons.length);
        if(info !=null )
        {

          if(info.numTasks==1 && experiment.recorder.intraEle.size()==0){
            fireQueue.insertItem( new AFireEvent(neurons.length-1+base,experiment.subExp[subExpId].trialLength,-1,-1)); //sensory neuron send spikes to the other neurons
          }
          else
          {
            fireQueue.insertItem( new AFireEvent(neurons.length-1+base,0.0,-1,-1)); //sensory neuron send spikes to the other neurons
          }
        }

      }
    }

    /** 
     *process the event from fireQueue 
     * 
     * @param pFirstFire 
     */
    public void pFireProcess(FireEvent firstFire) throws jpvmException,Exception
    ////////////////////////////////////////////////////////////////////////
    {

      double tmp_firetime;
      if(neurons[firstFire.index-base].realFire()) // only neuron really fires
      {

        //  System.out.println("neuron:"+(firstFire.index-base)+" fire:"+firstFire.time);
        int host;
        if(neurons[firstFire.index-base].getRecord()) //if the neuron is recordable
        {
          //      p.println(firstFire.index+" recorded");
          recordBuff.buff.add(new NetRecordSpike(firstFire.time,firstFire.index));
        }

        if(((AFireEvent) firstFire).avalancheId>=0 && !neurons[firstFire.index-base].isSensory())
        {
          aData.CountIt(this,((AFireEvent) firstFire).sourceId,((AFireEvent) firstFire).avalancheId);
        }


        long target = neurons[firstFire.index-base].getTHost();
        for(int i=0; i < info.numTasks; i++)
        {
          if((target & (1L<<i))!=0) // it has target at host i
          {
            if(i == info.idIndex)  //localhost 
            {
              for(int iter=0 ; iter< axons.get(firstFire.index).branches.length ; iter++)
              {
                inputQueue.insertItem(new AInputEvent(firstFire.time + axons.get(firstFire.index).branches[iter].delay,axons.get(firstFire.index).branches[iter],firstFire.index,((AFireEvent) firstFire).sourceId,((AFireEvent) firstFire).avalancheId));
              }
            }
            else //remote host
            {
              spikeBuffers[i].buff.add( new ANetMessage(firstFire.time, firstFire.index, ((AFireEvent) firstFire).sourceId, ((AFireEvent) firstFire).avalancheId));
            }
          }
        }
      }

      double timeOfNextEvent;
      if( (timeOfNextEvent= neurons[firstFire.index-base].updateFire())>=0) //neuron do fire
      {

        tmp_firetime=firstFire.time+ timeOfNextEvent;
        //  neurons[firstFire.index-base].timeOfFire();
        int aval=((AFireEvent) firstFire).avalancheId;      
        if(neurons[firstFire.index-base].isSensory())
        {
          aval=aData.getId(this);
          //p.println("sensory start ava: " + aval);
        }      

        fireQueue.insertItem(new AFireEvent(firstFire.index, tmp_firetime, ((AFireEvent) firstFire).sourceId, ((AFireEvent) firstFire).avalancheId)); //insert this into the new firing queue;
        neurons[firstFire.index-base].setTimeOfNextFire(tmp_firetime);
      }
      else
      {
        neurons[firstFire.index-base].setTimeOfNextFire(-1); //if the neuron doesn't fire
      }
      fireQueue.deleteItem(firstFire); //delete this item
    }


    public void pMainProcess(FireEvent firstFire, double freq, double weight, Seed idum)
    ////////////////////////////////////////////////////////////////////////
    {
    }


    /** 
     *process the event from inputQueue 
     * 
     * @param firstInput 
     */
    public void inputProcess(InputEvent firstInput)
    ////////////////////////////////////////////////////////////////////////
    {

      double tmp_firetime;
      //System.out.println("input: "+firstInput);
      // first delete the false prediction  
      //System.out.println("input:"+firstInput);
      for ( int i = 0; i < firstInput.branch.synapses.length ; i++)
      {
        //    if(firstInput.branch.synapses[i].to==8300)System.out.println("find trace"+firstInput+" 8300");
        if( neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )].getTimeOfNextFire() != -1) //if last time predicts the neuron will fire in future
        {
          //System.out.println(new FireEvent( firstInput.branch.synapses[i].getTargetNeuronIndex ( ), neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )].getTimeOfNextFire()));
          fireQueue.deleteItem( new AFireEvent( firstInput.branch.synapses[i].getTargetNeuronIndex ( ), neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )].getTimeOfNextFire(),((AInputEvent) firstInput).sourceId,((AInputEvent) firstInput).avalancheId));
        }

        double timeOfNextEvent;
        if(( timeOfNextEvent = neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )].updateInput(firstInput.time,firstInput.branch.synapses[i])) >=0.0) //neuron do fire
        {
          tmp_firetime=firstInput.time+ timeOfNextEvent;
          //neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )].timeOfFire();
          neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )].setTimeOfNextFire(tmp_firetime);
          fireQueue.insertItem(new AFireEvent(firstInput.branch.synapses[i].getTargetNeuronIndex ( ) , tmp_firetime, ((AInputEvent) firstInput).sourceId, ((AInputEvent) firstInput).avalancheId)); //insert this into the new firing queue;
        }
        else
        {
          neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )].setTimeOfNextFire(-1); //if the neuron doesn't fire
        }
      }
      inputQueue.deleteItem(firstInput); //delete this item
    }

    /** 
     *poisson input process 
     * 
     * @param poiInput 
     */
    public void poissonProcess(PInputEvent poiInput)
    ////////////////////////////////////////////////////////////////////////
    {

      double tmp_firetime;
      if( neurons[poiInput.synapse.getTargetNeuronIndex ( )].getTimeOfNextFire() != -1) //if last time predicts the neuron will fire in future
      {
        //System.out.println(new FireEvent( firstInput.branch.synapses[i].getTargetNeuronIndex ( ), neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )].getTimeOfNextFire()));
        fireQueue.deleteItem( new AFireEvent( poiInput.synapse.getTargetNeuronIndex ( ), neurons[poiInput.synapse.getTargetNeuronIndex ( )].getTimeOfNextFire(),0,0));
      }

      double timeOfNextEvent;
      if( ( timeOfNextEvent = neurons[poiInput.synapse.getTargetNeuronIndex ( )].updateInput(poiInput.time,poiInput.synapse)) >=0.0 ) //neuron do fire
      {
        tmp_firetime=poiInput.time+ timeOfNextEvent;
        //neurons[poiInput.synapse.getTargetNeuronIndex ( )].timeOfFire();
        neurons[poiInput.synapse.getTargetNeuronIndex ( )].setTimeOfNextFire(tmp_firetime);
        fireQueue.insertItem(new AFireEvent(poiInput.synapse.getTargetNeuronIndex ( ) , tmp_firetime,info.idIndex,aData.getId(this))); //insert this into the new firing queue;
      }
      else
      {
        neurons[poiInput.synapse.getTargetNeuronIndex ( )].setTimeOfNextFire(-1); //if the neuron doesn't fire
      }
      poissonQueue.insertItem( new PInputEvent(poiInput.time+(-Math.log(Cnsran.ran2(seed))/simulatorParser.backgroundFrequency) , poiInput.synapse,-1));
      poissonQueue.deleteItem(poiInput); //delete this item
    }


    /** 
     *poisson input process 
     * 
     * @param poiInput 
     */
    public void pPoissonProcess(PInputEvent poiInput)
    ////////////////////////////////////////////////////////////////////////
    {
//  p.println("t:"+poiInput.time+" sourceID:"+poiInput.sourceId+" id"+poiInput.synapse.getTargetNeuronIndex ( )+" syn:"+poiInput.synapse);
//  p.flush();
//  fireQueue.show(p);
//  inputQueue.show(p);
//  poissonQueue.show(p);


      double tmp_firetime;
      if( neurons[poiInput.synapse.getTargetNeuronIndex ( )-base].getTimeOfNextFire() != -1) //if last time predicts the neuron will fire in future
      {
        fireQueue.deleteItem( new AFireEvent( poiInput.synapse.getTargetNeuronIndex ( ), neurons[poiInput.synapse.getTargetNeuronIndex ( )-base].getTimeOfNextFire(), 0,0));
      }

      double timeOfNextEvent;
      if(( timeOfNextEvent = neurons[poiInput.synapse.getTargetNeuronIndex ( )-base].updateInput(poiInput.time,poiInput.synapse))>=0.0) //neuron do fire
      {
//  p.println("neuron fire");
//  p.flush();

        tmp_firetime=poiInput.time+ timeOfNextEvent;
        //neurons[poiInput.synapse.getTargetNeuronIndex ( )-base].timeOfFire();
        fireQueue.insertItem(new AFireEvent(poiInput.synapse.getTargetNeuronIndex ( ) , tmp_firetime, info.idIndex, aData.getId(this))); //insert this into the new firing queue;
        neurons[poiInput.synapse.getTargetNeuronIndex ( )-base].setTimeOfNextFire(tmp_firetime);          
//  p.println("neuron time caled");
//  p.flush();

      }
      else
      {
//  p.println("neuron doesn't fire");
//  p.flush();
        neurons[poiInput.synapse.getTargetNeuronIndex ( )-base].setTimeOfNextFire(-1); //if the neuron doesn't fire
      }
//  p.println("input is updated");
//  p.flush();
      if(poiInput.sourceId==-1)
      {
        poissonQueue.insertItem( new PInputEvent(poiInput.time+(-Math.log(Cnsran.ran2(seed))/simulatorParser.backgroundFrequency) , poiInput.synapse,-1));
      }
      else
      {
        double timeOfNext = experiment.subExp[subExpId].stimuli[poiInput.sourceId].getNextTime(poiInput.time);
        if(timeOfNext>=0)
        {
          //p.println("t:"+timeOfNext+" sourceID:"+poiInput.sourceId+" id"+poiInput.synapse.getTargetNeuronIndex ( ));
          poissonQueue.insertItem( new PInputEvent(timeOfNext , poiInput.synapse, poiInput.sourceId));
        }
      }
      poissonQueue.deleteItem(poiInput); //delete this item
    }

    /** 
     *process the event from inputQueue 
     * 
     * @param firstInput 
     */
    public void pInputProcess(InputEvent firstInput)
    {
//  p.println("INPUT");
//  fireQueue.show(p);
//  inputQueue.show(p);
//  poissonQueue.show(p);


      double tmp_firetime;
      for ( int i = 0; i < firstInput.branch.synapses.length ; i++)
      {
//    if(firstInput.branch.synapses[i].getTargetNeuronIndex ( )==8300)p.println("find trace"+firstInput+" 8300");
        if( neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )-base].getTimeOfNextFire() != -1) //if last time predicts the neuron will fire in future
        {
          fireQueue.deleteItem( new AFireEvent( firstInput.branch.synapses[i].getTargetNeuronIndex ( ), neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )-base].getTimeOfNextFire(), ((AInputEvent) firstInput).sourceId, ((AInputEvent) firstInput).avalancheId));
        }

        double timeOfNextEvent;
        if( (timeOfNextEvent = neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )-base].updateInput(firstInput.time,firstInput.branch.synapses[i]))>=0.0) //neuron do fire
        {
          tmp_firetime=firstInput.time + timeOfNextEvent;
          //      neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )-base].timeOfFire();
          fireQueue.insertItem(new AFireEvent(firstInput.branch.synapses[i].getTargetNeuronIndex ( ) , tmp_firetime, ((AInputEvent) firstInput).sourceId, ((AInputEvent) firstInput).avalancheId)); //insert this into the new firing queue;
          neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )-base].setTimeOfNextFire(tmp_firetime);
        }
        else
        {
          neurons[firstInput.branch.synapses[i].getTargetNeuronIndex ( )-base].setTimeOfNextFire(-1); //if the neuron doesn't fire
        }
      }
      inputQueue.deleteItem(firstInput); //delete this item
    }
/*
  public void testRun()
  {
    FireEvent firstFire;
    InputEvent firstInput;
    PInputEvent poiInput;
    double minTime;
    double fireTime, inputTime, pInputTime;
//    System.out.println("before running...");

//    inputQueue.show();

    while(!stop)
    {
      firstFire =  fireQueue.firstItem(); // take out the first fire element
      firstInput =  inputQueue.firstItem(); // take out the first input element
      poiInput =  poissonQueue.firstItem(); // take out the first poisson input element

//      System.out.println("running...");

      if(firstFire != null)
      {
        fireTime=firstFire.time;
      }
      else
      {
        fireTime=Double.MAX_VALUE;
      }

      if(firstInput != null)
      {
        inputTime=firstInput.time;
      }
      else
      {
        inputTime=Double.MAX_VALUE;
      }

      if(poiInput != null)
      {
        pInputTime=poiInput.time;
      }
      else
      {
        pInputTime=Double.MAX_VALUE;
      }

      minTime = ( fireTime < inputTime? fireTime: inputTime); 
      minTime = (minTime < pInputTime? minTime: pInputTime);

      if( firstFire != null && minTime == firstFire.time) //fire event
      {
//        System.out.println("fire.."+firstFire);
        fireProcess(firstFire);

      }
      else if ( firstInput !=null && minTime == firstInput.time) //input event
      {
        //        System.out.println("input..");
        inputProcess(firstInput);
      }
      else if (  poiInput !=null && minTime == poiInput.time) //poissoninput event
      {
//        System.out.println("background: "+poiInput.synapse.getTargetNeuronIndex ( ));
        poissonProcess(poiInput);
      }
    }
  }
 */
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }