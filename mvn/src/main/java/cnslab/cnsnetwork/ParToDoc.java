package cnslab.cnsnetwork;
import cnslab.cnsmath.*;
import org.w3c.dom.Document;
import java.util.LinkedList;
import java.util.HashMap;
@Deprecated
public interface ParToDoc
{
	
	/** 
	 *input parameter array 
	 * 
	 * @param para 
	 * @return new document descripbing the network with para input array
	 */
	public Document getDocument(double [] para) throws Exception;

	/** 
	 * from recorded data to evaluate the fit function value; 
	 * @param pas 
	 * @param intraReceiver 
	 * @param receiver 
	 * @return  fit function value
	 */
	public double getFitValue(SimulatorParser pas, LinkedList[] intraReceiver, RecorderData rdata);

	/** 
	 * from recorded data to evaluate the bootstrapped fit function value; 
	 * 
	 * @param pas 
	 * @param intraReceiver 
	 * @param receiver 
	 * @param receiver 
	 * @param idum 
	 * @return 
	 */
	public double getRanFitValue(SimulatorParser pas, LinkedList[] intraReceiver, RecorderData rdata, Seed idum);

}
