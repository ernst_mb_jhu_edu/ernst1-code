    package cnslab.cnsnetwork;
    
    import java.io.*;
    import java.util.*;

    /***********************************************************************
    * The processed recorded data for different electrodes.
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
     
    public final class  RecorderData
      implements Serializable, Transmissible
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private static final long  serialVersionUID = 0L;
    
    //
    
    final Map<String, LinkedList<Double>>  receiver;
    
    /** don't consider trials */
    final Map<String, Integer>  multiCounter;
    
    /** considering separate trials */
    final Map<String, Integer>  multiCounterAll;
    
    /** different time section, neuron */
    final Map<String, Integer>  fieldCounter;
    
    /** different time section, neuron for X axis */
    final Map<String, Double>  vectorCounterX;
    
    /** different time section, neuron for Y axis */ 
    final Map<String, Double>  vectorCounterY;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  RecorderData ( )
    ////////////////////////////////////////////////////////////////////////
    {
      receiver = new HashMap<String, LinkedList<Double> >();
      
      // don't consider trials
      
      multiCounter = new HashMap<String, Integer>();
      
      // considering separate trials
      
      multiCounterAll = new HashMap<String, Integer>();
      
      // different time section, neuron
      
      fieldCounter = new HashMap<String, Integer>();
      
      // different time section, neuron for X axis
      
      vectorCounterX = new HashMap<String, Double>();
      
      // different time section, neuron for Y axis
      
      vectorCounterY = new HashMap<String, Double>();
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public void  clear ( )
    ////////////////////////////////////////////////////////////////////////
    {
      receiver.clear ( );
      
      multiCounter.clear ( );
      
      multiCounterAll.clear ( );
      
      fieldCounter.clear ( );
      
      vectorCounterX.clear ( );
      
      vectorCounterY.clear ( );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }