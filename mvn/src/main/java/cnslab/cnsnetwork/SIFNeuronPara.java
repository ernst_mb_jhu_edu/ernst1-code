    package cnslab.cnsnetwork;
    
    /***********************************************************************
    * The data structure to store parameters of the SIF neuron.
    *  
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  SIFNeuronPara
      implements Para
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    /** capacitance of the neuron farad */
    public double CAP = 3E-11;
    
    /** leak conductance siemens */
    public double GL = 1.5E-9;

    /** the decay for excitation 5 ms */
    public double DECAY_E = 1 / 0.005;
    
    /** the decay for inhibition 50 ms */
    public double DECAY_I = 1 / 0.025;

    /** resting potential V */
    public double VRESET = -0.07;
    
    /** reset potential V */
    public double VREST = -0.07 ;  

    /** threshold potential V */
    public double THRESHOLD = -0.05;
    
    /** refractory current */
    public double REFCURR = 0E-10;
    
    /** voltage from threshold for which the neuron is considered to fire */
    public double EPSV = 1E-12;

    /** absolute refractory period */
    public double ABSREF = 0.002;
//  public double ABSREF = 0.000;

    /** minimum rising time for neuron to fire */
    public double MINRISETIME = 1e-4;

    /** initialized membrane voltage */
    public double ini_mem = -0.07;
    
    /** initialized membrane voltage variance */
    public double ini_memVar = 0.005;

    public boolean GlobalInit = false;

    /** look up table, memvoltage, +curr, - curr */
    public boolean [ ] [ ] [ ]  FIRETABLE;
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public String getModel()
    ////////////////////////////////////////////////////////////////////////
    {
      return "SIFNeuron";
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }