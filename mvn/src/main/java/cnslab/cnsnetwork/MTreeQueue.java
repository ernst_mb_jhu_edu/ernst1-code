    package cnslab.cnsnetwork;
    
    import java.io.PrintStream;
    import java.util.Map;
    
    import cnslab.cnsmath.*;
    
    /***********************************************************************
    * Implement my customized Queue with Red-backtree data structure. 
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public class  MTreeQueue<T extends Comparable<T>>
      implements Queue<T>
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private RBTree<T> treeQueue = new RBTree<T>();

    private T first;

    private RBCell<T> firstNode;
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    @Override
    public synchronized void  deleteItem ( final T  item )
    ////////////////////////////////////////////////////////////////////////
    {
      treeQueue.removeOneOf(item);
      
      if(treeQueue.root()==null)
      {
        firstNode=null;
        
        first=null;
      }
      else
      {
        firstNode = treeQueue.root().leftmost();
        
        first=firstNode.element();
      }
    }


    @Override
    public synchronized void  insertItem ( final T  item )
    ////////////////////////////////////////////////////////////////////////
    {
      treeQueue.add(item);
      
      firstNode = treeQueue.root().leftmost();
      
      first=firstNode.element();
    }

    @Override
    public synchronized T  firstItem ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return first;
    }

    @Override
    public synchronized void  show ( final PrintStream  p )
    ////////////////////////////////////////////////////////////////////////
    {
      if(treeQueue.root()!=null)
      {
        p.println("<");
        
        treeQueue.inorderTreeWalk(treeQueue.root(),p);
        
        p.println(">");
      }
    }

    @Override
    public synchronized String  show ( )
    ////////////////////////////////////////////////////////////////////////
    {
      StringWrap p =  new StringWrap();
      
      if(treeQueue.root()!=null)
      {
        p.out=p.out+"<";
        
        treeQueue.inorderTreeWalk(treeQueue.root(),p);
        
        p.out=p.out+">";
      }
      
      return p.out;
    }

    @Override
    public synchronized void  clear ( )
    ////////////////////////////////////////////////////////////////////////
    {
      treeQueue.clear();
      
      first = null;
      
      firstNode = null;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public synchronized T  lastItem ( )
    ////////////////////////////////////////////////////////////////////////
    {
      if(treeQueue.root()==null)
      {
        return null;
      }
      else
      {
        RBCell<T> tmp = treeQueue.root().rightmost();
        
        return tmp.element();
      }
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }