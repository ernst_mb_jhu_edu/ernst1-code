    package cnslab.cnsnetwork;
    
    import java.io.*;
    import java.util.*;

    /***********************************************************************
    * A buffer to keep all the recorded spikes for sending to main host.
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public class  RecordBuffer
      implements Serializable, Transmissible
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private static final long  serialVersionUID = 0L;
    
    //
    
    public final ArrayList<NetRecordSpike>  buff;
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  RecordBuffer ( )
    ////////////////////////////////////////////////////////////////////////
    {
      buff = new ArrayList<NetRecordSpike> ( );
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }