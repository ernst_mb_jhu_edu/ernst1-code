    package cnslab.cnsnetwork;

    /***********************************************************************
    * To assign meaning to message tags.  It is better to use enum type
    * though. 
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public interface  NetMessageTag
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    public static final int
      sendTids         = 0,
      sendNeurons      = 1,
      trialDone        = 111,
      stopSig          = 222,
      syncRoot         = 333,
      sendTick         = 444,
      sendSpike        = 555,
      gatherFire       = 777,
      gatherInput      = 888,
      sendTime         = 999,
      readySig         = 12345,
      spikeReceived    = 54321,
      syncTrialHost    = 24680,
      tempStopSig      = 67890, // for tuning parameter to send back data
      assignTargets    = 78901, // for new synaptic structure
      sendSeed         = 11111,
      oneTrial         = 22222,
      trialReady       = 33333,
      sendTids2        = 44444,
      changeConnection = 55555,
      netHostNotify    = 66666,
      checkTime        = 77777,
      resetNetHost     = 88888,
      getBackData      = 99999;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }
