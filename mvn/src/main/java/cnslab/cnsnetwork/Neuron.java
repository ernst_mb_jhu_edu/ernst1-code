    package cnslab.cnsnetwork;
   
    import cnslab.cnsmath.*;
    import edu.jhu.mb.ernst.model.Synapse;
    
    /***********************************************************************
    * Interface for a Neuron object.
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft, M.Sc.
    * @author
    *   Jeremy Cohen
    ***********************************************************************/
    public interface  Neuron
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    ////////////////////////////////////////////////////////////////////////
    // accessor methods
    ////////////////////////////////////////////////////////////////////////
    
    /***********************************************************************
    * @return neuron's axon structure
    ***********************************************************************/
    // Axon  getAxon ( );

    /***********************************************************************
    * @return
    *   neuron's current currents of all the channels
    ***********************************************************************/
    double [ ]  getCurr ( double  currTime );

    /***********************************************************************
    * @return
    *   neuron's current membrane voltage
    ***********************************************************************/
    double  getMemV ( double  currTime );

    /***********************************************************************
    * record the neuron
    *  
    * @return
    *   boolean whether it should be recorded
    ***********************************************************************/
    boolean  getRecord ( );

    /***********************************************************************
    * @return
    *   the target host, maximum NetHost 64
    ***********************************************************************/
    long  getTHost ( );

    /***********************************************************************
    * get the value of timeOfNextFire
    * 
    * @return the value of timeOfNextFire
    ***********************************************************************/
    // should be synchronized
    double  getTimeOfNextFire ( );

    /***********************************************************************
    * sensory report 
    * 
    * @return
    *   true if the neuron is a sensory neuron
    ***********************************************************************/
    boolean  isSensory ( );

    /***********************************************************************
    * @return
    *   whether the scheduled neuron fire means the neuron really fires
    ***********************************************************************/
    boolean  realFire ( );

    /***********************************************************************
    * @return the time when the neuron fires based on its current state
    ***********************************************************************/
    // double timeOfFire(double curr);

    ////////////////////////////////////////////////////////////////////////
    // mutator methods
    ////////////////////////////////////////////////////////////////////////
    
    /***********************************************************************
    * set the neuron record or not.
    ***********************************************************************/
    void  setRecord ( boolean  record );

    /***********************************************************************
    * set the target host id, 0 means none, 1 means yes 
    ***********************************************************************/
    void  setTHost ( long  id );

    /***********************************************************************
    * set a new value to timeOfNextFire
    * 
    * @param
    *   timeOfNextFire the new value to be used
    ***********************************************************************/
    // should be synchronized 
    void  setTimeOfNextFire ( double  timeOfNextFire );

    ////////////////////////////////////////////////////////////////////////
    // lifecycle methods
    ////////////////////////////////////////////////////////////////////////
    
    /***********************************************************************
    * initialize the neuron 
    ***********************************************************************/
    void  init (
      int      expid,
      int      trialid,
      Seed     idum,
      Network  net,
      int      id );
    // public void  init (
    //   int    expid,
    //   int    trialid,
    //   Seed   idum,
    //   Queue  fireQueue,
    //   int    id );

    /***********************************************************************
    * update of the neuron after firing 
    * the current time is TimeOfNextFire
    *  
    * @return
    *   the time interval until the next fire event,
    *    or negative if no spike is generated
    ***********************************************************************/
    double  updateFire ( );
  
    /***********************************************************************
    * update of the neuron after receiving a spike 
    * the current time is time
    * 
    * @param input
    *  it is a InputEvent event in the input queue
    *  
    * @return
    *   the time interval until next fire event,
    *    or negative if no spike is generated
    ***********************************************************************/
    double  updateInput (
      double   time,
      Synapse  input );

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }