    package cnslab.cnsnetwork;
    
    import java.io.*;
    import java.util.*;

    /***********************************************************************
    * Store target host id information.
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  TargetID
      implements Serializable, Transmissible
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private static final long  serialVersionUID = 0L;
    
    //

    /** list of hosts which have targets (postsynaptic neurons) */
    public List<Integer>  target;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  TargetID ( int  sourceID )
    ////////////////////////////////////////////////////////////////////////
    {
      target = new ArrayList<Integer> ( );
      
      target.add ( sourceID );
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }