package cnslab.cnsnetwork;
import cnslab.cnsmath.*;
import java.util.Map;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.io.PrintStream;
/**
 * Implement the java provided priority queue
 * 
 * @author Yi Dong
 */
public class PriQueue<T extends Comparable<T> > implements Queue<T>
{
	private PriorityQueue<T> treeQueue = new PriorityQueue<T>();

	/**
	 * @see cnslab.cnsnetwork.Queue#deleteItem(java.lang.Comparable) deleteItem
	 */
	public synchronized void deleteItem(T item) {

		if(item.compareTo( treeQueue.peek())!=0) throw new RuntimeException("delete non fist item (input):"+item+" first item"+treeQueue.peek());
		treeQueue.poll();
	}

	/**
	 * @see cnslab.cnsnetwork.Queue#insertItem(java.lang.Comparable) insertItem
	 */
	public synchronized void insertItem(T item) {
		treeQueue.add(item);
	}


	/**
	 * @see cnslab.cnsnetwork.Queue#firstItem() firstItem
	 */
	public synchronized T firstItem() {
		return treeQueue.peek();
	}

	/** 
	 * show all the elements in the Queue in order 
	 */
	public synchronized void show(PrintStream p) {
		Iterator<T> iter= treeQueue.iterator();
		while(iter.hasNext())
		{
		p.println(iter.next());
		}
	}

	public synchronized String show() {
		String out="";
		Iterator<T> iter= treeQueue.iterator();
		out=out+"<";
		while(iter.hasNext())
		{
			out=out+iter.next();
		}
		out=out+">";
		return out;
	}

	public synchronized void clear(){
		treeQueue.clear();
	}
}
