    package cnslab.cnsnetwork;
    
    /***********************************************************************
    * Neuron input event, contains input time, branch of synapses and id of
    * presynaptic neuron.
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public class  InputEvent
      implements Comparable<InputEvent>, Cloneable
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    /** time of the input event */
    public double  time;

    /** the branch data */
    public Branch  branch;

    /** the neuron which sends spike */
    public int  from;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  InputEvent (
      final double  time,
      final Branch  branch,
      final int     from )
    ////////////////////////////////////////////////////////////////////////
    {
      this.time = time;
      
      this.branch = branch;
      
      this.from = from;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public int  compareTo ( final InputEvent  arg0 )
    ////////////////////////////////////////////////////////////////////////
    {
      if ( time < (arg0).time )
      {
        return -1;
      }
      else if (time > (arg0).time)
      {
        return 1;
      }
      else
      {
        if( from  < (arg0).from )
        {
          return -1;
        }
        else if ( from  > (arg0).from)
        {
          return 1;
        }
        else
        {
          if (branch.delay < (arg0).branch.delay)
          {
            return -1;
          }
          else if (branch.delay > (arg0).branch.delay)
          {
            return 1;
          }
          else
          {
            final int
              targetNeuronIndexThis
                = branch.synapses [ 0 ].getTargetNeuronIndex ( ),
              targetNeuronIndexOther
                = arg0.branch.synapses [ 0 ].getTargetNeuronIndex ( );
            
            if ( targetNeuronIndexThis < targetNeuronIndexOther )
            {
              return -1;
            }
            
            if ( targetNeuronIndexThis > targetNeuronIndexOther )
            {
              return 1;
            }
            
            return 0;
          }
        }
      }
    }

    @Override
    public String  toString ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return new String (
        "time:" + time
        + " from:" + from
        + " branch:" + branch.synapses [ 0 ].toString ( )
        + " delay:" +branch.delay );
    }

    @Override
    public Object  clone ( )
    ////////////////////////////////////////////////////////////////////////
    {
      InputEvent  a1 = null;
      
      try
      {
        a1 = ( InputEvent ) super.clone ( );
      }
      catch ( final CloneNotSupportedException  exception )
      {
        System.err.println ( "Clone Abort." + exception );
      }
      
      return a1;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }