    package cnslab.cnsnetwork;
    
    import java.io.*;
    import java.util.*;
    import javax.xml.parsers.*;
    import org.w3c.dom.*;
    import org.xml.sax.*;    

    import jpvm.*;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import cnslab.cnsmath.*;
    import edu.jhu.mb.ernst.engine.DiscreteEvent;
    import edu.jhu.mb.ernst.engine.DiscreteEventQueue;
    import edu.jhu.mb.ernst.engine.EngineFactory;
    import edu.jhu.mb.ernst.engine.factory.DefaultEngineFactoryImp;
    import edu.jhu.mb.ernst.model.ModelFactory;
    import edu.jhu.mb.ernst.model.ModulatedSynapse;
    import edu.jhu.mb.ernst.model.Synapse;
    import edu.jhu.mb.ernst.model.factory.DefaultModelFactoryImp;
    import edu.jhu.mb.ernst.util.seq.ListSeq;
    import edu.jhu.mb.ernst.util.seq.Seq;

    /***********************************************************************
    * Simulator Parser, to parse XML file  
    * 
    * @version
    *   $Date: 2012-08-04 22:43:19 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 117 $
    *   $Author: jmcohen27 $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft, M.Sc.
    * @author
    *   Jeremy Cohen
    ***********************************************************************/
    public final class  SimulatorParser
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static final Class<SimulatorParser>
      CLASS = SimulatorParser.class;
   
    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );
    
    private static final EngineFactory
      DEFAULT_ENGINE_FACTORY = DefaultEngineFactoryImp.INSTANCE;
    
    private static final ModelFactory
      DEFAULT_MODEL_FACTORY = DefaultModelFactoryImp.INSTANCE;
    
    private static final String
      NUMBER_DOES_NOT_MATCH = "number does not match";
    
    //
   
    public PrintStream  p;

    //  public boolean connected;

    /** minimum synaptic delay */
    public double  minDelay;

    public Experiment  experiment;

    /** Layer structure */
    public LayerStructure
      layerStructure;
    
    /** seed number */
    public Seed  idum;

    /** length of xEdge */
    public int  xEdgeLength;
    
    /** length of yEdge */
    public int  yEdgeLength;

    /** number of Hosts; */
    public int  numOfHosts;

    /** cell map type abmap or linemap */
    public int  mapType;

    /** number of parallel hosts */
    public int  parallelHost;

    public int
      aSec,
      bSec;

    /** background firing rate */
    public double  backgroundFrequency;
    
    /** background synaptic strength */
    public double  backgroundStrength;
    
    /** background inputs channel */
    public int  bChannel; 

    /** output file name */
    public String  outFile = "simExperiment.nc";

    public Document  document;

    public Element  rootElement;

    public DocumentType  documentType;

    /** Names of sub experiment */
    public ArrayList<String>  subexpNames;

    /** Model parameters */
    public Map<String, Para>  modelParaMap;

    /** Double Data defined in Data section of XML file */
    public Map<String, Double>  doubleDataMap;
    
    /** Matrix Data defined in Data section of XML file */
    public Map<String, double [ ] [ ]>  matrixDataMap;
    
    // private final instance variables
    
    private final List<ModulatedSynapse>
      modulatedSynapseList = new ArrayList<ModulatedSynapse> ( );
    
    private final Seq<ModulatedSynapse>
      modulatedSynapseSeq
        = new ListSeq<ModulatedSynapse> ( modulatedSynapseList );
    
    // private non-final instance variables
  
    // TODO:  parse engineFactory and modelFactory class names from XML file
    
    private EngineFactory
      engineFactory = DEFAULT_ENGINE_FACTORY;
    
    private ModelFactory
      modelFactory = DEFAULT_MODEL_FACTORY;
    
    private DiscreteEventQueue
      discreteEventQueue = engineFactory.createDiscreteEventQueue ( );
    
    ////////////////////////////////////////////////////////////////////////
    // static methods
    ////////////////////////////////////////////////////////////////////////
      
    public static void  main ( final String [ ]  args )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      final SimulatorParser
        simulatorParser = new SimulatorParser (
          new Seed ( -3 ),
          new File ( "model/testxml.xml" ) );
      
      // pas.parseMapCells();
      
      simulatorParser.parseMapCells ( 5 );
      
      System.out.println (
        simulatorParser.layerStructure.base
        + " "
        + simulatorParser.layerStructure.neuron_end );
      
      simulatorParser.parsePopNeurons ( 5 );
      
      simulatorParser.parseConnection ( 5 );
      
      simulatorParser.parseExperiment ( );
      
      simulatorParser.findMinDelay ( );
      
      System.out.println ( simulatorParser.minDelay );

      //System.out.println(pas.ls.neurons.length);
      
      /*
      for(int i=0; i< pas.ls.neurons.length-1; i++)
      {
        System.out.println(pas.ls.neurons[i]);
      }
      */
      
      //for(int i=0; i< pas.ls.numberOfNeurons; i++)
      //{
      //  System.out.println(pas.ls.neurons[i]);
      //}
    }

    ////////////////////////////////////////////////////////////////////////
    // constructor methods
    ////////////////////////////////////////////////////////////////////////
    
    public  SimulatorParser (
      final Seed  idum,
      final File  modelFile )
    ////////////////////////////////////////////////////////////////////////
    {
      try 
      {
        System.setProperty (
          "javax.xml.parsers.DocumentBuilderFactory",
          "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl" );
        
        final DocumentBuilderFactory
          documentBuilderFactory = DocumentBuilderFactory.newInstance ( );

        documentBuilderFactory.setExpandEntityReferences ( true );
        
        documentBuilderFactory.setNamespaceAware ( true );
        
        documentBuilderFactory.setValidating ( true );
        
        documentBuilderFactory.setAttribute (
          "http://java.sun.com/xml/jaxp/properties/schemaLanguage",
          "http://www.w3.org/2001/XMLSchema" );
        
        /*
        try
        {
          dbf.setFeature (
            "http://apache.org/xml/features/validation/schema",
            true );
            
          dbf.setFeature (
            "http://apache.org/xml/features/validation/"
              + "schema-full-checking",
            true );
            
          dbf.setFeature (
            "http://xml.org/sax/features/validation",
            true );
        }
        catch ( final Exception  e )
        {
          e.printStackTrace ( );
        }
        */

        final DocumentBuilder
          documentBuilder = documentBuilderFactory.newDocumentBuilder ( );

        documentBuilder.setErrorHandler ( new SimpleErrorHandler ( ) );

        document = documentBuilder.parse ( modelFile );
        
        rootElement = document.getDocumentElement ( );

        rootElement.removeAttribute ( "xmlns:xsi" );
        
        rootElement.removeAttribute ( "xsi:schemaLocation" );

        final DOMConfiguration
          domConfiguration = document.getDomConfig ( );
        
        domConfiguration.setParameter (
          "entities",
          Boolean.FALSE );
        
        // config2.setParameter (
        //   "schema-type",
        //   "http://www.w3.org/2001/XMLSchema" );
        //
        // config2.setParameter (
        //   "validate",
        //   Boolean.TRUE );

        documentType = document.getDoctype ( );

        //doc = db.newDocument();
        //DOMConfiguration config = doc.getDomConfig();
        //config.setParameter("entities", Boolean.FALSE);
        //Node dup = doc.importNode(root , true);
        //doc.appendChild(dup);

        // if(root==null) throw new RuntimeException("reading fails");
        
        modelParaMap = new HashMap<String, Para> ( );
        
        doubleDataMap = new HashMap<String, Double> ( );
        
        matrixDataMap = new HashMap<String, double [ ] [ ]> ( );

        //ReferenceCheck();
      }
      catch ( final SAXParseException  err ) 
      {
        System.out.println (
          "Error parsing line: " + err.getLineNumber ( ) );
        
        throw new RuntimeException ( err );
      }
      catch ( final SAXException  e )
      {
        System.out.println ( "Error parsing, SAXException thrown!" );
        
        throw new RuntimeException ( e );
      }
      catch ( final Throwable  t )
      {
// TODO:  Should probably not catch Throwable
       
        LOGGER.error (
          t.getMessage ( ),
          t );
        
        System.out.println ( "Error parsing, has to throw!" );
        
        t.printStackTrace ( );
        
        throw new RuntimeException ( t );
      }

      //  this.connected = false;
      
      this.idum = idum ;
    }

//    public  SimulatorParser (
//      final Seed    idum,
//      final String  modelFilename ) 
//    //////////////////////////////////////////////////////////////////////
//    {
//      this ( idum, new File ( "model/" + modelFilename ) );
//    }
      
    public SimulatorParser (
      final Seed      idum,
      final Document  doc ) 
    ////////////////////////////////////////////////////////////////////////
    {
      modelParaMap = new HashMap<String, Para> ( );
      
      doubleDataMap = new HashMap<String, Double> ( );
      
      matrixDataMap = new HashMap<String, double [ ] [ ]> ( );
      
      this.document = doc;
      
      rootElement = doc.getDocumentElement ( );
      
      this.idum = idum ;
      
      // this.connected = false;
    }

    public  SimulatorParser ( )
    ////////////////////////////////////////////////////////////////////////
    {
      //
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public DiscreteEventQueue  getDiscreteEventQueue ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return discreteEventQueue;
    }
    
    public EngineFactory  getEngineFactory ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return engineFactory;
    }
    
    public ModelFactory  getModelFactory ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return modelFactory;
    }
    
    public Seq<ModulatedSynapse>  getModulatedSynapseSeq ( )
    ////////////////////////////////////////////////////////////////////////
    {
      if ( modulatedSynapseSeq.size ( ) == 0 )
      {
        // TODO:  move this to init instead of lazy initialization
        
        populateModulatedSynapseSeq ( );
      }
      
      return modulatedSynapseSeq;
    }
    
    /***********************************************************************
    * Check whether the XML file is valid or not 
    ***********************************************************************/
    public String  validate ( final String  modelFilename )
    ////////////////////////////////////////////////////////////////////////
    {
      return validate ( new File ( "model/" + modelFilename ) );
    }
    
    /***********************************************************************
    * Check whether the XML file is valid or not 
    ***********************************************************************/
    public String  validate ( final File  modelFile )
    ////////////////////////////////////////////////////////////////////////
    {
      String  tmp = "";
      
      try 
      {
        System.setProperty (
          "javax.xml.parsers.DocumentBuilderFactory",
          "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl" );
        
        final DocumentBuilderFactory
          dbf = DocumentBuilderFactory.newInstance ( );

        dbf.setExpandEntityReferences ( true );
        
        dbf.setNamespaceAware ( true );
        
        dbf.setValidating ( true );
       
        dbf.setAttribute (
          "http://java.sun.com/xml/jaxp/properties/schemaLanguage",
          "http://www.w3.org/2001/XMLSchema" );
        
        /*
        try
        {
          dbf.setFeature (
            "http://apache.org/xml/features/validation/schema",
            true );
            
          dbf.setFeature (
            "http://apache.org/xml/features/validation/"
              + "schema-full-checking",
            true );
            
          dbf.setFeature (
            "http://xml.org/sax/features/validation",
            true );
        }
        catch ( final Exception  e )
        {
          e.printStackTrace ( );
        }
        */

        final DocumentBuilder  db = dbf.newDocumentBuilder ( );

        db.setErrorHandler ( new SimpleErrorHandler ( ) );

        // System.out.println("1");
        
        document = db.parse ( modelFile );
        
        // System.out.println("2");

        rootElement = document.getDocumentElement ( );

        rootElement.removeAttribute ( "xmlns:xsi");
        
        rootElement.removeAttribute ( "xsi:schemaLocation" );

        final DOMConfiguration  config2 = document.getDomConfig ( );
        
        config2.setParameter (
          "entities",
          Boolean.FALSE );
        
        // config2.setParameter (
        //   "schema-type",
        //   "http://www.w3.org/2001/XMLSchema" );
        // config2.setParameter ("validate",Boolean.TRUE );

        documentType = document.getDoctype ( );

        modelParaMap = new HashMap<String,Para> ( );
        
        doubleDataMap = new HashMap<String,Double> ( );
        
        matrixDataMap = new HashMap<String,double [ ] [ ]> ( );

        //  ReferenceCheck();
      }
      catch ( final SAXParseException  err )
      {
        LOGGER.error (
          err.getMessage ( ),
          err );
        
        tmp = tmp + "Error parsing line: " + err.getLineNumber ( ) + "\n";
        
        // System.exit(-1);
      }
      catch ( final SAXException  e )
      {
        LOGGER.error (
          e.getMessage ( ),
          e );
        
        tmp = tmp + "Error parsing, SAXException thrown!\n";
        
        //System.exit(-1);
      }
      catch ( final Throwable  t ) 
      {
// TODO:  Should probably not catch Throwable
        
        LOGGER.error (
          t.getMessage ( ),
          t );
       
        // System.out.println("Error parsing, has to throw!");
        
        // t.printStackTrace ( );
        
        // System.exit(-1);
        
        tmp = tmp + t.getMessage ( ) + "\n";
      }
      
      return tmp;
    }

    /***********************************************************************
    * Check XML reference is good or not
    ***********************************************************************/
    public void  referenceCheck ( )
    ////////////////////////////////////////////////////////////////////////
    {
      // source in data section
      
      final Element
        dataSec = getFirstElement ( rootElement, "ConstantDefinition" ),
        neuDef  = getFirstElement ( rootElement, "NeuronDefinition" ),
        laySec  = getFirstElement ( rootElement, "Layers" );

      // first double
      
      final Set<String>
        doubleIdDataSet = createDataSetFromAttributeValues (
          dataSec,
          "Double",
          "id" );
      
      // check double
      
      checkDataSetContainsFirstGrandchildValues (
        doubleIdDataSet,
        rootElement,
        "Strength",
        "Ref",
        " as strength is not defined in the data section" );      

      checkDataSetContainsFirstGrandchildValues (
        doubleIdDataSet,
        rootElement,
        "Delay",
        "Ref",
        " as delay is not defined in the data section" );      

      final Set<String>
        matrixIdDataSet = createDataSetFromAttributeValues (
          dataSec,
          "Matrix",
          "id" );
      
      checkDataSetContainsFirstGrandchildValues (
        matrixIdDataSet,
        rootElement,
        "Matrix",
        "Ref",
        " as matrix is not defined in the data section" );

      // neuron type definition check
      
      final Set<String>
        neuronTypeNameDataSet = createDataSetFromFirstChildValues (
          neuDef,
          "Name" );
      
      checkDataSetContainsFirstChildValues (
        neuronTypeNameDataSet,
        rootElement,
        "NeuronType",
        " as a neuron model is not defined in the NeuronDefinition"
          + " section" );

      // layer prefix check
      
      final Set<String>
        layerPrefixDataSet = createDataSetFromFirstChildValues (
          laySec,
          "Prefix" );
      
      checkDataSetContainsAttributeValues (
        layerPrefixDataSet,
        rootElement,
        "FromLayer",
        "prefix",
        " as layer prefix in FromLayer is not defined in the Layers "
          + "section" );
      
      checkDataSetContainsAttributeValues (
        layerPrefixDataSet,
        rootElement,
        "ToLayer",
        "prefix",
        " as layer prefix in ToLayer is not defined in the Layers "
          + "section" );
      
      checkDataSetContainsAttributeValues (
        layerPrefixDataSet,
        rootElement,
        "LayerOrientation",
        "pre",
        " as layer prefix in VectorMap is not defined in the Layers"
          + " section" );
      
      checkDataSetContainsAttributeValues (
        layerPrefixDataSet,
        rootElement,
        "Line",
        "pre",
        " as layer prefix in Recorder section is not defined in the "
          + "Layers section" );

      checkDataSetContainsAttributeValues (
        layerPrefixDataSet,
        rootElement,
        "Square",
        "pre",
        " as layer prefix in Recorder section is not defined in the "
          + "Layers section" );

      checkDataSetContainsFirstChildValues (
        layerPrefixDataSet,
        rootElement,
        "Prefix",
        " as layer prefix name in Experiment or Recorder sections is"
          + " not defined in the Layers section" );

      // layer suffix check
      
      final Set<String>
        layerSuffixDataSet = createDataSetFromFirstChildValues (
          laySec,
          "Suffix" );
      
      checkDataSetContainsAttributeValues (
        layerSuffixDataSet,
        rootElement,
        "FromLayer",
        "suffix",
        " as layer suffix in FromLayer is not defined in the "
          + "Layers section" );

      checkDataSetContainsAttributeValues (
        layerSuffixDataSet,
        rootElement,
        "ToLayer",
        "suffix",
        " as layer suffix in ToLayer is not defined in the "
          + "Layers section" );

      checkDataSetContainsAttributeValues (
        layerSuffixDataSet,
        rootElement,
        "LayerOrientation",
        "suf",
        " as layer suffix in VectorMap is not defined in the "
          + " Layers section" );

      checkDataSetContainsAttributeValues (
        layerSuffixDataSet,
        rootElement,
        "Line",
        "suf",
        " as layer suffix in Recorder section is not defined in the "
          + "Layers section" );

      checkDataSetContainsAttributeValues (
        layerSuffixDataSet,
        rootElement,
        "Square",
        "suf",
        " as layer suffix in Recorder section is not defined in the "
          + "Layers section" );
      
      checkDataSetContainsFirstChildValues (
        layerSuffixDataSet,
        rootElement,
        "Suffix",
        " as layer suffix name in Experiment or Recorder sections is "
          + "not defined in the Layers section" );

      // System.out.println ( "Reference check...........OK!" );
    }

    /***********************************************************************
    * Parse the connection section, connect the neurons and build up the
    * network structure
    ***********************************************************************/
// TODO:  Is this method used?  If so, finish cleanup and create unit tests.    
    public double  parseConnection ( )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      double  maxOutput = -1.0;
      
      final NodeList
        connectionsNodeList
          = rootElement.getElementsByTagName ( "Connections" );
      
      final Element
        connectionsElement = ( Element ) connectionsNodeList.item ( 0 );

      // read layers
      
      // loop connections
      
      final NodeList  lConnectionNodeList
        = connectionsElement.getElementsByTagName ( "LConnection" );
      
      final int
        lConnectionNodeListLength = lConnectionNodeList.getLength ( );
      
      // add all the layers
      
      for ( int  it = 0; it < lConnectionNodeListLength; it++ )
      {
        final Element
          lConnectionElement = ( Element ) lConnectionNodeList.item ( it );
        
        int  max = 1;
        
        final NodeList  fromLayerNodeList
          = lConnectionElement.getElementsByTagName ( "FromLayer" );
        
        if ( fromLayerNodeList.getLength ( ) != 1 )
        {
          max = fromLayerNodeList.getLength ( );
        }
        
        final NodeList  toLayerNodeList
          = lConnectionElement.getElementsByTagName ( "ToLayer" );
        
        final int  toLayerNodeListLength = toLayerNodeList.getLength ( );
        
        if ( toLayerNodeListLength !=1
          && max != 1
          && toLayerNodeListLength != max )
        {
          throw new RuntimeException ( NUMBER_DOES_NOT_MATCH );
        }
        
        if ( toLayerNodeListLength != 1 )
        {
          max = toLayerNodeListLength;
        }
        
        final NodeList  matrixNodeList
          = lConnectionElement.getElementsByTagName ( "Matrix" );
        
        final int  matrixNodeListLength = matrixNodeList.getLength ( );
        
        if ( matrixNodeListLength != 1
          && max != 1
          && matrixNodeListLength != max )
        {
          throw new RuntimeException ( "matrix " + matrixNodeListLength
            + " max " + max + " " + NUMBER_DOES_NOT_MATCH );
        }
        
        if ( matrixNodeListLength !=1 )
        {
          max = matrixNodeListLength;
        }
        
        final NodeList
          orientationNodeList
            = lConnectionElement.getElementsByTagName ( "Rotation" );
        
        final int
          orientationNodeListLength = orientationNodeList.getLength ( ); 
        
        if ( orientationNodeListLength !=1
          && max != 1
          && orientationNodeListLength != max )
        {
          throw new RuntimeException ( NUMBER_DOES_NOT_MATCH );
        }
        
        if ( orientationNodeListLength != 1 )
        {
          max = orientationNodeListLength;
        }
        
        final NodeList
          styleNodeList
            = lConnectionElement.getElementsByTagName ( "Style" );
        
        final int  styleNodeListLength = styleNodeList.getLength ( );
        
        if ( styleNodeListLength != 1
          && max != 1
          && styleNodeListLength != max )
        {
          throw new RuntimeException ( NUMBER_DOES_NOT_MATCH );
        }
        
        if ( styleNodeListLength != 1 )
        {
          max = styleNodeListLength;
        }
        
        final NodeList
          typeNodeList
            = lConnectionElement.getElementsByTagName ( "Type" );
        
        final int  typeNodeListLength = typeNodeList.getLength ( );
        
        if ( typeNodeListLength != 1
          && max != 1
          && typeNodeListLength != max )
        {
          throw new RuntimeException ( NUMBER_DOES_NOT_MATCH );
        }
        
        if ( typeNodeListLength != 1 )
        {
          max = typeNodeListLength;
        }
        
        NodeList str =  lConnectionElement.getElementsByTagName("Strength");
        
        if(str.getLength() !=1 && max !=1 && str.getLength() != max)
          throw new RuntimeException(NUMBER_DOES_NOT_MATCH);
        
        if(str.getLength() !=1 ) max = str.getLength();
        
        NodeList multi =  lConnectionElement.getElementsByTagName(
          "Multiplier");
        
        if(multi.getLength() !=1 && max !=1 && multi.getLength() != max)
          throw new RuntimeException(NUMBER_DOES_NOT_MATCH);
        
        if(multi.getLength() !=1 ) max = multi.getLength();
        
        NodeList offset =  lConnectionElement.getElementsByTagName(
          "Offset");
        
        if(offset.getLength() !=1 && max !=1 && offset.getLength() != max)
          throw new RuntimeException(NUMBER_DOES_NOT_MATCH);
        
        if(offset.getLength() !=1 ) max = offset.getLength();
        
        NodeList delay =  lConnectionElement.getElementsByTagName("Delay");
        
        if(delay.getLength() !=1 && max !=1 && delay.getLength() != max)
          throw new RuntimeException(NUMBER_DOES_NOT_MATCH);
        
        if(delay.getLength() !=1 ) max = delay.getLength();
        
        //      System.out.println("max"+max);

        for(int i=0; i < max; i++)
        {
          Element lmatrix;
          if(matrixNodeList.getLength()==max)
          {
            lmatrix = (Element)(matrixNodeList.item(i));
          }
          else
          {
            lmatrix = (Element)(matrixNodeList.item(0));
          }
          double [][] conMatrix;
          if(lmatrix.getElementsByTagName("Ref").getLength()>0)
          {
            conMatrix = matrixDataMap.get(
              lmatrix.getElementsByTagName("Ref")
              .item(0).getFirstChild().getNodeValue());
            
            if(conMatrix == null)
              throw new RuntimeException(
                lmatrix.getElementsByTagName("Ref").item(0)
                .getFirstChild().getNodeValue()
                +" as a reference is not defined in Data Section");
          }
          else
          {
            NodeList rows = lmatrix.getElementsByTagName("Row");
            String firstRow = rows.item(0).getFirstChild().getNodeValue();
            String sep [] = firstRow.split("[a-z,\t ]");
            //      System.out.println(sep.length);
            conMatrix = new double [rows.getLength()][sep.length];
            for(int colId=0; colId < sep.length; colId++)
            {
              conMatrix[0][colId]=Double.parseDouble(sep[colId]);
            }

            for( int rowId=1; rowId<rows.getLength(); rowId++)
            {
              firstRow = rows.item(rowId).getFirstChild().getNodeValue();
              sep = firstRow.split("[a-z,\t ]");
              for(int colId=0; colId < sep.length; colId++)
              {
                conMatrix[rowId][colId]=Double.parseDouble(sep[colId]);
              }
            }
          }
          /*
      for(int ii=0; ii < conMatrix.length; ii++)
      {
        for(int jj=0 ; jj < conMatrix[0].length; jj++)
        {
          System.out.print(conMatrix[ii][jj]+" ");
        }
        System.out.println();
      }
           */


          Element lrotation;
          if(orientationNodeList.getLength()==max)
          {
            lrotation = (Element)(orientationNodeList.item(i));
          }
          else
          {
            lrotation = (Element)(orientationNodeList.item(0));
          }

          int rot = Integer.parseInt(
            lrotation.getFirstChild().getNodeValue())/90;

          if(rot>0)
          {
            for(int ii=0 ; ii< rot; ii++)
            {
              conMatrix = FunUtil.rRotate90(conMatrix);
            }
          }
          else if(rot <0)
          {
            for(int ii=0 ; ii< -rot; ii++)
            {
              conMatrix = FunUtil.lRotate90(conMatrix);
            }
          }
          //remeber to change the rows matrix. use rows
          Element lfrom;
          if(fromLayerNodeList.getLength()==max)
          {
            lfrom = (Element)(fromLayerNodeList.item(i));
          }
          else
          {
            lfrom = (Element)(fromLayerNodeList.item(0));
          }
          Element lto;
          if(toLayerNodeList.getLength()==max)
          {
            lto = (Element)(toLayerNodeList.item(i));
          }
          else
          {
            lto = (Element)(toLayerNodeList.item(0));
          }
          Element lstyle;
          if(styleNodeList.getLength()==max)
          {
            lstyle = (Element)(styleNodeList.item(i));
          }
          else
          {
            lstyle = (Element)(styleNodeList.item(0));
          }

          String sStyle =lstyle.getFirstChild().getNodeValue();
          
          final boolean  sty = parseVergenceStyle ( lstyle );
          
          Element ltype;
          if(typeNodeList.getLength()==max)
          {
            ltype = (Element)(typeNodeList.item(i));
          }
          else
          {
            ltype = (Element)(typeNodeList.item(0));
          }
          
          final int  iType = parseSynapseType ( ltype ); 

          Element lstr;
          if(str.getLength()==max)
          {
            lstr = (Element)(str.item(i));
          }
          else
          {
            lstr = (Element)(str.item(0));
          }

          Element lmulti;
          if(multi.getLength()==max)
          {
            lmulti = (Element)(multi.item(i));
          }
          else
          {
            lmulti = (Element)(multi.item(0));
          }
          Element loffset;
          if(offset.getLength()==max)
          {
            loffset = (Element)(offset.item(i));
          }
          else
          {
            loffset = (Element)(offset.item(0));
          }
          Element ldelay;
          if(delay.getLength()==max)
          {
            ldelay = (Element)(delay.item(i));
          }
          else
          {
            ldelay = (Element)(delay.item(0));
          }
          /*
      System.out.print(
        lfrom.getAttributeNode("prefix").getNodeValue()
        +" "+lfrom.getAttributeNode("suffix").getNodeValue()
        +" "+lto.getAttributeNode("prefix").getNodeValue()
        +" "+lto.getAttributeNode("suffix").getNodeValue());
      System.out.print(" "+sty);
      System.out.print(" "+lstr.getFirstChild().getNodeValue());
      System.out.print(" "+lmulti.getFirstChild().getNodeValue());
      System.out.print(" "+loffset.getFirstChild().getNodeValue());
      System.out.print(" "+ldelay.getFirstChild().getNodeValue());
      System.out.println();
           */
          /*
      for(int ii=0; ii < conMatrix.length; ii++)
      {
        for(int jj=0 ; jj < conMatrix[0].length; jj++)
        {
          System.out.print(conMatrix[ii][jj]+" ");
        }
        System.out.println();
      }
           */

          // System.out.println(Double.parseDouble(
          //   ((Element)(nList.item(it)))
          //   .getElementsByTagName("Multiplier").item(0)
          //   .getFirstChild().getNodeValue()));
          // System.out.println(Double.parseDouble(
          //   ((Element)(nList.item(it)))
          //   .getElementsByTagName("Offset").item(0)
          //   .getFirstChild().getNodeValue()));
          // System.out.println(Double.parseDouble(
          //   ((Element)(nList.item(it)))
          //   .getElementsByTagName("Strength").item(0)
          //   .getFirstChild().getNodeValue())
          //   *Double.parseDouble(((Element)(nList.item(it)))
          //   .getElementsByTagName("Multiplier").item(0)
          //   .getFirstChild().getNodeValue())
          //   + Double.parseDouble(((Element)(nList.item(it)))
          //   .getElementsByTagName("Offset").item(0)
          //   .getFirstChild().getNodeValue()));
          //
          
          Double theStr;
          
          if(lstr.getElementsByTagName("Ref").getLength()>0)
          {
            theStr = doubleDataMap.get(lstr.getElementsByTagName("Ref")
              .item(0).getFirstChild().getNodeValue());
            
            if(theStr == null)
              throw new RuntimeException(
                lstr.getElementsByTagName("Ref").item(0).getFirstChild()
                .getNodeValue()
                +" as a reference is not defined in Data Section");
          }
          else
          {
            theStr = Double.parseDouble(
              lstr.getFirstChild().getNodeValue());
          }
          
          Double theDelay;
          
          if(ldelay.getElementsByTagName("Ref").getLength()>0)
          {
            theDelay = doubleDataMap.get(
              ldelay.getElementsByTagName("Ref").item(0)
              .getFirstChild().getNodeValue());
            
            if(theDelay == null)
              throw new RuntimeException(
                ldelay.getElementsByTagName("Ref").item(0)
                .getFirstChild().getNodeValue()
                +" as a reference is not defined in Data Section");
          }
          else
          {
            theDelay = Double.parseDouble(
              ldelay.getFirstChild().getNodeValue());
          }

          double outVal;
          
          outVal = layerStructure.connect (
            lfrom.getAttributeNode("prefix").getNodeValue(),
            lfrom.getAttributeNode("suffix").getNodeValue(),
            lto.getAttributeNode("prefix").getNodeValue(),
            lto.getAttributeNode("suffix").getNodeValue(),
            sty,
            theStr*Double.parseDouble(
              lmulti.getFirstChild().getNodeValue())
              + Double.parseDouble(loffset.getFirstChild().getNodeValue()),
            iType,
            theDelay,
            conMatrix);
          if(outVal > maxOutput) maxOutput = outVal;
        }
      }

      final NodeList  nList
        = connectionsElement.getElementsByTagName("Connection");
      
      for(int it= 0 ; it< nList.getLength(); it++) // add all the layers;
      {
        Element matrix = (Element)(((Element)(nList.item(it)))
          .getElementsByTagName("Matrix").item(0));

        NodeList rows = matrix.getElementsByTagName("Row");
        
        String firstRow = rows.item(0).getFirstChild().getNodeValue();
        
        String sep [] = firstRow.split("[a-z,\t ]");
        
        //      System.out.println(sep.length);
        
        double [][] conMatrix = new double [rows.getLength()][sep.length];
        
        for(int colId=0; colId < sep.length; colId++)
        {
          conMatrix[0][colId]=Double.parseDouble(sep[colId]);
        }

        for( int rowId=1; rowId<rows.getLength(); rowId++)
        {
          firstRow = rows.item(rowId).getFirstChild().getNodeValue();
          
          sep = firstRow.split("[a-z,\t ]");
          
          for(int colId=0; colId < sep.length; colId++)
          {
            conMatrix[rowId][colId]=Double.parseDouble(sep[colId]);
          }
        }
        /*

      for(int ii=0; ii < conMatrix.length; ii++)
      {
        for(int jj=0 ; jj < conMatrix[0].length; jj++)
        {
          System.out.print(conMatrix[ii][jj]+" ");
        }
        System.out.println();
      }
         */

        int rotation = Integer.parseInt (
          ((Element)(nList.item(it)))
          .getElementsByTagName("Rotation").item(0)
          .getFirstChild().getNodeValue())/90;

        if(rotation>0)
        {
          for(int ii=0 ; ii< rotation; ii++)
          {
            conMatrix = FunUtil.rRotate90(conMatrix);
          }
        }
        else if(rotation <0)
        {
          for(int ii=0 ; ii< -rotation; ii++)
          {
            conMatrix = FunUtil.lRotate90(conMatrix);
          }
        }
        /*

      for(int ii=0; ii < conMatrix.length; ii++)
      {
        for(int jj=0 ; jj < conMatrix[0].length; jj++)
        {
          System.out.print(conMatrix[ii][jj]+" ");
        }
        System.out.println();
      }
         */


        Element from = (Element)(((Element)(nList.item(it)))
          .getElementsByTagName("FromLayer").item(0));
        
        Element to  = (Element)(((Element)(nList.item(it)))
          .getElementsByTagName("ToLayer").item(0));
        
        final Element  styleElement = getFirstElement (
          ( Element ) nList.item ( it ),
          "Style " );
        
        final boolean  sty = parseVergenceStyle ( styleElement );
        
        //synaptic type  
        
        final Element  typeElement = getFirstElement (
          ( Element ) nList.item ( it ),
          "Type" );

        final int  type = parseSynapseType ( typeElement );
        
        System.out.println(
          ((Element)(from.getElementsByTagName("Prefix").item(0)))
          .getFirstChild().getNodeValue()
          +" "
          + ((Element)(from.getElementsByTagName("Suffix").item(0)))
          .getFirstChild().getNodeValue()+" "
          + ((Element)(to.getElementsByTagName("Prefix").item(0)))
          .getFirstChild().getNodeValue()
          +" "
          + ((Element)(to.getElementsByTagName("Suffix").item(0)))
          .getFirstChild().getNodeValue());
        
        // System.out.println(sty);
        // System.out.println(Double.parseDouble(
        //   ((Element)(nList.item(it))).getElementsByTagName("Strength")
        //   .item(0).getFirstChild().getNodeValue()));
        // System.out.println(Double.parseDouble(
        //   ((Element)(nList.item(it))).getElementsByTagName("Multiplier")
        //   .item(0).getFirstChild().getNodeValue()));
        // System.out.println(Double.parseDouble(
        //   ((Element)(nList.item(it))).getElementsByTagName("Offset")
        //   .item(0).getFirstChild().getNodeValue()));
        // System.out.println(Double.parseDouble(
        //   ((Element)(nList.item(it))).getElementsByTagName("Strength")
        //   .item(0).getFirstChild().getNodeValue())
        //   *Double.parseDouble(((Element)(nList.item(it)))
        //   .getElementsByTagName("Multiplier").item(0)
        //   .getFirstChild().getNodeValue())
        //   + Double.parseDouble(((Element)(nList.item(it)))
        //   .getElementsByTagName("Offset").item(0)
        //   .getFirstChild().getNodeValue()));
        
        double outVal;
        
        outVal =  layerStructure.connect (
          ((Element)(from.getElementsByTagName("Prefix").item(0)))
            .getFirstChild().getNodeValue(),
          ((Element)(from.getElementsByTagName("Suffix").item(0)))
            .getFirstChild().getNodeValue(),
          ((Element)(to.getElementsByTagName("Prefix").item(0)))
            .getFirstChild().getNodeValue(),
          ((Element)(to.getElementsByTagName("Suffix").item(0)))
            .getFirstChild().getNodeValue(),
          sty,
          Double.parseDouble(((Element)(nList.item(it)))
            .getElementsByTagName("Strength").item(0).getFirstChild()
            .getNodeValue())
            *Double.parseDouble(((Element)(nList.item(it)))
            .getElementsByTagName("Multiplier").item(0)
            .getFirstChild().getNodeValue())
            + Double.parseDouble(((Element)(nList.item(it)))
            .getElementsByTagName("Offset").item(0)
            .getFirstChild().getNodeValue()),
          type,
          Double.parseDouble(((Element)(nList.item(it)))
            .getElementsByTagName("Delay").item(0).getFirstChild()
            .getNodeValue()),
          conMatrix);
        
        if(outVal > maxOutput) maxOutput = outVal;
      }
      
      return maxOutput;
    }

    /***********************************************************************
    * Build the scaffold.
    * 
    * This method is called by NetHost.createSimulatorParser().
    ***********************************************************************/
    public void  parseScaffold ( final int  hostId )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      // double  maxOutput = -1.0;
      
      final NodeList
        connectionsNodeList
          = rootElement.getElementsByTagName ( "Connections" );
      
      final Element
        connectionsElement = ( Element ) connectionsNodeList.item ( 0 );

      // read layers loop connections
      
      // loop connections
      
      final NodeList  lConnectionNodeList
        = connectionsElement.getElementsByTagName ( "LConnection" );
      
      final int
        lConnectionNodeListLength = lConnectionNodeList.getLength ( );
      
      // add all the layers
      
      for ( int  it = 0; it < lConnectionNodeListLength; it++ )
      {
        final Element  lConnectionElement
          = ( Element ) lConnectionNodeList.item ( it );
        
        parseScaffoldWithPeriodic ( hostId, lConnectionElement );
      }

      // read layers general loop connections
      
      // loop connections
      
      final NodeList  gConnectionNodeList
        = connectionsElement.getElementsByTagName ( "GConnection" );
      
      final int
        gConnectionNodeListLength = gConnectionNodeList.getLength ( );
      
      // add all the layers
      
      for ( int  it = 0; it < gConnectionNodeListLength; it++ )
      {
        final Element  gConnectionElement
          = ( Element ) gConnectionNodeList.item ( it );
        
        parseScaffold ( hostId, gConnectionElement );
      }

      // read layers probability connections
      
      // loop connections
      
      final NodeList  pConnectionNodeList
        = connectionsElement.getElementsByTagName ( "PConnection" );
      
      final int
        pConnectionNodeListLength = pConnectionNodeList.getLength ( );
      
      // add all the layers
      
      for ( int  it = 0; it < pConnectionNodeListLength; it++ )
      {
        final Element  pConnectionElement
          = ( Element ) pConnectionNodeList.item ( it );
        
        parseScaffoldWithPeriodicAndStrengthMatrix (
          hostId,
          pConnectionElement );
      }

      // read layers probability general connections
      
      // loop connections
      
      final NodeList  gpConnectionNodeList
        = connectionsElement.getElementsByTagName ( "GPConnection" );
      
      final int
        gpConnectionNodeListLength = gpConnectionNodeList.getLength ( );
      
      // add all the layers;
      
      for ( int  it = 0; it < gpConnectionNodeListLength; it++ )
      {
        final Element  connectionElement
          = ( Element ) gpConnectionNodeList.item ( it );
        
        parseScaffoldWithStrengthMatrix ( hostId, connectionElement );
      }

      // read layers

      final NodeList  connectionNodeList
        = connectionsElement.getElementsByTagName ( "Connection" );
      
      final int
        connectionNodeListLength = connectionNodeList.getLength ( );
      
      // add all the layers
      
      for ( int  it = 0; it < connectionNodeListLength; it++ )
      {
        final Element
          connectionElement = ( Element ) connectionNodeList.item ( it );
        
        parseScaffoldWithPeriodicAndSingleElement (
          hostId,
          connectionElement );
      }
    }

    @Deprecated
    public double parseChangeConnection(int hostId) throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      layerStructure.reSetSeed();
      double maxOutput = -1.0;
      NodeList nList = rootElement.getElementsByTagName("Connections");
      Element conns = (Element) nList.item(0);

      //read layers;
      nList = conns.getElementsByTagName("LConnection"); //loop connections
      for(int it= 0 ; it< nList.getLength(); it++) // add all the layers;
      {
        String perio = ((Element)(nList.item(it)))
          .getAttributeNode("periodic").getValue();
        boolean periodic;
        if(perio.equals("true") || perio.equals("TRUE"))
        {
          periodic = true;
        }
        else
        {
          periodic = false;
        }

        int max=1;
        NodeList from =  ((Element)(nList.item(it)))
          .getElementsByTagName("FromLayer");
        if(from.getLength() !=1) max = from.getLength();
        NodeList to =  ((Element)(nList.item(it)))
          .getElementsByTagName("ToLayer");
        if(to.getLength() !=1 && max !=1 && to.getLength() != max)
          throw new RuntimeException("number does not match");
        if(to.getLength() !=1 ) max = to.getLength();
        NodeList matrix =  ((Element)(nList.item(it)))
          .getElementsByTagName("Matrix");
        if(matrix.getLength() !=1 && max !=1 && matrix.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(matrix.getLength() !=1 ) max = matrix.getLength();
        NodeList orientation =  ((Element)(nList.item(it)))
          .getElementsByTagName("Rotation");
        if(orientation.getLength() !=1 && max !=1 
          && orientation.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(orientation.getLength() !=1 ) max = orientation.getLength();
        NodeList style =  ((Element)(nList.item(it)))
          .getElementsByTagName("Style");
        if(style.getLength() !=1 && max !=1 && style.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(style.getLength() !=1 ) max = style.getLength();
        NodeList type =  ((Element)(nList.item(it)))
          .getElementsByTagName("Type");
        if(type.getLength() !=1 && max !=1 && type.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(type.getLength() !=1 ) max = type.getLength();
        NodeList str =  ((Element)(nList.item(it)))
          .getElementsByTagName("Strength");
        if(str.getLength() !=1 && max !=1 && str.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(str.getLength() !=1 ) max = str.getLength();
        NodeList multi =  ((Element)(nList.item(it)))
          .getElementsByTagName("Multiplier");
        if(multi.getLength() !=1 && max !=1 && multi.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(multi.getLength() !=1 ) max = multi.getLength();
        NodeList offset =  ((Element)(nList.item(it)))
          .getElementsByTagName("Offset");
        if(offset.getLength() !=1 && max !=1 && offset.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(offset.getLength() !=1 ) max = offset.getLength();
        NodeList delay =  ((Element)(nList.item(it)))
          .getElementsByTagName("Delay");
        if(delay.getLength() !=1 && max !=1 && delay.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(delay.getLength() !=1 ) max = delay.getLength();
        //      System.out.println("max"+max);

        for(int i=0; i < max; i++)
        {
          Element lmatrix;
          if(matrix.getLength()==max)
          {
            lmatrix = (Element)(matrix.item(i));
          }
          else
          {
            lmatrix = (Element)(matrix.item(0));
          }


          double [][] conMatrix;
          if(lmatrix.getElementsByTagName("Ref").getLength()>0)
          {
            conMatrix = matrixDataMap.get(
              lmatrix.getElementsByTagName("Ref")
              .item(0).getFirstChild().getNodeValue());
            if(conMatrix == null) 
              throw new RuntimeException(
                lmatrix.getElementsByTagName("Ref").item(0)
                .getFirstChild().getNodeValue()
                +" as a reference is not defined in Data Section");
          }
          else
          {
            NodeList rows = lmatrix.getElementsByTagName("Row");
            String firstRow = rows.item(0).getFirstChild().getNodeValue();
            String sep [] = firstRow.split("[a-z,\t ]");
            //      System.out.println(sep.length);
            conMatrix = new double [rows.getLength()][sep.length];
            for(int colId=0; colId < sep.length; colId++)
            {
              conMatrix[0][colId]=Double.parseDouble(sep[colId]);
            }

            for( int rowId=1; rowId<rows.getLength(); rowId++)
            {
              firstRow = rows.item(rowId).getFirstChild().getNodeValue();
              sep = firstRow.split("[a-z,\t ]");
              for(int colId=0; colId < sep.length; colId++)
              {
                conMatrix[rowId][colId]=Double.parseDouble(sep[colId]);
              }
            }
          }

          Element lrotation;
          if(orientation.getLength()==max)
          {
            lrotation = (Element)(orientation.item(i));
          }
          else
          {
            lrotation = (Element)(orientation.item(0));
          }

          int rot =Integer.parseInt(
            lrotation.getFirstChild().getNodeValue())/90;

          if(rot>0)
          {
            for(int ii=0 ; ii< rot; ii++)
            {
              conMatrix = FunUtil.rRotate90(conMatrix);
            }
          }
          else if(rot <0)
          {
            for(int ii=0 ; ii< -rot; ii++)
            {
              conMatrix = FunUtil.lRotate90(conMatrix);
            }
          }
          //remeber to change the rows matrix. use rows
          Element lfrom;
          if(from.getLength()==max)
          {
            lfrom = (Element)(from.item(i));
          }
          else
          {
            lfrom = (Element)(from.item(0));
          }
          Element lto;
          if(to.getLength()==max)
          {
            lto = (Element)(to.item(i));
          }
          else
          {
            lto = (Element)(to.item(0));
          }
          Element lstyle;
          if(style.getLength()==max)
          {
            lstyle = (Element)(style.item(i));
          }
          else
          {
            lstyle = (Element)(style.item(0));
          }
          
          final boolean  sty = parseVergenceStyle ( lstyle );

          Element ltype;
          if(type.getLength()==max)
          {
            ltype = (Element)(type.item(i));
          }
          else
          {
            ltype = (Element)(type.item(0));
          }

          final int  iType = parseSynapseType ( ltype );
          
          Element lstr;
          
          if(str.getLength()==max)
          {
            lstr = (Element)(str.item(i));
          }
          else
          {
            lstr = (Element)(str.item(0));
          }
          Element lmulti;
          if(multi.getLength()==max)
          {
            lmulti = (Element)(multi.item(i));
          }
          else
          {
            lmulti = (Element)(multi.item(0));
          }
          Element loffset;
          if(offset.getLength()==max)
          {
            loffset = (Element)(offset.item(i));
          }
          else
          {
            loffset = (Element)(offset.item(0));
          }
          Element ldelay;
          if(delay.getLength()==max)
          {
            ldelay = (Element)(delay.item(i));
          }
          else
          {
            ldelay = (Element)(delay.item(0));
          }
          
          Double theStr;
          if(lstr.getElementsByTagName("Ref").getLength()>0)
          {
            theStr = doubleDataMap.get(lstr.getElementsByTagName("Ref")
              .item(0).getFirstChild().getNodeValue());
            if(theStr == null) 
              throw new RuntimeException(
                lstr.getElementsByTagName("Ref").item(0)
                .getFirstChild().getNodeValue()
                +" as a reference is not defined in Data Section");
          }
          else
          {
            theStr = Double.parseDouble(
              lstr.getFirstChild().getNodeValue());
          }
          
          Double theDelay;
          if(ldelay.getElementsByTagName("Ref").getLength()>0)
          {
            theDelay = doubleDataMap.get(
              ldelay.getElementsByTagName("Ref")
              .item(0).getFirstChild().getNodeValue());
            
            if(theDelay == null) 
              throw new RuntimeException(
                ldelay.getElementsByTagName("Ref").item(0)
                .getFirstChild().getNodeValue()
                +" as a reference is not defined in Data Section");
          }
          else
          {
            theDelay = Double.parseDouble(
              ldelay.getFirstChild().getNodeValue());
          }

          double outVal;
          outVal=  layerStructure.changeConnection(
            lfrom.getAttributeNode("prefix").getNodeValue(),
            lfrom.getAttributeNode("suffix").getNodeValue(),
            lto.getAttributeNode("prefix").getNodeValue(),
            lto.getAttributeNode("suffix").getNodeValue(),
            sty,
            theStr*Double.parseDouble(
              lmulti.getFirstChild().getNodeValue())
              + Double.parseDouble(loffset.getFirstChild().getNodeValue()),
            iType,
            theDelay,
            conMatrix,
            hostId,
            periodic);
          if(outVal > maxOutput) maxOutput = outVal;
        }
      }

      //read layers general loop connections;
      nList = conns.getElementsByTagName("GConnection"); //loop connections
      for(int it= 0 ; it< nList.getLength(); it++) // add all the layers;
      {
        int max=1;
        NodeList from =  ((Element)(nList.item(it)))
          .getElementsByTagName("FromLayer");
        if(from.getLength() !=1) max = from.getLength();
        NodeList to =  ((Element)(nList.item(it)))
          .getElementsByTagName("ToLayer");
        if(to.getLength() !=1 && max !=1 && to.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(to.getLength() !=1 ) max = to.getLength();
        NodeList matrix =  ((Element)(nList.item(it)))
          .getElementsByTagName("Matrix");
        if(matrix.getLength() !=1 && max !=1 && matrix.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(matrix.getLength() !=1 ) max = matrix.getLength();
        NodeList orientation =  ((Element)(nList.item(it)))
          .getElementsByTagName("Rotation");
        if(orientation.getLength() !=1 && max !=1 
          && orientation.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(orientation.getLength() !=1 ) max = orientation.getLength();

        NodeList type =  ((Element)(nList.item(it)))
          .getElementsByTagName("Type");
        if(type.getLength() !=1 && max !=1 && type.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(type.getLength() !=1 ) max = type.getLength();
        NodeList str =  ((Element)(nList.item(it)))
          .getElementsByTagName("Strength");
        if(str.getLength() !=1 && max !=1 && str.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(str.getLength() !=1 ) max = str.getLength();
        NodeList multi =  ((Element)(nList.item(it)))
          .getElementsByTagName("Multiplier");
        if(multi.getLength() !=1 && max !=1 && multi.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(multi.getLength() !=1 ) max = multi.getLength();
        NodeList offset =  ((Element)(nList.item(it)))
          .getElementsByTagName("Offset");
        if(offset.getLength() !=1 && max !=1 && offset.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(offset.getLength() !=1 ) max = offset.getLength();
        NodeList delay =  ((Element)(nList.item(it)))
          .getElementsByTagName("Delay");
        if(delay.getLength() !=1 && max !=1 && delay.getLength() != max) 
          throw new RuntimeException("number does not match");
        if(delay.getLength() !=1 ) max = delay.getLength();
        //      System.out.println("max"+max);

        for(int i=0; i < max; i++)
        {
          Element lmatrix;
          if(matrix.getLength()==max)
          {
            lmatrix = (Element)(matrix.item(i));
          }
          else
          {
            lmatrix = (Element)(matrix.item(0));
          }

          double [][] conMatrix;
          if(lmatrix.getElementsByTagName("Ref").getLength()>0)
          {
            conMatrix = matrixDataMap.get(
              lmatrix.getElementsByTagName("Ref").item(0)
              .getFirstChild().getNodeValue());
            if(conMatrix == null) 
              throw new RuntimeException(
                lmatrix.getElementsByTagName("Ref").item(0)
                .getFirstChild().getNodeValue()
                +" as a reference is not defined in Data Section");
          }
          else
          {
            NodeList rows = lmatrix.getElementsByTagName("Row");
            String firstRow = rows.item(0).getFirstChild().getNodeValue();
            String sep [] = firstRow.split("[a-z,\t ]");
            //      System.out.println(sep.length);
            conMatrix = new double [rows.getLength()][sep.length];
            for(int colId=0; colId < sep.length; colId++)
            {
              conMatrix[0][colId]=Double.parseDouble(sep[colId]);
            }

            for( int rowId=1; rowId<rows.getLength(); rowId++)
            {
              firstRow = rows.item(rowId).getFirstChild().getNodeValue();
              sep = firstRow.split("[a-z,\t ]");
              for(int colId=0; colId < sep.length; colId++)
              {
                conMatrix[rowId][colId]=Double.parseDouble(sep[colId]);
              }
            }
          }

          Element lrotation;
          if(orientation.getLength()==max)
          {
            lrotation = (Element)(orientation.item(i));
          }
          else
          {
            lrotation = (Element)(orientation.item(0));
          }

          int rot =Integer.parseInt(
            lrotation.getFirstChild().getNodeValue())/90;

          if(rot>0)
          {
            for(int ii=0 ; ii< rot; ii++)
            {
              conMatrix = FunUtil.rRotate90(conMatrix);
            }
          }
          else if(rot <0)
          {
            for(int ii=0 ; ii< -rot; ii++)
            {
              conMatrix = FunUtil.lRotate90(conMatrix);
            }
          }
          //remeber to change the rows matrix. use rows
          Element lfrom;
          if(from.getLength()==max)
          {
            lfrom = (Element)(from.item(i));
          }
          else
          {
            lfrom = (Element)(from.item(0));
          }
          Element lto;
          if(to.getLength()==max)
          {
            lto = (Element)(to.item(i));
          }
          else
          {
            lto = (Element)(to.item(0));
          }

          Element ltype;
          if(type.getLength()==max)
          {
            ltype = (Element)(type.item(i));
          }
          else
          {
            ltype = (Element)(type.item(0));
          }

          final int  iType = parseSynapseType ( ltype );
          
          Element lstr;
          if(str.getLength()==max)
          {
            lstr = (Element)(str.item(i));
          }
          else
          {
            lstr = (Element)(str.item(0));
          }
          Element lmulti;
          if(multi.getLength()==max)
          {
            lmulti = (Element)(multi.item(i));
          }
          else
          {
            lmulti = (Element)(multi.item(0));
          }
          Element loffset;
          if(offset.getLength()==max)
          {
            loffset = (Element)(offset.item(i));
          }
          else
          {
            loffset = (Element)(offset.item(0));
          }
          Element ldelay;
          if(delay.getLength()==max)
          {
            ldelay = (Element)(delay.item(i));
          }
          else
          {
            ldelay = (Element)(delay.item(0));
          }

          Double theStr;
          if(lstr.getElementsByTagName("Ref").getLength()>0)
          {
            theStr = doubleDataMap.get(
              lstr.getElementsByTagName("Ref").item(0).getFirstChild()
              .getNodeValue());
            if(theStr == null) 
              throw new RuntimeException(
                lstr.getElementsByTagName("Ref").item(0)
                .getFirstChild().getNodeValue()
                +" as a reference is not defined in Data Section");
          }
          else
          {
            theStr = Double.parseDouble(
              lstr.getFirstChild().getNodeValue());
          }
          Double theDelay;
          if(ldelay.getElementsByTagName("Ref").getLength()>0)
          {
            theDelay = doubleDataMap.get(
              ldelay.getElementsByTagName("Ref").item(0).getFirstChild()
                .getNodeValue());
            
            if(theDelay == null) throw new RuntimeException(
              ldelay.getElementsByTagName("Ref").item(0).getFirstChild()
                .getNodeValue()
                  +" as a reference is not defined in Data Section");
          }
          else
          {
            theDelay = Double.parseDouble(
              ldelay.getFirstChild().getNodeValue());
          }

          double outVal;
          
          outVal = layerStructure.changeConnection(
            lfrom.getAttributeNode("prefix").getNodeValue(),
            lfrom.getAttributeNode("suffix").getNodeValue(),
            lto.getAttributeNode("prefix").getNodeValue(),
            lto.getAttributeNode("suffix").getNodeValue(),
            theStr*Double.parseDouble(
              lmulti.getFirstChild().getNodeValue())
                + Double.parseDouble(
                  loffset.getFirstChild().getNodeValue()),
            iType,
            theDelay,
            conMatrix,
            hostId);
          if(outVal > maxOutput) maxOutput = outVal;
        }
      }

      //read layers;
      nList = conns.getElementsByTagName("Connection");
      for(int it= 0 ; it< nList.getLength(); it++) // add all the layers;
      {
        String perio = ((Element)(nList.item(it))).getAttributeNode(
          "periodic").getValue();
        
        boolean periodic;
        
        if(perio.equals("true") || perio.equals("TRUE"))
        {
          periodic = true;
        }
        else
        {
          periodic = false;
        }

        Element matrix = (Element)(((Element)(nList.item(it)))
          .getElementsByTagName("Matrix").item(0));

        NodeList rows = matrix.getElementsByTagName("Row");
        String firstRow = rows.item(0).getFirstChild().getNodeValue();
        String sep [] = firstRow.split("[a-z,\t ]");
        //      p.println(sep.length);
        //      p.flush();
        double [][] conMatrix = new double [rows.getLength()][sep.length];
        for(int colId=0; colId < sep.length; colId++)
        {
          conMatrix[0][colId]=Double.parseDouble(sep[colId]);
        }

        for( int rowId=1; rowId<rows.getLength(); rowId++)
        {
          firstRow = rows.item(rowId).getFirstChild().getNodeValue();
          sep = firstRow.split("[a-z,\t ]");
          for(int colId=0; colId < sep.length; colId++)
          {
            conMatrix[rowId][colId]=Double.parseDouble(sep[colId]);
          }
        }
        /*
      for(int ii=0; ii < conMatrix.length; ii++)
      {
        for(int jj=0 ; jj < conMatrix[0].length; jj++)
        {
          p.print(conMatrix[ii][jj]+" ");
        }
        p.println();
      }
         */

        int rotation =Integer.parseInt(
          ((Element)(nList.item(it))).getElementsByTagName("Rotation")
            .item(0).getFirstChild().getNodeValue())/90;

        if(rotation>0)
        {
          for(int ii=0 ; ii< rotation; ii++)
          {
            conMatrix = FunUtil.rRotate90(conMatrix);
          }
        }
        else if(rotation <0)
        {
          for(int ii=0 ; ii< -rotation; ii++)
          {
            conMatrix = FunUtil.lRotate90(conMatrix);
          }
        }
        /*
      for(int ii=0; ii < conMatrix.length; ii++)
      {
        for(int jj=0 ; jj < conMatrix[0].length; jj++)
        {
          p.print(conMatrix[ii][jj]+" ");
        }
        p.println();
      }
         */


        Element from = (Element)(((Element)(nList.item(it)))
          .getElementsByTagName("FromLayer").item(0));
        
        Element to  = (Element)(((Element)(nList.item(it)))
          .getElementsByTagName("ToLayer").item(0));
        
        final Element  styleElement = getFirstElement (
          ( Element ) ( nList.item ( it ) ),
          "Style" );
        
        final  boolean  sty = parseVergenceStyle ( styleElement );
        
        //synaptic type
        
        final Element  typeElement = ( Element )
          ( ( Element ) nList.item ( it ) )
          .getElementsByTagName ( "Type" ).item ( 0 );

        final int  type = parseSynapseType ( typeElement );
        
/*
        System.out.println(((Element)
          (from.getElementsByTagName("Prefix").item(0)))
          .getFirstChild().getNodeValue()
          +" "
          + ((Element)(from.getElementsByTagName("Suffix").item(0)))
          .getFirstChild().getNodeValue()+" "
          + ((Element)(to.getElementsByTagName("Prefix").item(0)))
          .getFirstChild().getNodeValue()+" "
          + ((Element)(to.getElementsByTagName("Suffix").item(0)))
          .getFirstChild().getNodeValue());
*/
/*
        if(connected)
        {
          p.println(
            "str"+Double.parseDouble(((Element)(nList.item(it)))
            .getElementsByTagName("Strength").item(0).getFirstChild()
            .getNodeValue())*Double.parseDouble(((Element)(nList.item(it)))
            .getElementsByTagName("Multiplier").item(0).getFirstChild()
            .getNodeValue())+ Double.parseDouble(((Element)(nList.item(it)))
            .getElementsByTagName("Offset").item(0).getFirstChild()
            .getNodeValue()));
          p.println(((Element)(from.getElementsByTagName("Prefix")
            .item(0))).getFirstChild().getNodeValue()+ " "
            +((Element)(from.getElementsByTagName("Suffix").item(0)))
            .getFirstChild().getNodeValue()+" "
            +((Element)(to.getElementsByTagName("Prefix").item(0)))
            .getFirstChild().getNodeValue()+" "
            +((Element)(to.getElementsByTagName("Suffix").item(0)))
            .getFirstChild().getNodeValue());
        }
*/
        double outVal;
        
        outVal = layerStructure.changeConnection(((Element)(
          from.getElementsByTagName("Prefix").item(0))).getFirstChild()
            .getNodeValue(),
          ((Element)(from.getElementsByTagName("Suffix").item(0)))
            .getFirstChild().getNodeValue(),
          ((Element)(to.getElementsByTagName("Prefix").item(0)))
            .getFirstChild().getNodeValue(),
          ((Element)(to.getElementsByTagName("Suffix").item(0)))
            .getFirstChild().getNodeValue(),
          sty,
          Double.parseDouble(
            ((Element)(nList.item(it))).getElementsByTagName("Strength")
              .item(0).getFirstChild().getNodeValue())
              * Double.parseDouble(((Element)(nList.item(it)))
                  .getElementsByTagName("Multiplier").item(0)
                  .getFirstChild().getNodeValue())
              + Double.parseDouble(((Element)(nList.item(it)))
                .getElementsByTagName("Offset").item(0).getFirstChild()
                .getNodeValue()),
          type,
          Double.parseDouble(
            ((Element)(nList.item(it))).getElementsByTagName("Delay")
              .item(0).getFirstChild().getNodeValue()),
          conMatrix,
          hostId,
          periodic);
        
        if(outVal > maxOutput) maxOutput = outVal;
      }
      
      return maxOutput;
    }

    /***********************************************************************
    * Parse the connection section, connect the neurons and build up the
    * network structure
    ***********************************************************************/
    public double parseConnection(int hostId) throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      layerStructure.reSetSeed(); //reset the seed number
      
      double maxOutput = -1.0;
      
      NodeList nList = rootElement.getElementsByTagName("Connections");
      
      Element conns = (Element) nList.item(0);

      //read layers;
      nList = conns.getElementsByTagName("LConnection"); //loop connections
      
      for(int it= 0 ; it< nList.getLength(); it++) // add all the layers;
      {
        String perio = ((Element)(nList.item(it)))
          .getAttributeNode("periodic").getValue();
        
        boolean periodic;
        
        if(perio.equals("true") || perio.equals("TRUE"))
        {
          periodic = true;
        }
        else
        {
          periodic = false;
        }

        int max=1;
        
        NodeList from = ((Element)(nList.item(it)))
          .getElementsByTagName("FromLayer");
        
        if(from.getLength() !=1) max = from.getLength();
        
        NodeList to = ((Element)(nList.item(it)))
          .getElementsByTagName("ToLayer");
        
        if(to.getLength() !=1 && max !=1 && to.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(to.getLength() !=1 ) max = to.getLength();
        
        NodeList
          matrix = ((Element)(nList.item(it)))
            .getElementsByTagName("Matrix");
        
        if(matrix.getLength() !=1 && max !=1 && matrix.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(matrix.getLength() !=1 ) max = matrix.getLength();
        
        NodeList orientation = ((Element)(nList.item(it)))
          .getElementsByTagName("Rotation");
        
        if(orientation.getLength() !=1 && max !=1
          && orientation.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(orientation.getLength() !=1 ) max = orientation.getLength();
        
        NodeList
          style = ((Element)(nList.item(it))).getElementsByTagName("Style");
        
        if (style.getLength() !=1 && max !=1 && style.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(style.getLength() !=1 ) max = style.getLength();
        
        NodeList
          type = ((Element)(nList.item(it))).getElementsByTagName("Type");
        
        if(type.getLength() !=1 && max !=1 && type.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(type.getLength() !=1 ) max = type.getLength();
        
        NodeList  str
          = ((Element)(nList.item(it))).getElementsByTagName("Strength");
        
        if(str.getLength() !=1 && max !=1 && str.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(str.getLength() !=1 ) max = str.getLength();
        
        NodeList multi =  ((Element)(nList.item(it))).getElementsByTagName(
          "Multiplier");
        
        if(multi.getLength() !=1 && max !=1 && multi.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(multi.getLength() !=1 ) max = multi.getLength();
        
        NodeList offset =  ((Element)(nList.item(it))).getElementsByTagName(
          "Offset");
        
        if(offset.getLength() !=1 && max !=1 && offset.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(offset.getLength() !=1 ) max = offset.getLength();
        
        NodeList delay = ((Element)(nList.item(it))).getElementsByTagName(
          "Delay");
        
        if(delay.getLength() !=1 && max !=1 && delay.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(delay.getLength() !=1 ) max = delay.getLength();
        
        //      System.out.println("max"+max);

        for(int i=0; i < max; i++)
        {
          Element lmatrix;
          if(matrix.getLength()==max)
          {
            lmatrix = (Element)(matrix.item(i));
          }
          else
          {
            lmatrix = (Element)(matrix.item(0));
          }

          double [][] conMatrix;
          
          if(lmatrix.getElementsByTagName("Ref").getLength()>0)
          {
            conMatrix = matrixDataMap.get(
              lmatrix.getElementsByTagName("Ref").item(0).getFirstChild()
              .getNodeValue());
            
            if(conMatrix == null)
              throw new RuntimeException(
                lmatrix.getElementsByTagName("Ref").item(0).getFirstChild()
                  .getNodeValue()
                    +" as a reference is not defined in Data Section");
          }
          else
          {
            NodeList rows = lmatrix.getElementsByTagName("Row");
            
            String firstRow = rows.item(0).getFirstChild().getNodeValue();
            
            String sep [] = firstRow.split("[a-z,\t ]");
            
            //      System.out.println(sep.length);
            
            conMatrix = new double [rows.getLength()][sep.length];
            
            for(int colId=0; colId < sep.length; colId++)
            {
              conMatrix[0][colId]=Double.parseDouble(sep[colId]);
            }

            for( int rowId=1; rowId<rows.getLength(); rowId++)
            {
              firstRow = rows.item(rowId).getFirstChild().getNodeValue();
              
              sep = firstRow.split("[a-z,\t ]");
              
              for(int colId=0; colId < sep.length; colId++)
              {
                conMatrix[rowId][colId]=Double.parseDouble(sep[colId]);
              }
            }
          }

          Element lrotation;
          
          if(orientation.getLength()==max)
          {
            lrotation = (Element)(orientation.item(i));
          }
          else
          {
            lrotation = (Element)(orientation.item(0));
          }

          int rot =Integer.parseInt(
            lrotation.getFirstChild().getNodeValue())/90;

          if(rot>0)
          {
            for(int ii=0 ; ii< rot; ii++)
            {
              conMatrix = FunUtil.rRotate90(conMatrix);
            }
          }
          else if(rot <0)
          {
            for(int ii=0 ; ii< -rot; ii++)
            {
              conMatrix = FunUtil.lRotate90(conMatrix);
            }
          }
          //remeber to change the rows matrix. use rows
          Element lfrom;
          if(from.getLength()==max)
          {
            lfrom = (Element)(from.item(i));
          }
          else
          {
            lfrom = (Element)(from.item(0));
          }
          Element lto;
          if(to.getLength()==max)
          {
            lto = (Element)(to.item(i));
          }
          else
          {
            lto = (Element)(to.item(0));
          }
          Element lstyle;
          if(style.getLength()==max)
          {
            lstyle = (Element)(style.item(i));
          }
          else
          {
            lstyle = (Element)(style.item(0));
          }
          
          final boolean  sty = parseVergenceStyle ( lstyle );

          Element ltype;
          if(type.getLength()==max)
          {
            ltype = (Element)(type.item(i));
          }
          else
          {
            ltype = (Element)(type.item(0));
          }

          final int  iType = parseSynapseType ( ltype );
          
          Element lstr;
          if(str.getLength()==max)
          {
            lstr = (Element)(str.item(i));
          }
          else
          {
            lstr = (Element)(str.item(0));
          }
          Element lmulti;
          if(multi.getLength()==max)
          {
            lmulti = (Element)(multi.item(i));
          }
          else
          {
            lmulti = (Element)(multi.item(0));
          }
          Element loffset;
          if(offset.getLength()==max)
          {
            loffset = (Element)(offset.item(i));
          }
          else
          {
            loffset = (Element)(offset.item(0));
          }
          Element ldelay;
          if(delay.getLength()==max)
          {
            ldelay = (Element)(delay.item(i));
          }
          else
          {
            ldelay = (Element)(delay.item(0));
          }
/*
          System.out.print(lfrom.getAttributeNode("prefix").getNodeValue()
            +" "+lfrom.getAttributeNode("suffix").getNodeValue()+" "
            +lto.getAttributeNode("prefix").getNodeValue()+" "
            +lto.getAttributeNode("suffix").getNodeValue());
            
          System.out.print(" "+sty);
          
          System.out.print(" "+lstr.getFirstChild().getNodeValue());
          
          System.out.print(" "+lmulti.getFirstChild().getNodeValue());
          
          System.out.print(" "+loffset.getFirstChild().getNodeValue());
          
          System.out.print(" "+ldelay.getFirstChild().getNodeValue());
          
          System.out.println();
*/
/*
          if(connected)
          {
          p.print(lfrom.getAttributeNode("prefix").getNodeValue()+" "
            +lfrom.getAttributeNode("suffix").getNodeValue()+" "
            +lto.getAttributeNode("prefix").getNodeValue()+" "
            +lto.getAttributeNode("suffix").getNodeValue());
            
          p.print(" "+sty);
          
          p.print(" "+lstr.getFirstChild().getNodeValue());
          
          p.print(" "+lmulti.getFirstChild().getNodeValue());
          
          p.print(" "+loffset.getFirstChild().getNodeValue());
          
          p.print(" "+ldelay.getFirstChild().getNodeValue());
          
          p.println();
          }
*/
/*
          for(int ii=0; ii < conMatrix.length; ii++)
          {
            for(int jj=0 ; jj < conMatrix[0].length; jj++)
            {
              System.out.print(conMatrix[ii][jj]+" ");
            }
            System.out.println();
          }
*/
          Double theStr;
          
          if(lstr.getElementsByTagName("Ref").getLength()>0)
          {
            theStr = doubleDataMap.get(lstr.getElementsByTagName("Ref")
              .item(0).getFirstChild().getNodeValue());
            
            if(theStr == null)
              throw new RuntimeException(
                lstr.getElementsByTagName("Ref").item(0).getFirstChild()
                  .getNodeValue()
                    +" as a reference is not defined in Data Section");
          }
          else
          {
            theStr = Double.parseDouble(
              lstr.getFirstChild().getNodeValue());
          }
          
          Double theDelay;
          
          if(ldelay.getElementsByTagName("Ref").getLength()>0)
          {
            theDelay = doubleDataMap.get(
              ldelay.getElementsByTagName("Ref").item(0)
                .getFirstChild().getNodeValue());
            
            if(theDelay == null)
              throw new RuntimeException(
                ldelay.getElementsByTagName("Ref").item(0).getFirstChild()
                  .getNodeValue()
                    +" as a reference is not defined in Data Section");
          }
          else
          {
            theDelay = Double.parseDouble(
              ldelay.getFirstChild().getNodeValue());
          }

          double outVal;
          
          outVal = layerStructure.connect(
            lfrom.getAttributeNode("prefix").getNodeValue(),
            lfrom.getAttributeNode("suffix").getNodeValue(),
            lto.getAttributeNode("prefix").getNodeValue(),
            lto.getAttributeNode("suffix").getNodeValue(),
            sty,
            theStr * Double.parseDouble (
              lmulti.getFirstChild().getNodeValue() )
              + Double.parseDouble(loffset.getFirstChild().getNodeValue()),
            iType,
            theDelay,
            conMatrix,
            hostId,
            periodic);
          
          if(outVal > maxOutput) maxOutput = outVal;
        }
      }

      //read layers general loop connections;
      
      nList = conns.getElementsByTagName("GConnection"); //loop connections
      
      for(int it= 0 ; it< nList.getLength(); it++) // add all the layers;
      {
        int max=1;
        
        NodeList from = ((Element)(nList.item(it))).getElementsByTagName(
          "FromLayer");
        
        if(from.getLength() !=1) max = from.getLength();
        
        NodeList to =  ((Element)(nList.item(it))).getElementsByTagName(
          "ToLayer");
        
        if(to.getLength() !=1 && max !=1 && to.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(to.getLength() !=1 ) max = to.getLength();
        
        NodeList matrix =  ((Element)(nList.item(it))).getElementsByTagName(
          "Matrix");
        
        if(matrix.getLength() !=1 && max !=1 && matrix.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(matrix.getLength() !=1 ) max = matrix.getLength();
        
        NodeList orientation = ((Element)(nList.item(it)))
          .getElementsByTagName("Rotation");
        
        if(orientation.getLength() !=1 && max !=1
          && orientation.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(orientation.getLength() !=1 ) max = orientation.getLength();

        NodeList type = ((Element)(nList.item(it)))
          .getElementsByTagName("Type");
        
        if(type.getLength() !=1 && max !=1 && type.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(type.getLength() !=1 ) max = type.getLength();
        
        NodeList str = ((Element)(nList.item(it)))
          .getElementsByTagName("Strength");
        
        if(str.getLength() !=1 && max !=1 && str.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(str.getLength() !=1 ) max = str.getLength();
        
        NodeList multi =  ((Element)(nList.item(it))).getElementsByTagName(
          "Multiplier");
        
        if(multi.getLength() !=1 && max !=1 && multi.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(multi.getLength() !=1 ) max = multi.getLength();
        
        NodeList offset =  ((Element)(nList.item(it))).getElementsByTagName(
          "Offset");
        
        if(offset.getLength() !=1 && max !=1 && offset.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(offset.getLength() !=1 ) max = offset.getLength();
        
        NodeList delay =  ((Element)(nList.item(it))).getElementsByTagName(
          "Delay");
        
        if(delay.getLength() !=1 && max !=1 && delay.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(delay.getLength() !=1 ) max = delay.getLength();
        
        //      System.out.println("max"+max);

        for(int i=0; i < max; i++)
        {
          Element lmatrix;
          if(matrix.getLength()==max)
          {
            lmatrix = (Element)(matrix.item(i));
          }
          else
          {
            lmatrix = (Element)(matrix.item(0));
          }
          double [][] conMatrix;
          if(lmatrix.getElementsByTagName("Ref").getLength()>0)
          {
            conMatrix = matrixDataMap.get(
              lmatrix.getElementsByTagName("Ref").item(0).getFirstChild()
              .getNodeValue());
            
            if(conMatrix == null)
              throw new RuntimeException(
                lmatrix.getElementsByTagName("Ref").item(0)
                  .getFirstChild().getNodeValue()
                    +" as a reference is not defined in Data Section");
          }
          else
          {
            NodeList rows = lmatrix.getElementsByTagName("Row");
            
            String firstRow = rows.item(0).getFirstChild().getNodeValue();
            
            String sep [] = firstRow.split("[a-z,\t ]");
            
            //      System.out.println(sep.length);
            
            conMatrix = new double [rows.getLength()][sep.length];
            
            for(int colId=0; colId < sep.length; colId++)
            {
              conMatrix[0][colId]=Double.parseDouble(sep[colId]);
            }

            for( int rowId=1; rowId<rows.getLength(); rowId++)
            {
              firstRow = rows.item(rowId).getFirstChild().getNodeValue();
              
              sep = firstRow.split("[a-z,\t ]");
              
              for(int colId=0; colId < sep.length; colId++)
              {
                conMatrix[rowId][colId]=Double.parseDouble(sep[colId]);
              }
            }
          }

          Element lrotation;
          
          if(orientation.getLength()==max)
          {
            lrotation = (Element)(orientation.item(i));
          }
          else
          {
            lrotation = (Element)(orientation.item(0));
          }

          int rot =Integer.parseInt(
            lrotation.getFirstChild().getNodeValue())/90;

          if(rot>0)
          {
            for(int ii=0 ; ii< rot; ii++)
            {
              conMatrix = FunUtil.rRotate90(conMatrix);
            }
          }
          else if(rot <0)
          {
            for(int ii=0 ; ii< -rot; ii++)
            {
              conMatrix = FunUtil.lRotate90(conMatrix);
            }
          }
          //remeber to change the rows matrix. use rows
          Element lfrom;
          if(from.getLength()==max)
          {
            lfrom = (Element)(from.item(i));
          }
          else
          {
            lfrom = (Element)(from.item(0));
          }
          Element lto;
          if(to.getLength()==max)
          {
            lto = (Element)(to.item(i));
          }
          else
          {
            lto = (Element)(to.item(0));
          }

          Element ltype;
          if(type.getLength()==max)
          {
            ltype = (Element)(type.item(i));
          }
          else
          {
            ltype = (Element)(type.item(0));
          }

          final int  iType = parseSynapseType ( ltype );
          
          Element lstr;
          if(str.getLength()==max)
          {
            lstr = (Element)(str.item(i));
          }
          else
          {
            lstr = (Element)(str.item(0));
          }
          Element lmulti;
          if(multi.getLength()==max)
          {
            lmulti = (Element)(multi.item(i));
          }
          else
          {
            lmulti = (Element)(multi.item(0));
          }
          Element loffset;
          if(offset.getLength()==max)
          {
            loffset = (Element)(offset.item(i));
          }
          else
          {
            loffset = (Element)(offset.item(0));
          }
          Element ldelay;
          if(delay.getLength()==max)
          {
            ldelay = (Element)(delay.item(i));
          }
          else
          {
            ldelay = (Element)(delay.item(0));
          }
          Double theStr;
          if(lstr.getElementsByTagName("Ref").getLength()>0)
          {
            theStr = doubleDataMap.get(
              lstr.getElementsByTagName("Ref").item(0).getFirstChild()
                .getNodeValue());
            
            if(theStr == null)
              throw new RuntimeException(
                  lstr.getElementsByTagName("Ref").item(0)
                    .getFirstChild().getNodeValue()
                      +" as a reference is not defined in Data Section");
          }
          else
          {
            theStr = Double.parseDouble(
              lstr.getFirstChild().getNodeValue());
          }
          
          Double theDelay;
          
          if(ldelay.getElementsByTagName("Ref").getLength()>0)
          {
            theDelay = doubleDataMap.get(
              ldelay.getElementsByTagName("Ref").item(0).getFirstChild()
                .getNodeValue());
            
            if(theDelay == null)
              throw new RuntimeException(
                ldelay.getElementsByTagName("Ref").item(0).getFirstChild()
                  .getNodeValue()
                    +" as a reference is not defined in Data Section");
          }
          else
          {
            theDelay = Double.parseDouble(
              ldelay.getFirstChild().getNodeValue());
          }

          double outVal;
          
          outVal = layerStructure.connect(
            lfrom.getAttributeNode("prefix").getNodeValue(),
            lfrom.getAttributeNode("suffix").getNodeValue(),
            lto.getAttributeNode("prefix").getNodeValue(),
            lto.getAttributeNode("suffix").getNodeValue(),
            theStr*Double.parseDouble(
              lmulti.getFirstChild().getNodeValue())
              + Double.parseDouble(loffset.getFirstChild().getNodeValue()),
            iType,
            theDelay,
            conMatrix,
            hostId);
          if(outVal > maxOutput) maxOutput = outVal;
        }
      }

      //read layers probability connections;
      
      nList = conns.getElementsByTagName("PConnection"); //loop connections
      
      for(int it= 0 ; it< nList.getLength(); it++) // add all the layers;
      {
        String perio = ((Element)(nList.item(it)))
          .getAttributeNode("periodic").getValue();
        
        boolean periodic;
        
        if(perio.equals("true") || perio.equals("TRUE"))
        {
          periodic = true;
        }
        else
        {
          periodic = false;
        }

        int max=1;
        
        boolean str_matrix; // strength matrix exist or not
        
        NodeList from = ((Element)(nList.item(it))).getElementsByTagName(
          "FromLayer");
        
        if(from.getLength() !=1) max = from.getLength();
        
        NodeList to =  ((Element)(nList.item(it))).getElementsByTagName(
          "ToLayer");
        
        if(to.getLength() !=1 && max !=1 && to.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(to.getLength() !=1 ) max = to.getLength();

        NodeList pmatrix = ((Element)(nList.item(it))).getElementsByTagName(
          "PMatrix");
        
        if(pmatrix.getLength() !=1 && max !=1 && pmatrix.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(pmatrix.getLength() !=1 ) max = pmatrix.getLength();

        NodeList smatrix = ((Element)(nList.item(it))).getElementsByTagName(
          "SMatrix");
        
        if(smatrix.getLength() ==0)
        {
          str_matrix = false;
        }
        else
        {
          if(smatrix.getLength() !=1 && max !=1
            && smatrix.getLength() != max)
            throw new RuntimeException("number does not match");
          
          if(smatrix.getLength() !=1 ) max = smatrix.getLength();
          
          str_matrix = true;
        }

        NodeList orientation = ((Element)(nList.item(it)))
          .getElementsByTagName("Rotation");
        
        if(orientation.getLength() !=1 && max !=1
          && orientation.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(orientation.getLength() !=1 ) max = orientation.getLength();
        
        NodeList style =  ((Element)(nList.item(it))).getElementsByTagName(
          "Style");

        if(style.getLength() !=1 && max !=1 && style.getLength() != max)
          throw new RuntimeException("number does not match");

        if(style.getLength() !=1 ) max = style.getLength();

        NodeList type =  ((Element)(nList.item(it))).getElementsByTagName(
          "Type");

        if(type.getLength() !=1 && max !=1 && type.getLength() != max)
          throw new RuntimeException("number does not match");

        if(type.getLength() !=1 ) max = type.getLength();

        NodeList str = ((Element)(nList.item(it))).getElementsByTagName(
          "Strength");

        if(str.getLength() !=1 && max !=1 && str.getLength() != max)
          throw new RuntimeException("number does not match");

        if(str.getLength() !=1 ) max = str.getLength();

        NodeList multi = ((Element)(nList.item(it))).getElementsByTagName(
          "Multiplier");
        
        if(multi.getLength() !=1 && max !=1 && multi.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(multi.getLength() !=1 ) max = multi.getLength();
        
        NodeList offset = ((Element)(nList.item(it))).getElementsByTagName(
          "Offset");
        
        if(offset.getLength() !=1 && max !=1 && offset.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(offset.getLength() !=1 ) max = offset.getLength();
        
        NodeList delay = ((Element)(nList.item(it))).getElementsByTagName(
          "Delay");
        
        if(delay.getLength() !=1 && max !=1 && delay.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(delay.getLength() !=1 ) max = delay.getLength();
        
        // System.out.println("max"+max);

        for(int i=0; i < max; i++)
        {
          Element lmatrix,eSmatrix;
          
          if(pmatrix.getLength()==max)
          {
            lmatrix = (Element)(pmatrix.item(i));
          }
          else
          {
            lmatrix = (Element)(pmatrix.item(0));
          }

          double [][] conMatrix;
          
          if(lmatrix.getElementsByTagName("Ref").getLength()>0)
          {
            conMatrix = matrixDataMap.get(
              lmatrix.getElementsByTagName("Ref").item(0).getFirstChild()
                .getNodeValue());
            
            if(conMatrix == null)
              throw new RuntimeException(
                lmatrix.getElementsByTagName("Ref").item(0).getFirstChild()
                  .getNodeValue()
                  +" as a reference is not defined in Data Section");
          }
          else 
          {
            NodeList rows = lmatrix.getElementsByTagName("Row");
            
            String firstRow = rows.item(0).getFirstChild().getNodeValue();
            
            String sep [] = firstRow.split("[a-z,\t ]");

            conMatrix = new double [rows.getLength()][sep.length];

            for(int colId=0; colId < sep.length; colId++)
            {
              conMatrix[0][colId]=Double.parseDouble(sep[colId]);
            }

            for( int rowId=1; rowId<rows.getLength(); rowId++)
            {
              firstRow = rows.item(rowId).getFirstChild().getNodeValue();
              
              sep = firstRow.split("[a-z,\t ]");
              
              for(int colId=0; colId < sep.length; colId++)
              {
                conMatrix[rowId][colId]=Double.parseDouble(sep[colId]);
              }
            }
          }

          // System.out.println(sep.length);
          
          double [][] sconMatrix = null;

          if(str_matrix) // if strength matrix exists
          {
            if(smatrix.getLength()==max)
            {
              eSmatrix = (Element)(smatrix.item(i));
            }
            else
            {
              eSmatrix = (Element)(smatrix.item(0));
            }

            if(eSmatrix.getElementsByTagName("Ref").getLength()>0)
            {
              sconMatrix = matrixDataMap.get(eSmatrix.getElementsByTagName(
                "Ref").item(0).getFirstChild().getNodeValue());
              
              if(sconMatrix == null)
                throw new RuntimeException(
                  eSmatrix.getElementsByTagName("Ref").item(0)
                    .getFirstChild().getNodeValue()
                    +" as a reference is not defined in Data Section");
            }
            else
            {
              NodeList rows = eSmatrix.getElementsByTagName("Row");
              
              String firstRow = rows.item(0).getFirstChild().getNodeValue();
              
              String [] sep = firstRow.split("[a-z,\t ]");
              
              // System.out.println(sep.length);

              sconMatrix = new double [rows.getLength()][sep.length];
              
              for(int colId=0; colId < sep.length; colId++)
              {
                sconMatrix[0][colId]=Double.parseDouble(sep[colId]);
              }

              for( int rowId=1; rowId<rows.getLength(); rowId++)
              {
                firstRow = rows.item(rowId).getFirstChild().getNodeValue();
                
                sep = firstRow.split("[a-z,\t ]");
                
                for(int colId=0; colId < sep.length; colId++)
                {
                  sconMatrix[rowId][colId]=Double.parseDouble(sep[colId]);
                }
              }
            }
          }

          Element lrotation;
          
          if(orientation.getLength()==max)
          {
            lrotation = (Element)(orientation.item(i));
          }
          else
          {
            lrotation = (Element)(orientation.item(0));
          }

          int rot =Integer.parseInt(
            lrotation.getFirstChild().getNodeValue())/90;

          if(rot>0)
          {
            for(int ii=0 ; ii< rot; ii++)
            {
              conMatrix = FunUtil.rRotate90(conMatrix);
              
              if(str_matrix)sconMatrix = FunUtil.rRotate90(sconMatrix);
            }
          }
          else if(rot <0)
          {
            for(int ii=0 ; ii< -rot; ii++)
            {
              conMatrix = FunUtil.lRotate90(conMatrix);
              if(str_matrix)sconMatrix = FunUtil.lRotate90(sconMatrix);
            }
          }
          //remeber to change the rows matrix. use rows
          Element lfrom;
          if(from.getLength()==max)
          {
            lfrom = (Element)(from.item(i));
          }
          else
          {
            lfrom = (Element)(from.item(0));
          }
          Element lto;
          if(to.getLength()==max)
          {
            lto = (Element)(to.item(i));
          }
          else
          {
            lto = (Element)(to.item(0));
          }
          Element lstyle;
          if(style.getLength()==max)
          {
            lstyle = (Element)(style.item(i));
          }
          else
          {
            lstyle = (Element)(style.item(0));
          }
          
          final boolean  sty = parseVergenceStyle ( lstyle );

          Element ltype;
          if(type.getLength()==max)
          {
            ltype = (Element)(type.item(i));
          }
          else
          {
            ltype = (Element)(type.item(0));
          }
          
          final int  iType = parseSynapseType ( ltype );

          Element lstr;
          
          if(str.getLength()==max)
          {
            lstr = (Element)(str.item(i));
          }
          else
          {
            lstr = (Element)(str.item(0));
          }
          Element lmulti;
          if(multi.getLength()==max)
          {
            lmulti = (Element)(multi.item(i));
          }
          else
          {
            lmulti = (Element)(multi.item(0));
          }
          Element loffset;
          if(offset.getLength()==max)
          {
            loffset = (Element)(offset.item(i));
          }
          else
          {
            loffset = (Element)(offset.item(0));
          }
          Element ldelay;
          if(delay.getLength()==max)
          {
            ldelay = (Element)(delay.item(i));
          }
          else
          {
            ldelay = (Element)(delay.item(0));
          }
/*
          System.out.print(
            lfrom.getAttributeNode("prefix").getNodeValue()
            +" "
            +lfrom.getAttributeNode("suffix").getNodeValue()
            +" "
            +lto.getAttributeNode("prefix").getNodeValue()
            +" "
            +lto.getAttributeNode("suffix").getNodeValue());
            
          System.out.print(" "+sty);
          
          System.out.print(" "+lstr.getFirstChild().getNodeValue());
          
          System.out.print(" "+lmulti.getFirstChild().getNodeValue());
          
          System.out.print(" "+loffset.getFirstChild().getNodeValue());
          
          System.out.print(" "+ldelay.getFirstChild().getNodeValue());
          
          System.out.println();
*/
/*
          if(connected)
          {
            p.print(lfrom.getAttributeNode("prefix").getNodeValue()
              +" "
              +lfrom.getAttributeNode("suffix").getNodeValue()
              +" "
              +lto.getAttributeNode("prefix").getNodeValue()
              +" "
              +lto.getAttributeNode("suffix").getNodeValue());
              
            p.print(" "+sty);
            
            p.print(" "+lstr.getFirstChild().getNodeValue());
            
            p.print(" "+lmulti.getFirstChild().getNodeValue());
            
            p.print(" "+loffset.getFirstChild().getNodeValue());
            
            p.print(" "+ldelay.getFirstChild().getNodeValue());
            
            p.println();
          }
*/
/*
          for(int ii=0; ii < conMatrix.length; ii++)
          {
            for(int jj=0 ; jj < conMatrix[0].length; jj++)
            {
              System.out.print(conMatrix[ii][jj]+" ");
            }
            System.out.println();
          }
*/
          Double theStr;
          
          if(lstr.getElementsByTagName("Ref").getLength()>0)
          {
            theStr = doubleDataMap.get(
              lstr.getElementsByTagName("Ref").item(0).getFirstChild()
                .getNodeValue());
            
            if(theStr == null)
              throw new RuntimeException(
                lstr.getElementsByTagName("Ref").item(0).getFirstChild()
                  .getNodeValue()
                    +" as a reference is not defined in Data Section");
          }
          else
          {
            theStr = Double.parseDouble(
              lstr.getFirstChild().getNodeValue());
          }
          
          Double theDelay;
          
          if(ldelay.getElementsByTagName("Ref").getLength()>0)
          {
            theDelay = doubleDataMap.get(
              ldelay.getElementsByTagName("Ref").item(0).getFirstChild()
                .getNodeValue());
            
            if(theDelay == null)
              throw new RuntimeException(
                ldelay.getElementsByTagName("Ref").item(0).getFirstChild()
                  .getNodeValue()
                    +" as a reference is not defined in Data Section");
          }
          else
          {
            theDelay = Double.parseDouble(
              ldelay.getFirstChild().getNodeValue());
          }

          double outVal;
          
          outVal = layerStructure.connect(
            lfrom.getAttributeNode("prefix").getNodeValue(),
            lfrom.getAttributeNode("suffix").getNodeValue(),
            lto.getAttributeNode("prefix").getNodeValue(),
            lto.getAttributeNode("suffix").getNodeValue(),
            sty,
            theStr*Double.parseDouble(
              lmulti.getFirstChild().getNodeValue())
              + Double.parseDouble(loffset.getFirstChild().getNodeValue()),
            iType,
            theDelay,
            conMatrix,
            sconMatrix,
            hostId,
            periodic);
          if(outVal > maxOutput) maxOutput = outVal;
        }
      }

      //read layers probability general connections;
      
      nList = conns.getElementsByTagName("GPConnection"); //loop connections
      
      for(int it= 0 ; it< nList.getLength(); it++) // add all the layers;
      {
        int max=1;
        
        boolean str_matrix; // strength matrix exist or not
        
        NodeList from = ((Element)(nList.item(it))).getElementsByTagName(
          "FromLayer");
        
        if(from.getLength() !=1) max = from.getLength();
        
        NodeList to =  ((Element)(nList.item(it))).getElementsByTagName(
          "ToLayer");
        
        if(to.getLength() !=1 && max !=1 && to.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(to.getLength() !=1 ) max = to.getLength();

        NodeList pmatrix = ((Element)(nList.item(it))).getElementsByTagName(
          "PMatrix");
        
        if(pmatrix.getLength() !=1 && max !=1 && pmatrix.getLength() != max)
            throw new RuntimeException("number does not match");
        
        if(pmatrix.getLength() !=1 ) max = pmatrix.getLength();

        NodeList smatrix = ((Element)(nList.item(it))).getElementsByTagName(
          "SMatrix");
        
        if(smatrix.getLength() ==0)
        {
          str_matrix = false;
        }
        else
        {
          if(smatrix.getLength() !=1 && max !=1
            && smatrix.getLength() != max)
            throw new RuntimeException("number does not match");
          
          if(smatrix.getLength() !=1 ) max = smatrix.getLength();
          
          str_matrix = true;
        }

        NodeList orientation = ((Element)(nList.item(it)))
          .getElementsByTagName("Rotation");
        
        if(orientation.getLength() !=1
          && max != 1
          && orientation.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(orientation.getLength() !=1 ) max = orientation.getLength();

        NodeList type = ((Element)(nList.item(it))).getElementsByTagName(
          "Type");
        
        if(type.getLength() !=1
          && max != 1
          && type.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(type.getLength() !=1 ) max = type.getLength();
        
        NodeList str = ((Element)(nList.item(it)))
          .getElementsByTagName("Strength");
        
        if ( str.getLength() != 1
          && max !=1
          && str.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(str.getLength() !=1 ) max = str.getLength();
        
        NodeList multi = ((Element)(nList.item(it))).getElementsByTagName(
          "Multiplier");
        
        if(multi.getLength() !=1 && max !=1 && multi.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(multi.getLength() !=1 ) max = multi.getLength();
        
        NodeList offset = ((Element)(nList.item(it)))
          .getElementsByTagName("Offset");
        
        if(offset.getLength() !=1
          && max != 1
          && offset.getLength() != max)
          throw new RuntimeException("number does not match");
        
        if(offset.getLength() !=1 ) max = offset.getLength();
        
        NodeList delay = ((Element)(nList.item(it)))
          .getElementsByTagName("Delay");
        
        if ( delay.getLength() != 1
          && max != 1
          && delay.getLength() != max )
          throw new RuntimeException("number does not match");
        
        if(delay.getLength() !=1 ) max = delay.getLength();
        
        // System.out.println("max"+max);

        for(int i=0; i < max; i++)
        {
          Element
            lmatrix,
            eSmatrix;
          
          if(pmatrix.getLength()==max)
          {
            lmatrix = (Element)(pmatrix.item(i));
          }
          else
          {
            lmatrix = (Element)(pmatrix.item(0));
          }

          double [][] conMatrix;
          
          if(lmatrix.getElementsByTagName("Ref").getLength()>0)
          {
            conMatrix = matrixDataMap.get(
              lmatrix.getElementsByTagName("Ref").item(0).getFirstChild()
                .getNodeValue());
            
            if(conMatrix == null)
              throw new RuntimeException(
                lmatrix.getElementsByTagName("Ref").item(0).getFirstChild()
                  .getNodeValue()
                    +" as a reference is not defined in Data Section");
          }
          else 
          {
            NodeList rows = lmatrix.getElementsByTagName("Row");
            
            String firstRow = rows.item(0).getFirstChild().getNodeValue();
            
            String sep [] = firstRow.split("[a-z,\t ]");

            conMatrix = new double [rows.getLength()][sep.length];

            for(int colId=0; colId < sep.length; colId++)
            {
              conMatrix[0][colId]=Double.parseDouble(sep[colId]);
            }

            for( int rowId=1; rowId<rows.getLength(); rowId++)
            {
              firstRow = rows.item(rowId).getFirstChild().getNodeValue();
              
              sep = firstRow.split("[a-z,\t ]");
              
              for(int colId=0; colId < sep.length; colId++)
              {
                conMatrix[rowId][colId]=Double.parseDouble(sep[colId]);
              }
            }
          }

          // System.out.println(sep.length);
          
          double [][] sconMatrix = null;

          if(str_matrix) // if strength matrix exists
          {
            if(smatrix.getLength()==max)
            {
              eSmatrix = (Element)(smatrix.item(i));
            }
            else
            {
              eSmatrix = (Element)(smatrix.item(0));
            }

            if(eSmatrix.getElementsByTagName("Ref").getLength()>0)
            {
              sconMatrix = matrixDataMap.get(
                eSmatrix.getElementsByTagName("Ref").item(0).getFirstChild()
                  .getNodeValue());
              
              if(sconMatrix == null)
                throw new RuntimeException(
                  eSmatrix.getElementsByTagName("Ref").item(0)
                    .getFirstChild().getNodeValue()
                    +" as a reference is not defined in Data Section");
            }
            else
            {
              NodeList rows = eSmatrix.getElementsByTagName("Row");
              
              String firstRow = rows.item(0).getFirstChild().getNodeValue();
              
              String [] sep = firstRow.split("[a-z,\t ]");
              
              // System.out.println(sep.length);

              sconMatrix = new double [rows.getLength()][sep.length];
              
              for(int colId=0; colId < sep.length; colId++)
              {
                sconMatrix[0][colId]=Double.parseDouble(sep[colId]);
              }

              for( int rowId=1; rowId<rows.getLength(); rowId++)
              {
                firstRow = rows.item(rowId).getFirstChild().getNodeValue();
                
                sep = firstRow.split("[a-z,\t ]");
                
                for(int colId=0; colId < sep.length; colId++)
                {
                  sconMatrix[rowId][colId]=Double.parseDouble(sep[colId]);
                }
              }
            }
          }

          Element lrotation;
          
          if(orientation.getLength()==max)
          {
            lrotation = (Element)(orientation.item(i));
          }
          else
          {
            lrotation = (Element)(orientation.item(0));
          }

          int rot = Integer.parseInt(
            lrotation.getFirstChild().getNodeValue())/90;

          if(rot>0)
          {
            for(int ii=0 ; ii< rot; ii++)
            {
              conMatrix = FunUtil.rRotate90(conMatrix);
              
              if(str_matrix)sconMatrix = FunUtil.rRotate90(sconMatrix);
            }
          }
          else if(rot <0)
          {
            for(int ii=0 ; ii< -rot; ii++)
            {
              conMatrix = FunUtil.lRotate90(conMatrix);
              
              if(str_matrix)sconMatrix = FunUtil.lRotate90(sconMatrix);
            }
          }
          
          //remember to change the rows matrix. use rows
          
          Element lfrom;
          
          if(from.getLength()==max)
          {
            lfrom = (Element)(from.item(i));
          }
          else
          {
            lfrom = (Element)(from.item(0));
          }
          
          Element lto;
          
          if(to.getLength()==max)
          {
            lto = (Element)(to.item(i));
          }
          else
          {
            lto = (Element)(to.item(0));
          }

          Element ltype;
          
          if(type.getLength()==max)
          {
            ltype = (Element)(type.item(i));
          }
          else
          {
            ltype = (Element)(type.item(0));
          }

          final int  iType = parseSynapseType ( ltype );
          
          Element lstr;
          
          if(str.getLength()==max)
          {
            lstr = (Element)(str.item(i));
          }
          else
          {
            lstr = (Element)(str.item(0));
          }
          
          Element lmulti;
          
          if(multi.getLength()==max)
          {
            lmulti = (Element)(multi.item(i));
          }
          else
          {
            lmulti = (Element)(multi.item(0));
          }
          
          Element loffset;
          
          if(offset.getLength()==max)
          {
            loffset = (Element)(offset.item(i));
          }
          else
          {
            loffset = (Element)(offset.item(0));
          }
          
          Element ldelay;
          
          if(delay.getLength()==max)
          {
            ldelay = (Element)(delay.item(i));
          }
          else
          {
            ldelay = (Element)(delay.item(0));
          }
          
/*
          System.out.print(lfrom.getAttributeNode("prefix").getNodeValue()
            +" "+lfrom.getAttributeNode("suffix").getNodeValue()+" "
            +lto.getAttributeNode("prefix").getNodeValue()+" "
            +lto.getAttributeNode("suffix").getNodeValue());
          System.out.print(" "+sty);
          System.out.print(" "+lstr.getFirstChild().getNodeValue());
          System.out.print(" "+lmulti.getFirstChild().getNodeValue());
          System.out.print(" "+loffset.getFirstChild().getNodeValue());
          System.out.print(" "+ldelay.getFirstChild().getNodeValue());
          System.out.println();
*/
/*
          if(connected)
          {
          p.print(lfrom.getAttributeNode("prefix").getNodeValue()+" "
            +lfrom.getAttributeNode("suffix").getNodeValue()+" "
            +lto.getAttributeNode("prefix").getNodeValue()+" "
            +lto.getAttributeNode("suffix").getNodeValue());
          p.print(" "+sty);
          p.print(" "+lstr.getFirstChild().getNodeValue());
          p.print(" "+lmulti.getFirstChild().getNodeValue());
          p.print(" "+loffset.getFirstChild().getNodeValue());
          p.print(" "+ldelay.getFirstChild().getNodeValue());
          p.println();
          }
*/
/*
          for(int ii=0; ii < conMatrix.length; ii++)
          {
          for(int jj=0 ; jj < conMatrix[0].length; jj++)
          {
          System.out.print(conMatrix[ii][jj]+" ");
          }
          System.out.println();
          }
*/
          Double theStr;
          
          if(lstr.getElementsByTagName("Ref").getLength()>0)
          {
            theStr = doubleDataMap.get(
              lstr.getElementsByTagName("Ref").item(0).getFirstChild()
                .getNodeValue());
            
            if(theStr == null)
              throw new RuntimeException(
                lstr.getElementsByTagName("Ref").item(0).getFirstChild()
                  .getNodeValue()
                    +" as a reference is not defined in Data Section");
          }
          else
          {
            theStr = Double.parseDouble(
              lstr.getFirstChild().getNodeValue());
          }
          
          Double theDelay;
          
          if(ldelay.getElementsByTagName("Ref").getLength()>0)
          {
            theDelay = doubleDataMap.get(
              ldelay.getElementsByTagName("Ref").item(0).getFirstChild()
                .getNodeValue());
            
            if(theDelay == null)
              throw new RuntimeException(
                ldelay.getElementsByTagName("Ref").item(0).getFirstChild()
                  .getNodeValue()
                    +" as a reference is not defined in Data Section");
          }
          else
          {
            theDelay = Double.parseDouble(
              ldelay.getFirstChild().getNodeValue());
          }

          double outVal;

          outVal = layerStructure.connectgp(
            lfrom.getAttributeNode("prefix").getNodeValue(),
            lfrom.getAttributeNode("suffix").getNodeValue(),
            lto.getAttributeNode("prefix").getNodeValue(),
            lto.getAttributeNode("suffix").getNodeValue(),
            theStr * Double.parseDouble(
              lmulti.getFirstChild().getNodeValue())
              + Double.parseDouble(
                loffset.getFirstChild().getNodeValue()),
            iType,
            theDelay,
            conMatrix,
            sconMatrix,
            hostId);
          
          if(outVal > maxOutput) maxOutput = outVal;
        }
      }

      //read layers;
      
      nList = conns.getElementsByTagName("Connection");
      
      for(int it= 0 ; it< nList.getLength(); it++) // add all the layers;
      {
        String perio = ((Element)(nList.item(it)))
          .getAttributeNode("periodic").getValue();
        
        boolean periodic;
        
        if(perio.equals("true") || perio.equals("TRUE"))
        {
          periodic = true;
        }
        else
        {
          periodic = false;
        }

        Element matrix = (Element)(((Element)(nList.item(it)))
          .getElementsByTagName("Matrix").item(0));

        NodeList rows = matrix.getElementsByTagName("Row");
        
        String firstRow = rows.item(0).getFirstChild().getNodeValue();
        
        String sep [] = firstRow.split("[a-z,\t ]");
        
        // p.println(sep.length);
        // p.flush();
        
        double [][]
          conMatrix = new double [rows.getLength()][sep.length];
        
        for(int colId=0; colId < sep.length; colId++)
        {
          conMatrix[0][colId]=Double.parseDouble(sep[colId]);
        }

        for( int rowId=1; rowId<rows.getLength(); rowId++)
        {
          firstRow = rows.item(rowId).getFirstChild().getNodeValue();
          
          sep = firstRow.split("[a-z,\t ]");
          
          for(int colId=0; colId < sep.length; colId++)
          {
            conMatrix[rowId][colId]=Double.parseDouble(sep[colId]);
          }
        }
/*
      for(int ii=0; ii < conMatrix.length; ii++)
      {
        for(int jj=0 ; jj < conMatrix[0].length; jj++)
        {
          p.print(conMatrix[ii][jj]+" ");
        }
        p.println();
      }
*/

        int rotation = Integer.parseInt(
          ((Element)(nList.item(it))).getElementsByTagName("Rotation")
          .item(0).getFirstChild().getNodeValue())/90;

        if(rotation>0)
        {
          for(int ii=0 ; ii< rotation; ii++)
          {
            conMatrix = FunUtil.rRotate90(conMatrix);
          }
        }
        else if(rotation <0)
        {
          for(int ii=0 ; ii< -rotation; ii++)
          {
            conMatrix = FunUtil.lRotate90(conMatrix);
          }
        }
/*
        for(int ii=0; ii < conMatrix.length; ii++)
        {
          for(int jj=0 ; jj < conMatrix[0].length; jj++)
          {
            p.print(conMatrix[ii][jj]+" ");
          }
          p.println();
        }
*/

        Element from = (Element)(((Element)(nList.item(it)))
          .getElementsByTagName("FromLayer").item(0));
        
        Element to = (Element)(((Element)(nList.item(it)))
          .getElementsByTagName("ToLayer").item(0));
        
        final Element  styleElement = getFirstElement (
          ( Element ) nList.item ( it ),
          "Style" );
        
        final boolean  sty = parseVergenceStyle ( styleElement );
        
        final Element  typeElement = getFirstElement (
          ( Element ) nList.item ( it ),
          "Type" );

        final int  type = parseSynapseType ( typeElement );
/*
        System.out.println(
          ((Element)(from.getElementsByTagName("Prefix").item(0)))
          .getFirstChild().getNodeValue()+" "
          + ((Element)(from.getElementsByTagName("Suffix").item(0)))
          .getFirstChild().getNodeValue()+" "
          + ((Element)(to.getElementsByTagName("Prefix").item(0)))
          .getFirstChild().getNodeValue()+" "
          + ((Element)(to.getElementsByTagName("Suffix").item(0)))
          .getFirstChild().getNodeValue());
*/
/*
        if(connected)
        {
          p.println("str"+Double.parseDouble(
            ((Element)(nList.item(it))).getElementsByTagName("Strength")
            .item(0).getFirstChild().getNodeValue())
            * Double.parseDouble(((Element)(nList.item(it)))
            .getElementsByTagName("Multiplier").item(0).getFirstChild()
            .getNodeValue())+ Double.parseDouble(((Element)(nList.item(it)))
            .getElementsByTagName("Offset").item(0).getFirstChild()
            .getNodeValue()));
            
          p.println(((Element)(from.getElementsByTagName("Prefix").item(0)))
            .getFirstChild().getNodeValue()+ " "
            +((Element)(from.getElementsByTagName("Suffix").item(0)))
              .getFirstChild().getNodeValue()+" "
            +((Element)(to.getElementsByTagName("Prefix").item(0)))
              .getFirstChild().getNodeValue()+" "
            +((Element)(to.getElementsByTagName("Suffix").item(0)))
              .getFirstChild().getNodeValue());
        }
*/
        double outVal;
        
        outVal = layerStructure.connect(
          ((Element)(from.getElementsByTagName("Prefix").item(0)))
            .getFirstChild().getNodeValue(),
          ((Element)(from.getElementsByTagName("Suffix").item(0)))
            .getFirstChild().getNodeValue(),
          ((Element)(to.getElementsByTagName("Prefix").item(0)))
            .getFirstChild().getNodeValue(),
          ((Element)(to.getElementsByTagName("Suffix").item(0)))
            .getFirstChild().getNodeValue(),
          sty,
          Double.parseDouble(
            ((Element)(nList.item(it))).getElementsByTagName("Strength")
            .item(0).getFirstChild().getNodeValue())
            * Double.parseDouble(
              ((Element)(nList.item(it))).getElementsByTagName("Multiplier")
              .item(0).getFirstChild().getNodeValue())
              + Double.parseDouble(((Element)(nList.item(it)))
                .getElementsByTagName("Offset").item(0).getFirstChild()
                .getNodeValue()),
          type,
          Double.parseDouble(
            ((Element)(nList.item(it))).getElementsByTagName("Delay")
            .item(0).getFirstChild().getNodeValue()),
          conMatrix,
          hostId,
          periodic);
        
        if(outVal > maxOutput) maxOutput = outVal;
      }
      
      return maxOutput;
    }

    /***********************************************************************
    * map the neurons, build up the index
    ***********************************************************************/
    public void parseMapCells(int hostId)
    ////////////////////////////////////////////////////////////////////////
    {
      NodeList nList = rootElement.getElementsByTagName("OverallConfiguration");
      
      Element gloable = (Element) nList.item(0);

      // layer size
      
      xEdgeLength = Integer.parseInt((gloable.getElementsByTagName("xEdgeLength").item(0)).getFirstChild().getNodeValue());
      
      yEdgeLength = Integer.parseInt((gloable.getElementsByTagName("yEdgeLength").item(0)).getFirstChild().getNodeValue());
      
      if(gloable.getElementsByTagName("DataFileName").getLength()>0)
      {
        outFile = (gloable.getElementsByTagName("DataFileName").item(0)).getFirstChild().getNodeValue();
      }
      
      // System.out.println(xEdgeLength);
      
      // System.out.println(yEdgeLength);
      
      parallelHost = Integer.parseInt(
        (gloable.getElementsByTagName("TrialHost").item(0))
        .getFirstChild().getNodeValue());

      // Host info
      
      Element ele = (Element)
        gloable.getElementsByTagName("NetHost").item(0);

      numOfHosts = Integer.parseInt(
        ele.getElementsByTagName("Number")
        .item(0).getFirstChild().getNodeValue());
      
      //  System.out.println(numOfHosts);
      
      Element method = (Element)
        ele.getElementsByTagName("MapMethod").item(0);
      
      aSec = Integer.parseInt(method.getAttributeNode("A").getValue());
      
      bSec = Integer.parseInt(method.getAttributeNode("B").getValue());
      
      mapType=LayerStructure.ABMAP;

      layerStructure = new LayerStructure (
        modelFactory,
        numOfHosts,
        xEdgeLength,
        yEdgeLength );

      if(aSec*bSec!=numOfHosts)
        throw new RuntimeException(aSec+"x"+bSec+"/="+numOfHosts);

      backgroundFrequency = Double.parseDouble(
        (gloable.getElementsByTagName("BackgroundFrequency").item(0))
        .getFirstChild().getNodeValue());
      
      backgroundStrength = Double.parseDouble(
        (gloable.getElementsByTagName("BackgroundStrength").item(0))
        .getFirstChild().getNodeValue());
      
      final Element  backgroundTypeElement
        = getFirstElement ( gloable, "BackgroundType" );
      
      final int  bChannel = parseSynapseType ( backgroundTypeElement );

      //read layers;
      
      nList = rootElement.getElementsByTagName("Layers");
      
      ele = (Element)nList.item(0);
      
      NodeList lay = ele.getElementsByTagName("Layer");
      
      for(int it= 0 ; it< lay.getLength(); it++) // add all the layers;
      {
        layerStructure.addLayer(
          ((Element)(lay.item(it))).getElementsByTagName("Prefix")
            .item(0).getFirstChild().getNodeValue(),
          ((Element)(lay.item(it))).getElementsByTagName("Suffix")
            .item(0).getFirstChild().getNodeValue(),
          Integer.parseInt(((Element)(lay.item(it)))
            .getElementsByTagName("MultiX")
            .item(0).getFirstChild().getNodeValue()),
          Integer.parseInt(((Element)(lay.item(it)))
            .getElementsByTagName("MultiY").item(0)
            .getFirstChild().getNodeValue()),
          ((Element)(lay.item(it))).getElementsByTagName("NeuronType")
            .item(0).getFirstChild().getNodeValue());
      }
      
      // System.out.println("total Neurons"+ls.totalNumNeurons());
      
      switch(mapType)
      {
        case LayerStructure.ABMAP:
          
          layerStructure.abmapcells(aSec,bSec,hostId);
          
          break;
          
        case LayerStructure.LINEMAP:
          
          layerStructure.mapcells();
          
          break;
          
        default:
          
          break;
      }
      
      layerStructure.neuronIndex(hostId);
    }

/*
    public String[] plotLine(int x1,int y1,int x2,int y2)
    ////////////////////////////////////////////////////////////////////////
    {
      ArrayList<String> corr =  new ArrayList<String>();
      
      if ( x1> xEdgeLength || x2>xEdgeLength || y1>yEdgeLength
        || y2> yEdgeLength )
        throw new RuntimeException("out of range error");

      return  corr.toArray(new String[0]);
    }
*/

    /***********************************************************************
    * Parse the experiment section, build stimuli accordingly
    ***********************************************************************/
    public void  parseExp ( int  hostId)
    ////////////////////////////////////////////////////////////////////////
    {
      LOGGER.trace ( "parseExp(hostId) entering" );
      
      NodeList nList = ((Element)
        ((rootElement.getElementsByTagName("Experiment")).item(0)))
        .getElementsByTagName("SubExperiment");
      
      // Element gloable = (Element) nList.item(0);
      
      SubExp[] subs= new SubExp[nList.getLength()];
      
      for(int it= 0 ; it< nList.getLength(); it++) // add all the layers;
      {
        final Element  subExperimentElement = (Element)nList.item(it);

        NodeList inputs
          = subExperimentElement.getElementsByTagName("Input");
        
        Stimulus[] stis = new Stimulus[inputs.getLength()];

        for(int iter=0; iter< inputs.getLength(); iter++)
        {
          NodeList mat = ((Element)(inputs.item(iter)))
            .getElementsByTagName("Matrix");
          
          double [][] conMatrix;
          
          if(mat.getLength()!=0) //filter for the retina
          {
            Element matrix = (Element)(mat.item(0));

            if(matrix.getElementsByTagName("Ref").getLength()>0)
            {
              conMatrix = matrixDataMap.get(
                matrix.getElementsByTagName("Ref").item(0).getFirstChild()
                  .getNodeValue());
              
              if(conMatrix == null)
                throw new RuntimeException(
                  matrix.getElementsByTagName("Ref").item(0)
                    .getFirstChild().getNodeValue()
                      +" as a reference is not defined in Data Section");
            }
            else
            {
              NodeList rows = matrix.getElementsByTagName("Row");
              
              String firstRow = rows.item(0).getFirstChild().getNodeValue();
              
              String sep [] = firstRow.split("[a-z,\t ]");
              
              // System.out.println(sep.length);
              
              conMatrix = new double [rows.getLength()][sep.length];
              
              for(int colId=0; colId < sep.length; colId++)
              {
                conMatrix[0][colId]=Double.parseDouble(sep[colId]);
              }

              for( int rowId=1; rowId<rows.getLength(); rowId++)
              {
                firstRow = rows.item(rowId).getFirstChild().getNodeValue();
                
                sep = firstRow.split("[a-z,\t ]");
                
                for(int colId=0; colId < sep.length; colId++)
                {
                  conMatrix[rowId][colId]=Double.parseDouble(sep[colId]);
                }
              }
            }
          }
          else
          {
            conMatrix = new double [1][1];
            
            conMatrix[0][0]=1.0;
          }

          final Element  typeElement = ( Element )
            ((Element)inputs.item(iter))
            .getElementsByTagName("Type").item(0);
          
          final int  type = parseSynapseType ( typeElement );
          
          // System.out.println(Double.parseDouble(
          //   ((Element)inputs.item(iter)).getElementsByTagName(
          //   "Strength").item(0).getFirstChild().getNodeValue()));
          
          stis[iter] = new Stimulus (
            modelFactory,
            this,
            Double.parseDouble(
              ((Element)inputs.item(iter)).getElementsByTagName("Strength")
                .item(0).getFirstChild().getNodeValue()),
            type,
            Double.parseDouble(((Element)inputs.item(iter))
              .getAttributeNode("on").getValue()),
            Double.parseDouble(((Element)inputs.item(iter))
              .getAttributeNode("off").getValue()),
            Double.parseDouble(((Element)inputs.item(iter))
              .getElementsByTagName("Frequency").item(0).getFirstChild()
              .getNodeValue()),
            Double.parseDouble(((Element)inputs.item(iter))
              .getElementsByTagName("Decay").item(0).getFirstChild()
              .getNodeValue()),
            idum,
            Integer.parseInt(((Element)inputs.item(iter))
              .getElementsByTagName("Channel").item(0).getFirstChild()
              .getNodeValue()));

          NodeList plots = ((Element)inputs.item(iter))
            .getElementsByTagName("Stimulus");
          
          for(int pl=0; pl<plots.getLength(); pl++)
          {
            NodeList lines = ((Element)plots.item(pl))
              .getElementsByTagName("Line");
            
            for(int li = 0 ; li < lines.getLength(); li++)
            {
              String sep [] = ((Element)lines.item(li))
                .getFirstChild().getNodeValue().split("[a-z,\t ]");
              
              if(sep.length!=4)
                throw new RuntimeException("need 4 arguments");
              
              stis[iter].plotLine(
                Integer.parseInt(sep[0]),
                Integer.parseInt(sep[1]),
                Integer.parseInt(sep[2]),
                Integer.parseInt(sep[3]),
                ((Element)inputs.item(iter))
                  .getElementsByTagName("Prefix").item(0)
                  .getFirstChild().getNodeValue(),
                ((Element)inputs.item(iter))
                  .getElementsByTagName("Suffix").item(0)
                  .getFirstChild().getNodeValue(),hostId);
            }
            
            NodeList squares = ((Element)plots.item(pl))
              .getElementsByTagName("Square");
            
            for(int li = 0 ; li < squares.getLength(); li++)
            {
              String sep [] = ((Element)squares.item(li))
                .getFirstChild().getNodeValue().split("[a-z,\t ]");
              
              if(sep.length!=4)
                throw new RuntimeException("need 4 arguments");
              
              stis[iter].plotSquare(
                Integer.parseInt(sep[0]),
                Integer.parseInt(sep[1]),
                Integer.parseInt(sep[2]),
                Integer.parseInt(sep[3]),
                ((Element)inputs.item(iter))
                  .getElementsByTagName("Prefix").item(0)
                    .getFirstChild().getNodeValue(),
                ((Element)inputs.item(iter))
                  .getElementsByTagName("Suffix").item(0)
                    .getFirstChild().getNodeValue(),hostId);
            }
            
            NodeList image
              = ((Element)plots.item(pl)).getElementsByTagName("Image");
            
            for(int li = 0 ; li < image.getLength(); li++)
            {
              String reverse = ((Element)
                image.item(li)).getAttributeNode("reverse").getValue();
              
              double gamma
                = Double.parseDouble(
                  ((Element)image.item(li))
                    .getAttributeNode("gamma").getValue());
              
              double k
                = Double.parseDouble(((Element)image.item(li))
                  .getAttributeNode("threshold").getValue());
              
              if(reverse.equals("true") || reverse.equals("TRUE"))
              {
                stis[iter].plotRImg(
                  ((Element)inputs.item(iter))
                    .getElementsByTagName("Prefix").item(0).getFirstChild()
                    .getNodeValue(),
                  ((Element)inputs.item(iter))
                    .getElementsByTagName("Suffix").item(0).getFirstChild()
                    .getNodeValue(),
                  ((Element)image.item(li)).getFirstChild().getNodeValue(),
                  conMatrix,
                  gamma,
                  k,
                  hostId);
              }
              else
              {
                stis[iter].plotImg(
                  ((Element)inputs.item(iter))
                    .getElementsByTagName("Prefix").item(0).getFirstChild()
                      .getNodeValue(),
                  ((Element)inputs.item(iter))
                    .getElementsByTagName("Suffix").item(0).getFirstChild()
                      .getNodeValue(),
                  ((Element)image.item(li)).getFirstChild().getNodeValue(),
                  conMatrix,
                  gamma,
                  k,
                  hostId);
              }
            }

            NodeList matrixplot = ((Element)plots.item(pl))
              .getElementsByTagName("PlotMatrix");
            
            for(int li = 0 ; li < matrixplot.getLength(); li++)
            {
              int cenX = Integer.parseInt(
                ((Element)matrixplot.item(li)).getAttributeNode("centerX")
                  .getValue());
              
              int cenY = Integer.parseInt(((Element)matrixplot.item(li))
                .getAttributeNode("centerY").getValue());

              double [][] plotMatrix;
              
              plotMatrix = matrixDataMap.get(
                ((Element)matrixplot.item(li)).getElementsByTagName("Ref")
                  .item(0).getFirstChild().getNodeValue());
              
              if(plotMatrix == null)
                throw new RuntimeException(
                  ((Element)matrixplot.item(li)).getElementsByTagName("Ref")
                    .item(0).getFirstChild().getNodeValue()
                      +" as a reference is not defined in Data Section");

              stis[iter].plotMatrix(
                ((Element)inputs.item(iter)).getElementsByTagName("Prefix")
                  .item(0).getFirstChild().getNodeValue(),
                ((Element)inputs.item(iter)).getElementsByTagName("Suffix")
                  .item(0).getFirstChild().getNodeValue(),
                cenX,
                cenY,
                plotMatrix,
                hostId);
            }

            NodeList back = ((Element)plots.item(pl)).getElementsByTagName(
              "Background");
            
            for(int li = 0 ; li < back.getLength(); li++)
            {
              stis[iter].background(
                ((Element)inputs.item(iter)).getElementsByTagName("Prefix")
                  .item(0).getFirstChild().getNodeValue(),
                ((Element)inputs.item(iter)).getElementsByTagName("Suffix")
                  .item(0).getFirstChild().getNodeValue(),
                hostId);    
            }

            squares = ((Element)plots.item(pl))
              .getElementsByTagName("FilledSquare");
            
            for(int li = 0 ; li < squares.getLength(); li++)
            {
              String sep [] = ((Element)squares.item(li))
                .getFirstChild().getNodeValue().split("[a-z,\t ]");
              
              if(sep.length!=4)
                throw new RuntimeException("need 4 arguments");
              
              stis[iter].plotFilledSquare(
                Integer.parseInt(sep[0]),
                Integer.parseInt(sep[1]),
                Integer.parseInt(sep[2]),
                Integer.parseInt(sep[3]),
                ((Element)inputs.item(iter))
                  .getElementsByTagName("Prefix").item(0).getFirstChild()
                    .getNodeValue(),
                ((Element)inputs.item(iter)).getElementsByTagName("Suffix")
                  .item(0).getFirstChild().getNodeValue(),hostId);
            }
          }
          // System.out.println(stis[iter].init(iter));
        } // END: for inputs
        
        // stis[it].pas=null; //release the memory
        
        for ( int iter=0; iter< inputs.getLength(); iter++ )
          stis[iter].simulatorParser=null; //release the memory
        
        final Collection<DiscreteEvent>
          modulatorDiscreteEventCollection
            = parseModulator ( subExperimentElement );        

        subs[it] = new SubExp (
          Integer.parseInt (
            subExperimentElement.getAttributeNode("repetition").getValue()),
          Double.parseDouble (
            subExperimentElement.getAttributeNode("length").getValue()),
          stis,
          modulatorDiscreteEventCollection );
        
        subs[it].name
          = subExperimentElement.getAttributeNode("name").getValue();
        
        // System.out.println(subExp.getAttributeNode("length").getValue()
        //   + " " + subExp.getAttributeNode("repetition").getValue());
        //
      } // END: for subexp

      // recorder part
      
      Element recorder = ((Element)
        ((rootElement.getElementsByTagName("Recorder")).item(0)));

      String plotStr = recorder.getAttributeNode("plot").getValue();
      
      boolean plot;
      
      if(plotStr.equals("true") || plotStr.equals("TRUE"))
      {
        plot = true;
      }
      else
      {
        plot = false;
      }

      Recorder rec = new Recorder(this, plot);
      
      // single unit electrode
      
      NodeList outputFile = recorder.getElementsByTagName("OutputFile");
      
      if ( outputFile.getLength()>0 )
        rec.outputFile=((Element)outputFile.item(0))
          .getFirstChild().getNodeValue();
      
      NodeList single = recorder.getElementsByTagName("SingleUnit");
      
      for(int su=0; su<single.getLength(); su++)
      {
        //line
        //      ArrayList<Integer> sUnit = new ArrayList<Integer>();
        
        NodeList
          line = ((Element)(single.item(su))).getElementsByTagName("Line");
        
        for(int li = 0 ; li < line.getLength(); li++)
        {
          String
            sep[] = ((Element)(line.item(li))).getFirstChild()
              .getNodeValue().split("[a-z,\t ]");
          
          rec.singleUnit.addAll (
            rec.addLine (
              Integer.parseInt(sep[0]),
              Integer.parseInt(sep[1]),
              Integer.parseInt(sep[2]),
              Integer.parseInt(sep[3]),
              ((Element)(line.item(li))).getAttributeNode("pre").getValue(),
              ((Element)(line.item(li))).getAttributeNode("suf").getValue(),
              hostId,
              rec.suNames));
        }

        //square
        
        NodeList
          square = ((Element)(single.item(su))).getElementsByTagName(
            "Square");
        
        for(int li = 0 ; li < square.getLength(); li++)
        {
          String sep[] = ((Element)(square.item(li))).getFirstChild()
            .getNodeValue().split("[a-z,\t ]");
          
          rec.singleUnit.addAll(
            rec.addSquare(
              Integer.parseInt(sep[0]),
              Integer.parseInt(sep[1]),
              Integer.parseInt(sep[2]),
              Integer.parseInt(sep[3]),
              ((Element)(square.item(li))).getAttributeNode("pre")
                .getValue(),
              ((Element)(square.item(li))).getAttributeNode("suf")
                .getValue(),
              hostId,
              rec.suNames));
        }
        
        // System.out.println(rec.suNames);
        // System.out.println(rec.singleUnit);
      }

      //multi unit electrode
      
      single = recorder.getElementsByTagName("MultiUnit");
      
      for(int su=0; su<single.getLength(); su++)
      {
        rec.timeBinSize = Double.parseDouble(
          ((Element)(single.item(su))).getAttributeNode("binSize")
            .getValue());
        
        rec.multiNames.add(
          ((Element)(single.item(su))).getElementsByTagName("Name")
            .item(0).getFirstChild().getNodeValue());
        
        //line
        
        ArrayList<Integer> mUnit = new ArrayList<Integer>();
        
        ArrayList<String> mName = new ArrayList<String>();

        NodeList
          line = ((Element)(single.item(su))).getElementsByTagName("Line");
        
        for(int li = 0 ; li < line.getLength(); li++)
        {
          String sep[] = ((Element)(line.item(li))).getFirstChild()
            .getNodeValue().split("[a-z,\t ]");
          
          mUnit.addAll(
            rec.addLine(
              Integer.parseInt(sep[0]),
              Integer.parseInt(sep[1]),
              Integer.parseInt(sep[2]),
              Integer.parseInt(sep[3]),
              ((Element)(line.item(li))).getAttributeNode("pre").getValue(),
              ((Element)(line.item(li))).getAttributeNode("suf").getValue(),
              hostId,
              mName));
        }

        //square
        
        NodeList square = ((Element)(single.item(su)))
          .getElementsByTagName("Square");
        
        for(int li = 0 ; li < square.getLength(); li++)
        {
          String sep[] = ((Element)(square.item(li)))
            .getFirstChild().getNodeValue().split("[a-z,\t ]");
          
          mUnit.addAll(
            rec.addSquare(
              Integer.parseInt(sep[0]),
              Integer.parseInt(sep[1]),
              Integer.parseInt(sep[2]),
              Integer.parseInt(sep[3]),
              ((Element)(square.item(li))).getAttributeNode("pre")
                .getValue(),
              ((Element)(square.item(li))).getAttributeNode("suf")
                .getValue(),
              hostId,
              mName));
        }

        rec.multiUnit.add(mUnit);
        
        rec.neuronNames.add(mName);
        
        //System.out.println(mUnit);
        
        //System.out.println(mName);
      }

      // field electrode
      
      //multi unit electrode
      
      single = recorder.getElementsByTagName("Field");
      
      for(int su=0; su<single.getLength(); su++)
      {
        rec.fieldNames.add(
          ((Element)(single.item(su))).getElementsByTagName("Name")
            .item(0).getFirstChild().getNodeValue());
        
        Layer tmpLayer = layerStructure.getLayer(
          ((Element)(single.item(su))).getElementsByTagName("Prefix")
            .item(0).getFirstChild().getNodeValue(),
          ((Element)(single.item(su))).getElementsByTagName("Suffix")
            .item(0).getFirstChild().getNodeValue());
        
        rec.fieldEle.add(tmpLayer);
        
        rec.fieldTimeBinSize = Double.parseDouble(
          ((Element)(single.item(su))).getAttributeNode("binSize")
            .getValue());
        
        rec.setLayerNeurons (
          tmpLayer,
          hostId); // for slave hosts, they need to set neurons recordable
      }

      // vector plots
      
      single = recorder.getElementsByTagName("VectorMap");
      
      for(int su=0; su<single.getLength(); su++)
      {
        rec.vectorNames.add(
          ((Element)(single.item(su))).getElementsByTagName("Name").item(0)
            .getFirstChild().getNodeValue());
        
        VComponents coms = rec.addComs();
        
        NodeList comlay = ((Element)(single.item(su)))
          .getElementsByTagName("LayerOrientation");
        
        for( int iCom =0 ; iCom < comlay.getLength(); iCom++)
        {
          Layer tmpLayer =layerStructure.getLayer(
            ((Element)(comlay.item(iCom))).getAttributeNode("pre")
              .getValue(),
            ((Element)(comlay.item(iCom))).getAttributeNode("suf")
              .getValue()); 
          coms.addCom(
            tmpLayer,
            Integer.parseInt(
              ((Element)(comlay.item(iCom))).getFirstChild()
                .getNodeValue()));
          
          rec.setLayerNeurons(
            tmpLayer,
            hostId);
        }
        
        rec.vectorTimeBinSize= Double.parseDouble(
          ((Element)(single.item(su))).getAttributeNode("binSize")
            .getValue());
      }

      // intracellular unit electrode
      
      single = recorder.getElementsByTagName("Intracellular");
      
      for(int su=0; su<single.getLength(); su++)
      {
        rec.intraNames.add(
          ((Element)(single.item(su))).getElementsByTagName("Name")
            .item(0).getFirstChild().getNodeValue());
        
        //line
        
        ArrayList<Integer> mUnit = new ArrayList<Integer>();
        
        ArrayList<String> mName = new ArrayList<String>();
        
        ArrayList<Integer> cha = new ArrayList<Integer>();

        NodeList line = ((Element)(single.item(su)))
          .getElementsByTagName("Line");
        
        for(int li = 0 ; li < line.getLength(); li++)
        {
          String sep[] = ((Element)(line.item(li)))
            .getFirstChild().getNodeValue().split("[a-z,\t ]");
          
          mUnit.addAll(
            rec.addLine(
              Integer.parseInt(sep[0]),
              Integer.parseInt(sep[1]),
              Integer.parseInt(sep[2]),
              Integer.parseInt(sep[3]),
              ((Element)(line.item(li))).getAttributeNode("pre").getValue(),
              ((Element)(line.item(li))).getAttributeNode("suf").getValue(),
              hostId,
              mName));
        }

        //square
        
        NodeList square = ((Element)(single.item(su)))
          .getElementsByTagName("Square");
        
        for(int li = 0 ; li < square.getLength(); li++)
        {
          String sep[] = ((Element)(square.item(li)))
            .getFirstChild().getNodeValue().split("[a-z,\t ]");
          
          mUnit.addAll(
            rec.addSquare(
              Integer.parseInt(sep[0]),
              Integer.parseInt(sep[1]),
              Integer.parseInt(sep[2]),
              Integer.parseInt(sep[3]),
              ((Element)(square.item(li))).getAttributeNode("pre")
                .getValue(),
              ((Element)(square.item(li))).getAttributeNode("suf")
                .getValue(),
              hostId,
              mName));
        }

        // channel
        
        square = ((Element)
          (single.item(su))).getElementsByTagName("Channel");
        
        for(int li = 0 ; li < square.getLength(); li++)
        {
          cha.add(
            Integer.parseInt(
              ((Element)(square.item(li))).getFirstChild().getNodeValue()));
        }
        rec.intraEle.add(mUnit);
        
        rec.currChannel.add(cha);
      }

      experiment = new Experiment(subs,rec);
      
      experiment.recorder.simulatorParser=null; //clear pas info;
      
      LOGGER.trace ( "parseExp(hostId) exiting" );
    }

    /***********************************************************************
    * Find out the minimum synaptic delay from network structure. 
    ***********************************************************************/
    public void  findMinDelay ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final NodeList
        delayNodeList = rootElement.getElementsByTagName ( "Delay" );
      
      final int  delayNodeListLength = delayNodeList.getLength ( );
      
      if ( delayNodeListLength == 0 )
      {
        // TODO:  I added this hack to get around null exception when just
        // one layer.  I am not sure if this is reasonable.  -- Croft
        
        minDelay = 0.010;
      }
      else
      {
        final Element
          firstDelayElement = ( Element ) delayNodeList.item ( 0 );

        Double theDelay;

        if ( firstDelayElement.getElementsByTagName ( "Ref" ).getLength ( )
          > 0 )
        {
          final String  refKey
            = getFirstElementFirstChildValue ( firstDelayElement, "Ref" );

          theDelay = doubleDataMap.get ( refKey );

          if ( theDelay == null )
          {
            throw new RuntimeException (
              refKey + " as a reference is not defined in Data Section" );
          }
        }
        else
        {
          theDelay = Double.parseDouble (
            getFirstChildValue ( firstDelayElement ) );
        }

        minDelay = theDelay.doubleValue ( );

// TODO:  Merge the above code with the following; loop from zero, not one      

        for ( int  i = 1; i < delayNodeListLength; i++ )
        {
          final Element
            delayElement = ( Element ) delayNodeList.item ( i );

          if ( delayElement.getElementsByTagName ( "Ref" ).getLength ( )
            > 0 )
          {
            final String  refKey
              = getFirstElementFirstChildValue ( delayElement, "Ref" );

            theDelay = doubleDataMap.get ( refKey );

            if ( theDelay == null )
            {
              throw new RuntimeException (
                refKey + " as a reference is not defined in Data Section" );
            }
          }
          else
          {
            theDelay = Double.parseDouble (
              getFirstChildValue ( delayElement ) );
          }

          final double  delayValue = theDelay.doubleValue ( ); 

          if ( delayValue < minDelay )
          {
            minDelay = delayValue;
          }
        }
      }
      
      LOGGER.trace ( "minDelay = " + minDelay );
      
      experiment.recorder.intraTimeBinSize = minDelay / 2.0;
    }

    /***********************************************************************
    * Parse the experiment for main host
    ***********************************************************************/
    public void  parseExperiment ( )
    ////////////////////////////////////////////////////////////////////////
    {
      LOGGER.trace ( "parseExperiment() entered" );
      
      final Element
        experimentElement = getFirstElement ( rootElement, "Experiment" );
      
      final NodeList  subExperimentNodeList
        = experimentElement.getElementsByTagName ( "SubExperiment" );
      
      final int
        subExperimentNodeListLength = subExperimentNodeList.getLength ( );
      
      // Element gloable = (Element) nList.item(0);
      
      final SubExp [ ]
        subExps = new SubExp [ subExperimentNodeListLength ];
      
      // add all the layers
      
      for ( int  subExperimentIndex = 0;
        subExperimentIndex < subExperimentNodeListLength;
        subExperimentIndex++ )
      {
        final Element  subExperimentElement
          = ( Element ) subExperimentNodeList.item ( subExperimentIndex );

        final NodeList  inputNodeList
          = subExperimentElement.getElementsByTagName ( "Input" );
        
        final int  inputNodeListLength = inputNodeList.getLength ( );
        
        //  System.out.println("num of inputs:"+inputs.getLength());
        
        final Stimulus [ ]  stimuli = new Stimulus [ inputNodeListLength ];

        for ( int inputIndex = 0; inputIndex < inputNodeListLength;
          inputIndex++ )
        {
          final Element
            inputElement = ( Element ) inputNodeList.item ( inputIndex );
          
          final NodeList
            matrixNodeList = inputElement.getElementsByTagName ( "Matrix" );
          
          double [ ] [ ]  conMatrix = null;
          
          // filter for the retina
          
          if ( matrixNodeList.getLength ( ) != 0 )
          {
            final Element
              matrixElement = ( Element ) matrixNodeList.item ( 0 );
            
            conMatrix = getRefMatrix ( matrixElement );
          }
          else
          {
            conMatrix = new double [ 1 ] [ 1 ];
            
            conMatrix [ 0 ] [ 0 ] = 1.0;
          }

          final Element  typeElement = getFirstElement (
            inputElement,
            "Type" );
          
          final int  type = parseSynapseType ( typeElement );
                   
          final Stimulus  stimulus = new Stimulus (
            modelFactory,
            this,
            Double.parseDouble (
              getFirstElementFirstChildValue (
                inputElement,
                "Strength" ) ),
            type,
            Double.parseDouble (
              inputElement.getAttribute ( "on" ) ),
            Double.parseDouble (
              inputElement.getAttribute ( "off" ) ),
            Double.parseDouble (
              getFirstElementFirstChildValue (
                inputElement,
                "Frequency"  ) ),
            Double.parseDouble (
              getFirstElementFirstChildValue (inputElement,"Decay")),
            idum,
            Integer.parseInt (
              getFirstElementFirstChildValue (
                inputElement,
                "Channel" ) ) );

          stimuli [ inputIndex ] = stimulus;
          
          final NodeList  stimulusNodeList
            = inputElement.getElementsByTagName ( "Stimulus" );
          
          final int
            stimulusNodeListLength = stimulusNodeList.getLength ( );
          
          for ( int  stimulusIndex = 0;
            stimulusIndex < stimulusNodeListLength;
            stimulusIndex++ )
          {
            final Element  stimulusElement
              = ( Element ) stimulusNodeList.item ( stimulusIndex );
            
            final NodeList  lineNodeList
              = stimulusElement.getElementsByTagName ( "Line" );
            
            final int  lineNodeListLength = lineNodeList.getLength ( );
            
            for ( int  lineIndex = 0; lineIndex < lineNodeListLength;
              lineIndex++)
            {
              final Element  lineElement
                = ( Element ) lineNodeList.item ( lineIndex );
              
              final String [ ]  sep
                = getFirstChildValue ( lineElement ).split ( "[a-z,\t ]" );
              
              if ( sep.length != 4 )
              {
                throw new RuntimeException ( "need 4 arguments" );
              }
              
              stimulus.plotLine (
                Integer.parseInt ( sep [ 0 ] ),
                Integer.parseInt ( sep [ 1 ] ),
                Integer.parseInt ( sep [ 2 ] ),
                Integer.parseInt ( sep [ 3 ] ),
                getFirstElementFirstChildValue (
                  inputElement,
                  "Prefix" ),
                getFirstElementFirstChildValue (
                  inputElement,
                  "Suffix" ) );
            }
            
            final NodeList  imageNodeList
              = stimulusElement.getElementsByTagName ( "Image" );
            
            final int  imageNodeListLength = imageNodeList.getLength ( );
            
            for ( int  imageIndex = 0; imageIndex < imageNodeListLength;
              imageIndex++ )
            {
              final Element  imageElement
                = ( Element ) imageNodeList.item ( imageIndex );
              
              final String
                reverseValue = imageElement.getAttribute ( "reverse" ),
                prefixValue = getFirstElementFirstChildValue (
                  inputElement,
                  "Prefix" ),
                suffixValue = getFirstElementFirstChildValue (
                  inputElement,
                  "Suffix" ),
                filenameValue = getFirstChildValue ( inputElement );
              
              if ( reverseValue.equals ( "true" )
                || reverseValue.equals ( "TRUE" ) )
              {
                stimulus.plotRImg (
                  prefixValue,
                  suffixValue,
                  filenameValue,
                  conMatrix );
              }
              else
              {
                stimulus.plotImg (
                  prefixValue,
                  suffixValue,
                  filenameValue,
                  conMatrix );
              }
            }

            final NodeList  plotMatrixNodeList
              = stimulusElement.getElementsByTagName ( "PlotMatrix" );
            
            final int
              plotMatrixNodeListLength = plotMatrixNodeList.getLength ( );
            
            for ( int  plotMatrixIndex = 0;
              plotMatrixIndex < plotMatrixNodeListLength;
              plotMatrixIndex++ )
            {
              final Element  plotMatrixElement = ( Element )
                plotMatrixNodeList.item ( plotMatrixIndex );
              
              final int  cenX = Integer.parseInt (
                plotMatrixElement.getAttribute ( "centerX" ) );
              
              final int  cenY = Integer.parseInt (
                plotMatrixElement.getAttribute ( "centerY" ) );

              final String  refKey = getFirstElementFirstChildValue (
                plotMatrixElement,
                "Ref" );
              
              final double [ ] [ ]
                plotMatrix = matrixDataMap.get ( refKey );
              
              if ( plotMatrix == null )
              {
                throw new RuntimeException ( refKey
                  + " as a reference is not defined in Data Section" );
              }

              stimulus.plotMatrix (
                getFirstElementFirstChildValue (
                  inputElement,
                  "Prefix" ),
                getFirstElementFirstChildValue (
                  inputElement,
                  "Suffix" ),
                cenX,
                cenY,
                plotMatrix );
            }

            final NodeList  backgroundNodeList
              = stimulusElement.getElementsByTagName ( "Background" );
            
            final int  backgroundNodeListLength
              = backgroundNodeList.getLength ( );
            
            for ( int  backgroundIndex = 0;
              backgroundIndex < backgroundNodeListLength;
              backgroundIndex++ )
            {
              stimulus.background (
                getFirstElementFirstChildValue (
                  inputElement,
                  "Prefix" ),
                getFirstElementFirstChildValue (
                  inputElement,
                  "Suffix" ) );
            }

            final NodeList  squareNodeList
              = stimulusElement.getElementsByTagName ( "Square" );
            
            final int  squareNodeListLength = squareNodeList.getLength ( );
            
            for ( int  squareIndex = 0; squareIndex < squareNodeListLength;
              squareIndex++ )
            {
              final Element  squareElement
                = ( Element ) squareNodeList.item ( squareIndex );
              
              final String [ ]  sep = getFirstChildValue (
                squareElement ).split ( "[a-z,\t ]" );
              
              if ( sep.length != 4 )
              {
                throw new RuntimeException ( "need 4 arguments" );
              }
              
              stimulus.plotSquare (
                Integer.parseInt ( sep [ 0 ] ),
                Integer.parseInt ( sep [ 1 ] ),
                Integer.parseInt ( sep [ 2 ] ),
                Integer.parseInt ( sep [ 3 ] ),
                getFirstElementFirstChildValue (
                  inputElement,
                  "Prefix" ),
                getFirstElementFirstChildValue (
                  inputElement,
                  "Suffix" ) );
            }
          }
          //System.out.println(stis[iter].init(iter));
        } // END: for inputs
        
        for ( int  inputIndex = 0; inputIndex < inputNodeListLength;
          inputIndex++ )
        {
          // release the memory
          
          stimuli [ inputIndex ].simulatorParser = null;
        }
        
        final Collection<DiscreteEvent>
          modulatorDiscreteEventCollection
            = parseModulator ( subExperimentElement );
        
        subExps [ subExperimentIndex ] = new SubExp (
          Integer.parseInt (
            subExperimentElement.getAttribute ( "repetition" ) ),
          Double.parseDouble (
            subExperimentElement.getAttribute ( "length" ) ),
          stimuli,
          modulatorDiscreteEventCollection );
        
        subExps [ subExperimentIndex ].name
          = subExperimentElement.getAttribute ( "name" );
      } // END: for subexp

      // recorder part

      final Element  recorderElement = getFirstElement (
        rootElement,
        "Recorder" );

      final String  plotStr = recorderElement.getAttribute ( "plot" );
      
      final boolean  plot
        =  plotStr.equals ( "true" )
        || plotStr.equals ( "TRUE" );

      final Recorder  recorder = new Recorder ( this, plot );
      
      final NodeList  outputFileNodeList
        = recorderElement.getElementsByTagName ( "OutputFile" );
      
      if ( outputFileNodeList.getLength ( ) > 0 )
      {
        final Element
          outputFileElement = ( Element ) outputFileNodeList.item ( 0 );
        
        recorder.outputFile = getFirstChildValue ( outputFileElement );
      }
      
      // single unit electrode
      
      final NodeList  singleUnitNodeList
        = recorderElement.getElementsByTagName ( "SingleUnit" );
      
      final int
        singleUnitNodeListLength = singleUnitNodeList.getLength ( );
      
      for ( int  singleUnitIndex = 0;
        singleUnitIndex < singleUnitNodeListLength;
        singleUnitIndex++ )
      {
        final Element  singleUnitElement
          = ( Element ) singleUnitNodeList.item ( singleUnitIndex );
        
        // line
        // ArrayList<Integer> sUnit = new ArrayList<Integer>();
        
        final NodeList  lineNodeList
          = singleUnitElement.getElementsByTagName ( "Line" );
        
        final int  lineNodeListLength = lineNodeList.getLength ( );
        
        for ( int  lineIndex = 0; lineIndex < lineNodeListLength;
          lineIndex++ )
        {
          final Element
            lineElement = ( Element ) lineNodeList.item ( lineIndex );
          
          final String [ ]
            sep = getFirstChildValue ( lineElement ).split ( "[a-z,\t ]" );
          
          recorder.singleUnit.addAll (
            recorder.addLine (
              Integer.parseInt ( sep [ 0 ] ),
              Integer.parseInt ( sep [ 1 ] ),
              Integer.parseInt ( sep [ 2 ] ),
              Integer.parseInt ( sep [ 3 ] ),
              lineElement.getAttribute ( "pre" ),
              lineElement.getAttribute ( "suf" ),
              recorder.suNames ) );
        }

        // square
        
        final NodeList  squareNodeList
          = singleUnitElement.getElementsByTagName ( "Square" );
        
        final int  squareNodeListLength = squareNodeList.getLength ( );
        
        for ( int squareIndex = 0; squareIndex < squareNodeListLength;
          squareIndex++ )
        {
          final Element
            squareElement = ( Element ) squareNodeList.item ( squareIndex );
          
          final String [ ]  sep
            = getFirstChildValue ( squareElement ).split ( "[a-z,\t ]" );
          
          recorder.singleUnit.addAll (
            recorder.addSquare (
              Integer.parseInt ( sep [ 0 ] ),
              Integer.parseInt ( sep [ 1 ] ),
              Integer.parseInt ( sep [ 2 ] ),
              Integer.parseInt ( sep [ 3 ] ),
              squareElement.getAttribute ( "pre" ),
              squareElement.getAttribute ( "suf" ),
              recorder.suNames ) );
        }
        //    System.out.println(rec.suNames);
        //    System.out.println(rec.singleUnit);
      }

      // multi unit electrode
      
      final NodeList  multiUnitNodeList
        = recorderElement.getElementsByTagName ( "MultiUnit" );
      
      final int  multiUnitNodeListLength = multiUnitNodeList.getLength ( );
      
      for ( int  multiUnitIndex = 0;
        multiUnitIndex < multiUnitNodeListLength;
        multiUnitIndex++ )
      {
        final Element  multiUnitElement
          = ( Element ) multiUnitNodeList.item ( multiUnitIndex );
        
        recorder.timeBinSize = Double.parseDouble (
          multiUnitElement.getAttribute ( "binSize" ) );
        
        recorder.multiNames.add (
          getFirstElementFirstChildValue (
            multiUnitElement,
            "Name" ) );
        
        // line
        
        final ArrayList<Integer>  mUnitList = new ArrayList<Integer> ( );
        
        final ArrayList<String>  mNameList = new ArrayList<String> ( );

        final NodeList
          lineNodeList = multiUnitElement.getElementsByTagName ( "Line" );
        
        final int  lineNodeListLength = lineNodeList.getLength ( );
        
        for ( int  lineIndex = 0; lineIndex < lineNodeListLength;
          lineIndex++ )
        {
          final Element
            lineElement = ( Element ) lineNodeList.item ( lineIndex );
          
          final String [ ]
            sep = getFirstChildValue ( lineElement ).split ( "[a-z,\t ]" );
          
          mUnitList.addAll (
            recorder.addLine (
              Integer.parseInt ( sep [ 0 ] ),
              Integer.parseInt ( sep [ 1 ] ),
              Integer.parseInt ( sep [ 2 ] ),
              Integer.parseInt ( sep [ 3 ] ),
              lineElement.getAttribute ( "pre" ),
              lineElement.getAttribute ( "suf" ),
              mNameList ) );
        }

        // square
        
        final NodeList  squareNodeList
          = multiUnitElement.getElementsByTagName ( "Square" );
        
        final int  squareNodeListLength = squareNodeList.getLength ( );
        
        for ( int  squareIndex = 0; squareIndex < squareNodeListLength;
          squareIndex++ )
        {
          // LOGGER.trace ( "parsing MultiUnit Square" );
          
          final Element
            squareElement = ( Element ) squareNodeList.item ( squareIndex );
          
          final String [ ]
            sep = getFirstChildValue ( squareElement ).split("[a-z,\t ]");
          
          mUnitList.addAll (
            recorder.addSquare (
              Integer.parseInt ( sep [ 0 ] ),
              Integer.parseInt ( sep [ 1 ] ),
              Integer.parseInt ( sep [ 2 ] ),
              Integer.parseInt ( sep [ 3 ] ),
              squareElement.getAttribute ( "pre" ),
              squareElement.getAttribute ( "suf" ),
              mNameList ) );
        }

        recorder.multiUnit.add ( mUnitList );
        
        recorder.neuronNames.add ( mNameList );
      }

      // field electrode
      // multi unit electrode
      
      final NodeList
        fieldNodeList = recorderElement.getElementsByTagName ( "Field" );
      
      final int  fieldNodeListLength = fieldNodeList.getLength ( );
      
      for ( int  fieldIndex = 0; fieldIndex < fieldNodeListLength;
        fieldIndex++ )
      {
        final Element
          fieldElement = ( Element ) fieldNodeList.item ( fieldIndex );
        
        recorder.fieldNames.add (
          getFirstElementFirstChildValue ( fieldElement, "Name" ) );
        
        final Layer  tmpLayer = layerStructure.getLayer (
          getFirstElementFirstChildValue ( fieldElement, "Prefix" ),
          getFirstElementFirstChildValue ( fieldElement, "Suffix" ) );
        
        recorder.fieldEle.add ( tmpLayer );
        
        recorder.fieldTimeBinSize
          = Double.parseDouble ( fieldElement.getAttribute ( "binSize" ) );
        
        // for slave hosts, they need to set neurons recordable
        
        recorder.setLayerNeurons ( tmpLayer );
      }

      // vector electrode
      
      // vector plots
      
      final NodeList  vectorMapNodeList
        = recorderElement.getElementsByTagName ( "VectorMap" );
      
      final int  vectorMapNodeListLength = vectorMapNodeList.getLength ( );
      
      for ( int  vectorMapIndex = 0;
        vectorMapIndex < vectorMapNodeListLength;
        vectorMapIndex++ )
      {
        final Element  vectorMapElement
          = ( Element ) vectorMapNodeList.item ( vectorMapIndex );        
          
        recorder.vectorNames.add (
          getFirstElementFirstChildValue ( vectorMapElement, "Name" ) );
        
        final VComponents  coms = recorder.addComs ( );
        
        final NodeList  layerOrientationNodeList
          = vectorMapElement.getElementsByTagName ( "LayerOrientation" );
        
        final int  layerOrientationNodeListLength
          = layerOrientationNodeList.getLength ( );
        
        for ( int  layerOrientationIndex = 0;
          layerOrientationIndex < layerOrientationNodeListLength;
          layerOrientationIndex++ )
        {
          final Element  layerOrientationElement = ( Element )
            layerOrientationNodeList.item ( layerOrientationIndex );
          
          final Layer  tmpLayer = layerStructure.getLayer (
            layerOrientationElement.getAttribute ( "pre" ),
            layerOrientationElement.getAttribute ( "suf" ) );
          
          coms.addCom (
            tmpLayer,
            Integer.parseInt (
              getFirstChildValue ( layerOrientationElement ) ) );
          
          recorder.setLayerNeurons ( tmpLayer );
        }
        
        recorder.vectorTimeBinSize = Double.parseDouble (
          vectorMapElement.getAttribute ( "binSize" ) );
      }
      
      // intracellular unit electrode
      
      final NodeList  intracellularNodeList
        = recorderElement.getElementsByTagName ( "Intracellular" );
      
      final int  intracellularNodeListLength
        = intracellularNodeList.getLength ( );
      
      for ( int  intracellularIndex = 0;
        intracellularIndex < intracellularNodeListLength;
        intracellularIndex++ )
      {
        final Element  intracellularElement = ( Element )
          intracellularNodeList.item ( intracellularIndex );
        
        recorder.intraNames.add (
          getFirstElementFirstChildValue ( intracellularElement, "Name" ) );
        
        // line
        
        final ArrayList<Integer>  mUnitList = new ArrayList<Integer> ( );
        
        final ArrayList<String>  mNameList = new ArrayList<String> ( );
        
        final ArrayList<Integer>  chaList = new ArrayList<Integer> ( );

        final NodeList  lineNodeList
          = intracellularElement.getElementsByTagName ( "Line" );
        
        final int  lineNodeListLength = lineNodeList.getLength ( ); 
        
        for ( int  lineIndex = 0; lineIndex < lineNodeListLength;
          lineIndex++ )
        {
          final Element
            lineElement = ( Element ) lineNodeList.item ( lineIndex );
          
          final String [ ]  sep
            = getFirstChildValue ( lineElement ).split ( "[a-z,\t ]" );
          
          mUnitList.addAll (
            recorder.addLine (
              Integer.parseInt ( sep [ 0 ] ),
              Integer.parseInt ( sep [ 1 ] ),
              Integer.parseInt ( sep [ 2 ] ),
              Integer.parseInt ( sep [ 3 ] ),
              lineElement.getAttribute ( "pre" ),
              lineElement.getAttribute ( "suf" ),
              mNameList ) );
        }

        // square
        
        final NodeList  squareNodeList
          = intracellularElement.getElementsByTagName ( "Square" );
        
        final int  squareNodeListLength = squareNodeList.getLength ( );
        
        for ( int  squareIndex = 0; squareIndex < squareNodeListLength;
          squareIndex++ )
        {
          final Element  squareElement
            = ( Element ) squareNodeList.item ( squareIndex );
          
          final String [ ]  sep
            = getFirstChildValue ( squareElement ).split ( "[a-z,\t ]" );
          
          mUnitList.addAll (
            recorder.addSquare (
              Integer.parseInt ( sep [ 0 ] ),
              Integer.parseInt ( sep [ 1 ] ),
              Integer.parseInt ( sep [ 2 ] ),
              Integer.parseInt ( sep [ 3 ] ),
              squareElement.getAttribute ( "pre" ),
              squareElement.getAttribute ( "suf" ),
              mNameList ) );
        }

        // channel
        
        final NodeList  channelNodeList
          = intracellularElement.getElementsByTagName ( "Channel" );
        
        final int  channelNodeListLength = channelNodeList.getLength ( );
        
        for ( int  channelIndex = 0; channelIndex < channelNodeListLength;
          channelIndex++ )
        {
          final Element  channelElement
            = ( Element ) channelNodeList.item ( channelIndex );
          
          chaList.add (
            Integer.parseInt ( getFirstChildValue ( channelElement ) ) );
        }

        recorder.intraEle.add ( mUnitList );
        
        recorder.currChannel.add ( chaList );
        
        // rec.neuronNames.add(mName);
      }
      
      experiment = new Experiment ( subExps, recorder );
      
      // clear info
      
      experiment.recorder.simulatorParser = null;
      
      LOGGER.trace ( "parseExperiment() exiting" );      
    }

    /***********************************************************************
    * Map the cells and build up the index, for main host.
    * 
    * Called by class MainSimulator method run().
    ***********************************************************************/
    public void  parseMapCells ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final Element  overallConfigurationElement = getFirstElement (
        rootElement,
        "OverallConfiguration" );

      // layer size
      
      xEdgeLength = Integer.parseInt (
        getFirstElementFirstChildValue (
          overallConfigurationElement,
          "xEdgeLength" ) );
      
      yEdgeLength = Integer.parseInt (
        getFirstElementFirstChildValue (
          overallConfigurationElement,
          "yEdgeLength" ) );
      
      LOGGER.trace ( "xEdgeLength = {}", xEdgeLength );
      
      LOGGER.trace ( "yEdgeLength = {}", yEdgeLength );

      if ( overallConfigurationElement.getElementsByTagName (
        "DataFileName" ).getLength ( ) > 0 )
      {
        outFile = getFirstElementFirstChildValue (
          overallConfigurationElement,
          "DataFileName" );
      }
      
      LOGGER.trace ( "outFile = {}", outFile );

      parallelHost = Integer.parseInt (
        getFirstElementFirstChildValue (
          overallConfigurationElement,
          "TrialHost" ) );
      
      LOGGER.trace ( "parallelHost = {}", parallelHost );

      // Host info
      
      final Element  netHostElement = getFirstElement (
        overallConfigurationElement,
        "NetHost" );
      
      numOfHosts = Integer.parseInt (
        getFirstElementFirstChildValue (
          netHostElement,
          "Number" ) );
      
      LOGGER.trace ( "numOfHosts = {}", numOfHosts );
      
      final Element  mapMethodElement = getFirstElement (
        netHostElement,
        "MapMethod" );
      
      aSec = Integer.parseInt ( mapMethodElement.getAttribute ( "A" ) );
      
      LOGGER.trace ( "aSec = {}", aSec );
      
      bSec = Integer.parseInt ( mapMethodElement.getAttribute ( "B" ) );
      
      LOGGER.trace ( "bSec = {}", bSec );
      
      mapType = LayerStructure.ABMAP;

      layerStructure = new LayerStructure (
        modelFactory,
        numOfHosts,
        xEdgeLength,
        yEdgeLength );

      if ( aSec * bSec != numOfHosts )
      {
        throw new RuntimeException (
          aSec + "x" + bSec + "!=" + numOfHosts );
      }

      backgroundFrequency = Double.parseDouble (
        getFirstElementFirstChildValue (
          overallConfigurationElement,
          "BackgroundFrequency" ) );
      
      LOGGER.trace ( "backgroundFrequency = {}", backgroundFrequency );
      
      backgroundStrength = Double.parseDouble (
        getFirstElementFirstChildValue (
          overallConfigurationElement,
          "BackgroundStrength" ) );
      
      LOGGER.trace ( "backgroundStrength = {}", backgroundStrength );

      final Element  backgroundTypeElement = getFirstElement (
        overallConfigurationElement,
        "BackgroundType" );
      
// TODO:  Why is bChannel not used here?  For exception check only?      
      
      final int  bChannel = parseSynapseType ( backgroundTypeElement );
      
      LOGGER.trace ( "bChannel = {}", bChannel );
      
      /*
      bFreq = Double.parseDouble(
        (gloable.getElementsByTagName("BackgroundFiring").item(0))
        .getFirstChild().getNodeValue());
        
      bSynStr = Double.parseDouble(
        (gloable.getElementsByTagName("BackgroundSynapse").item(0))
        .getFirstChild().getNodeValue());
        
      bChannel = Integer.parseInt(
        ((Element)(gloable.getElementsByTagName("BackgroundSynapse")
        .item(0))).getAttributeNode("channel").getValue());
      */

      // read layers
      
      final Element
        layersElement = getFirstElement ( rootElement, "Layers" );
      
      final NodeList
        layerNodeList = layersElement.getElementsByTagName ( "Layer" );
      
      // add all the layers
      
      final int  layerNodeListLength = layerNodeList.getLength ( );
      
      for ( int  i = 0; i < layerNodeListLength; i++ )
      {
        final Element  layerElement = ( Element ) layerNodeList.item ( i );
        
        final String  prefix = getFirstElementFirstChildValue (
          layerElement,
          "Prefix" );
        
        LOGGER.trace ( "prefix = {}", prefix );
        
        final String  suffix = getFirstElementFirstChildValue (
          layerElement,
          "Suffix" );
        
        LOGGER.trace ( "suffix = {}", suffix );
        
        final int  multiX = Integer.parseInt (
          getFirstElementFirstChildValue (
            layerElement,
            "MultiX" ) );
        
        LOGGER.trace ( "multiX = {}", multiX );
        
        final int  multiY = Integer.parseInt (
          getFirstElementFirstChildValue (
            layerElement,
            "MultiY" ) );
        
        LOGGER.trace ( "multiY = {}", multiY );
        
        final String  neuronType = getFirstElementFirstChildValue (
          layerElement,
          "NeuronType" );
        
        LOGGER.trace ( "neuronType = {}", neuronType );
        
        layerStructure.addLayer (
          prefix,
          suffix,
          multiX,
          multiY,
          neuronType );
      }
      
      LOGGER.trace (
        "totalNumNeurons = {}",
        layerStructure.totalNumNeurons ( ) );
      
      switch ( mapType )
      {
        case LayerStructure.ABMAP:
          
          layerStructure.abmapcells_Main ( aSec, bSec );
          
          break;
          
        case LayerStructure.LINEMAP:
          
// TODO:  Is this case possible?          
          
          layerStructure.mapcells ( );
          
          break;
          
        default:
          
          break;
      }
    }
    
    public Collection<DiscreteEvent>  parseModulator (
      final Element  subExperimentElement )
    ////////////////////////////////////////////////////////////////////////
    {
      // LOGGER.trace ( "parseModulator() entered" );

      // LOGGER.trace (
      //   "total synapse count {}",
      //   modulatedSynapses.length );
      
      final NodeList  nodeList
        = subExperimentElement.getElementsByTagName ( "Modulator" );
      
      final int  length = nodeList.getLength ( );
      
      if ( length == 0 )
      {
        return null;
      }
      
      final Collection<DiscreteEvent>
        discreteEventCollection = new ArrayList<DiscreteEvent> ( );
  
      for ( int  i = 0; i < length; i++ )
      {
        final Element  modulatorElement = ( Element ) nodeList.item ( i );
        
        final String
          attributeTime
            = modulatorElement.getAttribute ( "time" ),
          attributeName
            = modulatorElement.getAttribute ( "name" ),
          attributePeakFactor
            = modulatorElement.getAttribute ( "peakFactor" );
        
        LOGGER.debug ( "attribute time = {}", attributeTime );        
        
        LOGGER.debug ( "attribute name = {}", attributeName );        
        
        LOGGER.debug ( "attribute peakFactor = {}", attributePeakFactor );
        
        final double  time = parseDouble (
          attributeTime,
          0,
          "error parsing time" );
        
        final String  name = attributeName.trim ( );
        
        final double  peakFactor = parseDouble (
          attributePeakFactor,
          1,
          "error parsing peakFactor" );
        
// TODO:  parse specific synapses instead of defaulting to global
        
        discreteEventCollection.add (
          modelFactory.createModulationDiscreteEvent (
            time,
            name,
            peakFactor,
            modulatedSynapseSeq ) );
      }
      
      // LOGGER.trace ( "parseModulator() exiting" );
      
      return discreteEventCollection;
    }

    /***********************************************************************
    * Parse the neuron definition, for main host. 
    ***********************************************************************/
    public void  parseNeuronDef ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final Element  neuronDefinitionElement = getFirstElement (
        rootElement,
        "NeuronDefinition" );
      
      final NodeList  neuronNodeList
        = neuronDefinitionElement.getElementsByTagName ( "Neuron" );
      
      final int  neuronNodeListLength = neuronNodeList.getLength ( );
      
      // add all the layers

      for ( int  i = 0; i < neuronNodeListLength; i++ )
      {
        final Element
          neuronElement = ( Element ) neuronNodeList.item ( i );
        
        final String  name = getFirstElementFirstChildValue (
          neuronElement,
          "Name" );
        
        final String  model = getFirstElementFirstChildValue (
          neuronElement,
          "Model" );
        
        if ( model.equals ( "SIFNeuron" ) )
        {
          modelParaMap.put (
            name,
            parseParametersSifNeuron ( neuronElement ) );
        }
        else if ( model.equals ( "VSICLIFNeuron" ) )
        {
          modelParaMap.put (
            name,
            parseParametersVsiclifNeuron ( neuronElement ) );
        }
        else if ( model.equals ( "VSICLIFNeuronV2" ) )
        {
          modelParaMap.put (
            name,
            parseParametersVsiclifNeuronV2 ( neuronElement ) );
        }
        else if ( model.equals( "MiNiNeuron" ) )
        {
          modelParaMap.put(
              name,
              parseParametersMiNiNeuron ( neuronElement ) );
        }
        else if ( model.equals ( "BKPoissonNeuron" ) )
        {
          modelParaMap.put (
            name,
            parseParametersBkPoissonNeuron ( neuronElement ) );
        }
        else if ( model.equals ( "UserSenNeuron" ) )
        {
          modelParaMap.put (
            name,
            parseParametersUserSenNeuron ( neuronElement ) );
        }
        else
        {
          throw new RuntimeException ( model + " not defined neuron type" );
        }
      }

      // Read predefined data
      
      final Element  constantDefinitionElement = getFirstElement (
        rootElement,
        "ConstantDefinition" ); 
      
      final NodeList  doubleNodeList
        = constantDefinitionElement.getElementsByTagName ( "Double" );
      
      // add all the layers
      
      final int  doubleNodeListLength = doubleNodeList.getLength ( );
      
      for ( int  i = 0; i < doubleNodeListLength; i++ )
      {
// TODO:  Why is ab added to each loop?        
        
        doubleDataMap.put ( "ab", new Double ( 1.0 ) );
        
        final Element
          doubleElement = ( Element ) doubleNodeList.item ( i );
      
        doubleDataMap.put (
          doubleElement.getAttribute ( "id" ),
          Double.parseDouble ( getFirstChildValue ( doubleElement ) ) );
      }
      
      final NodeList  matrixNodeList
        = constantDefinitionElement.getElementsByTagName ( "Matrix" );
      
      // add all the layers
      
      for ( int  i = 0; i < matrixNodeList.getLength ( ); i++)
      {
        final Element
          matrixElement = ( Element ) matrixNodeList.item ( i );
        
        matrixDataMap.put (
          matrixElement.getAttribute ( "id" ),
          parseMatrix ( matrixElement ) );
      }
    }

    /***********************************************************************
    * populate the neurons for net host 
    ***********************************************************************/
    public void  parsePopNeurons ( final int  hostId )
    ////////////////////////////////////////////////////////////////////////
    {
      layerStructure.setSeed(idum);
      int secX = hostId/bSec;
      int secY = hostId%bSec;
      //    System.out.println("SecX"+ hostId/bSec);
      //    System.out.println("SecY"+ hostId%bSec);

      //    System.out.println(ls.numberOfNeurons);
      /*
      for(int i = 0 ; i< ls.nodeEndIndices.length; i++)
      {
        System.out.print(ls.nodeEndIndices[i]+" ");
      }
      System.out.println();
      */

      // reserve one place for the sync neuron
      
      layerStructure.neurons = new Neuron [
        layerStructure.neuron_end - layerStructure.base + 1 + 1 ];
      
      //    ls.branchmaps = new TreeMap[ls.neuron_end-ls.base+1+1];
      //    ls.scaffold = new TreeMap[ls.neuron_end-ls.base+1+1];

      //      for(int a=0;a<ls.neuron_end-ls.base+1+1;a++) 
      //    {
      //      ls.branchmaps[a]=new TreeMap<String, TreeSet<Synapse>>(); 
      //      ls.scaffold[a]=new TreeMap<String, SizeID>(); 
      //    }

      NodeList nList = rootElement.getElementsByTagName("Layers");
      
      Element ele = (Element)nList.item(0);
      
      NodeList lay = ele.getElementsByTagName("Layer");

      int xTotal=0;           
      int yTotal=0;
      int xstep=0;
      int ystep=0;
      int xMul,yMul;
      int xOri,yOri;
      int xresosize,yresosize;
      String neu;

      for(int it= 0 ; it< lay.getLength(); it++) // add all the layers;
      {
        xMul = Integer.parseInt(((Element)(lay.item(it)))
          .getElementsByTagName("MultiX").item(0).getFirstChild()
            .getNodeValue());
        
        yMul = Integer.parseInt(
          ((Element)(lay.item(it))).getElementsByTagName("MultiY").item(0)
            .getFirstChild().getNodeValue());
        
        if(xMul>0)
        {
          xstep=1;
          
          if(secX!=0)
          {
            xOri=(int)((double)xMul*(double)xEdgeLength
              /(double)aSec*(double)(secX));
          }
          else
          {
            xOri=0;
          }
          
          if(secX!=aSec-1)
          {
            xresosize=(int)((double)xMul*(double)xEdgeLength
              /(double)aSec*(double)(secX+1));
          }
          else
          {
            xresosize=xMul*xEdgeLength;
          }
        }
        else
        {
          xstep=-xMul;
          
          if(secX!=0)
          {
            xOri = (int)((double)xEdgeLength/(double)aSec*(double)(secX));
            
            xOri = xOri + xstep - xOri % xstep;
          }
          else
          {
            xOri=0;
          }
          
          if(secX!=aSec-1)
          {
            xresosize=(int)((double)xEdgeLength
              /(double)aSec*(double)(secX+1));
            
            xresosize = xresosize + xstep - xresosize % xstep;
            
            if(xresosize>xEdgeLength)
              xresosize=xEdgeLength;
          }
          else
          {
            xresosize=xEdgeLength;
          }
        }
        
        if(yMul>0)
        {
          ystep=1;
          
          if(secY!=0)
          {
            yOri=(int)((double)yMul*(double)yEdgeLength
              /(double)bSec*(double)(secY));
          }
          else
          {
            yOri=0;
          }
          if(secY!=bSec-1)
          {
            yresosize=(int)((double)yMul*(double)yEdgeLength
              /(double)bSec*(double)(secY+1));
          }
          else
          {
            yresosize=yMul*yEdgeLength;
          }
        }
        else
        {
          ystep=-(yMul);
          
          if(secY!=0)
          {
            yOri=(int)((double)yEdgeLength/(double)bSec*(double)(secY));
            
            yOri=yOri + ystep - yOri % ystep;
          }
          else
          {
            yOri=0;
          }
          
          if(secY!=bSec-1)
          {
            yresosize=(int)((double)yEdgeLength
              /(double)bSec*(double)(secY+1));
            
            yresosize=yresosize + ystep -  yresosize % ystep;
            
            if(yresosize>yEdgeLength)yresosize=yEdgeLength;
          }
          else
          {
            yresosize=yEdgeLength;
          }
        }
        
        String n_pre = ((Element)(lay.item(it)))
          .getElementsByTagName("Prefix").item(0)
          .getFirstChild().getNodeValue();
        
        String n_suf = ((Element)(lay.item(it)))
          .getElementsByTagName("Suffix").item(0)
          .getFirstChild().getNodeValue();
        
        for(int x=xOri;x<xresosize;x+=xstep)
        {
          for(int y=yOri;y<yresosize;y+=ystep)
          {
            // neu=((Element)(lay.item(it))).getElementsByTagName("Prefix")
            //   .item(0).getFirstChild().getNodeValue()+","+x+","+y+","
            //   +((Element)(lay.item(it))).getElementsByTagName("Suffix")
            //   .item(0).getFirstChild().getNodeValue();
            
            Para neuPara = modelParaMap.get(
              ((Element)(lay.item(it))).getElementsByTagName("NeuronType")
              .item(0).getFirstChild().getNodeValue());

            if(neuPara != null) 
            {
              if( (neuPara.getModel()).equals("SIFNeuron"))
              {
                final SIFNeuronPara
                  sifNeuronPara = ( SIFNeuronPara ) neuPara;
                
                final SIFNeuron
                  sifNeuron = new SIFNeuron ( sifNeuronPara );
                
                layerStructure.neurons[layerStructure.cellmap_slow(
                  n_pre,n_suf,x,y)-layerStructure.base] = sifNeuron;
              }
              else if((neuPara.getModel()).equals("BKPoissonNeuron"))
              {

                layerStructure.neurons[layerStructure.cellmap_slow(
                  n_pre,n_suf,x,y)-layerStructure.base]
                    = new BKPoissonNeuron(
                      idum,
                      (BKPoissonNeuronPara)neuPara);
              } 
              else if((neuPara.getModel()).equals("VSICLIFNeuron"))
              {
                layerStructure.neurons[layerStructure.cellmap_slow(
                  n_pre,n_suf,x,y)-layerStructure.base]
                    = new VSICLIFNeuron((VSICLIFNeuronPara)neuPara);
              } 
              else if((neuPara.getModel()).equals("VSICLIFNeuronV2"))
              {
                layerStructure.neurons[layerStructure.cellmap_slow(
                  n_pre,n_suf,x,y)-layerStructure.base]
                    = new VSICLIFNeuronV2((VSICLIFNeuronParaV2)neuPara);
              } 
              else if ((neuPara.getModel()).equals("MiNiNeuron"))
              {
                layerStructure.neurons[layerStructure.cellmap_slow(
                  n_pre,n_suf,x,y)-layerStructure.base]
                    = new MiNiNeuron((MiNiNeuronPara)neuPara);
              }
              else if((neuPara.getModel()).equals("UserSenNeuron"))
              {
                layerStructure.neurons[
                  layerStructure.cellmap_slow(n_pre,n_suf,x,y)
                    -layerStructure.base]
                      = new UserSenNeuron(
                        (UserSenNeuronPara)neuPara, n_pre, n_suf,x,y);
              }
            }
            else
            {
              throw new RuntimeException(
                "Neuron type "
                  +((Element)(lay.item(it))).getElementsByTagName(
                    "NeuronType").item(0).getFirstChild().getNodeValue()
                  +" not defined");
            }
          }
        }
      }
    }

    /***********************************************************************
    * populate the neurons for main host, minimum memory useage
    ***********************************************************************/
    public void parsePopNeurons()
    ////////////////////////////////////////////////////////////////////////
    {
      //    System.out.println(ls.numberOfNeurons);
      //    for(int i = 0 ; i< ls.nodeEndIndices.length; i++)
      //    {
      //      System.out.print(ls.nodeEndIndices[i]+" ");
      //    }
      //    System.out.println();

      layerStructure.neurons = new Neuron[layerStructure.numberOfNeurons];
      
      //    ls.branchmaps = new TreeMap[ls.numberOfNeurons];
      //    for(int a=0;a<ls.numberOfNeurons;a++) ls.branchmaps[a]
      //      =new TreeMap<String, TreeSet<Synapse>>();

      NodeList nList = rootElement.getElementsByTagName("Layers");
      
      Element ele = (Element)nList.item(0);
      
      NodeList lay = ele.getElementsByTagName("Layer");

      int xTotal=0;
      
      int yTotal=0;
      
      int xstep=0;
      
      int ystep=0;
      
      int xMul, yMul;
      
      String neu;

      for(int it= 0 ; it< lay.getLength(); it++) // add all the layers;
      {
        xMul = Integer.parseInt(
          ((Element)(lay.item(it))).getElementsByTagName("MultiX")
          .item(0).getFirstChild().getNodeValue());
        
        yMul = Integer.parseInt(
          ((Element)(lay.item(it))).getElementsByTagName("MultiY")
          .item(0).getFirstChild().getNodeValue());
        
        if(xMul < 0 )
        {           
          xTotal=layerStructure.edgeLengthX;
          
          xstep=-xMul;
        }
        else
        {       
          xTotal=layerStructure.edgeLengthX*xMul;
          
          xstep=1;
        } 
        
        if(yMul < 0 )
        {       
          yTotal=layerStructure.edgeLengthY;
          
          ystep=-yMul;
        }
        else
        {       
          yTotal=layerStructure.edgeLengthY*yMul;
          
          ystep=1;
        }

        String n_pre = ((Element)(lay.item(it)))
          .getElementsByTagName("Prefix").item(0).getFirstChild()
            .getNodeValue();
        
        String n_suf = ((Element)(lay.item(it)))
          .getElementsByTagName("Suffix").item(0).getFirstChild()
            .getNodeValue(); 

        for(int x = 0 ; x < xTotal; x+=xstep)
        {       
          for(int y = 0 ; y < yTotal; y+=ystep)
          {       
            Para neuPara = modelParaMap.get(
              ((Element)(lay.item(it))).getElementsByTagName("NeuronType")
                .item(0).getFirstChild().getNodeValue());

            if(neuPara != null) 
            {
              if( (neuPara.getModel()).equals("SIFNeuron"))
              {
                layerStructure.neurons[layerStructure.cellmap_slow(
                  n_pre,n_suf,x,y)] = new SIFNeuron((SIFNeuronPara)neuPara);
              }
              else if((neuPara.getModel()).equals("BKPoissonNeuron"))
              {
                layerStructure.neurons[layerStructure.cellmap_slow(
                  n_pre,n_suf,x,y)] = new BKPoissonNeuron(
                    idum,
                    (BKPoissonNeuronPara)neuPara);

              } 
              else if((neuPara.getModel()).equals("VSICLIFNeuron"))
              {
                layerStructure.neurons[
                  layerStructure.cellmap_slow(n_pre,n_suf,x,y)]
                    = new VSICLIFNeuron((VSICLIFNeuronPara)neuPara);
              } 
              else if((neuPara.getModel()).equals("VSICLIFNeuronV2"))
              {
                layerStructure.neurons[layerStructure.cellmap_slow(
                  n_pre,n_suf,x,y)-layerStructure.base]
                    = new VSICLIFNeuronV2((VSICLIFNeuronParaV2)neuPara);
              } 
              else if((neuPara.getModel()).equals("MiNiNeuron"))
              {
                layerStructure.neurons[layerStructure.cellmap_slow(
                  n_pre,n_suf,x,y)-layerStructure.base]
                    = new MiNiNeuron((MiNiNeuronPara)neuPara);
              } 
              else if((neuPara.getModel()).equals("UserSenNeuron"))
              {
                layerStructure.neurons[layerStructure.cellmap_slow(
                  n_pre,n_suf,x,y)] = new UserSenNeuron(
                    (UserSenNeuronPara)neuPara, n_pre, n_suf,x,y);
              }
            }
            else
            {
              throw new RuntimeException(
                "Neuron type "
                  +((Element)(lay.item(it))).getElementsByTagName(
                    "NeuronType").item(0).getFirstChild().getNodeValue()
                      +" not defined");
            }
          }
        }
      }
    }

/*
    private static double[][] parseConnections(String filename)
    ////////////////////////////////////////////////////////////////////////
    {  
    double[][] conn;
    FileReader fr=null;
    ArrayList<ArrayList<Double>>
      connList=new ArrayList<ArrayList<Double>>();

    try
    {
      fr= new FileReader(filename);
    }
    catch(FileNotFoundException e)
    {
      System.err.println("Couldn't get File: " + filename);
      System.exit(0);
    }

    BufferedReader br=new BufferedReader(fr);

    try
    {
      String currentLine;//holds the current line of the file being read.
      
      // holds each connection weight separated by a space on the line we
      // are currently reading.
     
      String[] currentConnections;
      int currLine=0;
      boolean moreLines=true;

      try
      {
        while(moreLines)
        {
          currentLine=br.readLine();
          currentConnections=currentLine.split(" ");
          if(currentConnections[0].equals("END"))
          {
            moreLines=false;
          }
          else
          {
            connList.add(currLine,new ArrayList<Double>());
            for(int a=0;a<currentConnections.length;a++)
            {
              ((ArrayList) connList.get(currLine)).add(
                a,new Double(Double.parseDouble(currentConnections[a])));
            }
            currLine++;
          }
        }
      }
      catch(NumberFormatException e)
      {
        System.out.println("Number formatting problem in: " + filename);
        System.exit(0);
      }

      br.close();
      fr.close();
    }
    catch(IOException e)
    {
      System.err.println("Couldn't get IO for: " + filename);
      System.exit(0);
    }

    conn=new double[((ArrayList) connList.get(0)).size()][connList.size()];
    for(int y=0;y<connList.size();y++)
    {
      for(int x=0;x<((ArrayList) connList.get(0)).size();x++)
      {
        conn[x][y]
          =((Double) ((ArrayList) connList.get(y)).get(x)).doubleValue();
      }
    }

    return conn;
    }
    */

    /***********************************************************************
    * set the target host id.
    ***********************************************************************/
    public void parseTarget(JpvmInfo info) throws jpvmException
    ////////////////////////////////////////////////////////////////////////
    {
      TargetID [] targetID = new TargetID[info.numTasks];

      for(int i = 0 ; i < info.numTasks; i++)
      {
        if(i != info.idIndex)
        {
          targetID[i] = new TargetID(info.idIndex);
        }
      }

      Iterator<Integer> keyIter = layerStructure.axons.keySet().iterator();
      long self = (1L<<info.idIndex);
      while(keyIter.hasNext())
      {
        int target = keyIter.next();
        int hostDD = FunUtil.hostId(layerStructure.nodeEndIndices,target);
        if(hostDD != info.idIndex)
        {
          targetID[hostDD].target.add(target);
        }
        else if(hostDD == info.idIndex)
        {
          // if the neuron doesn't have a target synapse at node then set it
          
          if((layerStructure.neurons[target - layerStructure.base]
            .getTHost() & self )==0)
          {
            layerStructure.neurons[target - layerStructure.base]
              .setTHost(
                (layerStructure.neurons[target - layerStructure.base]
                  .getTHost() | self));
          }
        }
      }

      for(int i = 0 ; i < info.numTasks; i++)
      {
        if(i != info.idIndex)
        {
          jpvmBuffer buf2 = new jpvmBuffer();

          buf2.pack(targetID[i]); //pack the info

          info.jpvm.pvm_send(
            buf2,
            info.tids[i],
            NetMessageTag.assignTargets);
        }
      }
      
      //receive all the target info
      
      jpvmMessage m;
      
      for(int i = 0 ; i < info.numTasks-1; i++)
      {
        // receive info from others
        
        m = info.jpvm.pvm_recv(NetMessageTag.assignTargets);
        
        TargetID reTID =  (TargetID)m.buffer.upkcnsobj();
        
        long setID = (1L<<(reTID.target.get(0)));
        
        int neuronID;
        
        for(int ii=1; ii< reTID.target.size(); ii++)
        {
          neuronID = reTID.target.get(ii);
          
          // if the neuron doesn't have a target synapse at node then set it
          
          if((layerStructure.neurons[neuronID - layerStructure.base]
            .getTHost() & setID )==0)
          {
            layerStructure.neurons[neuronID - layerStructure.base].setTHost(
              (layerStructure.neurons[neuronID - layerStructure.base]
                .getTHost() | (setID)));
          }
        }
      }
    }
    
    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
    
    private static void  checkDataSetContainsAttributeValues (
      final Set<String>  dataSet,
      final Element      branchElement,
      final String       tagName,
      final String       attributeName,
      final String       errorMessageSuffix )      
    ////////////////////////////////////////////////////////////////////////
    {
      final NodeList
        nodeList = branchElement.getElementsByTagName ( tagName );

      final int  nodeListLength = nodeList.getLength ( );

      for ( int  i = 0; i < nodeListLength; i++ )
      {
        final Element  element = ( Element ) nodeList.item ( i );
        
        final Attr  attr = element.getAttributeNode ( attributeName );
        
        final String  value = attr.getNodeValue ( );
        
        if ( !dataSet.contains ( value ) )
        {
          throw new RuntimeException ( value + errorMessageSuffix );
        }
      }
    }
    
    private static void  checkDataSetContainsFirstChildValues (
      final Set<String>  dataSet,
      final Element      branchElement,
      final String       tagName,
      final String       errorMessageSuffix )      
    ////////////////////////////////////////////////////////////////////////
    {
      final NodeList
        nodeList = branchElement.getElementsByTagName ( tagName );

      final int  nodeListLength = nodeList.getLength ( );

      for ( int  i = 0; i < nodeListLength; i++ )
      {
        final Element  element = ( Element ) nodeList.item ( i );
        
        final String  value = element.getFirstChild ( ).getNodeValue ( );
        
        if ( !dataSet.contains ( value ) )
        {
          throw new RuntimeException ( value + errorMessageSuffix );
        }
      }
    }
    
    private static void  checkDataSetContainsFirstGrandchildValues (
      final Set<String>  dataSet,
      final Element      branchElement,
      final String       childTagName,
      final String       grandchildTagName,
      final String       errorMessageSuffix )
    ////////////////////////////////////////////////////////////////////////
    {
      final NodeList
        nodeList = branchElement.getElementsByTagName ( childTagName );
  
      final int  nodeListLength = nodeList.getLength ( );
  
      for ( int  i = 0; i < nodeListLength; i++ )
      {
        final Element  childElement = ( Element ) nodeList.item ( i );
    
        final NodeList  grandchildNodeList
          = childElement.getElementsByTagName ( grandchildTagName );
        
        if ( grandchildNodeList.getLength ( ) > 0 )
        {
          final String  value = grandchildNodeList.item ( 0 )
            .getFirstChild ( ).getNodeValue ( );
      
          if ( !dataSet.contains ( value ) )
          {
            throw new RuntimeException ( value + errorMessageSuffix );
          }
        }
      }
    }
    
    private final static int  checkMax (
      final int       max,
      final NodeList  nodeList )
    ////////////////////////////////////////////////////////////////////////
    {
      final int  nodeListLength = nodeList.getLength ( );
      
      if ( nodeListLength != 1 )
      {
        if ( max != 1
          && nodeListLength != max )
        {
          throw new RuntimeException ( "number does not match" );
        }
        
        return nodeListLength;
      }
      
      return max;
    }
    
    private final static int  checkMax (
      final NodeList...  nodeLists )
    ////////////////////////////////////////////////////////////////////////
    {
      int  max = 1;
      
      for ( final NodeList  nodeList : nodeLists )
      {
        max = checkMax ( max, nodeList );
      }
      
      return max;
    }
    
    private final double  computeStrength (
      final Element  strengthElement,
      final Element  multiplierElement,
      final Element  offsetElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final double
        strength
          = getRefDouble ( strengthElement ),
        multiplier
          = Double.parseDouble (
            multiplierElement.getFirstChild ( ).getNodeValue ( ) ),
        offset
          = Double.parseDouble (
            offsetElement.getFirstChild ( ).getNodeValue ( ) );
      
      return strength * multiplier + offset;
    }
    
    private static Set<String>  createDataSetFromAttributeValues (
      final Element  branchElement,
      final String   tagName,
      final String   attributeName )
    ////////////////////////////////////////////////////////////////////////
    {
      final Set<String>  dataSet = new HashSet<String> ( );
      
      final NodeList
        nodeList = branchElement.getElementsByTagName ( tagName );
      
      final int  nodeListLength = nodeList.getLength ( );
      
      for ( int  i = 0; i < nodeListLength; i++ )
      {
        final Element  element = ( Element ) nodeList.item ( i );
        
        final Attr  attr = element.getAttributeNode ( attributeName );
        
        final String  value = attr.getNodeValue ( );
        
        dataSet.add ( value );
      }      
      
      return dataSet;
    }
    
    private static Set<String>  createDataSetFromFirstChildValues (
      final Element  branchElement,
      final String   tagName )
    ////////////////////////////////////////////////////////////////////////
    {
      final Set<String>  dataSet = new HashSet<String> ( );
      
      final NodeList
        nodeList = branchElement.getElementsByTagName ( tagName );
      
      final int  nodeListLength = nodeList.getLength ( );
      
      for ( int  i = 0; i < nodeListLength; i++ )
      {
        final Element  element = ( Element ) nodeList.item ( i );
        
        final String  value = element.getFirstChild ( ).getNodeValue ( );
        
        dataSet.add ( value );
      }      
      
      return dataSet;
    }
    
    private static Element  getElementAtIndex (
      final NodeList  nodeList,
      final int       max,
      final int       index )
    ////////////////////////////////////////////////////////////////////////
    {
      final int  nodeListLength = nodeList.getLength ( );
      
      if ( nodeListLength == max )
      {
        return ( Element ) nodeList.item ( index );
      }
      
      return ( Element ) nodeList.item ( 0 );
    }
    
    private static String  getFirstChildValue ( final Element  element )
    ////////////////////////////////////////////////////////////////////////
    {
      return element.getFirstChild ( ).getNodeValue ( );      
    }
    
    private static Element  getFirstElement (
      final Element  parentElement,
      final String   tagName )
    ////////////////////////////////////////////////////////////////////////
    {
      final NodeList
        nodeList = parentElement.getElementsByTagName ( tagName );
      
      if ( nodeList.getLength ( ) == 0 )
      {
        return null;
      }        
      
      return ( Element ) nodeList.item ( 0 );
    }
    
    private static String  getFirstElementFirstChildValue (
      final Element  parentElement,
      final String   tagName )
    ////////////////////////////////////////////////////////////////////////
    {
      final Element
        firstElement = getFirstElement ( parentElement, tagName );
      
      if ( firstElement == null )
      {
        return null;
      }
      
      return getFirstChildValue ( firstElement );
    }
    
    
    private Double  getRefDouble ( final Element  element )
    ////////////////////////////////////////////////////////////////////////
    {
      final NodeList  refNodeList = element.getElementsByTagName ( "Ref" );
      
      if ( refNodeList.getLength ( ) > 0 )
      {
        final String
          key = refNodeList.item ( 0 ).getFirstChild ( ).getNodeValue ( );
          
        final Double  value = doubleDataMap.get ( key );
        
        if ( value == null )
        {
          throw new RuntimeException (
            key + " as a reference is not defined in Data Section" );
        }
        
        return value;
      }
      
      return Double.parseDouble (
        element.getFirstChild ( ).getNodeValue ( ) );
    }
    
    private final double [ ] [ ]
      getRefMatrix ( final Element  matrixElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final String  matrixKey
        = getFirstElementFirstChildValue ( matrixElement, "Ref" );
      
      if ( matrixKey != null )
      {      
        final double [ ] [ ]  matrix = matrixDataMap.get ( matrixKey );

        if ( matrix == null )
        {
          throw new RuntimeException ( matrixKey
            +" as a reference is not defined in Data Section" );
        }
        
        return matrix;
      }
      
      return parseMatrix ( matrixElement );
    }
    
    private static final double [ ]  parseDoubleArray (
      final String  value )
    ////////////////////////////////////////////////////////////////////////
    {      
      final String [ ]  sep = value.split ( "[,\t ]" );
      
      final double [ ]  doubleArray = new double [ sep.length ];
      
      for ( int i = 0; i < sep.length; i++ )
      {
        doubleArray [ i ] = Double.parseDouble ( sep [ i ] );
      }
      
      return doubleArray;      
    }
    
    private static double [ ] [ ]
      parseMatrix ( final Element  matrixElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final NodeList  matrixRowNodeList
        = matrixElement.getElementsByTagName ( "Row" );

      final String  firstRowValue = matrixRowNodeList
        .item ( 0 ).getFirstChild ( ).getNodeValue ( );

      final String [ ]
        firstSep = firstRowValue.split ( "[a-z,\t ]" );

      // System.out.println(sep.length);

      final int  matrixRowNodeListLength = matrixRowNodeList.getLength ( );

      final double [ ] [ ]  matrix
        = new double [ matrixRowNodeListLength ] [ firstSep.length ];

      for ( int  colId = 0; colId < firstSep.length; colId++ )
      {
        matrix [ 0 ] [ colId ]
          = Double.parseDouble ( firstSep [ colId ] );
      }

      for ( int  rowId = 1; rowId < matrixRowNodeListLength; rowId++ )
      {
        final String  rowValue = matrixRowNodeList
          .item ( rowId ).getFirstChild ( ).getNodeValue ( );

        final String [ ]  sep = rowValue.split ( "[a-z,\t ]" );

        for ( int  colId = 0; colId < sep.length; colId++ )
        {
          matrix [ rowId ] [ colId ]
            = Double.parseDouble ( sep [ colId ] );
        }
      }
      
      return matrix;
    }
    
    private static final BKPoissonNeuronPara
      parseParametersBkPoissonNeuron ( final Element  neuronElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final BKPoissonNeuronPara
        bkPoissonNeuronPara = new BKPoissonNeuronPara ( );
      
      final String
        frequencyValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "Frequency" ),
        saturationFrequencyValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "SaturationFrequency" ),
        inverseFrequencyDecayTimeValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "InverseFrequencyDecayTime" );
      
// TODO:  Are high and low frequency flipped?      
      
      if ( frequencyValue != null )
      {
        bkPoissonNeuronPara.HighFre
          = Double.parseDouble ( frequencyValue );
      }
      
      if ( saturationFrequencyValue != null )
      {
        bkPoissonNeuronPara.LowFre
          = Double.parseDouble ( saturationFrequencyValue );
      }
      
      if ( inverseFrequencyDecayTimeValue != null )
      {
        bkPoissonNeuronPara.timeCon
          = Double.parseDouble ( inverseFrequencyDecayTimeValue );
      }
      
      return bkPoissonNeuronPara;
    }
    
    private static final SIFNeuronPara  parseParametersSifNeuron (
      final Element  neuronElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final SIFNeuronPara  sifNeuronPara = new SIFNeuronPara ( );
      
      final String
        capacitanceValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "Capacitance" ),
        leakConductanceValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "LeakConductance" ),
        decayConstantExcitatoryValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "DecayConstantE" ),
        decayConstantInhibitoryValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "DecayConstantI" ),
        restingPotentialValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "RestingPotential" ),
        resetPotentialValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "ResetPotential" ),
        thresholdValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "Threshold" ),
        refractoryCurrentValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "RefractoryCurrent" ),
        refractoryPeriodValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "RefractoryPeriod" ),
        minRiseTimeValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "MinRiseTime" ),
        membraneVoltageValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "MembraneVoltage" ),
        varMembraneVoltageValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "VarMembraneVoltage" );
      
      if ( capacitanceValue != null )
      {
        sifNeuronPara.CAP
          = Double.parseDouble ( capacitanceValue );
      }
      
      if ( leakConductanceValue != null )
      {
        sifNeuronPara.GL
          = Double.parseDouble ( leakConductanceValue );
      }
      
      if ( decayConstantExcitatoryValue != null )
      {
        sifNeuronPara.DECAY_E
          = 1 / Double.parseDouble ( decayConstantExcitatoryValue );
      }
      
      if ( decayConstantInhibitoryValue != null )
      {
        sifNeuronPara.DECAY_I
          = 1 / Double.parseDouble ( decayConstantInhibitoryValue );
      }
      
      if ( resetPotentialValue != null )
      {
        sifNeuronPara.VRESET
          = Double.parseDouble ( resetPotentialValue );
      }
      
      if ( restingPotentialValue != null )
      {
        sifNeuronPara.VREST
          = Double.parseDouble ( restingPotentialValue );
      }
      
      if ( thresholdValue != null )
      {
        sifNeuronPara.THRESHOLD
          = Double.parseDouble ( thresholdValue );
      }
      
      if ( refractoryCurrentValue != null )
      {
        sifNeuronPara.REFCURR
          = Math.abs ( Double.parseDouble ( refractoryCurrentValue ) );
      }
      
      if ( refractoryPeriodValue != null )
      {
        sifNeuronPara.ABSREF
          = Double.parseDouble ( refractoryPeriodValue );
      }
      
      if ( minRiseTimeValue != null )
      {
        sifNeuronPara.MINRISETIME
          = Double.parseDouble ( minRiseTimeValue );
      }
      
      if ( membraneVoltageValue != null )
      {
        sifNeuronPara.ini_mem
          = Double.parseDouble ( membraneVoltageValue );
      }
      
      if ( varMembraneVoltageValue != null )
      {
        sifNeuronPara.ini_memVar
          = Double.parseDouble ( varMembraneVoltageValue );
      }
      
      return sifNeuronPara;
    }
    
    private static UserSenNeuronPara  parseParametersUserSenNeuron (
      final Element  neuronElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final UserSenNeuronPara
        userSenNeuronPara = new UserSenNeuronPara ( );
      
      final String  dataFileValue = getFirstElementFirstChildValue (
        neuronElement,
        "DataFile" );

      if ( dataFileValue != null )
      {
        userSenNeuronPara.datafile = dataFileValue;
      }
      
      return userSenNeuronPara;
    }

    private static VSICLIFNeuronPara  parseParametersVsiclifNeuron (
      final Element  neuronElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final VSICLIFNeuronPara
        vsiclifNeuronPara = new VSICLIFNeuronPara ( );
      
      final String
        capacitanceValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "Capacitance" ),
        leakConductanceValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "LeakConductance" ),
        restingPotentialValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "RestingPotential" ),
        resetPotentialValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "ResetPotential" ),
        asymptoticThresholdValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "AsymptoticThreshold" ),
        refractoryCurrentValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "RefractoryCurrent" ),
        refractoryPeriodValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "RefractoryPeriod" ),
        minRiseTimeValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "MinRiseTime" ),
        membraneVoltageValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "MembraneVoltage" ),
        varMembraneVoltageValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "VarMembraneVoltage" ),
        thresholdAdaptationValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "ThresholdAdaptation" ),
        thresholdReboundValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "ThresholdRebound" ),
        thresholdResetValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "ThresholdReset" ),
        thresholdAdditionValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "ThresholdAddition" ),
        externalCurrentValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "ExternalCurrent" ),
        initThresholdValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "InitThreshold" ),
        decaysValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "Decays" ),
        currentsAdditionValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "CurrentsAddition" ),
        currentsRatioValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "CurrentsRatio" ),
        initCurrentsValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "InitCurrents" );
      
      if ( capacitanceValue != null )
      {
        vsiclifNeuronPara.CAP
          = Double.parseDouble ( capacitanceValue );
      }
      
      if ( leakConductanceValue != null )
      {
        vsiclifNeuronPara.GL
          = Double.parseDouble ( leakConductanceValue );
      }
      
      if ( resetPotentialValue != null )
      {
        vsiclifNeuronPara.VRESET
          = Double.parseDouble ( resetPotentialValue );
      }
      
      if ( restingPotentialValue != null )
      {
        vsiclifNeuronPara.VREST
          = Double.parseDouble ( restingPotentialValue );
      }
      
      if ( asymptoticThresholdValue != null )
      {
        vsiclifNeuronPara.THRESHOLD
          = Double.parseDouble ( asymptoticThresholdValue );
      }
      
      if ( refractoryCurrentValue != null )
      {
        vsiclifNeuronPara.REFCURR
          = Math.abs ( Double.parseDouble ( refractoryCurrentValue ) );
      }
      
      if ( refractoryPeriodValue != null )
      {
        vsiclifNeuronPara.ABSREF
          = Double.parseDouble ( refractoryPeriodValue );
      }
      
      if ( minRiseTimeValue != null )
      {
        vsiclifNeuronPara.MINRISETIME
          = Double.parseDouble ( minRiseTimeValue );
      }
      
      if ( membraneVoltageValue != null )
      {
        vsiclifNeuronPara.ini_mem
          = Double.parseDouble ( membraneVoltageValue );
      }
      
      if ( varMembraneVoltageValue != null )
      {
        vsiclifNeuronPara.ini_memVar
          = Double.parseDouble ( varMembraneVoltageValue );
      }
      
      if ( thresholdAdaptationValue != null )
      {
        vsiclifNeuronPara.A
          = Double.parseDouble ( thresholdAdaptationValue );
      }
      
      if ( thresholdReboundValue != null )
      {
        vsiclifNeuronPara.B
          = Double.parseDouble ( thresholdReboundValue );
      }
      
      if ( thresholdResetValue != null )
      {
        vsiclifNeuronPara.RRESET
          = Double.parseDouble ( thresholdResetValue );
      }
      
      if ( thresholdAdditionValue != null )
      {
        vsiclifNeuronPara.THRESHOLDADD
          = Double.parseDouble ( thresholdAdditionValue );
      }
      
      if ( externalCurrentValue != null )
      {
        vsiclifNeuronPara.IEXT
          = Double.parseDouble ( externalCurrentValue );
      }
      
      if ( initThresholdValue != null )
      {
        vsiclifNeuronPara.ini_threshold
          = Double.parseDouble ( initThresholdValue );
      }
      
      if ( decaysValue != null )
      {
        vsiclifNeuronPara.DECAY
          = invertDoubleArray ( parseDoubleArray ( decaysValue ) );
      }
      
      if ( currentsAdditionValue != null )
      {
        vsiclifNeuronPara.IADD
          = parseDoubleArray ( currentsAdditionValue );
      }
      
      if ( currentsRatioValue != null )
      {
        vsiclifNeuronPara.IRATIO
          = parseDoubleArray ( currentsRatioValue );
      }
      
      if ( initCurrentsValue != null )
      {
        vsiclifNeuronPara.ini_curr
          = parseDoubleArray ( initCurrentsValue );
      }
      
      // calculate all the necessary things

      vsiclifNeuronPara.calConstants ( );
      
      return vsiclifNeuronPara;
    }
    
    private static MiNiNeuronPara  parseParametersMiNiNeuron (
      final Element  neuronElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final MiNiNeuronPara
        miNiNeuronPara = new MiNiNeuronPara ( );
      
      final String
        capacitanceValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "Capacitance" ),
        leakConductanceValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "LeakConductance" ),
        restingPotentialValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "RestingPotential" ),
        resetPotentialValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "ResetPotential" ),
        asymptoticThresholdValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "AsymptoticThreshold" ),
        refractoryCurrentValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "RefractoryCurrent" ),
        refractoryPeriodValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "RefractoryPeriod" ),
        membraneVoltageValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "MembraneVoltage" ),
        varMembraneVoltageValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "VarMembraneVoltage" ),
        thresholdAdaptationValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "ThresholdAdaptation" ),
        thresholdReboundValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "ThresholdRebound" ),
        thresholdResetValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "ThresholdReset" ),
        thresholdAdditionValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "ThresholdAddition" ),
        externalCurrentValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "ExternalCurrent" ),
        initThresholdValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "InitThreshold" ),
        kLtdValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "K_LTD" ),
        kLtpValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "K_LTP" ),
        alphaLtpValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "Alpha_LTP" ),
        alphaLtdValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "Alpha_LTD" ),
        spikeDecaysValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "SpikeDecays" ),
        synapseDecaysValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "SynapseDecays" ),
        spikeCurrentsAdditionValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "SpikeCurrentsAddition" ),
        spikeCurrentsRatioValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "SpikeCurrentsRatio" ),
        initSpikeCurrentsValue
          = getFirstElementFirstChildValue (
            neuronElement,
            "InitSpikeCurrents" );
      
      if ( capacitanceValue != null )
      {
        miNiNeuronPara.CAP
          = Double.parseDouble ( capacitanceValue );
      }
      
      if ( leakConductanceValue != null )
      {
        miNiNeuronPara.GL
          = Double.parseDouble ( leakConductanceValue );
      }
      
      if ( resetPotentialValue != null )
      {
        miNiNeuronPara.VRESET
          = Double.parseDouble ( resetPotentialValue );
      }
      
      if ( restingPotentialValue != null )
      {
        miNiNeuronPara.VREST
          = Double.parseDouble ( restingPotentialValue );
      }
      
      if ( asymptoticThresholdValue != null )
      {
        miNiNeuronPara.THRESHOLD
          = Double.parseDouble ( asymptoticThresholdValue );
      }
      
      if ( refractoryCurrentValue != null )
      {
        miNiNeuronPara.REFCURR
          = Math.abs ( Double.parseDouble ( refractoryCurrentValue ) );
      }
      
      if ( refractoryPeriodValue != null )
      {
        miNiNeuronPara.ABSREF
          = Double.parseDouble ( refractoryPeriodValue );
      }
          
      if ( membraneVoltageValue != null )
      {
        miNiNeuronPara.ini_mem
          = Double.parseDouble ( membraneVoltageValue );
      }
      
      if ( varMembraneVoltageValue != null )
      {
        miNiNeuronPara.ini_memVar
          = Double.parseDouble ( varMembraneVoltageValue );
      }
      
      if ( thresholdAdaptationValue != null )
      {
        miNiNeuronPara.A
          = Double.parseDouble ( thresholdAdaptationValue );
      }
      
      if ( thresholdReboundValue != null )
      {
        miNiNeuronPara.B
          = Double.parseDouble ( thresholdReboundValue );
      }
      
      if ( thresholdResetValue != null )
      {
        miNiNeuronPara.RRESET
          = Double.parseDouble ( thresholdResetValue );
      }
      
      if ( thresholdAdditionValue != null )
      {
        miNiNeuronPara.THRESHOLDADD
          = Double.parseDouble ( thresholdAdditionValue );
      }
      
      if ( externalCurrentValue != null )
      {
        miNiNeuronPara.IEXT
          = Double.parseDouble ( externalCurrentValue );
      }
      
      if ( initThresholdValue != null )
      {
        miNiNeuronPara.ini_threshold
          = Double.parseDouble ( initThresholdValue );
      }
      
      // begin spike-timing depependent plasticity (STDP) parameters
      
      if ( kLtdValue != null )
      {
        miNiNeuronPara.K_LTD
          = Double.parseDouble ( kLtdValue );
      }
      
      if ( kLtpValue != null )
      {
        miNiNeuronPara.K_LTP
          = Double.parseDouble ( kLtpValue );
      }
      
      if ( alphaLtpValue != null )
      {
        miNiNeuronPara.Alpha_LTP
          = Double.parseDouble ( alphaLtpValue );
      }
      
      if ( alphaLtdValue != null )
      {
        miNiNeuronPara.Alpha_LTD
          = Double.parseDouble ( alphaLtdValue );
      }
      
      // end STDP parameters
      
      if ( spikeDecaysValue != null )
      {
        miNiNeuronPara.DECAY_SPIKE
          = invertDoubleArray ( parseDoubleArray ( spikeDecaysValue ) );
      }
      
      if ( synapseDecaysValue != null )
      {
        miNiNeuronPara.DECAY_SYNAPSE
          = invertDoubleArray ( parseDoubleArray ( synapseDecaysValue ) );
      }
      
      if ( spikeCurrentsAdditionValue != null )
      {
        miNiNeuronPara.SPIKE_ADD
          = parseDoubleArray ( spikeCurrentsAdditionValue );
      }
      
      if ( spikeCurrentsRatioValue != null )
      {
        miNiNeuronPara.SPIKE_RATIO
          = parseDoubleArray ( spikeCurrentsRatioValue );
      }
      
      if ( initSpikeCurrentsValue != null )
      {
        miNiNeuronPara.ini_spike_curr
          = parseDoubleArray ( initSpikeCurrentsValue );
      }
      
      // calculate all the necessary things

      miNiNeuronPara.calConstants ( );
    
      return miNiNeuronPara;
    }
    
    private static VSICLIFNeuronParaV2  parseParametersVsiclifNeuronV2 (
        final Element  neuronElement )
      ////////////////////////////////////////////////////////////////////////
      {
        final VSICLIFNeuronParaV2
          vsiclifNeuronParaV2 = new VSICLIFNeuronParaV2 ( );
        
        final String
          capacitanceValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "Capacitance" ),
          leakConductanceValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "LeakConductance" ),
          restingPotentialValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "RestingPotential" ),
          resetPotentialValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "ResetPotential" ),
          asymptoticThresholdValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "AsymptoticThreshold" ),
          refractoryCurrentValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "RefractoryCurrent" ),
          refractoryPeriodValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "RefractoryPeriod" ),
          minRiseTimeValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "MinRiseTime" ),
          membraneVoltageValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "MembraneVoltage" ),
          varMembraneVoltageValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "VarMembraneVoltage" ),
          thresholdAdaptationValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "ThresholdAdaptation" ),
          thresholdReboundValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "ThresholdRebound" ),
          thresholdResetValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "ThresholdReset" ),
          thresholdAdditionValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "ThresholdAddition" ),
          externalCurrentValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "ExternalCurrent" ),
          initThresholdValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "InitThreshold" ),
          kLtdValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "K_LTD" ),
          kLtpValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "K_LTP" ),
          alphaLtpValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "Alpha_LTP" ),
          alphaLtdValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "Alpha_LTD" ),
          decaysValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "Decays" ),
          currentsAdditionValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "CurrentsAddition" ),
          currentsRatioValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "CurrentsRatio" ),
          initCurrentsValue
            = getFirstElementFirstChildValue (
              neuronElement,
              "InitCurrents" );
        
        if ( capacitanceValue != null )
        {
          vsiclifNeuronParaV2.CAP
            = Double.parseDouble ( capacitanceValue );
        }
        
        if ( leakConductanceValue != null )
        {
          vsiclifNeuronParaV2.GL
            = Double.parseDouble ( leakConductanceValue );
        }
        
        if ( resetPotentialValue != null )
        {
          vsiclifNeuronParaV2.VRESET
            = Double.parseDouble ( resetPotentialValue );
        }
        
        if ( restingPotentialValue != null )
        {
          vsiclifNeuronParaV2.VREST
            = Double.parseDouble ( restingPotentialValue );
        }
        
        if ( asymptoticThresholdValue != null )
        {
          vsiclifNeuronParaV2.THRESHOLD
            = Double.parseDouble ( asymptoticThresholdValue );
        }
        
        if ( refractoryCurrentValue != null )
        {
          vsiclifNeuronParaV2.REFCURR
            = Math.abs ( Double.parseDouble ( refractoryCurrentValue ) );
        }
        
        if ( refractoryPeriodValue != null )
        {
          vsiclifNeuronParaV2.ABSREF
            = Double.parseDouble ( refractoryPeriodValue );
        }
        
        if ( minRiseTimeValue != null )
        {
          vsiclifNeuronParaV2.MINRISETIME
            = Double.parseDouble ( minRiseTimeValue );
        }
        
        if ( membraneVoltageValue != null )
        {
          vsiclifNeuronParaV2.ini_mem
            = Double.parseDouble ( membraneVoltageValue );
        }
        
        if ( varMembraneVoltageValue != null )
        {
          vsiclifNeuronParaV2.ini_memVar
            = Double.parseDouble ( varMembraneVoltageValue );
        }
        
        if ( thresholdAdaptationValue != null )
        {
          vsiclifNeuronParaV2.A
            = Double.parseDouble ( thresholdAdaptationValue );
        }
        
        if ( thresholdReboundValue != null )
        {
          vsiclifNeuronParaV2.B
            = Double.parseDouble ( thresholdReboundValue );
        }
        
        if ( thresholdResetValue != null )
        {
          vsiclifNeuronParaV2.RRESET
            = Double.parseDouble ( thresholdResetValue );
        }
        
        if ( thresholdAdditionValue != null )
        {
          vsiclifNeuronParaV2.THRESHOLDADD
            = Double.parseDouble ( thresholdAdditionValue );
        }
        
        if ( externalCurrentValue != null )
        {
          vsiclifNeuronParaV2.IEXT
            = Double.parseDouble ( externalCurrentValue );
        }
        
        if ( initThresholdValue != null )
        {
          vsiclifNeuronParaV2.ini_threshold
            = Double.parseDouble ( initThresholdValue );
        }
        
        // begin spike-timing depependent plasticity (STDP) parameters
        
        if ( kLtdValue != null )
        {
          vsiclifNeuronParaV2.K_LTD
            = Double.parseDouble ( kLtdValue );
        }
        
        if ( kLtpValue != null )
        {
          vsiclifNeuronParaV2.K_LTP
            = Double.parseDouble ( kLtpValue );
        }
        
        if ( alphaLtpValue != null )
        {
          vsiclifNeuronParaV2.Alpha_LTP
            = Double.parseDouble ( alphaLtpValue );
        }
        
        if ( alphaLtdValue != null )
        {
          vsiclifNeuronParaV2.Alpha_LTD
            = Double.parseDouble ( alphaLtdValue );
        }
        
        // end STDP parameters
        
        if ( decaysValue != null )
        {
          vsiclifNeuronParaV2.DECAY
            = invertDoubleArray ( parseDoubleArray ( decaysValue ) );
        }
        
        if ( currentsAdditionValue != null )
        {
          vsiclifNeuronParaV2.IADD
            = parseDoubleArray ( currentsAdditionValue );
        }
        
        if ( currentsRatioValue != null )
        {
          vsiclifNeuronParaV2.IRATIO
            = parseDoubleArray ( currentsRatioValue );
        }
        
        if ( initCurrentsValue != null )
        {
          vsiclifNeuronParaV2.ini_curr
            = parseDoubleArray ( initCurrentsValue );
        }
        
        // calculate all the necessary things

        vsiclifNeuronParaV2.calConstants ( );
      
        return vsiclifNeuronParaV2;
      }
    
    private static boolean  parsePeriodic (
      final Element  connectionElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final String  value = connectionElement.getAttribute ( "periodic" );

      return value.equals ( "true" )
          || value.equals ( "TRUE" );
    }
    
// TODO:  Merge the parseScaffold methods into one if possible
    
    private void  parseScaffold (
      final int      hostId,
      final Element  connectionElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final NodeList
        fromLayerNodeList
          = connectionElement.getElementsByTagName ( "FromLayer" ),
        toLayerNodeList
          = connectionElement.getElementsByTagName ( "ToLayer" ),
        strengthNodeList
          = connectionElement.getElementsByTagName ( "Strength" ),
        multiplierNodeList
          = connectionElement.getElementsByTagName ( "Multiplier" ),
        offsetNodeList
          = connectionElement.getElementsByTagName ( "Offset" ),
        typeNodeList
          = connectionElement.getElementsByTagName ( "Type" ),
        delayNodeList
          = connectionElement.getElementsByTagName ( "Delay" ),          
        matrixNodeList
          = connectionElement.getElementsByTagName ( "Matrix" ),
        rotationNodeList
          = connectionElement.getElementsByTagName ( "Rotation" );
      
      final int  max = checkMax (
        fromLayerNodeList,
        toLayerNodeList,
        strengthNodeList,
        multiplierNodeList,
        offsetNodeList,
        typeNodeList,
        delayNodeList,
        matrixNodeList,
        rotationNodeList );
      
      for ( int  i = 0; i < max; i++ )
      {
        final Element
          fromLayerElement
            = getElementAtIndex ( fromLayerNodeList,  max, i ),
          toLayerElement
            = getElementAtIndex ( toLayerNodeList,    max, i ),
          strengthElement
            = getElementAtIndex ( strengthNodeList,   max, i ),
          multiplierElement
            = getElementAtIndex ( multiplierNodeList, max, i ),
          offsetElement
            = getElementAtIndex ( offsetNodeList,     max, i ),
          typeElement
            = getElementAtIndex ( typeNodeList,       max, i ),
          delayElement
            = getElementAtIndex ( delayNodeList,      max, i ),
          matrixElement
            = getElementAtIndex ( matrixNodeList,     max, i ),
          rotationElement
            = getElementAtIndex ( rotationNodeList,   max, i );
            
        
        // no periodic, no vergence, no strength matrix arguments

        layerStructure.popScaffold (
          fromLayerElement.getAttribute ( "prefix" ),
          fromLayerElement.getAttribute ( "suffix" ),
          toLayerElement  .getAttribute ( "prefix" ),
          toLayerElement  .getAttribute ( "suffix" ),
          computeStrength (
            strengthElement,
            multiplierElement,
            offsetElement ),
          parseSynapseType ( typeElement ),
          getRefDouble ( delayElement ),
          rotateMatrix (
            getRefMatrix ( matrixElement ),
            rotationElement ),
          hostId );
      }
    }
    
    private void  parseScaffoldWithPeriodicAndStrengthMatrix (
      final int      hostId,
      final Element  connectionElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final NodeList
        fromLayerNodeList
          = connectionElement.getElementsByTagName ( "FromLayer" ),
        toLayerNodeList
          = connectionElement.getElementsByTagName ( "ToLayer" ),          
        styleNodeList
          = connectionElement.getElementsByTagName ( "Style" ),
        strengthNodeList
          = connectionElement.getElementsByTagName ( "Strength" ),
        multiplierNodeList
          = connectionElement.getElementsByTagName ( "Multiplier" ),
        offsetNodeList
          = connectionElement.getElementsByTagName ( "Offset" ),
        typeNodeList
          = connectionElement.getElementsByTagName ( "Type" ),
        delayNodeList
          = connectionElement.getElementsByTagName ( "Delay" ),
        pMatrixNodeList
          = connectionElement.getElementsByTagName ( "PMatrix" ),
        sMatrixNodeList
          = connectionElement.getElementsByTagName ( "SMatrix" ),
        rotationNodeList
          = connectionElement.getElementsByTagName ( "Rotation" );
      
      int  max = checkMax (
        fromLayerNodeList,
        toLayerNodeList,
        styleNodeList,
        strengthNodeList,
        multiplierNodeList,
        offsetNodeList,
        typeNodeList,
        delayNodeList,        
        pMatrixNodeList,
        rotationNodeList );
      
      // strength matrix exist or not

      boolean  str_matrix = false;

      if ( sMatrixNodeList.getLength ( ) > 0 )
      {
        max = checkMax ( max, sMatrixNodeList );

        str_matrix = true;
      }

      final boolean  periodic = parsePeriodic ( connectionElement );

      for ( int  i = 0; i < max; i++ )
      {
        final Element
          fromLayerElement
            = getElementAtIndex ( fromLayerNodeList,  max, i ),
          toLayerElement
            = getElementAtIndex ( toLayerNodeList,    max, i ),
          styleElement
            = getElementAtIndex ( styleNodeList,      max, i ),
          strengthElement
            = getElementAtIndex ( strengthNodeList,   max, i ),
          multiplierElement
            = getElementAtIndex ( multiplierNodeList, max, i ),
          offsetElement
            = getElementAtIndex ( offsetNodeList,     max, i ),
          typeElement
            = getElementAtIndex ( typeNodeList,       max, i ),
          delayElement
            = getElementAtIndex ( delayNodeList,      max, i ),
          pMatrixElement
            = getElementAtIndex ( pMatrixNodeList,    max, i ),
          rotationElement
            = getElementAtIndex ( rotationNodeList,   max, i );
      
        double [ ] [ ]  sconMatrix = null;

        // if strength matrix exists
        
        if ( str_matrix )
        {
          final Element
            eSmatrix = getElementAtIndex ( sMatrixNodeList, max, i );

          sconMatrix = rotateMatrix (
            getRefMatrix ( eSmatrix ),
            rotationElement );
        }
        
        // accepts periodic and strength matrix

        layerStructure.popScaffold (
          fromLayerElement.getAttribute ( "prefix" ),
          fromLayerElement.getAttribute ( "suffix" ),
          toLayerElement  .getAttribute ( "prefix" ),
          toLayerElement  .getAttribute ( "suffix" ),
          parseVergenceStyle ( styleElement ),
          computeStrength (
            strengthElement,
            multiplierElement,
            offsetElement ),
          parseSynapseType ( typeElement ),
          getRefDouble ( delayElement ),
          rotateMatrix (
            getRefMatrix ( pMatrixElement ),
            rotationElement ),
          sconMatrix,
          hostId,
          periodic );
      }
    }
    
    private void  parseScaffoldWithPeriodic (
      final int      hostId,
      final Element  connectionElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final NodeList
        fromLayerNodeList
          = connectionElement.getElementsByTagName ( "FromLayer" ),
        toLayerNodeList
          = connectionElement.getElementsByTagName ( "ToLayer" ),
        styleNodeList
          = connectionElement.getElementsByTagName ( "Style" ),
        strengthNodeList
          = connectionElement.getElementsByTagName ( "Strength" ),
        multiplierNodeList
          = connectionElement.getElementsByTagName ( "Multiplier" ),
        offsetNodeList
          = connectionElement.getElementsByTagName ( "Offset" ),
        typeNodeList
          = connectionElement.getElementsByTagName ( "Type" ),
        delayNodeList
          = connectionElement.getElementsByTagName ( "Delay" ),
        matrixNodeList
          = connectionElement.getElementsByTagName ( "Matrix" ),
        rotationNodeList
          = connectionElement.getElementsByTagName ( "Rotation" );
      
      final int  max = checkMax (
        fromLayerNodeList,
        toLayerNodeList,
        styleNodeList,
        strengthNodeList,
        multiplierNodeList,
        offsetNodeList,
        typeNodeList,
        delayNodeList,
        matrixNodeList,
        rotationNodeList );
      
      final boolean  periodic = parsePeriodic ( connectionElement );
      
      for ( int  i = 0; i < max; i++ )
      {
        final Element
          fromLayerElement
            = getElementAtIndex ( fromLayerNodeList,  max, i ),
          toLayerElement
            = getElementAtIndex ( toLayerNodeList,    max, i ),
          styleElement
            = getElementAtIndex ( styleNodeList,      max, i ),
          strengthElement
            = getElementAtIndex ( strengthNodeList,   max, i ),
          multiplierElement
            = getElementAtIndex ( multiplierNodeList, max, i ),
          offsetElement
            = getElementAtIndex ( offsetNodeList,     max, i ),
          typeElement
            = getElementAtIndex ( typeNodeList,       max, i ),
          delayElement
            = getElementAtIndex ( delayNodeList,      max, i ),
          matrixElement
            = getElementAtIndex ( matrixNodeList,     max, i ),
          rotationElement
            = getElementAtIndex ( rotationNodeList,   max, i );
        
        // accepts periodic argument
        
        layerStructure.popScaffold (
          fromLayerElement.getAttribute ( "prefix" ),
          fromLayerElement.getAttribute ( "suffix" ),
          toLayerElement  .getAttribute ( "prefix" ),
          toLayerElement  .getAttribute ( "suffix" ),
          parseVergenceStyle ( styleElement ),
          computeStrength (
            strengthElement,
            multiplierElement,
            offsetElement ),
          parseSynapseType ( typeElement ),
          getRefDouble ( delayElement ),
          rotateMatrix (
            getRefMatrix ( matrixElement ),
            rotationElement ),
          hostId,
          periodic );
      }
    }
    
    private void  parseScaffoldWithStrengthMatrix (
      int      hostId,
      Element  connectionElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final NodeList
        fromLayerNodeList
          = connectionElement.getElementsByTagName ( "FromLayer" ),
        toLayerNodeList
          = connectionElement.getElementsByTagName ( "ToLayer" ),
        styleNodeList
          = connectionElement.getElementsByTagName ( "Style" ),
        strengthNodeList
          = connectionElement.getElementsByTagName ( "Strength" ),
        multiplierNodeList
          = connectionElement.getElementsByTagName ( "Multiplier" ),
        offsetNodeList
          = connectionElement.getElementsByTagName ( "Offset" ),
        typeNodeList
          = connectionElement.getElementsByTagName ( "Type" ),
        delayNodeList
          = connectionElement.getElementsByTagName ( "Delay" ),
        pMatrixNodeList
          = connectionElement.getElementsByTagName ( "PMatrix" ),
        sMatrixNodeList
          = connectionElement.getElementsByTagName ( "SMatrix" ),
        rotationNodeList
          = connectionElement.getElementsByTagName ( "Rotation" );

      int  max = checkMax (
        fromLayerNodeList,
        toLayerNodeList,
        styleNodeList,
        strengthNodeList,
        multiplierNodeList,
        offsetNodeList,
        typeNodeList,
        delayNodeList,
        pMatrixNodeList,
        rotationNodeList );
      
      // strength matrix exist or not

      boolean  str_matrix = false;

      if ( sMatrixNodeList.getLength ( ) > 0 )
      {
        max = checkMax ( max, sMatrixNodeList );

        str_matrix = true;
      }

      // System.out.println("max"+max);

      for ( int  i = 0; i < max; i++ )
      {
        final Element
          fromElement
            = getElementAtIndex ( fromLayerNodeList,  max, i ),
          toElement
            = getElementAtIndex ( toLayerNodeList,    max, i ),
          strengthElement
            = getElementAtIndex ( strengthNodeList,   max, i ),
          multiplierElement
            = getElementAtIndex ( multiplierNodeList, max, i ),
          offsetElement
            = getElementAtIndex ( offsetNodeList,     max, i ),
          typeElement
            = getElementAtIndex ( typeNodeList,       max, i ),
          delayElement
            = getElementAtIndex ( delayNodeList,      max, i ),
          pMatrixElement
            = getElementAtIndex ( pMatrixNodeList,    max, i ),
          rotationElement
            = getElementAtIndex ( rotationNodeList,   max, i );
    
        double [ ] [ ]  sConMatrix = null;

        if ( str_matrix ) // if strength matrix exists
        {
          final Element
            eSmatrix = getElementAtIndex ( strengthNodeList, max, i );

          sConMatrix = rotateMatrix (
            getRefMatrix ( eSmatrix ),
            rotationElement );
        }
        
        // accepts strength matrix but no vergence style argument

        layerStructure.popScaffold (
          fromElement.getAttribute ( "prefix" ),
          fromElement.getAttribute ( "suffix" ),
          toElement  .getAttribute ( "prefix" ),
          toElement  .getAttribute ( "suffix" ),
          computeStrength (
            strengthElement,
            multiplierElement,
            offsetElement ),
          parseSynapseType ( typeElement ),
          getRefDouble ( delayElement ),
          rotateMatrix (
            getRefMatrix ( pMatrixElement ),
            rotationElement ),
          sConMatrix,
          hostId );
      }
    }
    
    /***********************************************************************
    * This one differs from the others in that it assumes there is just one
    * of each element and that the prefixes and suffixes are taken from
    * node values instead of attributes. 
    ***********************************************************************/    
    private void  parseScaffoldWithPeriodicAndSingleElement (
      final int      hostId,
      final Element  connectionElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final Element
        fromLayerElement
          = getFirstElement ( connectionElement, "FromLayer" ),
        toLayerElement
          = getFirstElement ( connectionElement, "ToLayer" ),
        styleElement
          = getFirstElement ( connectionElement, "Style" ),
        strengthElement
          = getFirstElement ( connectionElement, "Strength" ),
        multiplierElement
          = getFirstElement ( connectionElement, "Multiplier" ),
        offsetElement
          = getFirstElement ( connectionElement, "Offset" ),
        typeElement
          = getFirstElement ( connectionElement, "Type" ),
        delayElement
          = getFirstElement ( connectionElement, "Delay" ),
        matrixElement
          = getFirstElement ( connectionElement, "Matrix" ),
        rotationElement
          = getFirstElement ( connectionElement, "Rotation" ),
        fromLayerPrefixElement
          = getFirstElement ( fromLayerElement, "Prefix" ),
        fromLayerSuffixElement
          = getFirstElement ( fromLayerElement, "Suffix" ),
        toLayerPrefixElement
          = getFirstElement ( toLayerElement,   "Prefix" ),
        toLayerSuffixElement
          = getFirstElement ( toLayerElement,   "Suffix" );
      
      // accepts periodic argument

      layerStructure.popScaffold (
        getFirstChildValue ( fromLayerPrefixElement ),
        getFirstChildValue ( fromLayerSuffixElement ),
        getFirstChildValue ( toLayerPrefixElement ),
        getFirstChildValue ( toLayerSuffixElement ),
        parseVergenceStyle ( styleElement ),
        computeStrength (
          strengthElement,
          multiplierElement,
          offsetElement ),
        parseSynapseType ( typeElement ),
        Double.parseDouble ( getFirstChildValue ( delayElement ) ),
        rotateMatrix (
          parseMatrix ( matrixElement ),
          rotationElement ),
        hostId,
        parsePeriodic ( connectionElement ) );
    }
    
    private static int  parseSynapseType ( final Element  typeElement )
    ////////////////////////////////////////////////////////////////////////
    {
      final String
        typeValue = typeElement.getFirstChild ( ).getNodeValue ( );

      if ( typeValue.equals ( "Glutamate" ) )
      {
        return 0;
      }
      
      if ( typeValue.equals ( "GABA" ) )
      {
        return 1;
      }
      
      throw new RuntimeException ( "only Glutamate and GABA are allowed" );
    }
    
    private static boolean  parseVergenceStyle (
      final Element  styleElement )
    ////////////////////////////////////////////////////////////////////////
    {    
      final String
        styleValue = styleElement.getFirstChild ( ).getNodeValue ( );
    
      if ( styleValue.equals ( "Convergent" ) )
      {
        return true;
      }
      
      if ( styleValue.equals ( "Divergent" ) )
      {
        return false;
      }

      throw new RuntimeException (
        "only convergent and divergent are allowed" );
    }
    
    private static double [ ] [ ]  rotateMatrix (
      final double [ ] [ ]  matrix,
      final Element         rotationElement )
    ////////////////////////////////////////////////////////////////////////
    {
      double [ ] [ ]  rotatedMatrix = matrix;
      
      final int  rotation = Integer.parseInt (
        rotationElement.getFirstChild ( ).getNodeValue ( ) ) / 90;

      if ( rotation > 0 )
      {
        for ( int  ii = 0; ii < rotation; ii++ )
        {
          rotatedMatrix = FunUtil.rRotate90 ( rotatedMatrix );
        }
      }
      else if ( rotation < 0 )
      {
        for ( int  ii = 0; ii < -rotation; ii++ )
        {
          rotatedMatrix = FunUtil.lRotate90 ( rotatedMatrix );
        }
      }
      
      return rotatedMatrix;
    }
    
    private void  populateModulatedSynapseSeq ( )
    ////////////////////////////////////////////////////////////////////////
    {    
      if ( layerStructure == null )
      {
        // parseMapCells() has not been called
        
        return;
      }
      
      final Map<Integer, Axon>  neuronIndexToAxonMap = layerStructure.axons;

      final Collection<Axon>  values = neuronIndexToAxonMap.values ( );

      // LOGGER.trace ( "axon count:  {}", values.size ( ) );

      for ( final Axon  axon : values )
      {
        final Branch [ ]  branches = axon.branches;

        // LOGGER.trace ( "branch count:  {}", branches.length );

        for ( final Branch  branch : branches )
        {
          final Synapse [ ]  synapses = branch.synapses;

          // LOGGER.trace ( "synapse count:  {}", synapses.length );

          for ( final Synapse  synapse : synapses )
          {
            modulatedSynapseList.add ( ( ModulatedSynapse ) synapse );
          }
        }
      }
    }
    
    private static double  parseDouble (
      final String  text,
      final double  defaultValue,
      final String  errorMessage )
      throws NumberFormatException
    ////////////////////////////////////////////////////////////////////////
    {
      if ( text == null )
      {
        return defaultValue;
      }
      
      final String  trimmedText = text.trim ( );
      
      if ( trimmedText.equals ( "" ) )
      {
        return defaultValue;
      }
      
      try
      {
        return Double.parseDouble ( trimmedText );
      }
      catch ( NumberFormatException  nfe )
      {
        LOGGER.error ( errorMessage, nfe );
        
        throw nfe;
      }
    }
    
    private static double[] invertDoubleArray ( double [ ] array)
    ////////////////////////////////////////////////////////////////////////
    {
      double [ ] out = new double [ array.length ];
      
      for (int i = 0; i < array.length; i++)
      {
        out [ i ] = 1 / array [ i ];
      }
      
      return out;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }