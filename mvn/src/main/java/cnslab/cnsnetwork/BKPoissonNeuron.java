    package cnslab.cnsnetwork;
    
    import cnslab.cnsmath.*;
    import java.io.Serializable;

    import edu.jhu.mb.ernst.model.Synapse;
    import edu.jhu.mb.ernst.util.slot.Slot;

    /***********************************************************************
    * Background sensory neuron, it generate heterogeneous Poisson spike
    * trains. The lamda is Decay from para.HighFre to para.LowFre with
    * inverse decay constant para.timeCon
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  BKPoissonNeuron
      implements Neuron, Serializable
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static final long  serialVersionUID = 0L;

    private boolean  record;
    
    // Neuron axons
    // public Axon axon;

    /** the seed for this neuron */
    private Seed  idum;

    /** Target Host id, binary format */
    public long  tHost;

    public double  time = 0.0;

    /** Neuron parameter class */
    BKPoissonNeuronPara  para;

    ////////////////////////////////////////////////////////////////////////
    // constructor methods
    ////////////////////////////////////////////////////////////////////////
    
    public  BKPoissonNeuron (
      final Seed                 idum,
      final BKPoissonNeuronPara  para )
    ////////////////////////////////////////////////////////////////////////
    {
      // this.idum = new Seed(-idum.seed);
      
      this.idum = idum;
      
      this.para = para;
    }

    ////////////////////////////////////////////////////////////////////////
    // interface Neuron accessor methods
    ////////////////////////////////////////////////////////////////////////

/*
    public Axon  getAxon ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return axon;
    }
*/
    
    @Override
    public double [ ]  getCurr ( final double  currTime )
    ////////////////////////////////////////////////////////////////////////
    {
      throw new RuntimeException ( "no curr for sensory neuron" );
    }

    @Override
    public double  getMemV ( final double  currTime )
    ////////////////////////////////////////////////////////////////////////
    {
      throw new RuntimeException ( "no memV for sensory neuron" );
    }

    @Override
    public boolean  getRecord ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return record;
    }

    @Override
    public long  getTHost ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return tHost;
    }

    @Override
    public double  getTimeOfNextFire ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return -1;
    }
    
    @Override
    public boolean  isSensory ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return true;
    }

    @Override
    public boolean  realFire ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return true;
    }

    ////////////////////////////////////////////////////////////////////////
    // interface Neuron mutator methods
    ////////////////////////////////////////////////////////////////////////

    @Override
    public void  setRecord ( final boolean  record )
    ////////////////////////////////////////////////////////////////////////
    {
      this.record = record;
    }

    @Override
    public void  setTHost ( final long  id )
    ////////////////////////////////////////////////////////////////////////
    {
      this.tHost = id;
    }

    @Override
    public void  setTimeOfNextFire ( final double  arg0 )
    ////////////////////////////////////////////////////////////////////////
    {
      this.time = arg0;
    }

    ////////////////////////////////////////////////////////////////////////
    // interface Neuron lifecycle methods
    ////////////////////////////////////////////////////////////////////////

    @Override
    public void  init (
      final int      expid,
      final int      trialid,
      final Seed     seed,
      final Network  network,
      final int      id )
    ////////////////////////////////////////////////////////////////////////
    {
      this.idum = seed;
      
      this.time = updateFire ( );
      
      final Slot<FireEvent>  fireEventSlot = network.getFireEventSlot ( ); 
      
      if ( network.getClass ( ).getName ( ).equals (
        "cnslab.cnsnetwork.ANetwork"))
      {
        fireEventSlot.offer (
          new AFireEvent (
            id,
            this.time,
            network.info.idIndex,
            ( ( ANetwork ) network ).aData.getId (
              ( ANetwork ) network ) ) );
      }
      else if ( network.getClass ( ).getName ( ).equals (
        "cnslab.cnsnetwork.Network" ) )
      {
        fireEventSlot.offer (
          new FireEvent ( id, this.time ) );
      }
      else
      {
        throw new RuntimeException ( "Other Network Class doesn't exist" );
      }
      
      //throw new RuntimeException(
      //"no need of initlization for sensory neuron");
    }

    @Override
    public double  updateFire ( )
    ////////////////////////////////////////////////////////////////////////
    {
      double  next = ( -Math.log ( Cnsran.ran2 ( idum ) ) / para.HighFre );
      
      while ( Cnsran.ran2 ( idum ) > ( 1.0 - para.LowFre / para.HighFre )
        * Math.exp ( -para.timeCon * ( time + next ) )
        + para.LowFre / para.HighFre )
      {       
        next += ( -Math.log ( Cnsran.ran2 ( idum ) ) / para.HighFre );
      }
      
      return next;
    }
    
    @Override
    public double  updateInput (
      final double   dummyTime,
      final Synapse  input )
    ////////////////////////////////////////////////////////////////////////
    {
      throw new RuntimeException ( "sensory neuron won't receive inputs" );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public String  toString ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final String  tmp = "Sensory Poisson Neuron\n";
      
      // tmp = tmp + axon;
      
      return tmp;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }