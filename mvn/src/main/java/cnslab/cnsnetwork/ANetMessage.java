package cnslab.cnsnetwork;
import cnslab.cnsmath.*;
import java.io.Serializable;
/**
 * Same as NetMessage, but has some extra information about avalanche
 * 
 * @author  Yi Dong
 */
public class ANetMessage extends NetMessage
{

	public ANetMessage(double time, int from, int sourceId, int avalancheId) {
		super(time,from);
		this.sourceId=sourceId;
		this.avalancheId=avalancheId;
	}
	public int sourceId;
	public int avalancheId;
}
