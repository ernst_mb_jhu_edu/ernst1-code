package cnslab.cnsnetwork;
import cnslab.cnsmath.*;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Used to count avalanch in the network
 * 
 * @author  Yi Dong
 */
public class AvalancheCounter implements Serializable,Transmissible
{
	public int IDcounter;
	public int oldExp;
	public int oldTrial;

	AvalancheCounter(Experiment exp)
	{
		avalancheCounter  = new ArrayList< ArrayList< Map<String, Integer> > >(exp.subExp.length);
		for(int i=0; i < exp.subExp.length; i++)
		{
			ArrayList< Map<String, Integer> > tmp = new ArrayList<Map<String, Integer>>(exp.subExp[i].repetition);
			avalancheCounter.add(i,tmp);
			for(int j=0; j < exp.subExp[i].repetition; j++)
			{
				Map<String, Integer> tmp2 = new HashMap<String, Integer>();
				tmp.add(j,tmp2);
			}
		}
//		avalancheCounter  = new HashMap<String, Integer>();
		IDcounter = 0;
		oldExp=0;
		oldTrial=0;
	}

//	public Map<String, Integer> avalancheCounter;
	public ArrayList< ArrayList< Map<String, Integer> > > avalancheCounter;

	public void clear()
	{
		avalancheCounter.clear();
	}

	/**
	 * 
	 *  
	 * @param anetwork
	 * @return IDcounter
	 */
	public int getId(ANetwork anetwork)
	{
		if(anetwork.trialId != oldTrial || anetwork.subExpId != oldExp)
		{
			IDcounter=0;
			oldTrial = anetwork.trialId;
			oldExp = anetwork.subExpId;
		}
		IDcounter++;
		return IDcounter;
	}

	public void CountIt(ANetwork anetwork, int source, int id)
	{
		//anetwork.p.println("CountIt called!");
		String key = "S"+source+",I"+id;
		Integer tmpInt = avalancheCounter.get(anetwork.subExpId).get(anetwork.trialId).get(key);
		if(tmpInt == null)
		{
			avalancheCounter.get(anetwork.subExpId).get(anetwork.trialId).put(key, tmpInt = (new Integer(0)));
		}
		tmpInt = tmpInt + 1;
		avalancheCounter.get(anetwork.subExpId).get(anetwork.trialId).put(key,tmpInt);
	}
}
