    package cnslab.cnsnetwork;
    
    import java.io.Serializable;
    
    import edu.jhu.mb.ernst.model.Synapse;
    
    /***********************************************************************
    * Branch put all the synapses with same synaptic delay and same target
    * together to reduce the queue size
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  Branch
      implements Serializable
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    private static final long  serialVersionUID = 0L;
    
    //

    /** Synaptic delay time, second is the unit  */
    public double  delay;

    /** grouped synapses */
    public Synapse [ ]  synapses;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  Branch (
      final Synapse [ ]  synapses,
      final double       delay )
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      this.synapses = synapses;
      
      this.delay = delay;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public String toString()
    ////////////////////////////////////////////////////////////////////////
    {
      String tmp="   delay:"+delay+"\n";
      
      tmp=   tmp+"      synapses:\n";
      
      for(int i=0; i< synapses.length; i++)
      {
        tmp=tmp+"         "+synapses[i]+"\n";
      }
      
      return tmp;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }