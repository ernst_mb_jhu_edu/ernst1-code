    package cnslab.cnsnetwork;
    
    import java.io.*;
    import java.util.*;

    /***********************************************************************
    * Static method library to support RecorderData.
    * 
    * @see
    *   RecorderData
    * @see
    *   UserSenNeuron
    *   
    * @version
    *   $Date: 2012-08-13 23:26:48 +0200 (Mon, 13 Aug 2012) $
    *   $Rev: 124 $
    *   $Author: jmcohen27 $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  RecorderDataLib
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    /***********************************************************************
    * Inserts experiment data into a RecorderData instance.
    ***********************************************************************/ 
    public static void  insertExperimentDataIntoRecorderData (
      final RecorderData        recorderData,
      final int                 experimentId,
      final int                 trialId,
      final String              posi,
      final LinkedList<Double>  times )
    ////////////////////////////////////////////////////////////////////////
    {
      if ( times != null
        && times.size ( ) != 0 )
      {
        recorderData.receiver.put (
          "Exp"
            + experimentId
            + "Tri"
            + trialId
            + "/"
            + posi,
          times );
      }
    }

    public static void  insertExperimentRecorderDataIntoRecorderData (
      final RecorderData  recorderData,
      final Experiment    experiment,
      final RecorderData  experimentRecorderData )
    ////////////////////////////////////////////////////////////////////////
    {
      final ArrayList<String>
        singleUnitNameList = experiment.recorder.suNames;
      
      final ArrayList<Integer>
        singleUnitIdList = experiment.recorder.singleUnit;
      
      final int
        singleUnitIdListSize = singleUnitIdList.size ( );
      
      final Map<String, LinkedList<Double>>
        dataKeyToTimeListMap = experimentRecorderData.receiver;
      
      for ( int  subExperimentId = 0;
        subExperimentId < experiment.subExp.length;
        subExperimentId++ )
      {
        final SubExp
          subExp = experiment.subExp [ subExperimentId ];
        
        final int
          repetition = subExp.repetition;
        
        for ( int trialId = 0;
          trialId < repetition;
          trialId++ )
        {       
          for ( int singleUnitIdIndex = 0;
            singleUnitIdIndex < singleUnitIdListSize;
            singleUnitIdIndex++ ) 
          {
            final int 
              singleUnitId = singleUnitIdList.get( singleUnitIdIndex );
            
            final String
              singleUnitName = singleUnitNameList.get ( singleUnitIdIndex );
            
            final String
              dataKey = "E"
                + subExperimentId
                + "T"
                + trialId
                + "N"
                + singleUnitId;
            
            final LinkedList<Double>
              timeList = dataKeyToTimeListMap.get ( dataKey );
            
            insertExperimentDataIntoRecorderData (
              recorderData,
              subExperimentId,
              trialId,
              singleUnitName,
              timeList );
          }
        }
      }
    }
    
    public static RecorderData  readRecorderDataFromBinaryFile (
      final File  binaryFile )
      throws ClassNotFoundException, IOException
    ////////////////////////////////////////////////////////////////////////
    {
      ObjectInputStream  objectInputStream = null;
      
      try
      {
        objectInputStream
          = new ObjectInputStream (
            new BufferedInputStream (
              new FileInputStream ( binaryFile ) ) );
        
        return ( RecorderData ) objectInputStream.readObject ( );
      }
      finally
      {
        if ( objectInputStream != null )
        {        
          objectInputStream.close ( );
        }
      }
    }

    /***********************************************************************
    * Creates a RecorderData and populates it with data from a text file.
    ***********************************************************************/ 
    public static RecorderData  readRecorderDataFromTextFile (
      final File  textFile )
      throws IOException
    ////////////////////////////////////////////////////////////////////////
    {
      final RecorderData
        recorderData = new RecorderData ( );
      
      readRecorderDataFromTextFile (
        recorderData,
        textFile );
      
      return recorderData;
    }
    
    /***********************************************************************
    * Parses the input from a text file.
    * 
    * Text data file format, each neuron and each trial is one line, e.g.
    * 
    * 0 0 pre,0,0,suf 0.00049314 0.00065926 0.004612 0.018346 0.029955
    * 0.055654 0.063001 0.08199 0.082669 0.087991 0.10277 0.10769 0.12964
    * 0.14332 0.14447 0.15082 0.17663 0.17955 0.18728 0.22081 0.24705
    * 0.25536 0.75184 0.81812 1.3039 1.4853 1.5252 1.5371 1.6114 1.7159
    * 1.7312 1.7467 1.8764 2.0049 2.0568 2.0735 2.1627 2.194 2.2083 2.2187
    * 2.2302 2.3075 2.3331 2.3656 2.4241 2.4364 2.4476 2.4575 2.4806 2.499
    * 2.5649 2.5898 2.6003 2.6038 2.6444 2.6871 2.6916 2.7372 2.744 2.747
    * 2.7927 2.8374 2.9312 3.0107 3.0625 3.0678 3.1224 3.1777 3.1779 3.2035
    * 3.2372 3.2734 3.3049 3.3341 3.3505 3.4168 3.5189 3.5232 3.5254 3.5669
    * 3.6241 3.6284 3.6733 3.6869 3.688 3.7007 3.7051 3.8492 3.9113 3.9176
    * 3.9439 3.9476 3.9572 3.9689 4.0021<br>
    * 
    * first subexp id, then trial id, then neuron string, then neuron spikes
    * starting at time zero. 
    ***********************************************************************/ 
    public static void  readRecorderDataFromTextFile (
      final RecorderData  recorderData,
      final File          textFile )
      throws IOException
    ////////////////////////////////////////////////////////////////////////
    {
      BufferedReader  bufferedReader = null;
      
      try
      {
        bufferedReader = new BufferedReader (
          new FileReader ( textFile ) );
        
        String strLine;
        
        // Read File Line By Line
        
        while ( ( strLine = bufferedReader.readLine ( ) ) != null )
        {
          final String [ ]
            res = strLine.split ( " " );
          
          if ( res.length < 4 )
          {
            throw new RuntimeException (
              "error in file format" );
          }
          
          final LinkedList<Double>
            data = new LinkedList<Double> ( );
          
          for ( int  i = 3; i < res.length; i++ )
          {
            data.add ( Double.parseDouble ( res [ i ] ) );
          }
          
          insertExperimentDataIntoRecorderData (
            recorderData,
            Integer.parseInt ( res [ 0 ] ),
            Integer.parseInt ( res [ 1 ] ),
            res [ 2 ],
            data );
          
          // Print the content on the console
          
          // System.out.println (strLine);
        }
      }
      finally
      {
        if ( bufferedReader != null )
        {
          bufferedReader.close ( );
        }
      }
    }

    /***********************************************************************
    * Writes the RecorderData to the file in binary format.
    ***********************************************************************/ 
    public static void  writeRecorderDataToBinaryFile (
      final RecorderData  recorderData,
      final File          binaryFile )
      throws IOException
    ////////////////////////////////////////////////////////////////////////
    {
      ObjectOutputStream  objectOutputStream = null;
      
      try
      {
        objectOutputStream = new ObjectOutputStream (
          new FileOutputStream ( binaryFile ) );
        
        objectOutputStream.writeObject ( recorderData );
      }
      finally
      {
        if ( objectOutputStream != null )
        {
          objectOutputStream.close ( );
        }
      }
    }
    
    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
    
    private  RecorderDataLib ( )
    ////////////////////////////////////////////////////////////////////////
    {
      // private constructor
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }