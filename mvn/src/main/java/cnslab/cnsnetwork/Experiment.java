    package cnslab.cnsnetwork;
    
    import java.io.Serializable;

    /***********************************************************************
    * Experiment consists of sub experiment.
    *
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public class  Experiment
      implements Serializable, Transmissible
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    private static final long serialVersionUID = 0L;
    
    //

    /** array of sub experiments */
    public final SubExp [ ]  subExp;

    /** recorder info */
    public final Recorder  recorder;

    ////////////////////////////////////////////////////////////////////////
    // constructor methods
    ////////////////////////////////////////////////////////////////////////

    public  Experiment (
      final SubExp [ ]  subExp,
      final Recorder    recorder )
    ////////////////////////////////////////////////////////////////////////
    {
      this.subExp = subExp;
      
      this.recorder = recorder;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public String  toString ( )
    ////////////////////////////////////////////////////////////////////////
    {
      String  tmp = "Experiment\n";
      
      for ( int  i = 0; i < subExp.length; i++ )
      {
        tmp = tmp + "\tSub" + i + " " + subExp [ i ] + "\n";
      }
      
      return tmp;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }