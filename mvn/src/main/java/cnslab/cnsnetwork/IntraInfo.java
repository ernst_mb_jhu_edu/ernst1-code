    package cnslab.cnsnetwork;

    import java.io.Serializable;
    import java.util.LinkedList;

    /***********************************************************************
    * Class stored the information for intracellular recorder. 
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  IntraInfo
      implements Serializable, Transmissible
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private static final long  serialVersionUID = 0L;
    
    //

    /** neuron membrane voltage */
    public double  memV;

    /** currents for each of the receptors */
    public LinkedList<Double>  curr;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  IntraInfo (
      final double              memV,
      final LinkedList<Double>  curr )
    ////////////////////////////////////////////////////////////////////////
    {
      this.memV = memV;
      
      this.curr = curr;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    /***********************************************************************
    * Sum the membrane voltage and currents of other IntraInfo to current
    * one
    * 
    * @param other
    *   IntraInfo
    ***********************************************************************/
    public void  plus ( final IntraInfo  other )
    ////////////////////////////////////////////////////////////////////////
    {
      this.memV = this.memV + other.memV;
      
      for ( int  i = 0; i < curr.size ( ); i++ )
      {
        this.curr.set ( i, curr.get ( i ) + other.curr.get ( i ) );
      }
    }

    /***********************************************************************
    * divide the membrane and currents by j
    * 
    * @param j
    *   number of parts
    ***********************************************************************/
    public void divide ( int  j )
    ////////////////////////////////////////////////////////////////////////
    {
      this.memV = this.memV / ( ( double ) j );
      
      for ( int  i = 0; i < curr.size ( ); i++ )
      {
        this.curr.set ( i, curr.get ( i ) /  ( ( double ) j ) );
      }
    }
    
    @Override    
    public String  toString ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return new String ( "memV:" + memV + " curr:" + curr );
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }