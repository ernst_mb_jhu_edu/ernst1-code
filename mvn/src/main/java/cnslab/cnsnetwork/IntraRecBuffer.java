    package cnslab.cnsnetwork;
    
    import java.io.*;
    import java.util.*;
    
    /***********************************************************************
    * Stored array of neurons which needs to be recorded. 
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  IntraRecBuffer
      implements Serializable, Transmissible
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private static final long  serialVersionUID = 0L;
    
    /** Array of intracellularly recorded neuron. */
    public int [ ]
      neurons;

    /** Array of IntraInfo */
    public List<LinkedList<IntraInfo>>
      buff;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    public  IntraRecBuffer ( final int [ ]  neurons )
    ////////////////////////////////////////////////////////////////////////
    {
      this.neurons = neurons;
      
      buff = new ArrayList<LinkedList<IntraInfo>> ( );
      
      for ( int  i = 0; i < this.neurons.length; i++ )
      {
        buff.add ( new LinkedList<IntraInfo> ( ) );
      }
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public void  init ( )
    ////////////////////////////////////////////////////////////////////////
    {
      for ( int  i = 0; i < this.neurons.length; i++ )
      {
        buff.get ( i ).clear ( );
      }
    }

    @Override
    public String  toString ( )
    ////////////////////////////////////////////////////////////////////////
    {
      String  out = "print intra size" + (neurons.length) + "\n";
      
      for ( int  i = 0; i < neurons.length; i++ )
      {
        out = out
          + " neuron" + i + " intrainfo" + buff.get ( i ).size ( ) + "\n";
        
        for ( int  j = 0; j < buff.get ( i ).size ( ); j++ )
        {
          out = out + buff.get ( i ).get ( j ) + " ";
        }
        
        out = out + "\n";
      }
      
      return out;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }