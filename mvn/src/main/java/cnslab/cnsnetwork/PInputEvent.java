    package cnslab.cnsnetwork;
    
    import edu.jhu.mb.ernst.model.Synapse;
    
    /***********************************************************************
    * Poisson input events, similar to InputEvent
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  PInputEvent
      implements Comparable<PInputEvent>, Cloneable
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    /** time of the input event */
    public double  time;

    /** the synapse data */
    public Synapse  synapse;

    /** the source of input id */
    public int  sourceId;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
      
    public  PInputEvent (
      final double   time,
      final Synapse  synapse,
      final int      sourceId )
    ////////////////////////////////////////////////////////////////////////
    {
      this.time = time;
      
      this.synapse = synapse;
      
      this.sourceId = sourceId;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public Object  clone ( )
    ////////////////////////////////////////////////////////////////////////
    {
      try
      {
        return super.clone ( );
      }
      catch ( final CloneNotSupportedException  cnse )
      {
// TODO:  Why catch the exception and return null?
        
        System.err.println ( "Clone Abort." + cnse );
        
        return null;
      }
    }
    
    @Override
    public int  compareTo ( final PInputEvent  arg0 )
    ////////////////////////////////////////////////////////////////////////
    {
      if ( time < arg0.time )
      {
        return -1;
      }
      
      if ( time > arg0.time )
      {
        return 1;
      }
      
      final int
        targetNeuronIndexThis
          = synapse.getTargetNeuronIndex ( ),
        targetNeuronIndexOther
          = arg0.synapse.getTargetNeuronIndex ( );
      
      if ( targetNeuronIndexThis < targetNeuronIndexOther )
      {
        return -1;
      }
      
      if ( targetNeuronIndexThis > targetNeuronIndexOther )
      {
        return 1;
      }
      
      if ( sourceId < arg0.sourceId )
      {
        return -1;
      }
      
      if ( sourceId > arg0.sourceId )
      {
        return 1;
      }
      
      return 0;
    }


    @Override
    public String  toString ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return new String (
        "time:"
        + time
        + " sourceId:"
        + sourceId
        + " "
        + synapse );
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }