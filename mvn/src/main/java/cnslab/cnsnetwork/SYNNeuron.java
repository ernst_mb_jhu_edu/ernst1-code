    package cnslab.cnsnetwork;
    
    import java.io.*;
    import java.util.*;

    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import jpvm.*;

    import cnslab.cnsmath.*;
    import edu.jhu.mb.ernst.model.Synapse;

    /***********************************************************************
    * Sync neuron (fake neuron, id is the last neuron index),
    * used to synchronize between NetHosts, used to send half minimum
    * synaptic delay time ticks, send buffered spikes
    * 
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  SYNNeuron
      implements Neuron, Serializable
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
    
    private static final Class<SYNNeuron>
      CLASS = SYNNeuron.class;

    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );  
    
    private static final long  serialVersionUID = 0L;
    
    //
    
    /** background frequency */
    private Network  network;

    public double    tick;

//  Axon axon;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  SYNNeuron ( final Network  network )
    ////////////////////////////////////////////////////////////////////////
    {
      this.network = network;
      
      tick = network.minSynapticDelay / 2.0;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public void  setTimeOfNextFire ( final double  nextFire )
    ////////////////////////////////////////////////////////////////////////
    {
      if ( network.info.jpvm == null )
      {
        // TODO:  Hack for unit testing without distributed computing
        // I am not sure if this is correct. -- Croft
        
        final double  currTime = nextFire - tick;
        
        if ( currTime >= network.endOfTrial )
        {
          int trial = network.trialId;
          
          int expid = network.subExpId;

          processData ( expid, trial );
          
          network.p.println ( "trial done" );
          
          network.trialDone = true;

          network.spikeState = true;
          
          for ( int  iter = 0; iter < network.info.numTasks; iter++ )
          {       
            network.received [ iter ] = 1;
          }
          
          network.rootTime = 0.0;
          
          network.clearQueues ( );
          
          network.countTrial++;
          
          // double hack
          
          network.stop = true;
        }
        
        return;
      }
      
      // if synNeuron at currTime could be processed, it was guaranteed that
      // the spikes sent by synNeuron at currTime-tick are received.
      
//    network.p.print("sync T"+":"+network.rootTime+" ");
//    network.p.print("\n");
      
      try
      {
        // send current time to the root;
        
        final double  currTime = nextFire - tick;

        jpvmBuffer buff = new jpvmBuffer ( );
        
        buff.pack ( currTime ); //pack the sysTime
        
        buff.pack ( network.info.idIndex ); //pack the hostId
        
        // tell the root the current time
        
        network.info.jpvm.pvm_send (
          buff,
          network.info.parentJpvmTaskId,
          NetMessageTag.sendTick );

        int  countN = 0;
        
        for ( int  k = 0;
          k < network.experiment.recorder.intraEle.size ( ); k++ )
        {
          for ( int  m = 0;
            m < network.experiment.recorder.intraEle.get ( k ).size ( ); m++ )
          {
            final Neuron  tmpN = network.neurons [
              network.intraRecBuffers.neurons [ k ] - network.base ];
            
            final LinkedList<Double>  curr = new LinkedList<Double> ( );
            
            final double [ ]  allCurr = tmpN.getCurr ( currTime );
            
            for( int  j = 0;
              j < network.experiment.recorder.currChannel.get ( k ).size ( );
              j++ )
            {
              curr.add ( allCurr [
                network.experiment.recorder.currChannel.get ( k ).get ( j ) ] );
            }
            
            network.intraRecBuffers.buff.get ( countN ).add (
              new IntraInfo ( tmpN.getMemV ( currTime ), curr ) );
            
            countN++;
          }
        }

        // network.p.print(network.intraBuff);

        if ( currTime + network.minSynapticDelay < network.endOfTrial )
        {
//        network.p.println("sent at "+currTime);
          
          for ( int  i = 0; i < network.info.numTasks; i++ )
          {
            if ( i != network.info.idIndex ) //not to him self
            {
              buff = new jpvmBuffer();
              
              buff.pack(network.info.idIndex); //source id;
              
              buff.pack(network.countTrial);// trial id info

//            buff.pack(network.rootTime); // local root time;
//            buff.pack(currTime); // local root time;
              
              buff.pack(network.spikeBuffers[i]); //pack the spike buffer
              
              // send the spikes to the target host
              
              network.info.jpvm.pvm_send (
                buff,
                network.info.tids [ i ],
                NetMessageTag.sendSpike );
              
              // clear buffer
              
              network.spikeBuffers [ i ].buff.clear ( );
            }
          }
        }
        else
        {
          for ( int  iter = 0; iter < network.info.numTasks; iter++ )
          {
            if ( iter != network.info.idIndex )
            {
              ( network.received [ iter ] )++;
            }
          }
          
          network.spikeState = true;
        }

        if ( currTime >= network.endOfTrial )
        {
          // network.p.println("trial done ini"+minTime);
          
          jpvmBuffer buf = new jpvmBuffer();
          
          int trial = network.trialId;
          
          int expid = network.subExpId;

//        network.p.print("E"+network.subExpId+" T"+trial);

          buf.pack(trial); //trial id and subexp id
          
          buf.pack(expid);
          
          //buf.pack(network.info.idIndex);

          // don't send it, process it first
          //  buf.pack(network.recordBuff);
          
          processData ( expid, trial );
          
          // send the intracellular info back to root

          buf.pack ( network.intraRecBuffers );
          
          //send root to say the trial is done
          
          // network.info.jpvm.pvm_send(
          //   buf,network.info.parent,NetMessageTag.trialDone);
          
          network.p.println ( "trial done" );
          
          //break;
          
          network.trialDone = true;

          //added;
          
          network.spikeState = true;
          
          for ( int  iter = 0; iter < network.info.numTasks; iter++ )
          {       
            /*
            while(!network.received[iter].empty())
            {
              network.received[iter].pop();
            }
            */
            
            // leave mark here
            
            network.received [ iter ] = 1;
          }
          
          //added over;

          //initialization
          
          network.rootTime = 0.0;
          
          network.clearQueues ( );
          
          network.countTrial++;
          
          //network.init();
          
          network.info.jpvm.pvm_send (
            buf,
            network.info.tids [ network.info.idIndex ],
            NetMessageTag.trialDone );
          
          buf = new jpvmBuffer ( );
          
          // network.p.println(
          // "e:"+expid+"t:"+trial+" host:"+network.info.parent.getHost());
          
          network.info.jpvm.pvm_barrier (
            network.info.parentJpvmTaskId.getHost ( ),
            expid * network.experiment.subExp.length + trial,
            network.info.numTasks );
          
          // network.p.println("ov_e:"+expid+"t:"+trial);
          // network.p.flush();
          
          network.info.jpvm.pvm_send (
            buf,
            network.info.parentJpvmTaskId,
            NetMessageTag.trialDone );
        }
      }
      catch ( final jpvmException  ex )
      {
        ex.printStackTrace(network.p);
        
        network.p.println (
          "Error - Sync failed to send message to the root "
          +network.rootTime );
        
        network.p.println ( ex );
      }
      catch ( final Exception  e )
      {
        e.printStackTrace ( );
      }
    }

    /**
     * @see cnslab.cnsnetwork.Neuron#timeOfFire() timeOfFire
     */
// TODO:  Why does @Override not work here? Interface changed?    
// @Override
    public double timeOfFire(double curr)
    ////////////////////////////////////////////////////////////////////////
    {
      return tick;
    }
    
    /**
     * @see cnslab.cnsnetwork.Neuron#updateFire() updateFire
     */
    @Override
    public double updateFire()
    ////////////////////////////////////////////////////////////////////////
    {
//    throw new RuntimeException("sensory neuron won't update fire");
      return tick;
//    return true;
    }
    
    /**
     * @see cnslab.cnsnetwork.Neuron#updateInput(
     * cnslab.cnsnetwork.InputEvent) updateInput
     */
    @Override
    public double updateInput(double time, Synapse input)
    ////////////////////////////////////////////////////////////////////////
    {
      throw new RuntimeException("sensory neuron won't recieve inputs");
    }
    
    /**
     * @see cnslab.cnsnetwork.Neuron#getTimeOfNextFire() getTimeOfNextFire
     */
    @Override
    public double getTimeOfNextFire()
    ////////////////////////////////////////////////////////////////////////
    {
      return -1;
    }
    
/*
  public Axon getAxon()
  {
    return axon;
  }
 */

    /**
     * @see java.lang.Object#toString() toString
     */
    @Override
    public String toString()
    ////////////////////////////////////////////////////////////////////////
    {
      String tmp="Synchroizing clock neuron\n";
      return tmp;
    } // END: toString

    /**
     * @see cnslab.cnsnetwork.Neuron#isSensory() isSensory
     */
    @Override
    public boolean isSensory()
    ////////////////////////////////////////////////////////////////////////
    {
      return true;
    }

    /**
     * {@inheritDoc}
     * @see Neuron#init(int,int,Seed,Network,int)
     */
    @Override
    public void init(int expid, int trialid, Seed idum, Network net, int id)
    ////////////////////////////////////////////////////////////////////////
    {
      throw new RuntimeException("sensory neuron won't need init");
    }

    /** 
     * record the neuron 
     * @return  boolean whether it should be recorded
     */
    @Override
    public boolean getRecord()
    ////////////////////////////////////////////////////////////////////////
    {
      return false; //fake neuron can't be recorded
    }

    /** 
     *set the neuron record or not.
     */
    @Override
    public void setRecord(boolean record)
    ////////////////////////////////////////////////////////////////////////
    {
      //nothing is done
    }

    /** 
     * @return neuron's current memory voltage
     */
    @Override
    public double getMemV(double currTime)
    ////////////////////////////////////////////////////////////////////////
    {
      throw new RuntimeException("no memV for sensory neuron");
    }

    /** 
     * 
     * @return neuron's current currents of all the channels
     */
    @Override
    public double [] getCurr(double currTime)
    ////////////////////////////////////////////////////////////////////////
    {
      throw new RuntimeException("no curr for sensory neuron");
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    private void  processData (
      final int  expid,
      final int  trial )
    ////////////////////////////////////////////////////////////////////////
    {
      //RecordBuffer spikes = Network.recordBuff;
      
      final Iterator<NetRecordSpike>
        iter_spike = network.recordBuff.buff.iterator();
      
      while(iter_spike.hasNext())
      {
        NetRecordSpike spike = iter_spike.next();
        
        //single unit processing
        
        //if neuron is in single unit recorder
        
        if(network.experiment.recorder.singleUnit.contains(spike.from))
        {
          LinkedList<Double>  tmp
            = network.recorderData.receiver.get (
              "E" + expid + "T" + trial + "N" + spike.from );

          if ( tmp == null )
          {
            network.recorderData.receiver.put (
              "E" + expid + "T" + trial + "N" + spike.from,
              tmp = ( new LinkedList<Double> ( ) ) );
          }
          
          //put received info into memory
          // if ( spike.time !=0.0 ) tmp.add(spike.time);
          
          //put received info into memory
          tmp.add ( spike.time );
          
          // System.out.println(
          //   "fire: time:"+spike.time+ " index:"+spike.from);
        }
        
        // multiple unit processing
        
        final int
          multiUnitSize = network.experiment.recorder.multiUnit.size ( );
        
        for ( int  mID = 0; mID < multiUnitSize; mID++ )
        {
          final ArrayList<Integer>  neuronIndexList
            = network.experiment.recorder.multiUnit.get ( mID );
          
          if ( neuronIndexList.contains ( spike.from ) )
          {
            // LOGGER.trace (
            //  "Recording MultiUnit spike from {}",
            //  spike.from );
            
            final int  binIndex = ( int )
              ( spike.time / network.experiment.recorder.timeBinSize );
            
            // LOGGER.trace ( "binIndex {}", binIndex );
            
            final String  key = "E" + expid + "N" + mID + "B" + binIndex; 
            
            Integer  tmpInt = network.recorderData.multiCounter.get ( key );
            
            if ( tmpInt == null )
            {
              tmpInt = new Integer ( 0 );
            }
            
            tmpInt = tmpInt + 1;
            
            network.recorderData.multiCounter.put (
              key,
              tmpInt );
            
            // LOGGER.trace ( "key {} tmpInt {}", key, tmpInt );
            
            final String  keyAll
              = "E" + expid + "T" + trial + "N" + mID + "B" + binIndex; 
            
            Integer  tmpIntAll
              = network.recorderData.multiCounterAll.get ( keyAll );
            
            if ( tmpIntAll == null )
            {
              tmpIntAll = new Integer ( 0 );
            }
            
            tmpIntAll = tmpIntAll + 1;
            
            network.recorderData.multiCounterAll.put (
              keyAll,
              tmpIntAll );
          }
        }
        
        // field processing
        for ( int  fID = 0;
          fID < network.experiment.recorder.fieldEle.size ( ); fID++ )
        {
          if ( network.simulatorParser.layerStructure.celltype [
            spike.from - network.base ].matches ( "^"
            + network.experiment.recorder.fieldEle.get ( fID ).getPrefix ( )
            + ",.*,"
            + network.experiment.recorder.fieldEle.get ( fID ).getSuffix ( )
            + "$" ) )
          {
//          int [] xy  = network.pas.ls.celllayer_cordinate(
//            spike.from,network.exp.rec.fieldEle.get(fID).getPrefix(),
//            network.exp.rec.fieldEle.get(fID).getSuffix());
            
            final int  binIndex = ( int )
              ( spike.time / network.experiment.recorder.fieldTimeBinSize );
            
            Integer  tmpInt = network.recorderData.fieldCounter.get (
              "E" + expid + ","
              + network.simulatorParser.layerStructure.celltype [
              spike.from - network.base ]
              + "," + "B" + binIndex );
            
            if ( tmpInt == null )
            {
              network.recorderData.fieldCounter.put (
                "E"+expid+","
                  + network.simulatorParser.layerStructure.celltype [
                  spike.from-network.base ]
                  + "," + "B" + binIndex,
                tmpInt = ( new Integer ( 0 ) ) );
            }
            
            tmpInt = tmpInt + 1;
            
            network.recorderData.fieldCounter.put (
              "E" + expid + ","
                + network.simulatorParser.layerStructure.celltype [
                spike.from-network.base ]
                + "," + "B" + binIndex,
              tmpInt);
          }
        }
        
        //vector processing
        
        for ( int  vID = 0;
          vID < network.experiment.recorder.vectorUnit.size ( ); vID++ )
        {
          for ( int  cID = 0;
            cID
              < network.experiment.recorder.vectorUnit.get ( vID ).coms.size ( );
            cID++)
          {
            if ( network.simulatorParser.layerStructure.celltype [
              spike.from-network.base ].matches (
                "^" + network.experiment.recorder.vectorUnit.get ( vID )
                .coms.get ( cID ).layer.getPrefix ( )
                + ",.*," + network.experiment.recorder.vectorUnit.get ( vID )
                .coms.get ( cID ).layer.getSuffix ( ) + "$" ) )
            {
              // int [] xy  = network.pas.ls.celllayer_cordinate(
              // spike.from,network.exp.rec.vectorUnit.get(vID)
              // .coms.get(cID).layer.getPrefix(),
              // network.exp.rec.vectorUnit.get(vID)
              // .coms.get(cID).layer.getSuffix());
              
              int id_begin=0;
              
              int id_mid=0;
              
              int id_end=0;
              
              id_begin = network.simulatorParser.layerStructure.celltype[
                spike.from-network.base].indexOf(',',0);
              
              id_mid = network.simulatorParser.layerStructure.celltype[
                spike.from-network.base].indexOf(',',id_begin+1);
              
              id_end = network.simulatorParser.layerStructure.celltype[
                spike.from-network.base].indexOf(',',id_mid+1);
              
              String xy_x = network.simulatorParser.layerStructure.celltype[
                spike.from-network.base].substring(id_begin+1,id_mid);
              
              String xy_y = network.simulatorParser.layerStructure.celltype[
                spike.from-network.base].substring(id_mid+1,id_end);

              final int  binIndex = ( int )
                ( spike.time / network.experiment.recorder.vectorTimeBinSize );
              
              Double  tmpDX = network.recorderData.vectorCounterX.get (
                "E"+expid+"V"+vID+"B"+binIndex+"C"+xy_x+","+xy_y );
              
              Double tmpDY = network.recorderData.vectorCounterY.get (
                "E"+expid+"V"+vID+"B"+binIndex+"C"+xy_x+","+xy_y );

              if ( tmpDX == null )
              {
                network.recorderData.vectorCounterX.put (
                  "E"+expid+"V"+vID+"B"+binIndex+"C"+xy_x+","+xy_y,
                  tmpDX=(new Double(0.0)));
                
                network.recorderData.vectorCounterY.put (
                  "E"+expid+"V"+vID+"B"+binIndex+"C"+xy_x+","+xy_y,
                  tmpDY=(new Double(0.0)));
              }

              tmpDX = tmpDX + Math.cos ( Math.PI / 180.0
                * ( double ) network.experiment.recorder.vectorUnit.get ( vID )
                .coms.get ( cID ).orientation );
              
              tmpDY = tmpDY + Math.sin ( Math.PI / 180.0
                * ( double ) network.experiment.recorder.vectorUnit.get ( vID )
                .coms.get ( cID ).orientation );
              
              network.recorderData.vectorCounterX.put (
                "E"+expid+"V"+vID+"B"+binIndex+"C"+xy_x+","+xy_y,
                tmpDX );
              
              network.recorderData.vectorCounterY.put (
                "E"+expid+"V"+vID+"B"+binIndex+"C"+xy_x+","+xy_y,
                tmpDY );
            }
          }
        }
      }
    }

    /**
     * {@inheritDoc}
     * @see Neuron#getTHost()
     */
    @Override
    public long getTHost()
    ////////////////////////////////////////////////////////////////////////
    {
      return 0;
    }

    /** 
     * set the target host id //0 means none , 1 means yes 
     */
    @Override
    public void setTHost(long id)
    ////////////////////////////////////////////////////////////////////////
    {

    }

    /**
     * {@inheritDoc}
     * @see Neuron#realFire()
     */
    @Override
    public boolean realFire()
    ////////////////////////////////////////////////////////////////////////
    {
      return true;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }