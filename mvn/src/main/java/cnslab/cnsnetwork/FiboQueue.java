package cnslab.cnsnetwork;
import cnslab.cnsmath.*;
import java.util.Map;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.io.PrintStream;
/**
 * Implementation of FiboQueue
 * 
 * @author  Yi Dong
 */
public class FiboQueue<T extends Comparable<T> > implements Queue<T>
{
	private FibonacciHeap<T> treeQueue = new FibonacciHeap<T>();

	/**
	 * @see cnslab.cnsnetwork.Queue#deleteItem(java.lang.Comparable) deleteItem
	 */
	public synchronized void deleteItem(T item) {

//		if(item.compareTo( treeQueue.peek())!=0) throw new RuntimeException("delete non fist item (input):"+item+" first item"+treeQueue.peek());
		if(item.compareTo((treeQueue.peekMin())) !=0 ) 
		{
			System.out.println("not the same");
//			treeQueue.delete(new FibonacciHeapNode<T>(item));
		}
		else
		{
			treeQueue.popMin();
		}
	}

	/**
	 * @see cnslab.cnsnetwork.Queue#insertItem(java.lang.Comparable) insertItem
	 */
	public synchronized void insertItem(T item) {
		treeQueue.add(item);
	}


	/**
	 * @see cnslab.cnsnetwork.Queue#firstItem() firstItem
	 */
	public synchronized T firstItem() {
		return treeQueue.peekMin();
	}

	/** 
	 * show all the elements in the Queue in order 
	 */
	public synchronized void show(PrintStream p) {
//		System.out.println(treeQueue.toString());
	}

	public synchronized String show() {
//		System.out.println(treeQueue.toString());
		return "";
	}

	public synchronized void clear(){
//		treeQueue.clear();
	}
}
