    package cnslab.cnsnetwork;
    
    /***********************************************************************
    * Neuron fire event, it contains the neuron fire time and neuron index.
    *
    * @version
    *   $Date: 2012-08-04 20:43:22 +0200 (Sat, 04 Aug 2012) $
    *   $Rev: 104 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public class  FireEvent
      implements Comparable<FireEvent>, Cloneable
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    /** neuron index */
    public int  index;

    /** time of neuron fire */
    public double  time;

    ////////////////////////////////////////////////////////////////////////
    // constructor methods
    ////////////////////////////////////////////////////////////////////////
    
    public  FireEvent (
      final int     index,
      final double  time )
    ////////////////////////////////////////////////////////////////////////
    {
      this.index = index;
      
      this.time = time;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public Object  clone ( )
    ////////////////////////////////////////////////////////////////////////
    {
      FireEvent a1 = null;
      
      try
      {
        a1 = (FireEvent)super.clone();
      }
      catch (CloneNotSupportedException exception)
      {
        System.err.println("Clone Abort."+ exception );
      }
      
      return a1;
    }

    @Override
    public int  compareTo ( final FireEvent  arg0 )
    ////////////////////////////////////////////////////////////////////////
    {
      if ( time < (arg0).time)
      {
        return -1;
      }
      else if (time > (arg0).time)
      {
        return 1;
      }
      else
      {
        if( index  < (arg0).index )
        {
          return -1;
        }
        else if ( index  > (arg0).index)
        {
          return 1;
        }
        else
        {
          return 0;
        }
      }
    }

    @Override
    public String  toString ( )
    ////////////////////////////////////////////////////////////////////////
    {
      return new String ( "time:" + time + " index:" + index);
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }