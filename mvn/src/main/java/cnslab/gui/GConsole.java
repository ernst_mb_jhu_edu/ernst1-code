package cnslab.gui;
import  java.io.*;
import jpvm.*;

/**
 * Graphic console for JPVM
 * 
 * @author Yi Dong
 */
public class GConsole implements Runnable{

	public jpvmEnvironment  jpvm;
	public BufferedReader   user;


	public GConsole(){

	}

	 /**
	 * 
	 * Test whether the background daemon is started or not
	 * @return boolean
	 */
	public boolean testDaemon()
	{

		try{
			jpvm  = new jpvmEnvironment("jpvm console");
		}
		catch (jpvmException jpe) {
	//		perror("internal jpvm error - "+jpe.toString());
			jpvm=null;	
			return false;
		}
		return true;
	}
	/**
	 * @see java.lang.Runnable#run() run
	 */
	public void run() {

	}

	/**
	 * 
	 *  stop the jpvm
	 * 
	 * @throws jpvmException
	 */
	public void Quit() throws jpvmException {
		//String tmp="";
		//tmp=tmp+"jpvmd still running.";
		jpvm.pvm_exit();
		System.exit(0);
	}

	/**
	 * 
	 * 
	 * @return help information
	 * 
	 * @throws jpvmException
	 */
	public String Help() throws jpvmException {
		String tmp="";
		tmp=tmp+"Commands are:";
		tmp=tmp+"  add\t- Add a host to the virtual "+
					"machine";
		tmp=tmp+"  delhost\t- delete a host from the virtual "+ "machine";
		tmp=tmp+"  delallhost\t- delete all the hosts from the virtual machine";
		tmp=tmp+"  halt\t- Stop jpvm daemons";
		tmp=tmp+"  help\t- Print helpful information " +
					"about commands";
		tmp=tmp+"  ps\t- List tasks";
		tmp=tmp+"  quit\t- Exit console";
		tmp=tmp+"  addall\t- Add hosts from file myhosts";
		tmp=tmp+"  del\t- Delete task lists";
		return tmp;
	}

	/**
	 * 
	 * 
	 * @return show configuration  information
	 * 
	 * @throws jpvmException
	 */
	public String Conf() throws jpvmException {
		String tmp="";
		jpvmConfiguration conf = jpvm.pvm_config();
		tmp=tmp+""+conf.numHosts+" hosts:\n";
		for(int i=0;i<conf.numHosts;i++)
			tmp=tmp+"\t"+conf.hostNames[i]+"\n";
		return tmp;
	}

	/**
	 * 
	 * 
	 * @return process information
	 * 
	 * @throws jpvmException
	 */
	public String Ps() throws jpvmException {
		String tmp="";
		jpvmConfiguration conf = jpvm.pvm_config();
		for(int i=0;i<conf.numHosts;i++) {
			jpvmTaskStatus ps = jpvm.pvm_tasks(conf,i);
			tmp=tmp+ps.hostName+", "+ps.numTasks+
				" tasks:\n";
			for(int j=0;j<ps.numTasks;j++)
			    tmp=tmp+"\t"+ps.taskNames[j]+"\n";
		}
		return tmp;
	}

	/**
	 * 
	 * Delete all the hosts from the JPVM
	 * @return
	 * 
	 * @throws jpvmException
	 */
	public String DelAllHosts() throws jpvmException {
		jpvmConfiguration conf = jpvm.pvm_config();
		for(int i=0;i<conf.numHosts;i++) {
			jpvmTaskStatus ps = jpvm.pvm_tasks(conf,i);
			try {
				jpvm.pvm_delhosts(ps.hostName);
			}
			catch (jpvmException jpe) {
				return perror("error - couldn't del host " + ps.hostName);
			}
		}
		return new String("success!\n");
	}

	/**
	 * 
	 *  stop all the current process.
	 * 
	 * @throws jpvmException
	 */
	public void Halt() throws jpvmException {
		jpvm.pvm_halt();
		/*
		try {
//                       Thread.sleep(2000);
                }
                catch (InterruptedException ie) {
                }
//		System.exit(0);
		*/
	}

	/**
	 * 
	 * delete one task
	 * @return
	 * 
	 * @throws jpvmException
	 */
	public String Delete() throws jpvmException {
		jpvm.pvm_deleteTasks();
		try {
                        Thread.sleep(100);
                }
                catch (InterruptedException ie) {
                }
		return "success!\n";
	}


	/**
	 * Add host
	 * 
	 * @return
	 */
	public String Add() {
		String tmp="";
		String host = null;
		int    port = 0;
		try {
			tmp=tmp+"\tHost name   : ";
			System.out.flush();
			host = user.readLine();
			tmp=tmp+"\tPort number : ";
			System.out.flush();
			String port_str = user.readLine();
		    try {
			port = Integer.valueOf(port_str).intValue();
		    }
		    catch (NumberFormatException nfe) {
			tmp=tmp+"Bad port.";
			return tmp;
		    }
		}
		catch (IOException e) {
			tmp=tmp+"i/o exception";
			try {
				Quit();
			}
			catch (jpvmException jpe) {
				System.exit(0);
			}
			return tmp;
		}
		jpvmTaskId tid = new jpvmTaskId(host,port);
		String h[] = new String[1];
		jpvmTaskId t[] = new jpvmTaskId[1];
		h[0] = host;
		t[0] = tid;
		try {
		  jpvm.pvm_addhosts(1,h,t);
		}
		catch (jpvmException jpe) {
		  return perror("error - couldn't add host " + host);
		}
		return tmp;
	}

	public String Add(String host,int port) {
		String tmp="Adding host:"+host+" listening at:"+port+"\n";
		jpvmTaskId tid = new jpvmTaskId(host,port);
		String h[] = new String[1];
		jpvmTaskId t[] = new jpvmTaskId[1];
		h[0] = host;
		t[0] = tid;
		try {
		  jpvm.pvm_addhosts(1,h,t);
		}
		catch (jpvmException jpe) {
		  return perror("error - couldn't add host " + host);
		}
		return tmp+"success!\n";
	}

	public String DelHost() {
		String tmp="";
		String host = null;
		int    port = 0;
		try {
			tmp=tmp+"\tHost name   : ";
			host = user.readLine();
		}
		catch (IOException e) {
			tmp=tmp+"i/o exception";
			try {
				Quit();
				return tmp;
			}
			catch (jpvmException jpe) {
				System.exit(0);
			}
			return tmp;
		}
		try {
		  jpvm.pvm_delhosts(host);
		}
		catch (jpvmException jpe) {
		  return perror("error - couldn't del host " + host+"\n");
		}
		return tmp;
	}

	public String DelHost(String host) {
		String tmp="Deleteing "+host+"\n";
		try {
		  jpvm.pvm_delhosts(host);
		}
		catch (jpvmException jpe) {
		  return perror("error - couldn't del host " + host+"\n");
		}
		return tmp+"success!\n";
	}

	/**
	 *  Add host from file myhosts
	 * 
	 * @return
	 */
	public String AddAll() {
		String tmp="";
		String host = null;
		int    port = 0;
		try{
		FileReader input = new FileReader("myhosts");
		BufferedReader bufRead = new BufferedReader(input);
		String line;    // String that holds current
		int count = 0;  // Line number of count 
		line = bufRead.readLine();
		count++;
		while (line != null){
			if(line.startsWith("#"))continue; // "#"is the comments line
			//tmp=tmp+count+": "+line);
			String [] tmp1 = null;
			//if(line ==null ) break;
			tmp1=line.split(" ++");
			if(tmp1.length<2)  return perror("myhosts file error -format: hosts port\n");
			host = tmp1[0];
			try{
				port = Integer.valueOf(tmp1[1]).intValue();
			} catch (NumberFormatException nfe) {
				tmp=tmp+"Bad port.\n";
				return tmp;
			}
			tmp=tmp+"Adding host: "+tmp1[0]+" port: "+tmp1[1]+"\n";

			jpvmTaskId tid = new jpvmTaskId(host,port);
			String h[] = new String[1];
			jpvmTaskId t[] = new jpvmTaskId[1];
			h[0] = host;
			t[0] = tid;
			try {
				jpvm.pvm_addhosts(1,h,t);
			}
			catch (jpvmException jpe) {
				return perror("error - couldn't add host " + host+"\n");
			}

			count++;
			line = bufRead.readLine();
		}
		bufRead.close();
		}catch (IOException e){
			tmp=tmp+"You need to prepare myhosts file in format:";
			tmp=tmp+"Hostsname Port";
//			e.printStackTrace();
			return tmp;
		}
		return tmp+"done\n";
	}

	public String perror(String message) {
		String tmp="";
                tmp=tmp+"jpvm console: "+ message;
		return tmp;
        }

}
