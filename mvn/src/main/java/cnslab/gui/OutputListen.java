    package cnslab.gui;
    
    import java.io.*;

    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    import thinlet.Thinlet;

    import cnslab.cnsmath.Seed;
    import cnslab.cnsnetwork.SimulatorParser;
    
    /***********************************************************************
    * @version
    *   $Date: 2011-09-10 18:17:21 -0500 (Sat, 10 Sep 2011) $
    *   $Rev: 211 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  OutputListen
      extends Thread
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static final Class<OutputListen>
      CLASS = OutputListen.class;
   
//    private static final String
//      CLASS_NAME = CLASS.getName ( );
 
    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );    

    private final InputStream  in;

    private final Thinlet      thinlet;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  OutputListen (
      final InputStream  in,
      final Thinlet      thinlet )
    ////////////////////////////////////////////////////////////////////////
    {
      this.in      = in;
      
      this.thinlet = thinlet;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    @Override
    public void  run ( )
    ////////////////////////////////////////////////////////////////////////
    {
      try
      {
        final Object  dis = thinlet.find ( "simStatusDisplay" );
        
        thinlet.setString (
          dis,
          "text",
          "Simulation starts ........\n" );

        final BufferedReader
          input = new BufferedReader ( new InputStreamReader ( in ) );
        
        String  a = input.readLine ( );
        
        while ( a != null )
        {
          if ( a.startsWith ( "E" ) )
          {
            thinlet.setString (
              thinlet.find ( "showProgress" ),
              "text",
              a );
          }
          else
          {
            thinlet.setString (
              dis,
              "text",
              thinlet.getString (
                dis,
                "text" )
                + a + "\n" );
          }

          new Thread (
            new UpdateScroll (
              thinlet,
              dis,
              -1,
              0.99f ) ).start ( );
          
          // thin.setScroll(dis, (float)(-1), (float)0.99);
          
          // Print file line to screen
          
          // System.out.println(a);
          
          a = input.readLine ( );
        }
        
        input.close ( );
        
        thinlet.setString (
          dis,
          "text",
          thinlet.getString (
            dis,
            "text" )
            + "Simulation done\n" );
        
        new Thread (
          new UpdateScroll (
            thinlet,
            dis,
            -1,
            0.99f ) ).start ( );
        
        thinlet.setBoolean (
          thinlet.find ( "startSim" ),
          "enabled",
          true );
        
        thinlet.setBoolean (
          thinlet.find ( "stopSim" ),
          "enabled",
          false );
        
        thinlet.setString (
          thinlet.find ( "showProgress" ),
          "text",
          "" );
        
        final SimulatorParser
          sim = new SimulatorParser (
            new Seed ( -1 ),
            new File (
              "model/" + thinlet.getString (
                thinlet.getSelectedItem ( thinlet.find ( "xmlModles" ) ),
                "text" ) ) );
        
        sim.parseMapCells ( );
        
        // System.out.println(sim.outFile);
        
        new Thread (
          new ContentProcessResult2 (
            thinlet,
            sim.outFile ) ).start ( );

        // NodeList nList = root.getElementsByTagName("GlobalVariable");
        
        // Element global = (Element) nList.item(0);

        // if(global.getElementsByTagName("DataFileName").getLength()>0)
      }
      catch ( final Exception  e )
      {
        LOGGER.error (
          e.getMessage ( ),
          e );
        
        e.printStackTrace ( );
      }
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }