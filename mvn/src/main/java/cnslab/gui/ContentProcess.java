    package cnslab.gui;
    
    import java.io.*;
    import java.util.*;

    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    import thinlet.*;

    import cnslab.cnsnetwork.SimulatorParser;
    
    /***********************************************************************
    * Presents a list of XML models to choose from and validates selected.
    *  
    * @version
    *   $Date: 2011-11-01 15:06:31 -0500 (Tue, 01 Nov 2011) $
    *   $Rev: 273 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  ContentProcess
      implements Runnable
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private static final Logger
      LOGGER = LoggerFactory.getLogger ( ContentProcess.class );
    
    //
    
    private final Thinlet  thinlet;
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  ContentProcess ( final Thinlet  thinlet )
    ////////////////////////////////////////////////////////////////////////
    {
      this.thinlet = thinlet;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    @Override
    public void  run ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final File  dir = new File ( "model" );
      
      final FilenameFilter
        filter = new FilenameFilter ( )
        {
          @Override
          public boolean  accept (
            final File    dirFile,
            final String  name )
          {
            return name.endsWith ( ".xml" ) || name.endsWith ( ".XML" );
          }
        };

      // System.out.println(thin.getCount(thin.find("xmlModles")));
        
      final String [ ]  children = dir.list ( filter );
      
      if ( thinlet.getCount ( thinlet.find ( "xmlModles" ) )
        != children.length )
      {
        Arrays.sort ( children );
        
        thinlet.removeAll ( thinlet.find ( "xmlModles" ) );
        
        //System.out.println("files:" +children.length);
        
        for ( int i = 0; i < children.length; i++ )
        {
          //    System.out.println("file:"+children[i]);
          
          final Object
            o = Thinlet.create ( "choice" );
          
          thinlet.setString (
            o,
            "text",
            children [ i ] );
          
          thinlet.add (
            thinlet.find ( "xmlModles" ),
            o );
        }
      }

      if ( thinlet.getSelectedItem ( thinlet.find ( "xmlModles" ) )
        != null )
      {
        // System.out.println(
        //   getString(getSelectedItem(find("xmlModles")),"text"));
        
        final SimulatorParser
          simulatorParser = new SimulatorParser ( );
        
        final String
          out = simulatorParser.validate (
            thinlet.getString (
              thinlet.getSelectedItem ( thinlet.find ( "xmlModles" ) ),
              "text" ) );
        
        LOGGER.debug ( "parser validation output:  {}", out );
        
        if ( out.equals ( "" ) )
        {
          thinlet.setString (
            thinlet.find ( "simStatusDisplay" ),
            "text",
            thinlet.getString (
              thinlet.getSelectedItem ( thinlet.find ( "xmlModles" ) ),
              "text" )
              + " ...OK!\n" );
          
          thinlet.setBoolean (
            thinlet.find ( "startSim" ),
            "enabled",
            true );
          
          thinlet.setBoolean (
            thinlet.find ( "stopSim" ),
            "enabled",
            false );
          
          thinlet.setBoolean (
            thinlet.find ( "showXML" ),
            "enabled",
            true );
        }
        else
        {
          thinlet.setString (
            thinlet.find ( "simStatusDisplay" ),
            "text",
            thinlet.getString (
              thinlet.getSelectedItem ( thinlet.find ( "xmlModles" ) ),
              "text" )
              + " ...Error!\n" + out );
          
          thinlet.setBoolean (
            thinlet.find ( "startSim" ),
            "enabled",
            false );
          
          thinlet.setBoolean (
            thinlet.find ( "stopSim" ),
            "enabled",
            false );
          
          thinlet.setBoolean (
            thinlet.find ( "showXML" ),
            "enabled",
            true );
        }
      }
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }