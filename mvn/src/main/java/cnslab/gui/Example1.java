package cnslab.gui;
import jpvm.*;
import thinlet.*;

/*
 * Demonstrate:
 *  a panel, buttons and a textfield
 */
public class Example1 extends Thinlet {
  public Example1() throws Exception {
    add(parse("cal.xml"));
  }

  public void display(Object button, Object result) {
    String s = getString(result, "text");
    String t = getString(button, "name");
    setString(result, "text", s+t);
  }

  public void clear(Object result) {
    setString(result, "text", "");
  }

  public static void main(String[] args) throws jpvmException,Exception {
//    new FrameLauncher("Calculator", new Example1(), 100, 140);
    System.out.println("here");
    GConsole g= new GConsole();
    if(g.testDaemon())
    {
    	System.out.println("Server okay");
    	g.Delete();
    	System.out.println(g.Ps());
    }
    else
    {
    	System.out.println("Server down");
    }
  }
}
