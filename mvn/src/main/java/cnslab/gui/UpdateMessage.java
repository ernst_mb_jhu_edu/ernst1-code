    package cnslab.gui;

    import thinlet.*;

    /***********************************************************************
    * Graphics interface class for simulator:  update message
    * 
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  UpdateMessage
      implements Runnable
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private Thinlet  thin;
    
    private Object   obj;

    private String   mess;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  UpdateMessage (
      final Thinlet  thin,
      final Object   obj,
      final String   mess )
    ////////////////////////////////////////////////////////////////////////
    {
      this.obj = obj;
      
      this.mess = mess;
      
      this.thin = thin;
    }

    @Override
    public void  run ( )
    ////////////////////////////////////////////////////////////////////////
    {
      try
      {
        Thread.sleep ( 10 );
      }
      catch ( final Exception  e )
      {
        e.printStackTrace ( );
      }
      
      thin.setString ( obj, "text", mess );
    }
    
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }