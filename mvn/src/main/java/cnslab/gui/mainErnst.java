    package cnslab.gui;

    import java.awt.*;
    import java.util.Arrays;

    import jaxe.*;
    import jpvm.*;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    import thinlet.*;

    import cnslab.cnsnetwork.*;

    /***********************************************************************
    * Graphic interface for the simulator.
    * 
    * @version
    *   $Date: 2011-09-10 20:21:56 -0500 (Sat, 10 Sep 2011) $
    *   $Rev: 222 $
    *   $Author: croft $
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft
    ***********************************************************************/
    public final class  mainErnst
      extends Thinlet
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {

    private static final long  serialVersionUID = 0L;
    
    private static final Class<mainErnst>
      CLASS = mainErnst.class;
    
    private static final Logger
      LOGGER = LoggerFactory.getLogger ( CLASS );    

    /** Graphic console */
    public GConsole  g;

    public Object    console;
    
    public Object    status;
    
    public Object    start_button;

    public Process   process;
    
    ////////////////////////////////////////////////////////////////////////
    // static methods
    ////////////////////////////////////////////////////////////////////////
    
    public static void  main ( final String [ ]  args )
      throws jpvmException, Exception
    ////////////////////////////////////////////////////////////////////////
    {
      final mainErnst  main = new mainErnst ( );
      
      new FrameLauncher (
        "Ernst Simulator",
        main,
        350,
        800 );
      
      new Thread ( new ContentProcessResult ( main ) ).start ( );
      
      new Thread ( new ContentProcess ( main ) ).start ( );
      
      // System.out.println("here");
    }
    
    ////////////////////////////////////////////////////////////////////////
    // constructor methods
    ////////////////////////////////////////////////////////////////////////
    
    public  mainErnst ( )
      throws Exception, jpvmException
    ////////////////////////////////////////////////////////////////////////
    {
      add ( parse ( "ernstGui.xml" ) );
      
      console = find ( "console" );
      
      status = find ( "statusText" );
      
      start_button = find ( "daemonToggle" );
      
      g = new GConsole ( );
      
      initServer ( );
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    /***********************************************************************
    * Process the user command in the console 
    * 
    * @throws jpvmException
    * @throws Exception
    ***********************************************************************/
    public void  userCommandProcess ( )
      throws jpvmException, Exception
    ////////////////////////////////////////////////////////////////////////
    {
      final String
        comm = getString (
          getSelectedItem ( find ( "commandGroup" ) ),
          "text" );
      
      if ( comm.equals ( "Add a host" ) )
      {
        // setBoolean(find("addHost"),"visible",true);
        
        add ( parse ( "addDialog.xml" ) );

        setString (
          status,
          "text",
          g.jpvm.pvm_config ( ).numHosts
            + " hosts, this daemon listening at:"
            + g.jpvm.readDaemonFile ( ) );
      }
      else if ( comm.equals ( "Add hosts from file" ) )
      {
        setString (
          console,
          "text",
          getString ( console, "text" )
            + "jpvm>add hosts from myhosts\n"
            + g.AddAll ( ) );
        
        setString (
          status,
          "text",
          g.jpvm.pvm_config ( ).numHosts
            + " hosts, this daemon listening at:"
            + g.jpvm.readDaemonFile ( ) );
      }
      else if ( comm.equals ( "Clear console" ) )
      {
        setString ( console, "text", "" );
        
        // setScroll(console, (float)(-1), (float)0.99);
      }
      else if ( comm.equals ( "Delete a host" ) )
      {
        final Object  delDia = parse ( "delDialog.xml" );
        
        add ( delDia );

        final jpvmConfiguration  conf = g.jpvm.pvm_config ( );
        
        for ( int  i = 0; i < conf.numHosts; i++ )
        {
          final Object  o = create ( "choice" );
          
          setString (
            o,
            "text",
            conf.hostNames [ i ] );
          
          add (
            find (
              delDia,
              "delHostList" ),
            o );
        }

        setString (
          status,
          "text",
          g.jpvm.pvm_config ( ).numHosts
            + " hosts, this daemon listening at:"
            + g.jpvm.readDaemonFile ( ) );
      }
      else if ( comm.equals ( "Delete all hosts" ) )
      {
        setString (
          console,
          "text",
          getString (
            console,
            "text" )
            + "jpvm>delete all hosts\n" + g.DelAllHosts ( ) );
        
        setString (
          status,
          "text",
          g.jpvm.pvm_config ( ).numHosts
            + " hosts, this daemon listening at:"
            + g.jpvm.readDaemonFile ( ) );
      }
      else if ( comm.equals ( "Kill all the processes" ) )
      {
        setString (
          console,
          "text",
          getString (
            console,
            "text" )
            + "jpvm>kill all processes\n"
            + g.Delete ( ) );
      }
      else if ( comm.equals ( "List hosts" ) )
      {
        setString (
          console,
          "text",
          getString (
            console,
            "text" )
            + "jpvm>list hosts\n"
            + g.Conf ( ) );
        
        // setScroll(console, (float)(-1), (float)0.99);
      }
      else if ( comm.equals ( "Show processes" ) )
      {
        setString (
          console,
          "text",
          getString (
            console,
            "text" )
            + "jpvm>show processes\n"
            + g.Ps ( ) );
      }

      new Thread (
        new UpdateScroll (
          this,
          console,
          -1,
          0.99f ) ).start ( );
    }

    /***********************************************************************
    * start the JPVM daemon  
    * 
    * @throws jpvmException
    * @throws Exception
    ***********************************************************************/
    public void  startServer ( )
      throws jpvmException,Exception
    ////////////////////////////////////////////////////////////////////////
    {
      new Thread (
        new UpdateMessage (
          this,
          find ( "serverStatus" ),
          "Please wait 3 secs..." ) ).start ( );
      
      if ( getString ( start_button, "text" ).equals ( "Start" ) )
      {
        final Object  cus_checkbox = find ( "cusPort" );
        
        // TODO:  Why is this not used?
        final Object  ran_checkbox = find ( "ranPort" );
        
        if ( getBoolean ( cus_checkbox, "selected" ) )
        {
          final String [ ]  args = new String [ 3 ];
          
          args [ 0 ] = "java";
          
          args [ 1 ] = "jpvm.jpvmDaemon";
          
          final Object  cus_text = find ( "customPort" );
          
          args [ 2 ] = getString ( cus_text, "text" );
          
          try
          {
            Integer.parseInt ( args [ 2 ] );
          }
          catch ( final NumberFormatException  nfe )
          {
            getToolkit ( ).beep ( );
            
            return;
          }
          
          LOGGER.info ( Arrays.toString ( args ) );
          
          try
          {
// TODO:  Is this intentionally hiding the instance variable "process"?            
            final Process  process = Runtime.getRuntime ( ).exec ( args );
          }
          catch ( final Exception  e )
          {
            e.printStackTrace ( );
          }
        }
        else
        {
          final String [ ]  args = new String [ ] {
            "java",
            "jpvm.jpvmDaemon" };
            
          LOGGER.info ( Arrays.toString ( args ) );
          
          try
          { 
// TODO:  Is this intentionally hiding the instance variable "process"?
            
            final Process  process = Runtime.getRuntime ( ).exec ( args );
          }
          catch ( final Exception  e )
          {
            e.printStackTrace ( );
          }
        }
      }
      else if ( getString ( start_button, "text" ).equals ( "Stop" ) )
      {
        g.jpvm.pvm_exit ( );
        
        g.jpvm.pvm_halt ( );
      }
      
      Thread.sleep ( 3000 );
      
      initServer ( );
      
      setString (
        find ( "serverStatus" ),
        "text",
        "" );
    }

    /***********************************************************************
    * added one host 
    * 
    * @param okay
    * 
    * @throws jpvmException
    * @throws Exception
    ***********************************************************************/
    public void  addHostNow ( final Object  okay )
      throws jpvmException, Exception
    ////////////////////////////////////////////////////////////////////////
    {
      if ( getString ( okay, "text" ).equals ( "Add" ) )
      {
        int port;
        
        try
        {
          port = Integer.parseInt (
            getString (
              find ( "portNumInt" ),
              "text" ) );
        }
        catch ( final NumberFormatException  nfe )
        {
          getToolkit ( ).beep ( );
          
          return;
        }
        
        setString (
          console,
          "text",
          getString (
            console,
            "text" )
            + "jpvm>add a host\n"
            + g.Add (
              getString (
                find ( "hostNameStr" ),
                "text" ),
              port ) );
        
        setString (
          status,
          "text",
          g.jpvm.pvm_config ( ).numHosts
            + " hosts, this daemon listening at:"
            + g.jpvm.readDaemonFile ( ) );
      }
      else
      {
        // TODO:  Why is this block empty?
      }
      
      remove ( find ( "addHost" ) );
    }

    /***********************************************************************
    * Delete one host
    *  
    * @param okay
    * 
    * @throws jpvmException
    * @throws Exception
    ***********************************************************************/
    public void  delHostNow ( final Object  okay )
      throws jpvmException, Exception
    ////////////////////////////////////////////////////////////////////////
    {
      if ( getString ( okay, "text" ).equals ( "Del" ) )
      {
        // setString (
        //   console,
        //    "text",
        //    getString ( console, "text" )
        //      + "jpvm>add a host\n"
        //      + g.Add (getString(find("hostNameStr"),"text"),port));
        
        setString (
          console,
          "text",
          getString ( console, "text" )
            + "jpvm>del a host\n"
            + g.DelHost (
              getString (
                getSelectedItem (
                  find ( "delHostList" ) ),
                "text" ) ) );
        
        setString (
          status,
          "text",
          g.jpvm.pvm_config ( ).numHosts
            + " hosts, this daemon listening at:"
            + g.jpvm.readDaemonFile ( ) );
      }
      else
      {
        // TODO:  Why is this block empty?
      }
      
      remove ( find ( "delHost" ) );
    }

    /***********************************************************************
    * The part starts the main simulation 
    * 
    * @param button
    * 
    * @throws Exception
    * @throws jpvmException
    ***********************************************************************/
    public void  startSimulation ( final Object  button )
      throws Exception, jpvmException
    ////////////////////////////////////////////////////////////////////////
    {
      if ( getString ( button, "text" ).equals ( "Start" ) )
      {
        // start the simulation
        
        setString (
          find ( "showProgress" ),
          "text",
          "" );
        
        // avalanche or not
        
        if ( getBoolean ( find ( "avalanch" ), "selected" ) )
        {
          // yes
          
          final String [ ]  args = new String [ ] {
            "java",
            "-Xmx" + getString ( find ( "mHeap" ), "text" ) + "m",
            "starter",
            getString ( getSelectedItem ( find ( "xmlModles" ) ), "text" ),
            getString ( find ( "seedInt" ), "text" ),
            getString ( find ( "nHeap" ), "text" ),
            "-a" };
          
          LOGGER.info ( Arrays.toString ( args ) );
          
          process = Runtime.getRuntime ( ).exec ( args );
          
          final OutputListen
            output = new OutputListen ( process.getInputStream ( ), this );
          
          output.start ( );
        }
        else
        {
          // no
          
          final String mHeap = getString ( find ( "mHeap" ), "text" );
          
          final String  xmlModels = getString (
            getSelectedItem ( find ( "xmlModles" ) ), "text" );
          
          final String  seedInt = getString ( find ( "seedInt" ), "text" );
          
          final String nHeap = getString ( find ( "nHeap" ), "text" );
          
          final String [ ]  args = new String [ ] {
            "java",
            "-Xmx" + mHeap + "m",
            "starter",
            xmlModels,
            seedInt,
            nHeap };
          
          LOGGER.info ( Arrays.toString ( args ) );
            
          process = Runtime.getRuntime ( ).exec ( args );
          
          final OutputListen
            output = new OutputListen ( process.getInputStream ( ), this );
          
          output.start ( );
        }
        
        setBoolean (
          find ( "startSim" ),
          "enabled",
          false );
        
        setBoolean (
          find ( "stopSim" ),
          "enabled",
          true );
      }
      else
      {
        process.destroy ( );
        
        g.Delete ( );
        
        setBoolean (
          find ( "startSim" ),
          "enabled",
          true );
        
        setBoolean (
          find ( "stopSim"),
          "enabled",
          false );
        
        setString (
          find ( "simStatusDisplay" ),
          "text",
          getString (
            find ( "simStatusDisplay" ),
            "text" )
            + "Simulation is killed\n" );
        
        new Thread (
          new UpdateScroll (
            this,
            find ( "simStatusDisplay" ),
            -1,
            0.99f ) ).start ( );
        
        setString (
          find ( "showProgress" ),
          "text",
          "" );
      }
      
      /*
      process = Runtime.getRuntime ( ).exec ( args );
      
      // if(process ==null) throw new RuntimeException("process null");

      jpvmDaemon.int2P[jpvmDaemon.pointer]=process;
      
      // System.out.println(
      // process+" id:"+jpvmDaemon.pointer
      // +" pro:"+jpvmDaemon.int2P[jpvmDaemon.pointer]);

      jpvmDaemon.pointer++;
      
      if(jpvmDaemon.pointer==jpvmDaemon.maxHosts)jpvmDaemon.pointer=0;
      
      jpvmOutputListen
        output = new jpvmOutputListen(process.getInputStream());
        
      output.start();
      */
    }

    public void  getContent ( )
    ////////////////////////////////////////////////////////////////////////
    {
      setBoolean (
        find ( "startSim" ),
        "enabled",
        false );
      
      setBoolean (
        find ( "stopSim" ),
        "enabled",
        false );
      
      setBoolean (
        find ( "showXML" ),
        "enabled",
        false );
      
      new Thread (
        new ContentProcess ( this ) ).start ( );

      //  System.out.println("getContent");
      
      /*
      File dir = new File("model");
      
      FilenameFilter filter = new FilenameFilter() {
        public boolean accept(File dir, String name) {
          return name.endsWith(".xml") || name.endsWith(".XML");
        }
      };
      
      String[] children = dir.list(filter);

      java.util.Arrays.sort(children);
      
      for( int i=0;i < children.length; i++)
      {
        // System.out.println("file:"+children[i]);
 
        Object o = create("choice");
        
        setString(o,"text",children[i]);
        
        add(find("xmlModles"),o);
      }

      if(getSelectedItem(find("xmlModles"))!=null)
      {
        System.out.println(
          getString(getSelectedItem(find("xmlModles")),"text"));
          
        SimulatorParser sim = new SimulatorParser();
        
        String  out = sim.validate(
          getString(getSelectedItem(find("xmlModles")),"text"));
          
        if(out.equals(""))
        {
          System.out.println("ok");
        }
        else
        {
          System.out.println("error format");
          System.out.println(out);
        }
      }
      */
    }

    public void  getResults ( )
    ////////////////////////////////////////////////////////////////////////
    {
      new Thread ( new ContentProcessResult ( this ) ).start ( );
    }

    public void  showPic ( final Object  tree )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      // System.out.println(getString(getSelectedItem(tree),"text"));
      
      // System.out.println(getString(getSelectedItem(tree),"name"));
      
      if ( getString ( getSelectedItem ( tree ), "name" ) != null )
      {
        final String [ ]
          arg = getString (
            getSelectedItem ( tree ),
            "name" ).split ( "," );

        final String
          fileName = getString (
            getSelectedItem ( find ( "resultsCombo" ) ),
            "text" );
        
        final PlotResult  pr = new PlotResult ( "test" );
        
        PlotResult.init ( "results/" + fileName );

        Container  frame = this;
        
        while ( !( frame instanceof Frame ) )
        { 
          frame = frame.getParent ( ); 
        }
        
        if ( frame.getComponentCount ( ) == 2 )
        {
          frame.remove ( frame.getComponent ( 1 ) );
        }
        else
        {
          // System.out.println("Subcomponent number: "
          // +((Container)frame.getComponent(0)).getComponentCount());
          
          // frame.add(pr.getFrame(Integer.parseInt(arg[0]),
          // Integer.parseInt(arg[1]),Integer.parseInt(arg[2])),"East");
          
          pr.getFrame (
            Integer.parseInt ( arg [ 0 ] ),
            Integer.parseInt ( arg [ 1 ] ),
            Integer.parseInt ( arg [ 2 ] ) );
        }

        // ((Frame)frame).pack();
        
        // frame.setVisible(true);

        PlotResult.stop ( );
      }
      else
      {
        // setBoolean(
        //   getSelectedItem(tree),
        //   "expanded",
        //   getBoolean(getSelectedItem(tree),"expanded")^true);
      }
    }

    public void  showXMLFile ( )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      // System.out.println(
      //   getString(getSelectedItem(find("xmlModles")),"text"));
      
      final String [ ]  args = new String [ ] {
        "model/" + getString (
          getSelectedItem ( find ( "xmlModles" ) ),
          "text" ) };
      
      Jaxe.main ( args, this );
      
      //System.out.println("Editor finished");
      //new Thread(new contentProcess(main)).start();

      // int nbconf = 0;

      //  File dir = new File("config");
      //     if (!dir.exists()) {
      //              JOptionPane.showMessageDialog(
      // null, JaxeResourceBundle.getRB().getString("erreur.DossierConfig"),
      // JaxeResourceBundle.getRB().getString("config.ErreurLancement"),
      // JOptionPane.ERROR_MESSAGE);
      //            System.exit(1);
      //            }
      //
      //             if (args.length > 0)
      //              Jaxe.ouvrir(new File(args[0]), null);
      //       else if (nbconf > 1) {
      //       synchronized (lock) { // synchronisation avec MacJaxe.ouvrir
      //              if (Jaxe.allFrames.size() == 0)
      //                 Jaxe.dialogueDepart();
      //     /          }
      //    } else if (nbconf == 1)
      //       Jaxe.nouveau(null);
      
      /*
      try {
      // workaround a bug in WebStart with file permissions on windows
      try {
      System.setSecurityManager(null);
      } catch (Throwable t) {
      t.printStackTrace();
      }
      String [] args = new String[1];
      args[0]="model/"+getString(getSelectedItem(find("xmlModles")),"text");
      XMLEditor editor = new XMLEditor(args);
      editor.run();
      } catch (MerlotException e) {
      e.printStackTrace();
      // MerlotDebug.exception(e);
      }
      */
    }

    ////////////////////////////////////////////////////////////////////////
    // private methods
    ////////////////////////////////////////////////////////////////////////
    
    /***********************************************************************
    * Initialzed the server 
    * 
    * @throws Exception
    ***********************************************************************/
    private void  initServer ( )
      throws Exception
    ////////////////////////////////////////////////////////////////////////
    {
      try
      {
        if ( g.testDaemon ( ) )
        {
          setString (
            status,
            "text",
            g.jpvm.pvm_config ( ).numHosts
              + " hosts, this daemon listening at:"
              + g.jpvm.readDaemonFile ( ) );
          
          g.Delete ( );
          
          setBoolean (
            console,
            "visible",
            true );
          
          setBoolean (
            find ( "Ernst" ),
            "enabled",
            true );
          
          setBoolean (
            find ( "commandPanel" ),
            "visible",
            true );
          
          setBoolean (
            find ( "portNamePanel" ),
            "visible",
            false );
          
          setString (
            console,
            "text",
            g.Conf ( ) );
          
          setString (
            start_button,
            "text",
            "Stop" );
        }
        else
        {
          setString (
            status,
            "text",
            "warning, The jpvm daemon is not detected!" );
          
          setBoolean (
            console,
            "visible",
            false );
          
          setBoolean (
            find ( "Ernst" ),
            "enabled",
            false );
          
          setBoolean (
            find ( "commandPanel" ),
            "visible", false );
          
          setBoolean (
            find ( "portNamePanel" ),
            "visible",
            true );
          
          setString (
            start_button,
            "text",
            "Start" );
        }
      }
      catch ( final jpvmException  e )
      {
        e.printStackTrace ( );
      }
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }