    package cnslab.gui;
    
    import java.io.*;
    import java.util.*;

    import ucar.ma2.ArrayChar;
    import ucar.nc2.Dimension;
    import ucar.nc2.Variable;    
    import thinlet.Thinlet;

    import cnslab.cnsnetwork.PlotResult;
    
    /***********************************************************************
    * @version
    *   $Date$
    *   $Rev$
    *   $Author$
    * @author
    *   Yi Dong
    * @author
    *   David Wallace Croft, M.Sc.
    * @author
    *   Jeremy Cohen
    ***********************************************************************/
    public final class  ContentProcessResult2
      implements Runnable
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    {
      
    private final Thinlet  thinlet;
    
    private final String   file;

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    public  ContentProcessResult2 (
      final Thinlet  thinlet,
      final String   file )
    ////////////////////////////////////////////////////////////////////////
    {
      this.thinlet = thinlet;
      
      this.file = file;
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    @Override
    public void  run ( )
    ////////////////////////////////////////////////////////////////////////
    {
      final File  dir = new File ( "results" );
      
      final FilenameFilter
        filter = new FilenameFilter ( )
        {
          @Override
          public boolean  accept (
            final File    dirFile,
            final String  name )
          {
            return name.endsWith ( ".nc" ) || name.endsWith ( ".NC" );
          }
        };

      //System.out.println(thin.getCount(thin.find("xmlModles")));
        
      final String [ ]  children = dir.list ( filter );
      
      // if( thin.getCount(thin.find("resultsCombo")) != children.length )
      {
        Arrays.sort ( children );
        
        thinlet.removeAll ( thinlet.find ( "resultsCombo" ) );
        
        //System.out.println("files:" +children.length);
        
        for ( int i = 0; i < children.length; i++ )
        {
          //    System.out.println("file:"+children[i]);
          
          final Object o = Thinlet.create ( "choice" );
          
          thinlet.setString (
            o,
            "text",
            children [ i ] );
          
          if ( children [ i ].equals ( file ) )
          {
            thinlet.setInteger (
              thinlet.find ( "resultsCombo" ),
              "selected",
              i );
          }
          
          thinlet.add (
            thinlet.find ( "resultsCombo" ),
            o );
        }
      }

      if ( thinlet.getSelectedItem ( thinlet.find ( "resultsCombo" ) )
        != null )
      {
        try
        {
          thinlet.removeAll ( thinlet.find ( "eleList" ) );
          
          final String
            fileName = thinlet.getString (
              thinlet.getSelectedItem ( thinlet.find ( "resultsCombo" ) ),
              "text");
          
          //System.out.println(fileName);
          
          // TODO:  Why is this not used?          
          final PlotResult  pr = new PlotResult ( "test" );
          
          PlotResult.init ( "results/" + fileName );
          
          //System.out.println(pr.numExp);
          
          final Dimension
            exp = PlotResult.ncfile.findDimension ( "suEleId" );
          
          for ( int i = 0; i < PlotResult.numExp; i++ )
          {
            //Subexperiment node
            
            final Object  o = Thinlet.create ( "node" );
            
            thinlet.setString (
              o,
              "text",
              PlotResult.subexpNames.getString ( i ) );

            //Single Unit node
            
            if ( exp != null )
            {
              final Object  su = Thinlet.create ( "node" );
              
              thinlet.setString (
                su,
                "text",
                "Single Unit" );
              
              thinlet.setBoolean (
                su,
                "expanded",
                false );

              final Variable
                v = PlotResult.ncfile.findVariable ( "suNames" );
              
              final ArrayChar.D2  suNames = ( ArrayChar.D2 ) v.read ( );

              // System.out.println(names.getString(0));
              
              final int [ ]  varShape = v.getShape ( );
              
              // TODO:  Why is this not used?
              final int  suSeries = varShape [ 0 ];
              
              // System.out.println(varShape[0]+" "+varShape[1]);
              
              for ( int  eleId = 0; eleId < varShape [ 0 ]; eleId++ )
              {
                final Object  su_item = Thinlet.create ( "node" );
                
                thinlet.setString (
                  su_item,
                  "text",
                  suNames.getString ( eleId ) );
                
                thinlet.setString (
                  su_item,
                  "name",
                  i + ",0," + eleId );
                
                thinlet.add ( su, su_item );
              }
              
              thinlet.add ( o, su );
            }

            //Mu node
            //
            //

            Variable  v = PlotResult.ncfile.findVariable ( "muNames" );
            
            if ( v != null )
            {
              final ArrayChar.D2  suNames = ( ArrayChar.D2 ) v.read ( );
              
              // System.out.println(names.getString(0));
              
              final int [ ]  varShape = v.getShape ( );
              
              // TODO:  Why is this not used?
              final int  suSeries = varShape [ 0 ];
              
              //suNames.getString(i);

              final Object  mu = Thinlet.create ( "node" );
              
              thinlet.setString (
                mu,
                "text",
                "Multiple Unit" );
              
              thinlet.setBoolean (
                mu,
                "expanded",
                false );

              for ( int  eleId = 0; eleId < varShape [ 0]; eleId++ )
              {
                final Object  mu_item = Thinlet.create ( "node" );
                
                thinlet.setString (
                  mu_item,
                  "text",
                  suNames.getString ( eleId ) );
                
                thinlet.setString (
                  mu_item,
                  "name",
                  i + ",1," + eleId );
                
                thinlet.add (
                  mu,
                  mu_item);
              }
              
              thinlet.add ( o, mu );
            }

            //Field node
            
            if ( PlotResult.numField > 0 )
            {
              v = PlotResult.ncfile.findVariable("fieldNames");
              
              final ArrayChar.D2  fieldNames = ( ArrayChar.D2 ) v.read ( );

              final Object  field = Thinlet.create ( "node" );
              
              thinlet.setString (
                field,
                "text",
                "Field" );
              
              thinlet.setBoolean (
                field,
                "expanded",
                false );

              for ( int ii = 0; ii < PlotResult.numField; ii++ )
              {
                final Object  fie_item = Thinlet.create ( "node" );
                
                thinlet.setString (
                  fie_item,
                  "text",
                  fieldNames.getString ( ii ) );
                
                thinlet.setString (
                  fie_item,
                  "name",
                  i + ",2," + ii );
                
                thinlet.add (
                  field,
                  fie_item );
              }
              
              thinlet.add (
                o,
                field );
            }

            //Vector node
            
            if ( PlotResult.numVector > 0 )
            {
              v = PlotResult.ncfile.findVariable ( "vectorNames" );
              
              final ArrayChar.D2  vectorNames = ( ArrayChar.D2 ) v.read ( );

              final Object  vector = Thinlet.create ( "node" );
              
              thinlet.setString (
                vector,
                "text",
                "Vector" );
              
              thinlet.setBoolean (
                vector,
                "expanded",
                false );

              for ( int ii = 0; ii < PlotResult.numVector; ii++ )
              {
                final Object  fie_item = Thinlet.create ( "node" );
                
                thinlet.setString (
                  fie_item,
                  "text",
                  vectorNames.getString ( ii ) );
                
                thinlet.setString (
                  fie_item,
                  "name",
                  i + ",3," + ii );
                
                thinlet.add (
                  vector,
                  fie_item );
              }
              
              thinlet.add (
                o,
                vector );
            }
            
            // Intracellular node
            
            if ( PlotResult.numIntra > 0 )
            {
              v = PlotResult.ncfile.findVariable ( "intraNames" );
              
              final ArrayChar.D2  intraNames = ( ArrayChar.D2 ) v.read ( );

              final Object  intra = Thinlet.create ( "node" );
              
              thinlet.setString (
                intra,
                "text",
                "Intracellular" );
              
              thinlet.setBoolean (
                intra,
                "expanded",
                false );

              for ( int  ii = 0; ii < PlotResult.numIntra; ii++ )
              {
                final Object  intra_item = Thinlet.create ( "node" );
                
                thinlet.setString (
                  intra_item,
                  "text",
                  intraNames.getString ( ii ) );
                
                thinlet.setString (
                  intra_item,
                  "name",
                  i + ",4," + ii );
                
                thinlet.add (
                  intra,
                  intra_item );
              }
              
              thinlet.add (
                o,
                intra );
            }


            thinlet.add ( thinlet.find ( "eleList" ), o );
          }

          /*
          if ( out.equals ( "" ) )
          {
            thin.setString (
              thin.find ( "simStatusDisplay" ),
              "text",
              thin.getString (
                thin.getSelectedItem (
                  thin.find ( "xmlModles" ) ),
                "text" )
                + " ...OK!\n" );
                
            thin.setBoolean (
              thin.find ( "startSim" ),
              "enabled",
              true );
              
            thin.setBoolean (
              thin.find ( "stopSim" ),
              "enabled",
              false );
          }
          else
          {
            thin.setString (
              thin.find ( "simStatusDisplay" ),
              "text",
              thin.getString (
                thin.getSelectedItem (
                  thin.find ( "xmlModles" ) ),
                "text" ) + " ...Error!\n" + out );
                
            thin.setBoolean (
              thin.find ( "startSim" ),
              "enabled",
              false );
              
            thin.setBoolean (
              thin.find ( "stopSim" ),
              "enabled",
              false );
          }
          */
          
          PlotResult.stop ( );
        }
        catch ( final Exception  e )
        {
          e.printStackTrace ( );
        }
      }
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    }