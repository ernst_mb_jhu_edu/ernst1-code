package cnslab.image;
import cnslab.cnsmath.*;
public class Convolution
{
	public static int [][] convolve(int[][] initial, double [][] response)
	{
		//find out the proper padding
		int m_mx = response.length/2;
		int m_my = response[0].length/2;
		int m_px = response.length - response.length/2;
		int m_py = response[0].length - response[0].length/2;

		int ix = initial.length;
		int iy = initial[0].length;

		int rx = response.length;
		int ry = response[0].length;


		boolean adjustX=true;//padding for x
		boolean adjustY=true;//padding for y

		int fullX=-1,fullY=-1;

		for(int i=1;i<32;i++) //maximum size 2^32
		{
			if(ix==(1<<i))
			{
				fullX = ix;
				adjustX=false;
				break;
			}
		}
		for(int i=1;i<32;i++)
		{
			if(iy==(1<<i))
			{
				fullY = iy;
				adjustY=false;
				break;
			}
		}


		if(adjustX)
		{
			for(int i=1;i<32;i++) //maximum size 2^32
			{
				if(ix+m_mx+m_px<=(1<<i))
				{
					fullX = 1<<i;
					break;
				}
			}  
		}

		if(adjustY)
		{
			for(int i=1;i<32;i++) //maximum size 2^32
			{
				if(iy+m_my+m_py<=(1<<i))
				{
					fullY = 1<<i;
					break;
				}
			}  
		}


		//prepare matrix for FFT
		double [] sig = new double[fullX*fullY*2]; 
		double [] res = new double[fullX*fullY*2]; 
	//	System.out.println(fullX+" "+fullY+" "+ix+" "+iy+" mx"+m_mx+" px"+m_px+" my"+m_my+" py"+m_py);

		for(int i=0; i< ix; i++)
		{
			for(int j=0; j< iy; j++)
			{
				sig[i*fullY*2+j*2] = (double)initial[i][j];
			}	
		}

		for(int i=0; i< m_mx+m_px; i++)
		{
			for(int j=0; j< iy; j++)
			{
				sig[(i+fullX-m_mx-m_px)*fullY*2+j*2] = (double)initial[i+ix-m_mx-m_px][j];
			}	
		}

		for(int i=0; i< ix; i++)
		{
			for(int j=0; j< m_my+m_py; j++)
			{
				sig[i*fullY*2+(j+fullY-m_my-m_py)*2] = (double)initial[i][j+iy-m_my-m_py];
			}	
		}

		for(int i=0; i< m_mx+m_px; i++)
		{
			for(int j=0; j< m_my+m_py; j++)
			{
				sig[(i+fullX-m_mx-m_px)*fullY*2+(j+fullY-m_my-m_py)*2] = (double)initial[i+ix-m_mx-m_px][j+iy-m_my-m_py];
			}	
		}

		for(int i=0; i< m_px ; i++) //left  top
		{
			for(int j=0; j < m_py; j++)
			{
				res[i*fullY*2+j*2] = response[i][j];
			}
		}

		for(int i=0; i< m_mx ; i++) // left bot
		{
			for(int j=0; j < m_py; j++)
			{
				res[(i+fullX-m_mx)*fullY*2+j*2] = response[i+rx-m_mx][j];
			}
		}

		for(int i=0; i< m_px ; i++) //right top
		{
			for(int j=0; j < m_my; j++)
			{
				res[i*fullY*2+(j+fullY-m_my)*2] = response[i][j+ry-m_my];
			}
		}

		for(int i=0; i< m_mx ; i++) //right bot 
		{
			for(int j=0; j < m_my; j++)
			{
				res[(i+fullX-m_mx)*fullY*2+(j+fullY-m_my)*2] = response[i+rx-m_mx][j+ry-m_my];
			}
		}

		//fourier transferformation for both
		int [] nn = new int [] {fullX,fullY};
		FFT.fourn(sig, nn, 1);
		FFT.fourn(res, nn, 1);

		for(int i =0 ; i< fullX*fullY; i++)
		{
			double tmpR = sig[2*i];
			sig[2*i] = sig[2*i]*res[2*i] - sig[2*i+1]*res[2*i+1];
			sig[2*i+1] = tmpR*res[2*i+1] + sig[2*i+1]*res[2*i];
		}

		//reverse transformation
		FFT.fourn(sig, nn , -1);

		int totalDim=fullX*fullY;


		int [][] out = new int[initial.length][initial[0].length];

		for(int i=0; i< ix - m_mx ; i++)
		{
			for(int j=0; j< iy - m_my; j++)
			{
				out[i][j] = (int)Math.round(sig[i*fullY*2+ j*2]/(double)totalDim);
			}
		}

		for(int i=0; i< m_mx ; i++)
		{
			for(int j=0; j< iy - m_my; j++)
			{
				out[i+ix-m_mx][j] = (int)Math.round(sig[(i+fullX-m_mx)*fullY*2+ j*2]/(double)totalDim);
			}
		}

		for(int i=0; i< ix - m_mx ; i++)
		{
			for(int j=0; j< m_my; j++)
			{
				out[i][j+iy-m_my] = (int)Math.round(sig[i*fullY*2+ (j+fullY-m_my)*2]/(double)totalDim);
			}
		}

		for(int i=0; i< m_mx ; i++)
		{
			for(int j=0; j< m_my; j++)
			{
				out[i+ix-m_mx][j+iy-m_my] = (int)Math.round(sig[(i+fullX-m_mx)*fullY*2+ (j+fullY-m_my)*2]/(double)totalDim);
			}
		}
		return out;
	}

	public static int [][] convolve_safe(int[][] initial, double [][] response)
	{
		int [][] out = new int[initial.length][initial[0].length];

		//find out the proper padding
		int m_mx = response.length/2;
		int m_my = response[0].length/2;

		for(int i=0; i<initial.length; i++)
		{
			for(int j=0;j<initial[0].length;j++)
			{
				double sum=0.0;

				for(int m=0; m<response.length;m++)
				{
					for(int n=0; n<response[0].length;n++)
					{
						int oldx=i+m-m_mx;
						int oldy=j+n-m_my;
						oldx = oldx % initial.length;
						oldy = oldy % initial[0].length;
						if(oldx<0)oldx+=initial.length;
						if(oldy<0)oldy+=initial[0].length;
						sum=sum+ response[response.length-1-m][response[0].length-1-n]*(double)initial[oldx][oldy];
					}
				}	
				out[i][j]= (int)Math.round(sum);
			}
		}
		return out;
	}
}
