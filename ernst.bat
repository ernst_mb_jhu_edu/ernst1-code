@echo off
REM author ...:  Yi Dong
REM author ...:  David Wallace Croft
REM author ...:  Jeremy Cohen

mkdir log

mkdir results

mkdir pics

set CLASSPATH=.;%cd%\simulator.jar

java -jar simulator.jar
