\documentclass{article}

\usepackage{hyperref}

\begin{document}

\title {The ERNST User Guide}
\author {Jeremy Cohen and David Wallace Croft}

\maketitle

\section {Preface}

ERNST (Event-Related Neural Simulation Tool) is an event-driven simulator of biological neural networks.  ERNST was developed at Johns Hopkins University by Yi Dong, Stefan Mihalas, and Ernst Niebur, and is currently maintained by David Wallace Croft.  This document is the user guide to ERNST; it describes how to run the simulator software.  This document does \emph{not} describe what ERNST is, why you should use it, or how it works.  For that information, see ``An Event-Related Neural Simulation Tool.''  Also see that document for a tutorial on ERNST XML, the markup language used to define simulations.

\tableofcontents

\section {Installation}
Download a copy of ERNST from \url{http://sourceforge.net/p/ernst} and unzip the folder into your desired installation directory.

\section {The ERNST Directory}
The ERNST directory contains a number of files and subdirectories:

\begin {description}

\item[\texttt{bin/}] contains scripts for running ERNST from the command line.  Users of the GUI shouldn't need to access this folder.

\begin {description}

\item[\texttt{jpvm.sh and jpvm.bat}] are scripts to open a JPVM console.

\item[\texttt{jpvmd.sh and jpvmd.bat}] are scripts to launch a JPVM daemon.

\item[\texttt{setup.sh}] is a script to launch a JPVM daemon on each machine specified in \texttt{myhosts}.

\item[\texttt{jkill.sh}] is a script to kill a specified process (usually Java) on each machine specified in \texttt{myhosts}.  Usage: \texttt{bin/jkill.sh java}.

\item[\texttt{run.sh and run.bat}] are scripts to run the ERNST simulator on a specified ERNST XML file in the \texttt{model} subdirectory.  JPVM must have been launched first.  Usage: \texttt{bin/run.sh simulator\_heap\_size \newline modelfile.xml [seed] [nethost\_heap\_size]}.

\item[\texttt{plot.sh and plot.bat}] are scripts to plot a specified NetCDF result file.  Usage: \texttt{bin/plot.sh results/resultfile.nc}.

\end {description}

\item[\texttt{config/}] contains files required by various libraries used by ERNST. \newline You shouldn't need to access this folder.

\item[\texttt{doc/}] contains documentation and licenses.

\item[\texttt{imagelib/}] contains images that can be used by ERNST as stimuli.  You should place any such images in this folder.

\item[\texttt{lib/}] contains various libraries used by ERNST.  You shouldn't need to access this folder.

\item[\texttt{log/}] contains logging data outputted by ERNST in the course of the simulation.  You should consult log files in this folder if ERNST keeps crashing with no apparent cause.

\item[\texttt{model/}] contains input ERNST XML files that define a simulation.  You should place any such input files in this folder. 

\item[\texttt{pics/}] contains pictures that you have generated and saved from the ERNST GUI.

\item[\texttt{results/}] contains output NetCDF files.

\item[\texttt{ernst.sh and ernst.bat}] are scripts to launch the ERNST simulator GUI.

\item[\texttt{logback.xml}] specifies logging parameters for the simulator.  You shouldn't need to access this file.

\item[\texttt{myhosts}] is a plain text file that allows you to specify distributed virtual machine daemons on other hosts.

\item[\texttt{simulator.jar}] is the Java archive of ERNST.  You shouldn't need to run this file directly.

\end {description}

\section {The ERNST Workflow}
You can run the ERNST simulator from either a Graphical User Interface (GUI) or the command line.  Either way, the workflow is the same:

\begin{enumerate}

\item Launch Java Parallel Virtual Machine (JPVM), a Java message-passing environment used by the simulator to communicate between host machines.

\item Select an ERNST XML file from \texttt{model/} that defines the simulation. ERNST XML is beyond the scope of this document.

\item Run the simulator.

\item When ERNST is finished, it outputs data from recorders, as well as basic information about the simulation, into a NetCDF file in \texttt{results/}.  \footnote{See \url{http://www.unidata.ucar.edu/software/netcdf/} for information on the NetCDF format.}  This NetCDF output can be either viewed using the ERNST GUI or processed manually using Matlab, R, or any other NetCDF reader.

\item Exit JPVM.

\end{enumerate}

\section {Running ERNST from the GUI}

Launch the ERNST GUI by navigating to your ERNST root directory and executing \texttt{ernst.sh} (Unix) or \texttt{ernst.bat} (Windows).  The GUI has three tabs: \texttt{jpvn}, \texttt{Ernst}, and \texttt{Result}.  The \texttt{Ernst} tab is disabled until JPVM is launched.


\subsection{The JPVM Tab} 
Use the \texttt{jpvm} tab to manage JPVM and host machines.  To launch JPVM, click the \texttt{Start} button.  If all goes well, the JPVM console should activate aned the \texttt{Ernst} tab should become enabled.  (Consult Section \ref{sec:troubleshooting-jpvm} for troubleshooting.)  One of ERNST's greatest strengths is that it can be run in parallel on multiple host machines.  To add an additional host machine (beyond the default of \texttt{localhost}), select \texttt{Add Host} from the \texttt{User Command} drop-down menu, and enter the host name and port.\footnote{Note that you can only fill in the host name but not the IP address. So you need to edit \texttt{/etc/hosts(Linux)} or \texttt{/private/etc/hosts} (Mac) or \texttt{\%SystemRoot\%\textbackslash system32\textbackslash drivers\textbackslash etc\textbackslash} (Windows) to make sure the machines can ping each other by host name.} Later, when you are preparing to exit the GUI, make sure to first click the \texttt{Close} button, or else JPVM will become a zombie process.

\subsection{The Ernst Tab} 

Use the \texttt{Ernst} tab to manage simulation runs.  First, select the desired ERNST XML file from the \texttt{model} drop-down menu, which lists all ERNST XML files in the \texttt{model/} subdirectory.  Optionally adjust the size of the simulator and nethost heaps, or the value used to seed the simulator.  Optionally view and edit the selected ERNST XML file with the \texttt{Show model} button.  Optionally record neuronal avalanches with the \texttt{avalanch} checkbox.  To run the simulator, press the \texttt{Start} button.  When the simulator is finished, it will print ``simulation done.''

\subsection{The Result Tab}

Use the \texttt{result} tab to graphically view NetCDF output files.  First, select the desired NetCDF output file from the \texttt{results} drop-down menu, which lists all NetCDF files in the \texttt{results} subdirectory.  The GUI will show a nested list of recordings.  Click on a recorder to show a plot of that recorder's data in a new window.  Optionally click the \texttt{SaveEPS} button to save an image of the plot in the \texttt{pics/} subdirectory.

\section {Running ERNST from the Command Line}

\subsection {Configuration}

One of ERNST's greatest strengths is that it can run in parallel on multiple machines.  These host machines are specified in \texttt{myhosts}.  Each line should consist of a host name, followed by a space, followed by a port number.  In addition, before you run ERNST for the first time via the command line, you must edit line 12 of \texttt{bin/setup.sh} so that \texttt{JAVASIMPATH} points the ERNST root directory on each host machine. For example: \texttt{JAVASIMPATH="\$HOME/ernst-1.3"}.

\subsection {Launching JPVM}

Navigate to the ERNST root directory and execute \texttt{bash bin/setup.sh}.

\subsection {Running the Simulator}

Navigate to the ERNST root directory and execute \texttt{bash bin/run.sh \newline simulator\_heap\_size modelfile.xml[seed] [nethost\_heap\_size]}, where \newline \texttt{simulator\_heap\_size} is the size of the simulator heap (suggested = 512), \texttt{modelfile.xml} is the name of the ERNST XML file in the \texttt{model} subdirectory that you wish to run, \texttt{seed} (\emph{optional}) is the simulator seed, and \newline \texttt{nethost\_heap\_size} (\emph{optional}) is the size of the nethost heap (default = 256).

\subsection {Plotting Results}

Navigate to the ERNST root directory and execute \texttt{bash bin/plot.sh \newline results/resultfile.nc}, where \texttt{resultfile.nc} is the name of the results NetCDF file in the \texttt{results} subdirectory.  

\subsection {Closing JPVM}

Safely close any open Java programs. Navigate to the ERNST root directory and execute \texttt{bash bin/jkill.sh java}.

\section {Troubleshooting}

\subsection {JPVM Doesn't Launch}

\label{sec:troubleshooting-jpvm}

This is usually because another instance of JPVM is already running on your machine.  If possible, try to properly stop the first JPVM instance in the place where you started it.  If that fails, safely close all open Java programs and execute \texttt{killall java}.

\subsection {Simulation Never Ends}

This is usually because the simulator encountered an error while constructing the network.  Check the \texttt{log/} subdirectory for an error file.

\end{document}
