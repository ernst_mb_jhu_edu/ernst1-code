REM purpose ...:  installs simulator.jar to Maven repository   
REM since .....:  2011-06-04
REM author ....:  David Wallace Croft

mvn install:install-file -Dfile=simulator.jar -DgroupId=edu.jhu.mb.ernst -DartifactId=simulator -Dversion=1.1.1 -Dpackaging=jar